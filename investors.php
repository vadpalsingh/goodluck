<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India ::Investor</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <main>
        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item  active ">
                        <img src="./assets/img/about/inv.jpg" class="d-block w-100" alt="...">
                        <!-- <h5>Investor </h5> -->
                        <!-- <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        </div> -->
                    </div>
                </div>

            </div>
        </section>

        <section class="innerpagenav">
            <div class="container-fluid">
                <div class="row">
                <ul class="pagenav">
                        <li><a href="investors.php" class="active">Investors</a></li>
                        <li><a href="financial.php">Financial</a></li>
                        <li><a href="press-release.php">Press Release</a></li>
                        <li><a href="investors-news.php">Investors News</a></li>
                        <li><a href="shareholding-pattern.php">Shareholding Pattern</a></li>
                        <li><a href="shareholder-information.php">Shareholder Information</a></li>
                        <li><a href="downloads.php">Downloads</a></li>
                        <li><a href="corporate-governance.php">Corporate Governance</a></li>
                    </ul>
                </div>
            </div>
        </section>

        <!-- end -->

        <section class="export pb-60 p60  wow fadeInUp   animated" data-wow-delay="0.5s"
            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ins-det">
                            <h1>Investor</h1>

                            <p class="text-justify">Through our untiring effort to maximize customer satisfaction by
                                delivering products of high quality, the group has gained a special position and
                                preference in domestic and international market. As a result the company has been profit
                                making, growing and also dividend paying since its inception in 1986.</p>

                            <p class="text-justify">Considering the financial year performance and strength of the
                                Company CRISIL (one of the reputed rating agencies of India) has assigned long term
                                rating <strong>"CRISIL A-/ STABLE"</strong> (pronounced as CRISIL A Negative) and
                                short-term rating <strong>"CRISIL A2+"</strong> (pronounced as CRISIL A two Positive) to
                                the company. The outlook on the long term rating is <strong>Stable</strong>. </p>
                            <div class="clearfix"></div>
                        </div>

                        <div class="ins-det">
                            <h3>CODE OF CONDUCT</h3>
                            <p><b> <u> INTRODUCTION </u>:</b></p>
                            <p class="text-justify">GOODLUCK Group is one of India's leading and fastest growing
                                business groups with over 4000  employees and having multi location plants and units.
                                With experience of more than two decades in the industry GOODLUCK has diversified
                                business interests in the national and international market. By recognizing the
                                contribution of the company in the export from the country the Government of India has
                                recognized it as an export house.<br><br>
                                The Company’s philosophy on Corporate Governance is built on a rich legacy of fair,
                                transparent and effective governance. This includes respect for human values, individual
                                dignity and adherence to honest, ethical and professional conduct. This enables
                                customers and all stake holders to be partners in the Company’s growth and prosperity.
                            </p>

                            <p><b><u> OBJECT : </u></b></p>
                            <p class="text-justify">This code of conduct document has been created in furtherance of the
                                Company’s commitment to building a strong culture of corporate governance by promoting
                                the importance of ethical conduct and transparency in the conduct of its
                                operations.<br><br>
                                The Company’s Code of Conduct not only ensures compliance with the Company Law, the
                                provisions of the listing agreement with Stock Exchanges and other laws, but goes beyond
                                to ensure exemplary Corporate Governance.</p>

                            <p><b><u> APPLICABILITY : </u></b></p>
                            <p class="text-justify">This Code is applicable to the following:<br>
                                <b>a.</b> All Employees of the Company including Senior Management; and <br><br>
                                <b>b.</b> All Directors of the Company.<br><br>

                                This Code does not address every possible form of unacceptable conduct and it is
                                expected that the Directors and the Employee shall apply their sound judgment to comply
                                with the principles set forth in the Code.
                            </p>


                            <p><b><u> DEFINITION : </u></b></p>
                            <p class="text-justify">The definitions of some of the key terms used in this Code are given
                                below <br>
                                <b>"Director"</b> means any Executive, Non-Executive, Nominee or Alternate Director of
                                the Company. <br><br>
                                <b>"Employee"</b> means any employee or officer of the Company. <br><br>
                                <b>"Relative"</b> means 'relative, as defined in the Companies Act, 2013. <br><br>
                                <b>"Senior Management"</b> means personnel of the Company who are members of its Core
                                Management team excluding the Board of Directors and shall include all personnel above
                                the level of Vice-President and all function heads.
                            </p>


                            <p><b><u> KEY REQUIREMENTS : </u></b></p>
                            <p class="text-justify">The members of the Board and senior management personnel are required to operate within their designated authority, prioritizing the company's best interests. They are expected to adhere to the following guidelines:</p>
                            <ul class="key_requ">
                                <li> Execute their responsibilities with diligence and uphold a high standard of integrity.</li>
                                <li> Act with the utmost good faith in all their actions.</li>
                                <li> Refrain from participating in decisions concerning matters where a conflict of interest exists or is likely to arise, based on their judgment.</li>
                                <li> Disclose and avoid any personal or financial stakes in business transactions involving the company.</li>
                                <li>Abstain from engaging in interactions with contractors or suppliers that could compromise the company's ability to conduct business professionally, fairly, and competitively, or that could influence company decisions.</li>
                                <li>Avoid holding positions, jobs, or engaging in external business ventures that could harm the company's interests.</li>
                                <li>Seek permission from the competent authority and disclose any personal gain-seeking endeavours that arise from the use of corporate property, information, or position before pursuing them.</li>
                                <li>Refrain from making statements that could negatively criticize government policies or company actions, potentially straining relations between the company and the public, as well as the company's management and employees, including stakeholders.</li>
                                <li>Prevent involvement in any criminal activities that involve moral wrongdoing or go against public policy and result in a conviction.</li>
                             

                            </ul>



                            <p><b><u>STANDARD OF CONDUCTS</u></b></p>
                            <p class="text-justify">The Directors and employees shall conduct the Company's business in
                                an efficient and transparent manner in meeting its obligations towards the shareholders
                                and other stakeholders. The Directors and employees shall not be involved in any
                                activity that would have any adverse effect on the objectives of the Company or against
                                national interest. The following elucidates the Company’s position on the manner of
                                conduct of the Company’s business and transactions :</p>

                            <p><b>A. <u>CORPORATE CITIZENSHIP &amp; REGULATORY COMPLIANCE</u></b></p>
                            <p class="text-justify">The company shall be committed to good corporate citizenship, not
                                only in the compliance of all relevant laws and regulations but also by actively
                                assisting in the improvement of quality of life of the people in the communities in
                                which it operates.<br><br>

                                The company shall not treat these activities as optional, but should strive to
                                incorporate them as an integral part of its business plan.<br><br>

                                Employees of the company, in their business conduct, shall comply with all applicable
                                laws and regulations, in letter and spirit, in all the territories in which they
                                operate.</p>

                            <p><b>B. <u>EQUAL OPPORTUNITIES</u></b></p>
                            <p class="text-justify">The company shall provide equal opportunities to all its employees
                                and all qualified applicants for employment without regard to their race, caste,
                                religion, colour, ancestry, marital status, gender, sexual orientation, age,
                                nationality, ethnic origin or disability.<br><br>

                                Human resource policies shall promote diversity and equality in the workplace, as well
                                as compliance with all local labour laws, encouraging the adoption of best practices.
                            </p>


                            <p><b>C. <u>HEALTH SAFETY &amp; ENVOIRNMENT</u></b></p>
                            <p class="text-justify">The policy of the company is offering to its employees a safe and
                                healthy workplace. The company is against all forms of exploitation of children and
                                believes in abiding by the laws and applicable regulations for prevention of child
                                labour.<br><br>

                                The Company is committed to prevent the wasteful use of natural resources and minimize
                                any hazardous impact of the development, use and disposal of any of the intermediaries
                                or direct materials used in its product and service offerings on the ecological
                                environment.</p>

                            <p><b>D. <u>CONFLICT OF INTEREST</u></b></p>
                            <p class="text-justify">An employee or director of the company shall always act in the
                                interest of the company, and ensure that any business or personal association which he /
                                she may have does not involve a conflict of interest with the operations of the company
                                and his / her role therein. <br><br>

                                In case of an Employee, where such conflict appears at any time or is in existence at
                                the time of the implementation of this policy, such Employee shall forthwith make a
                                disclosure in writing to the Management. Upon reviewed the Employee may be directed to
                                avoid/resolve the conflict or to take such remedial action as is deemed suitable by
                                management. <br><br>

                                A Director shall disclose any potential conflicts of interests to the Board of Directors
                                or any Committee thereof and abstain from participating in the decision making or in
                                influencing the decision on the areas resulting in the potential conflict of interest in
                                accordance with the applicable rules under the Companies Act. In addition, the Director
                                shall provide on a periodic basis, such disclosure as is required by the Board of
                                Directors or any Committee thereof.<br><br>

                                If the management becomes aware of an instance of conflict of interest the management
                                shall take a serious view of the matter and consider suitable disciplinary action
                                against the employee.</p>


                            <p><b>E. <u>CONFIDENTIALITY</u></b></p>
                            <p class="text-justify">Any information concerning the Company’s business, its customers,
                                suppliers, etc. to which the Directors or the employees have access or which is
                                possessed by the Directors and the employees, must be considered privileged and
                                confidential and should be held in confidence at all times, and should not be disclosed
                                to any person, unless <br><br>

                                1. authorised by the Board; or <br>
                                2. the same is part of the public domain at the time of disclosure; or <br>
                                3. is required to be disclosed in accordance with applicable laws.</p>

                            <p><b>F. <u>EXCLUSIVITY</u></b></p>
                            <p class="text-justify">Senior Officers are expected to devote their full attention with
                                integrity and honesty to the business interests of the Company. They are prohibited from
                                engaging in any activity that interferes with his proper discharge of responsibilities
                                of the Company, or is in conflict with or prejudicial to the interests of the Company.
                            </p>

                            <p><b>G. <u>INSIDER TRADING</u></b></p>
                            <p class="text-justify">A Director or the Employees and their relatives shall not derive any
                                benefit or assist others to derive any benefit from the access to and possession of
                                information about the Company, which is not in the public domain and thus constitutes
                                insider information. They shall also ensure compliance with the SEBI (Prohibition of
                                Insider Trading) Regulations, 2015 as also other regulations as may become applicable to
                                them from time to time. <br><br>

                                The Company also prohibits its Directors and Employees in undertaking any fraudulent or
                                unfair trade practice in connection with the securities of the Company.</p>

                            <p><b>H. <u>PROTECTION OF COMPANIES ASSETS</u></b></p>
                            <p class="text-justify">The Directors and the Employees shall not use the Company’s tangible
                                assets such as equipment and machinery, systems, facilities, materials etc. or
                                intangible assets such as proprietary information, relationships with customers and
                                suppliers, etc. for their personal benefit or for the benefit of a related party or for
                                any purpose other than of conducting the business of the company.</p>

                            <p><b>I. <u>PUBLIC REPRESENTATION</u></b></p>
                            <p class="text-justify">The company honours the information requirements of the public and
                                its stakeholders. In all its public appearances, with respect to disclosing company and
                                business information to public constituencies such as the media, the financial
                                community, employees, shareholders, agents, franchisees, dealers, distributors and
                                importers, the company shall be represented only by specifically authorised directors
                                and employees. It shall be the sole responsibility of these authorised representatives
                                to disclose information about the company.</p>

                            <p><b>J. <u>GIFT &amp; DONATIONS</u></b></p>
                            <p class="text-justify">The Company, it’s Directors and Employees shall neither receive nor
                                offer or make directly /indirectly any illegal payments, gifts, donations or any
                                benefits which are intended to obtain business or unethical favours. However, the
                                Directors or Employees may receive such nominal gifts which are customary in nature or
                                are associated with festivals.</p>

                            <p><b>K. <u>ANTI CORRUPTION &amp; BRIBERY</u></b></p>
                            <p class="text-justify">The employee shall not directly or indirectly offer, promise or
                                receipt of any gift, hospitality, loan, fee reward or other advantage, except bonafide
                                corporate hospitality to improve company’s image, to induce or reward behavior, which is
                                dishonest, illegal or a breach of trust, duty, good faith or impartiality in the
                                performance of a person’s functions or activities (including but not limited to, a
                                person’s public functions, activities in their employment or otherwise in connection
                                with a business) to obtain business advantage(s).</p>

                            <p><b><u>NON- ADHERENCE</u></b></p>
                            <p class="text-justify">The matters covered in this Code are of the utmost importance to the
                                Company, its stockholders and its business partners, and are essential to the Company's
                                ability to conduct its business in accordance with its stated values. We expect all of
                                our Directors and Employees to adhere to these rules in carrying out their duties for
                                the Company. <br><br>

                                The Company will take appropriate action against Director or the Employee whose actions
                                are found to violate these policies or any other policy of the Company. In case of
                                breach of this Code, the same shall be considered by the Board of Directors for
                                initiating appropriate action, as deemed necessary.</p>

                            <p><b><u>AMENDMENTS &amp; WAIVER</u></b></p>
                            <p class="text-justify">The Code may be amended or modified by the Board as and when it is
                                found necessary to do so. Any waiver of any provision of this Code for a Director or the
                                Employee must be approved by the Company's Board of Directors.</p>

                            <div class="clearfix"></div>
                        </div>

                        <div class="ins-det">
                            <h3>INVESTOR’S GRIEVANCES</h3>
                            <p class="text-justify">For any query/grievance the investors may contact with : <br><br>
                            <div class="row">
                                <div class="col-md-6">
                                    <b> 1 . Mas Services Limited </b> <br><br>
                                    Registrar and Transfer Agent,<br>
                                    T-34, 2nd Floor,<br>
                                    Okhla Industrial Area, Phase - II,<br>
                                    New Delhi - 110 020 <br><br>

                                    Ph:- 26387281/82/83 <br>
                                    Fax:- 26387384<br>
                                    Email:- info@masserv.com<br><br>
                                </div>
                                <div class="col-md-6">
                                    <b> 2 . Company Secretary </b> <br><br>
                                    Good Luck House, <br>
                                    II- F, 166-167, <br>
                                    Nehru Nagar, Ambedkar Road, <br>
                                    Ghaziabad, U.P. – 201001<br><br>

                                    Ph. :- 0120 419 6600/6700 <br>
                                    Fax :- 0120 419 6666 <br>
                                    Email:- investor@goodluckindia.com
                                    </p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="ins-det">
                            <h3>NODAL OFFICER UNDER IEPF RULES</h3>
                            <p class="text-justify">
                                <b>Name of Nodal Officer : </b> Mr. Abhishek Agrawal<br>
                                <b>Email ID : </b> investor@goodluckindia.com
                            </p>
                            <div class="clearfix"></div>
                        </div>


                    </div>
                </div>
            </div>
        </section>
        <!-- end -->
    </main>
    <?php include './inc/footer.php';?>