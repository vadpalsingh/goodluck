<!doctype html>
<html class="no-js" lang="eng">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Goodluck Industries  CDW Tubes (Precision Tube Division) :: Milestone </title>
<meta name="description" content="">

<?php include './inc/header.php'; ?>


<main>
   
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                <div class="carousel-item active">
                    <img src="../assets/cwdpipe/about/banner.jpg" class="d-block w-100" alt="...">
                    <h5>Milestone</h5>
                    <div class="carousel-caption d-none d-md-block c1 bg_tr">
                        <p>The company’s capability of producing more than 85,000 metric tons of high-frequency induction welded tubes annually, speaks volumes of its command within the industry. </p>
                    </div>
                </div>
            </div>
          
        </div>
    </section>
    <!-- hero -->
   <section class="innerpagenav">
	 <div class="container">
		<div class="row">
			<ul class="pagenav">
				<li><a href="company-profile.php">Company Profile</a></li>
				<li><a href="milestone.php" class="active">Milestone</a></li>
				<li><a href="leadership.php">Leadership</a></li>
				<li><a href="future-plans.php">Future Plans</a></li>
			</ul>
		</div>
	 </div>   
   </section>
   <section class="milestone">
	  <div class="container-fluid">
		  <div class="milestonebody">
        <ul>
          <li class="wow fadeInDown  animated" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInDown;">
            <span class="yearbox wow fadeInDown  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">1986</span>
            <span class="milestextbox wow fadeInUp  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Incorporated Pvt.  Ltd. Company</span>
          </li>
          <li class="wow fadeInDown  animated" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInDown;">
            <span class="yearbox wow fadeInDown  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">1986</span>
            <span class="milestextbox wow fadeInUp  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Incorporated Pvt.  Ltd. Company</span>
          </li>
          <li class="wow fadeInDown  animated" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInDown;">
            <span class="yearbox wow fadeInDown  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">1986</span>
            <span class="milestextbox wow fadeInUp  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Incorporated Pvt.  Ltd. Company</span>
          </li>
          <li class="wow fadeInDown  animated" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInDown;">
            <span class="yearbox wow fadeInDown  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">1986</span>
            <span class="milestextbox wow fadeInUp  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Incorporated Pvt.  Ltd. Company</span>
          </li>
          <li class="wow fadeInDown  animated" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInDown;">
            <span class="yearbox wow fadeInDown  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">1986</span>
            <span class="milestextbox wow fadeInUp  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Incorporated Pvt.  Ltd. Company</span>
          </li>
          <li class="wow fadeInDown  animated" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInDown;">
            <span class="yearbox wow fadeInDown  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">1986</span>
            <span class="milestextbox wow fadeInUp  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Incorporated Pvt.  Ltd. Company</span>
          </li>
          <li class="wow fadeInDown  animated" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInDown;">
            <span class="yearbox wow fadeInDown  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">1986</span>
            <span class="milestextbox wow fadeInUp  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Incorporated Pvt.  Ltd. Company</span>
          </li>
          <li class="wow fadeInDown  animated" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInDown;">
            <span class="yearbox wow fadeInDown  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">1986</span>
            <span class="milestextbox wow fadeInUp  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Incorporated Pvt.  Ltd. Company</span>
          </li>
          <li class="wow fadeInDown  animated" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInDown;">
            <span class="yearbox wow fadeInDown  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">1986</span>
            <span class="milestextbox wow fadeInUp  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Incorporated Pvt.  Ltd. Company</span>
          </li>
          <li class="wow fadeInDown  animated" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInDown;">
            <span class="yearbox wow fadeInDown  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">1986</span>
            <span class="milestextbox wow fadeInUp  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Incorporated Pvt.  Ltd. Company</span>
          </li>
          <li class="wow fadeInDown  animated" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInDown;">
            <span class="yearbox wow fadeInDown  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">1986</span>
            <span class="milestextbox wow fadeInUp  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Incorporated Pvt.  Ltd. Company</span>
          </li>
          <li class="wow fadeInDown  animated" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInDown;">
            <span class="yearbox wow fadeInDown  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">1986</span>
            <span class="milestextbox wow fadeInUp  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Incorporated Pvt.  Ltd. Company</span>
          </li>
          <li class="wow fadeInDown  animated" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInDown;">
            <span class="yearbox wow fadeInDown  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">1986</span>
            <span class="milestextbox wow fadeInUp  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Incorporated Pvt.  Ltd. Company</span>
          </li>
          <li class="wow fadeInDown  animated" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInDown;">
            <span class="yearbox wow fadeInDown  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">1986</span>
            <span class="milestextbox wow fadeInUp  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Incorporated Pvt.  Ltd. Company</span>
          </li>
          <li class="wow fadeInDown  animated" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInDown;">
            <span class="yearbox wow fadeInDown  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">1986</span>
            <span class="milestextbox wow fadeInUp  animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">Incorporated Pvt.  Ltd. Company</span>
          </li>
        </ul>   
      </div>  
	  </div>   
   </section>


</main>



<?php include './inc/footer.php';?>