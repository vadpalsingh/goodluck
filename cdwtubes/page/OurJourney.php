<?php include './inc/header.php'; ?>
<section class="brand__area1  title1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div class="sectiontitle1 mt-0 mb-3">
                    <h2>Our Journey</h2>
                    <span class="headerLine"></span>
             
                </div>
            </div>
        </div>
        <div class="row  ">

            <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                    <a href="#">
                        <div class="produ_ct1">
                            <div class="p_im_g">
                            <h1>1986</h1>
                            </div>
                            <p> Incorporated Pvt. Ltd. Company</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                    <a href="#">
                        <div class="produ_ct1">
                            <div class="p_im_g">
                            <h1>1987</h1>
                            </div>
                            <p>First plant commissioned </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                    <a href="#">
                        <div class="produ_ct1">
                            <div class="p_im_g">
                            <h1>1994</h1>
                            </div>
                            <p>Converted in to Public Ltd. Co. </p>
                        </div>
                    </a>
                </div>
            </div>
            <!-- end -->
            <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                    <a href="#">
                        <div class="produ_ct1">
                            <div class="p_im_g">
                            <h1>1995</h1>
                            </div>
                            <p> Got listed on Stock Exchange</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                    <a href="#">
                        <div class="produ_ct1">
                            <div class="p_im_g">
                            <h1>1997</h1>
                            </div>
                            <p>Capacity enhanced to 50000 MTPA </p>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                    <a href="#">
                        <div class="produ_ct1">
                            <div class="p_im_g">
                            <h1>2003</h1>
                            </div>
                            <p>Turnover crossed 1,500 mn</p>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                    <a href="#">
                        <div class="produ_ct1">
                            <div class="p_im_g">
                            <h1>2006</h1>
                            </div>
                            <p> Commissioned First Forging Plant </p>
                        </div>
                    </a>
                </div>
            </div>
            <!-- end -->
            <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                    <a href="#">
                        <div class="produ_ct1">
                            <div class="p_im_g">
                            <h1>2007</h1>
                            </div>
                            <p>First Plant Commissioned for ERW/CDW Precision Tubes</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                    <a href="#">
                        <div class="produ_ct1">
                            <div class="p_im_g">
                            <h1>2009</h1>
                            </div>
                            <p>Turnover crossed INR 5,000 Mn </p>
                        </div>
                    </a>
                </div>
            </div>

            <!-- end -->
            <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                    <a href="#">
                        <div class="produ_ct1">
                            <div class="p_im_g">
                            <h1>2013</h1>
                            </div>
                            <p>Turnover crossed INR 10,000 Mn</p>
                        </div>
                    </a>
                </div>
            </div>


            <!-- end -->


            <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                    <a href="#">
                        <div class="produ_ct1">
                            <div class="p_im_g">
                            <h1>2014</h1>
                            </div>
                            <p>Commissioned another plant for ERW/CDW Precision Tubes</p>
                        </div>
                    </a>
                </div>
            </div>


            <!-- end -->

            <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                    <a href="#">
                        <div class="produ_ct1">
                            <div class="p_im_g">
                            <h1>2015</h1>
                            </div>
                            <p>Expanded Engineering structured products to high growth sectors like solar and railways</p>
                        </div>
                    </a>
                </div>
            </div>


            <!-- end -->
            <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                    <a href="#">
                        <div class="produ_ct1">
                            <div class="p_im_g">
                            <h1>2016</h1>
                            </div>
                            <p>Company changed name to Goodluck India Ltd.</p>
                        </div>
                    </a>
                </div>
            </div>


            <!-- end -->
            <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                    <a href="#">
                        <div class="produ_ct1">
                            <div class="p_im_g">
                            <h1>2018</h1>
                            </div>
                            <p>Commissioned Kachchh Plant for ERW Precision Tubes</p>
                        </div>
                    </a>
                </div>
            </div>


            <!-- end -->
            <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                    <a href="#">
                        <div class="produ_ct1">
                            <div class="p_im_g">
                            <h1>2022</h1>
                            </div>
                            <p>Turnover crossed – INR 2500 crore (330M USD) Export Turnover crossed – INR 1200 Crore (159M USD)</p>
                        </div>
                    </a>
                </div>
            </div>


            <!-- end -->
        </div>

    </div>
</section>
<?php include './inc/footer.php'; ?>