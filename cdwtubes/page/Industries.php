<?php include './inc/header.php'; ?>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=PT+Sans+Caption&display=swap" rel="stylesheet">

<section class="brand__area1  title1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sectiontitle1 mt-0 mb-3">
                    <h2>Industries</h2>
                    <span class="headerLine"></span>

                </div>
            </div>
        </div>
    </div>

</section>

<!-- end -->
<section class=" ptb  pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="precision   p-3  ">
                    <h3>Precision tubes for applications
                        that demand accuracy</h3>
                    <h5>Key provider for auto industry</h5>
                    <div class="proc_detail">
                        <div class="detail wd40">
                            <h6>Boilers and Heaters</h6>
                            <ul class="bl_det">
                                <li>Chemical/Sugar Industry</li>
                                <li>Super Heater</li>
                                <li>Paper/Process Industry</li>
                                <li>Air Pre Heater</li>
                                <li>Heat Exchange</li>

                            </ul>
                        </div>
                        <div class="detail wd60">
                            <h6>Boilers and Heaters</h6>
                            <ul>
                                <li>Bearings and Spindles,
                                    Trolley Handles Hydraulic
                                    and Pneumatic Line,
                                    Electrical Conduit</li>
                                <li>Oxygen Lancing Pipes,
                                    Cycle Pumps, Industrial
                                    Chain Roller, Gas Stove
                                    Lighter</li>
                                <li>Electrostatic Precipitations</li>
                                <li>Textile Frames and Bobbins</li>
                                <li>Main Beam for Pedestal Fans</li>
                                <li>Hydraulic Cylinder</li>

                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-6">
                <div class="precision ">
                    <img src="../assets/cwdpipe/industries/a1.png" alt="industries" class="w100">
                </div>
            </div>

        </div>
    </div>

</section>
<section class=" ptb  pt-5 pb-5">
<div class="container">
        <div class="row">
        <div class="col-md-6">
                <div class="precision ">
                    <img src="../assets/cwdpipe/industries/a2.png" alt="industries" class="w100">
                </div>
            </div>
            <div class="col-md-6">
                <div class="precision   p-3  ">
                    <h3>Precision tubes for applications
that demand accuracy</h3>
                    <h5>Key provider for Two-wheeler industry</h5>
                    <h6 class="h_color">Automobiles</h6>
                    <div class="proc_detail">
                        <div class="detail wd40">
                            
                            <ul class="bl_det">
                               <li> Front Fork Top / Bottom</li>
                               <li> Steering Column</li>
                               <li> Two-wheeler Main Frame</li>
                               <li> Bottom Chassis</li>
                               <li> Fuel Tank Spacer</li>
                               <li> Swing Arm</li>
                               <li> Propeller Shaft</li>
                               <li> Seat Frame</li>
                               <li> Tie Rod</li>
                              

                            </ul>
                        </div>
                        <div class="detail wd60">
                       
                            <ul>
                            <li> Rocker Arm Shaft</li>
                               <li> Catalytic Converter</li>
                               <li> Fuel Injection</li>
                               <li> Side Impact Beams</li>
                               <li> Four Wheeler Dash Board Frame</li>
                               <li> Shock Absorber</li>
                               <li> Silent Blocks</li>
                               <li> Control Arms</li>
                               <li> Gear Shift Lever</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
           

        </div>
    </div>
</section>
<?php include './inc/footer.php'; ?>