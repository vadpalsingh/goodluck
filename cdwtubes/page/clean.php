<?php include './inc/header.php'; ?>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=PT+Sans+Caption&display=swap" rel="stylesheet">

<section class="brand__area1  title1" style=" padding-top: 55px;     padding-bottom: 0px;">
<div class="precisio_n   pb-3  mt-5" style="background-image:url('../assets/cwdpipe/clean/b_image.png');background-size: 100% 100%; 
    text-align: center;">
                    <img src="../assets/cwdpipe/clean/logo.png" alt="industries" style="width:12%;margin:auto;margin_bottom:20px">
                    <h3 class="text-white">Clean Energy
                        &amp; Climate Action</h3>
                    <p class="text-white" style="    border-bottom: 1px solid #eee;
    margin: 0 0 10px;
    padding: 0 0 8px 0;">has never been
                        more urgent.</p>
                    <p class="text-white"><b>Goodluck as a group</b><br>
                        is striving to bring new means
                        for long term sustainability solutions
                        in manufacturing</p>
                </div>
</section>

<!-- end -->
<section class="    pt-5  ">
    <div class="container">
        <div class="row">
            
            <div class="col-md-12">
                <div class="precisi_on  text-center  p-3 mt-5">
                    <h2> GOODLUCK Clean Energy & Climate Action</h2>
                    <p>
                        IndiaMART TrustSeal Established in the year 1986, Goodluck India Limited is an ISO 9001:2008
                        certified organization, engaged in manufacturing and exporting of a wide range of galvanized
                        sheets & coils, towers, hollow sections, CR coils CRCA and pipes & tubes. We also specialize in
                        providing Telecommunication Structures, ERW Steel Tubes, ERW Steel Pipes, and Galvanized Black
                        Steel Tubes. These are acclaimed for high tensile strength, long service life and higher
                        efficiency.</p>
                </div>
            </div>
        </div>
    </div>
     
</section>
<!-- end -->
<section class=" ptb   pb-5">
<div class="container">
    <div class="row">
        <div class="col-md-6">
        <div class="leftimf">
            <img src="../assets/cwdpipe/clean/c1.png" alt="industries"  class="w100 r_5" >
            </div>
        </div>
        <div class="col-md-6">
        <h3 class="text-dark pt_10">Solar</h3>
           <p class="text-dark"> In process of installing solar panel on the rooftop giving long term sustainable solution for lightning for plants and offices.<br> The CDW (Cold Drawn Welded) Tubes are formed through our state of the art manufacturing plant. We design high quality tubes on fully automated drawn benche. These tubes are especially used in automotive and general industries</p>
           <p class="text-dark">In the manufacturing of CDW Tubes, the process starts with annealing and surface treatment. The cold swaging point operations for preparing pointed ends with high precision close machined die to control outer diameter is installed.</p>
           <p class="text-dark">The tubes are repeatedly tested on various global parameters and are passed through stringent quality tests before being offered to our clients.</p>
            </div>
        </div>
        <div class="row mt_40">
        
        <div class="col-md-6 col-xs-pull-6 ">
        <h3 class="text-dark pt_10">Water</h3>
           <p class="text-dark">In process of installing solar panel on the rooftop giving long term sustainable solution for lightning for plants and offices.<br> The CDW (Cold Drawn Welded) Tubes are formed through our state of the art manufacturing plant. We design high quality tubes on fully automated drawn benche. These tubes are especially used in automotive and general industries</p>
           <p class="text-dark">In the manufacturing of CDW Tubes, the process starts with annealing and surface treatment. The cold swaging point operations for preparing pointed ends with high precision close machined die to control outer diameter is installed.</p>
           <p class="text-dark">The tubes are repeatedly tested on various global parameters and are passed through stringent quality tests before being offered to our clients.</p>
            </div>
            <div class="col-md-6  col-xs-push-6 ">
        <div class="leftimf">
            <img src="../assets/cwdpipe/clean/c2.png" alt="industries"  class="w100 r_5" >
            </div>
        </div>
       
        </div>
        <div class="row mt_40">
        <div class="col-md-6">
        <div class="leftimf">
            <img src="../assets/cwdpipe/clean/c3.png" alt="industries"  class="w100 r_5" >
            </div>
        </div>
        <div class="col-md-6">
        <h3 class="text-dark pt_10">Offices & Plants</h3>
           <p class="text-dark"> In process of installing solar panel on the rooftop giving long term sustainable solution for lightning for plants and offices.<br> The CDW (Cold Drawn Welded) Tubes are formed through our state of the art manufacturing plant. We design high quality tubes on fully automated drawn benche. These tubes are especially used in automotive and general industries</p>
           <p class="text-dark">In the manufacturing of CDW Tubes, the process starts with annealing and surface treatment. The cold swaging point operations for preparing pointed ends with high precision close machined die to control outer diameter is installed.</p>
           <p class="text-dark">The tubes are repeatedly tested on various global parameters and are passed through stringent quality tests before being offered to our clients.</p>
            </div>
        </div>


    </div>

</div>
 
</section>

<?php include './inc/footer.php'; ?>