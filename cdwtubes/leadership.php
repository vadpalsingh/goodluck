<!doctype html>
<html class="no-js" lang="eng">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Goodluck Industries  CDW Tubes (Precision Tube Division) :: Leadership </title>
<meta name="description" content="">

<?php include './inc/header.php'; ?>


<main>
   
    
    <!-- hero -->
   <section class="innerpagenav">
	 <div class="container">
		<div class="row">
			<ul class="pagenav">
				<li><a href="company-profile.php">Company Profile</a></li>				
				<li><a href="leadership.php" class="active">Leadership</a></li>
				<li><a href="future-plans.php">Future Plans</a></li>
			</ul>
		</div>
	 </div>   
   </section>
   <section class="p60 leadership light-grey">
	   <div class="container">
	    <div class="row align-items-center">
	       <div class="col-lg-4 col-md-5">
           <img src="../assets/cwdpipe/leadership.png" class="wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">
			</div>
            <div class="col-lg-8 col-md-7">
			    <div class="home-about wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">
				   <h2 class="boxdesignmd">Visionary leadership inspiring<br> to deliver sustainable value</h2>
					<p>At Goodluck Industries, leadership is the driving force that makes us to work together seamlessly to achieve our collective enterprise goals.</p>      
                    <p>Customer-centric strategies, responsiveness, flexibility, and innovation define our leadership principles are a way of life. Inspired by the same, we take on all challenges that come our way and simply excel at them.</p>
                    <p>Be it organizational behavior, the mindfulness towards the employees’ concerns, or any development in the leadership theory, our visionary leadership always strives to create an encouraging, and positively-charged working environment. Our forward-looking leadership team:</p>
                    <ul class="liststyle">
                        <li>Drives our company</li>
                        <li>Guides our strategy</li>
                        <li>Leads our people</li>
                        <li>Underpins all that we do</li>
                        <li>Helps us make a difference</li>    
                    <ul>
                </div>
			</div> 			
		 </div>
       </div>
    </section>


</main>



<?php include './inc/footer.php';?>