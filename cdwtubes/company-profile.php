<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India CDW Tubes (Precision Tube Division) :: Company Profile </title>
    <meta name="description" content="">

    <?php include './inc/header.php'; ?>


    <main>

        <section class="good_luck">
            <div class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item active">
                        <img src="../assets/cwdpipe/about/banner.jpg" class="d-block w-100" alt="...">
                        <h5 class="wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">Company Profile</h5>
                        <div class="carousel-caption d-none d-md-block bg_tr wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">
                            <p>The company’s capability of producing more than 85,000 metric tons of high-frequency
                                induction welded tubes annually, speaks volumes of its command within the industry. </p>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- hero -->
        <section class="innerpagenav">
            <div class="container">
                <div class="row">
                    <ul class="pagenav">
                        <li><a href="company-profile.php" class="active">Company Profile</a></li>
                        <li><a href="leadership.php">Leadership</a></li>
                        <li><a href="future-plans.php">Future Plans</a></li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- about -->
        <section class="p60 abou_t">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="comman-head">
                            <h2 class="pb3">Welcome to Goodluck Industries<span>Enlisting Global Presence with
                                    international partners</span></h2>

                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="g_bout">
                            <div class="home-about wow fadeInUp" data-wow-delay=".2s"
                                style="visibility: visible; animation-delay: 0.2s;">

                                <p class="wow fadeInUp" data-wow-delay=".9s"
                                    style="visibility: visible; animation-delay: 0.8s;">Goodluck Industries is an
                                    IATF-16949 certified company and Government of India Recognized three-star export
                                    house. Incepted in year 2007, this company has made its presence worldwide in the
                                    field of ERW/CDW Precision Tubes. For various applications in the field of
                                    Automotive Industries, Bicycle Industry, General Engineering, Textile Industry,
                                    Boilers, Air-Heaters, Hydraulic Cylinder, Non-automotive Tubes for Cylinders of
                                    Heavy Earth Movers & JCB, Tubes for Tractor Parts etc </p>
                                <div class="row">
                                    <div class="col-md-7">
                                        <h4>Catering to the ever-escalating demands of the automobile & industrial
                                            application</h4>
                                        <p>A 36 Years old group to cater to the evolving demands of the automobile
                                            industry in India</p>
                                        <p> Headquartered in the Ghaziabed, the company has 2 state-of-the-art
                                            manufacturing facilities across the country. The locations include
                                            Sikandrabad (Uttar Pradesh), Kuchchh (Gujarat).</p>
                                        <p>All the production units are equipped with a unique combination of
                                            professional manpower and ultramodern machines, led by visionary management.
                                        </p>
                                        <p><img src="../assets/cwdpipe/about/euro-cert.png" class="d-block w-100"
                                                alt="...">
                                            <span class="eurocert">The facilities at Goodluck Industries
                                                are<br>individually EN ISO 9001-2015 certified</span>
                                        </p>
                                    </div>
                                    <div class="col-md-5">
                                        <h4>FACTS</h4>
                                        <div class="a_im_g">
                                            <div class="gray cunt p-3  wow fadeIn">
                                                <!-- <i class="fa fa-briefcase"></i> -->
                                                <p class="number" style="display:inline-block">85000 </p>
                                                <span
                                                    style="color:#838080;font-size: 21px;font-weight: 600;">Tonnes</span>
                                                <span></span>
                                                <p>Annual Installed Capacity</p>
                                            </div>

                                            <div class="gray cunt p-3  wow fadeIn">
                                                <p class="number">100</p>
                                                <span></span>
                                                <p>Countries </p>
                                            </div>
                                            <div class="gray cunt p-3  wow fadeIn">
                                                <p class="number">1000 +</p>
                                                <span></span>
                                                <p>Workforce</p>
                                            </div>
                                            <div class="gray cunt p-3  wow fadeIn">
                                                <p class="number">300+</p>
                                                <span></span>
                                                <p>Customers </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <img src="../assets/cwdpipe/about/map.jpg" class="wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;"
                            style="background: white;padding: 10px;border-radius: 5px;margin-top: 25px;display: inline-block;">
                    </div>
                </div>
            </div>
        </section>

        <section class="p60 light-grey wow fadeInUp" data-wow-delay=".2s"
            style="visibility: visible; animation-delay: 0.2s;">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6">
                        <div id="carouselExample" class="carousel slide">
                            <div class="carousel-inner cdwtubesbanner">
                                <div class="carousel-item active">
                                    <img src="../assets/cwdpipe/about/slide1.jpg" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                    <img src="../assets/cwdpipe/about/slide3.jpg" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                    <img src="../assets/cwdpipe/about/slide2.jpg" class="d-block w-100" alt="...">
                                </div>
                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample"
                                    data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                </button>
                                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample"
                                    data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="home-about wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">
                            <h2 class="boxdesignmd">Among the ‘first’ companies to deploy the ultra-modern HFIW
                                Technology for carbon steel tubes in India.</h2>
                            <p>Goodluck Industries has taken the manufacturing and global supply of carbon steel tubes
                                to the next level. Right from the very beginning, the company has been committed to
                                delivering a comprehensive range of tubular products that are an epitome of quality with
                                matchlessness on all parameters. All variants of tubes that the company delivers are
                                valued the world over for their precise dimensions, accurate mechanical characteristics,
                                and flawless surface finish.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>





    </main>



    <?php include './inc/footer.php';?>