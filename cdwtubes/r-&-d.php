<!doctype html>
<html class="no-js" lang="eng">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Goodluck Industries  CDW Tubes (Precision Tube Division) :: R & D </title>
<meta name="description" content="">
<?php include './inc/header.php'; ?>
<main>
   
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                <div class="carousel-item active">
                    <img src="../assets/cwdpipe/banner/R&Dbanner-enhance-1920w.jpg" class="d-block w-100" alt="...">
                    <h5 class="wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">R & D</h5>
                    <div class="carousel-caption d-none d-md-block bg_tr wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">
                        <h4>One step ahead of the future</h4>
                        <p>Furthermore , we possess R&D unit, which assists us in under taking metallurgical tests on our range or cold drawn welded tubes and precision tubes</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- hero -->
   
   <section class="p60 leadership light-grey">
	   <div class="container">
	    <div class="row row-design justify-content-center align-items-center"> 	
	        <div class="col-lg-5 col-md-6">
              <img src="../assets/cwdpipe/rnd1.jpg" class="wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
			</div>
            <div class="col-lg-6 col-md-6">
			    <div class="wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">					
                   <h2>OUR R&D UNIT IS EQUIPPED WITH FOLLOWING MACHINES AND EQUIPMENT</h2>                       
                    <ul>
                    <li>Lecco carbon apparatus</li>
                    <li>Scanning electron microscope</li>
                    <li>Atomic absorption emission spectrophotometer</li>
                    <li>Universal microscope</li>
                    <li>Micro hardness tester</li>
                    </ul>
                    <h4>This has helped us in upgrading our quality process, which further helps us in retaining the trust of our clients
</h4>
                </div>
			</div> 			
		 </div>

         <div class="row row-reverse justify-content-center align-items-center mt-40"> 	
	        <div class="col-lg-5 col-md-6">
              <img src="../assets/cwdpipe/rnd2.jpg" class="wow fadeInUp" data-wow-delay=".4s" style="visibility: visible; animation-delay: 0.4s;">
			</div>
            <div class="col-lg-6 col-md-6">
			    <div class="wow fadeInUp" data-wow-delay=".4s" style="visibility: visible; animation-delay: 0.4s;">					
                   <h2>Staying in sync with the industry dynamics</h2>                       
                   <p>The dynamics of the industry all across the globe is changing continuously & in order to stay in sync with these changes the role of R&D becomes critical & crucial.</p>                   
                </div>
			</div> 			
		 </div>

         <div class="row row-design  justify-content-center align-items-center mt-40"> 	
	        <div class="col-lg-5 col-md-6">
              <img src="../assets/cwdpipe/rnd3.jpg" class="wow fadeInUp" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s;">
			</div>
            <div class="col-lg-6 col-md-6">
			    <div class="wow fadeInUp" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s;">					
                <h2>It is the only effective way to achieve improved innovation and erase negative product margins </h2>                       
                   <p>Goodluck Industries with trained R&D experts to keeps track of all the advancements in the techniques of manufacturing.</p>                   
                </div>
			</div> 			
		 </div>

         <div class="row row-reverse justify-content-center align-items-center mt-40"> 	
	        <div class="col-lg-5 col-md-6">
              <img src="../assets/cwdpipe/rnd4.jpg" class="wow fadeInUp" data-wow-delay=".6s" style="visibility: visible; animation-delay: 0.6s;">
			</div>
            <div class="col-lg-6 col-md-6">
			    <div class="wow fadeInUp" data-wow-delay=".6s" style="visibility: visible; animation-delay: 0.6s;">					
                   <h2>Hi-tech machines, advanced designs, & improved operational efficiency.</h2>                       
                   <p>Consequently, the hi-tech machines with improved and advanced designs become part of our production unit empowering us to develop more innovative and sophisticated array of stainless steel and aluminized tubes. In addition to this , judicious use of technology has also made it possible for the company to cut down the cost of production and elevate operational efficiency.</p>                   
                </div>
			</div> 			
		 </div>

         

       </div>
    </section>


</main>



<?php include './inc/footer.php';?>