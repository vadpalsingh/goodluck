<!doctype html>
<html class="no-js" lang="eng">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Goodluck Industries  CDW Tubes (Precision Tube Division) :: Quality </title>
<meta name="description" content="">

<?php include './inc/header.php'; ?>


<main>
   
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                <div class="carousel-item active">
                    <img src="../assets/cwdpipe/banner/qualitybanner.jpg" class="d-block w-100" alt="...">
                    <h5 class="wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">Quality</h5>
                    <div class="carousel-caption d-none d-md-block bg_tr wow fadeInUp" data-wow-delay=".2s" style="visibility: visible;animation-delay: 0.2s;top: 0%;width: 30%;transform: perspective(0px) rotateX(0deg);height: 100%;left: 0px !important;border-radius: 0;padding-top: 109px;">
                        <h4>A Front-runner within the Industry</h4>
                        <p>A ceaseless devotion to international quality standards and the consistent production and supply of a world-class product line have made the company a front runner in the industrial arena of the country. Therefore, quality is the USP of Goodluck Industries and is responsible for its progress over the years.</p>                 
                    </div>
                </div>
            </div>          
        </div>
    </section>
    <!-- hero -->
   
   <section class="p60 leadership light-grey">
	   <div class="container">
	    <div class="row row-design justify-content-center align-items-center"> 	
	        <div class="col-lg-5 col-md-6 image-slider">                        
                <div class="wow fadeInUp gallery-container" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    <img src="../assets/cwdpipe/quality1.jpg" alt="Quality">
                    <img src="../assets/cwdpipe/quality2.jpg" alt="Quality">
                    <img src="../assets/cwdpipe/quality3.jpg" alt="Quality">
                    <img src="../assets/cwdpipe/quality4.jpg" alt="Quality">
                </div>              
			</div>
            <div class="col-lg-6 col-md-6">
			    <div class="wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">					
                   <h2>Like every industrial success story, quality forms the central character in Goodluck Industries tale of unprecedented market growth.</h2>                       
                   <p>Be it raw material selection, manufacturing or control on processes, every step contributes significantly to the overall product quality. Quality is ensured through a well-equipped lab and support staff including <p>                   
                </div>
			</div> 			
		 </div>

         <div class="row row-reverse justify-content-center align-items-center mt-40"> 	
	        <div class="col-lg-5 col-md-6">
               <div class="wow fadeInUp gallery-container" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
               <img src="../assets/cwdpipe/quality5.jpg" alt="Quality">
                    
                    <img src="../assets/cwdpipe/quality7.jpg" alt="Quality">
                    <img src="../assets/cwdpipe/quality8.jpg" alt="Quality">
                    <img src="../assets/cwdpipe/quality6.jpg" alt="Quality">           
                </div>     
			</div>
            <div class="col-lg-6 col-md-6">
			    <div class="wow fadeInUp" data-wow-delay=".4s" style="visibility: visible; animation-delay: 0.4s;">					
                   <h2>Stringent checkpoints and quality controls across production stages  </h2>                       
                   <p>Goodluck Industries is ISO 9001 :2015, AS 9100D, IATF-16949, ISO-14001 & OH&SMS-45001& CE certified company & processes are in adherence to customer specifications complemented by a series of checkpoints and controls at various production stages. Tests like raw material inspection, chemical compositions tests, online finishing stage inspections and pre-dispatch checks are carried out religiously</p>                   
                </div>
			</div> 			
		 </div>
         
         
       </div>
    </section>
    <section class="p60 leadership">
	   <div class="container">
	    <div class="row justify-content-center align-items-center"> 
           <h2>Download Quality  Certificate</h2>      	
	        <div class="col-lg-3 col-md-3 col-sm-6">                        
                <div class="wow fadeInUp pdfbox" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    <a href="../assets/cwdpipe/certificate/Good-Luck-Industrie-EMS-607465-ISO-14001-Valid-16-11-2025.pdf" target="_blank">
                       EMS-607465 ISO-14001<span class="Date">Valid : 16-NOV-2025</span>
                       <img src="../assets/cwdpipe/certificate/pdficon.png">
                    </a>   
                </div>              
			</div>
            <div class="col-lg-3 col-md-3 col-sm-6">      
                <div class="wow fadeInUp pdfbox" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    <a href="../assets/cwdpipe/certificate/Good-Luck-Industries-OHS-607466-ISO-45001-Valid-16-11-2025.pdf" target="_blank">
                    OHS-607466 ISO-45001<span class="Date">Valid : 16-NOV-2025</span>
                    <img src="../assets/cwdpipe/certificate/pdficon.png">
                    </a>   
                </div>  
			</div> 			
            <div class="col-lg-3 col-md-3 col-sm-6">      
                <div class="wow fadeInUp pdfbox" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    <a href="../assets/cwdpipe/certificate/Goodluck-industries-IATF-16949-Valid-9-4-24 .pdf" target="_blank">
                      IATF-16949<span class="Date">Valid : 9-MARCH-2024</span>
                      <img src="../assets/cwdpipe/certificate/pdficon.png">
                    </a>   
                </div>  
			</div> 		
            <div class="col-lg-3 col-md-3 col-sm-6">      
                <div class="wow fadeInUp pdfbox" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    <a href="../assets/cwdpipe/certificate/Goodluck-Industries-00.12.0737-ISO9001-Valid-4-11-2023.pdf" target="_blank">
                      00.12.0737 EN ISO-9001<span class="Date">Valid : 4-NOV-2023</span>
                      <img src="../assets/cwdpipe/certificate/pdficon.png">
                    </a>   
                </div>  
			</div>     	
             			
		 </div>        
       </div>
    </section>


</main>



<?php include './inc/footer.php';?>