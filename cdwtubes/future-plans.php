<!doctype html>
<html class="no-js" lang="eng">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Goodluck Industries  CDW Tubes (Precision Tube Division) :: Future Plans </title>
<meta name="description" content="">

<?php include './inc/header.php'; ?>


<main>
   
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                <div class="carousel-item active">
                    <img src="../assets/cwdpipe/banner/futureplan.jpg" class="d-block w-100" alt="...">
                    <h5>Future Plans</h5>
                    <div class="carousel-caption d-none d-md-block bg_tr">
                        <h4>Good Industries has its goals set based on a sustainable roadmap to the future</h4>
                    </div>
                </div>
            </div>
          
        </div>
    </section>
    <!-- hero -->
   <section class="innerpagenav">
	 <div class="container">
		<div class="row">
			<ul class="pagenav">
				<li><a href="company-profile.php">Company Profile</a></li>				
				<li><a href="leadership.php">Leadership</a></li>
				<li><a href="future-plans.php" class="active">Future Plans</a></li>
			</ul>
		</div>
	 </div>   
   </section>
   <section class="p60 leadership light-grey">
	   <div class="container">
	    <div class="row justify-content-center align-items-center">         	 			

	        <div class="col-lg-4 col-md-5">
            <img src="../assets/cwdpipe/futureplan.png" class="wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">
			</div>
            <div class="col-lg-7 col-md-7">
			    <div class="home-about wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">
				   
					
                   <h2 class="boxdesignmd">The company is setting up a new production unit in Sikandrabad in tune with the growth of the auto sector.</h2>   
                    
                   <p>
                    Further, the focus is on consistent 
                    improvement in product quality and diversification 
                    of the product portfolio to all material 
                    grades of tubing.
                </p>

                    <p>Goodluck industries vision is to expand/diversify into related fields by setting up a green field plant in the state of Uttar Pradesh.  </p> <p><strong>This new greenfield Large diameter Plant (LDP) facility will be established by in the end of  second quarter 2024. Where the Tube diameter range is started from 88.9mm to 219 mm & thickness up to 16mm. This new plant capacity will be 2500 MT/Month.</strong></p>  <p>With its eyes firmly set on its goals, Goodluck Industries is exploring new territories and avenues of success. Keeping the customers as the prime motivator behind all its operations, it is surging ahead.</p>
                   
                </div>
			</div> 			
		 </div>
       </div>
    </section>


</main>



<?php include './inc/footer.php';?>