<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India CDW Tubes (Precision Tube Division) </title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <style>
    .produ_ct {
        text-align: center;
        border-radius: 5px;
        border: 1px solid #dddddd;
        background-color: #fff;
    }
    </style>
    <main>
        <section class="good_luck">
            <div id="carouselExample" class="carousel slide banner__slider" data-bs-ride="carousel" data-pause="false">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item">
                        <img src="../assets/cwdpipe/banner/s1.png" class="d-block w-100" alt="...">
                        <h5>Sikandrabad Plant</h5>
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                            <p>The company’s capability of producing more than 85,000 metric tons of high-frequency
                                induction welded tubes annually, speaks volumes of its command within the industry. </p>
                        </div>
                    </div>
                    <div class="carousel-item active">
                        <img src="../assets/cwdpipe/banner/bg4.jpg" class="d-block w-100" alt="...">
                        <h5> Sikandrabad Plant</h5>
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                            <p>The company’s capability of producing more than 85,000 metric tons of high-frequency
                                induction welded tubes annually, speaks volumes of its command within the industry. </p>
                        </div>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </section>
        <!-- hero -->
        <!-- about -->
        <section class="p60 abou_t">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="comman-head">
                            <h2 class="pb3">Welcome to Goodluck Industries<span>Enlisting Global Presence with
                                    international partners</span></h2>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="g_bout">
                            <div class="home-about wow fadeInUp" data-wow-delay=".2s"
                                style="visibility: visible; animation-delay: 0.2s;">
                                <p class="wow fadeInUp" data-wow-delay=".9s"
                                    style="visibility: visible; animation-delay: 0.8s;">Goodluck Industries is an
                                    IATF-16949 certified company and Government of India Recognized three-star export
                                    house. Incepted in year 2007, this company has made its presence worldwide in the
                                    field of ERW/CDW Precision Tubes. For various applications in the field of
                                    Automotive Industries, Bicycle Industry, General Engineering, Textile Industry,
                                    Boilers, Air-Heaters, Hydraulic Cylinder, Non-automotive Tubes for Cylinders of
                                    Heavy Earth Movers & JCB, Tubes for Tractor Parts etc </p>
                                <div class="row">
                                    <div class="col-md-7">
                                        <h4>Catering to the ever-escalating demands of the automobile & industrial
                                            application</h4>
                                        <p>A 36 Years old group to cater to the evolving demands of the automobile
                                            industry in India</p>
                                        <p> Headquartered in the Ghaziabed, the company has 2 state-of-the-art
                                            manufacturing facilities across the country. The locations include
                                            Sikandrabad (Uttar Pradesh), Kuchchh (Gujarat).</p>
                                        <p>All the production units are equipped with a unique combination of
                                            professional manpower and ultramodern machines, led by visionary management.
                                        </p>
                                        <p><img src="../assets/cwdpipe/about/euro-cert.png" class="d-block w-100"
                                                alt="...">
                                            <span class="eurocert">The facilities at Goodluck Industries
                                                are<br>individually EN ISO 9001-2015 certified</span>
                                        </p>
                                    </div>
                                    <div class="col-md-5">
                                        <h4>FACTS</h4>
                                        <div class="a_im_g">
                                            <div class="gray cunt p-3  wow fadeIn">
                                                <!-- <i class="fa fa-briefcase"></i> -->
                                                <p class="number" style="display:inline-block">80000 </p>
                                                <span
                                                    style="color:#838080;font-size: 21px;font-weight: 600;">Tonnes</span>
                                                <span></span>
                                                <p>Annual Installed Capacity</p>
                                            </div>
                                            <div class="gray cunt p-3  wow fadeIn">
                                                <p class="number">100</p>
                                                <span></span>
                                                <p>Countries </p>
                                            </div>
                                            <div class="gray cunt p-3  wow fadeIn">
                                                <p class="number">1000 +</p>
                                                <span></span>
                                                <p>Workforce</p>
                                            </div>
                                            <div class="gray cunt p-3  wow fadeIn">
                                                <p class="number">450+</p>
                                                <span></span>
                                                <p>Customers </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="read-more">
                                    <a href="company-profile.php">Know more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <img src="../assets/cwdpipe/about/map.jpg"
                            style="background: white;padding: 10px;border-radius: 5px;margin-top: 25px;display: inline-block;">
                    </div>
                </div>
            </div>
        </section>
        <!-- counter -->
        <!-- service -->
        <section class="brand__area p60">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sectiontitle">
                            <h2>Application / Product</h2>
                            <span class="headerLine"></span>
                            <p class="p_color">Established in the year 1986, Goodluck India Limited is an ISO 9001:2008
                                certified
                                organization,<br> engaged in manufacturing and exporting of a wide range of galvanized
                                sheets & coils, <br>towers, hollow sections, CR coils CRCA and pipes & tubes.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <!-- <img src="../assets/cwdpipe/product/1.png" alt="" class="res"> -->
                                        <img src="../assets/cwdpipe/product/Two-wheelerframetubes.png"
                                            alt="Two wheeler Frame Tubes" class="res">
                                    </div>
                                    <h3>Two wheeler Frame Tubes</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/squire.png" alt="Squire" class="res">
                                    </div>
                                    <h3>Squire</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/HydraulicCylinderTube.png"
                                            alt="Hydraulic Cylinder Tube" class="res">
                                    </div>
                                    <h3>Hydraulic Cylinder Tube </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- end -->
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/BoilerTube.png" alt="Boiler Tube"
                                            class="res">
                                    </div>
                                    <h3>Boiler Tube </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/CBQPressureOuter.png"
                                            alt="CBQ Pressure Outer" class="res">
                                    </div>
                                    <h3>CBQ Pressure Outer </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/FrontForkTube.png" alt="TFF - Front Fork"
                                            class="res">
                                    </div>
                                    <h3>TFF - Front Fork </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/DriveShaft.png"
                                            alt="Drive Shaft - Drive Shaft " class="res">
                                    </div>
                                    <h3> Drive Shaft - Drive Shaft </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- end -->
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/RearAxle.jpg" alt="Axle - Rear  Axle"
                                            class="res">
                                    </div>
                                    <h3>Axle - Rear Axle</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/TransformerTubes.jpg"
                                            alt="Section  (Rectangle / Square / Oval)" class="res">
                                    </div>
                                    <h3>Section (Rectangle / Square / Oval) </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- end -->
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/3.jpg" alt="Control Arms" class="res">
                                    </div>
                                    <h3>Control Arms </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- end -->
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/4.jpg" alt="Crash Bumper Tube"
                                            class="res">
                                    </div>
                                    <h3>Crash Bumper Tube</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/5.jpg"
                                            alt="Cycle frames, forks, hub tubes" class="res">
                                    </div>
                                    <h3>Cycle frames, forks, hub tubes </h3>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/7.jpg" alt="Engine Mounting Tube"
                                            class="res">
                                    </div>
                                    <h3> Engine Mounting Tube </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- end -->
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/8.jpg" alt="Exhaust tubes" class="res">
                                    </div>
                                    <h3>Exhaust tubes</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/9.jpg" alt="Fuel Lines" class="res">
                                    </div>
                                    <h3>Fuel Lines </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/10.jpg" alt="Furniture industry"
                                            class="res">
                                    </div>
                                    <h3>Furniture industry</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/11.jpg" alt="GAS STRUT TUBE"
                                            class="res">
                                    </div>
                                    <h3>GAS STRUT TUBE</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/12.jpg" alt="Gear Shit Lever"
                                            class="res">
                                    </div>
                                    <h3>Gear Shit Lever </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/13.jpg" alt="Handle Bar" class="res">
                                    </div>
                                    <h3>Handle Bar</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/16.jpg" alt="Propeller shafts"
                                            class="res">
                                    </div>
                                    <h3>Propeller shafts</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/2.jpg" alt="Construction industry"
                                            class="res">
                                    </div>
                                    <h3>Construction industry</h3>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/14.jpg" alt="Machinery manufacture"
                                            class="res">
                                    </div>
                                    <h3>Machinery manufacture </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/15.jpg" alt="OIL INDUSTRY  Line pipes"
                                            class="res">
                                    </div>
                                    <h3>OIL INDUSTRY Line pipes </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/6.jpg" alt="Transformers" class="res">
                                    </div>
                                    <h3>Transformers</h3>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/17.jpg" alt="Railway coaches tubes"
                                            class="res">
                                    </div>
                                    <h3>Railway coaches tubes </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/18.jpg" alt="Seat Frame" class="res">
                                    </div>
                                    <h3>Seat Frame </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/19.jpg" alt="Side Impact Beam"
                                            class="res">
                                    </div>
                                    <h3>Side Impact Beam </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/20.jpg" alt="Steering stem new"
                                            class="res">
                                    </div>
                                    <h3>Steering stem new</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/21.jpg" alt="Steering Stem" class="res">
                                    </div>
                                    <h3>Steering Stem </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/22.jpg" alt="SWING ARM" class="res">
                                    </div>
                                    <h3>SWING ARM</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/23.jpg" alt="Tie rods and drag links"
                                            class="res">
                                    </div>
                                    <h3>Tie rods and drag links</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 all_hide">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <!-- <img src="../assets/cwdpipe/product/1.png" alt="" class="res"> -->
                                        <img src="../assets/cwdpipe/application/1.jpg"
                                            alt="Chassis Cross Member & long Member tube" class="res">
                                    </div>
                                    <h3>Chassis Cross Member & long Member tube</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- end -->
                    <!-- end -->
                </div>
            </div>
            <div class="col-md-12">
                <div class="ViewAll text-center mt-4">
                    <button class="v_all"><i class="fa fa-plus"></i> View All</button>
                </div>
            </div>
            </div>
        </section>
        <!-- service -->
        <!-- brand__area start -->
        <section class="brand__area1 p60" style="    background-color: #fdfdfd;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sectiontitle">
                            <h2>Our Clients<span>Nurturing collaboration with prominent names</span></h2>

                            <span class="headerLine"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="brand__slider p-0 swiper-container">
                            <div class="swiper-wrapper">
                                <div class="brand__slider-item swiper-slide">
                                    <a href="javascript:void(0)"><img src="../assets/img/brand/1.png" alt=""></a>
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                    <a href="javascript:void(0)"><img src="../assets/img/brand/2.png" alt=""></a>
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                    <a href="javascript:void(0)"><img src="../assets/img/brand/3.png" alt=""></a>
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                    <a href="javascript:void(0)"><img src="../assets/img/brand/4.png" alt=""></a>
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                    <a href="javascript:void(0)"><img src="../assets/img/brand/5.png" alt=""></a>
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                    <a href="javascript:void(0)"><img src="../assets/img/brand/6.png" alt=""></a>
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                    <a href="javascript:void(0)"><img src="../assets/img/brand/7.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- brand__area end -->
    </main>
    <?php include './inc/footer.php';?>