<!doctype html>
<html class="no-js" lang="eng">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Goodluck Industries  CDW Tubes (Precision Tube Division) :: Product & Capability </title>
<meta name="description" content="">

<?php include './inc/header.php'; ?>


<main>
   
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                <div class="carousel-item active">
                    <img src="../assets/cwdpipe/banner/productcapabilitiesbanner.jpg" class="d-block w-100" alt="...">
                    <h5 class="wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">Product & Capability</h5>
                    <div class="carousel-caption d-none d-md-block bg_tr wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">
                        <h4>Championing the tube & tubular range making technology</h4>
                        <h4>It's all about in-time delivery</h4>
                    </div>
                </div>
            </div>
          
        </div>
    </section>
    <!-- hero -->
   
   <section class="p60 leadership abou_t">
	   <div class="container">
	    <div class="row row-design justify-content-center align-items-center"> 	
	        <div class="col-lg-5 col-md-6">
              <img src="../assets/cwdpipe/productcapablities1.png" class="wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
			</div>
            <div class="col-lg-7 col-md-6">
			    <div class="wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">					
                   
                   <h2>Presenting Durable Range Carbon steel tubes High-quality products for diverse industry applications</h2>
                   <p>In our range of grades we offer Special Stainless Steel grades and Aluminized Steel tubes in the following grades of stainless steel. Our tubes enable exhaust manufacturers to meet the revised emissions norms for the 2-wheeler, 4-wheeler as well as commercial vehicle industries</p>   
                   <h4><i>The tubes are offered in comprehensive size ranges ranging from</i></h4>                    
                   <img src="../assets/cwdpipe/productcapablities.png" class="wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">                  
                </div>
			</div> 			
		 </div>
        
       </div>
    </section>

    <section class="p60 leadership productcapebilties light-grey wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">
	   <div class="container">
       <div class="row row-design justify-content-center align-items-center"> 	
	        <div class="col-lg-7 col-md-9 image">
              <img src="../assets/cwdpipe/productr1.jpg" class="wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
			</div>
            <div class="col-lg-4 col-md-3 image">
			    <div class="wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">					
                   <h2>Products Range-Specification of Tubes </h2>                       
                   <p>Other Grades Can Be Develop As Per Requirement</p>                   
                </div>
			</div> 			
		 </div>

         <div class="row row-reverse justify-content-center align-items-center mt-40"> 	
	        <div class="col-lg-7 col-md-9 image">
              <img src="../assets/cwdpipe/productr2.jpg" class="wow fadeInUp" data-wow-delay=".4s" style="visibility: visible; animation-delay: 0.4s;">
			</div>
            <div class="col-lg-4 col-md-3">
			    <div class="wow fadeInUp" data-wow-delay=".4s" style="visibility: visible; animation-delay: 0.4s;">					
                   <h2>ERW Tube size chart</h2>                       
                   <p>ERW-ROUND Tubes Product Range<br> 20.00 to 127.00 OD and 1.00 to 9.50 THICKNESS</p>                   
                </div>
			</div> 			
		 </div>

         <div class="row row-design  justify-content-center align-items-center mt-40"> 	
	        <div class="col-lg-7 col-md-9 image">
              <img src="../assets/cwdpipe/productr3.jpg" class="wow fadeInUp" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s;">
			</div>
            <div class="col-lg-4 col-md-3">
			    <div class="wow fadeInUp" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s;">					
                <h2>CDW Tube size chart</h2>                       
                   <p>CDW-ROUND Tubes Product Range<br>8.00 to 120.00 OD and 0.89 to 9.50 THICKNESS</p>                   
                </div>
			</div> 			
		 </div>

         <div class="row row-reverse justify-content-center align-items-center mt-40"> 	
	        <div class="col-lg-7 col-md-9 image">
              <img src="../assets/cwdpipe/productr4.jpg" class="wow fadeInUp" data-wow-delay=".6s" style="visibility: visible; animation-delay: 0.6s;">
			</div>
            <div class="col-lg-4 col-md-3">
			    <div class="wow fadeInUp" data-wow-delay=".6s" style="visibility: visible; animation-delay: 0.6s;">					
                   <h2>Rectangle Tube Product Range </h2>                       
                     <p>(SECTION  TUBE -RECTANGLE ) ( 1.2 to 5.0 THK , 28.58 to 108.00 OD )</p>
                </div>
			</div> 			
		 </div>	
         <div class="row row-design  justify-content-center align-items-center mt-40"> 	
	        <div class="col-lg-7 col-md-9 image">
              <img src="../assets/cwdpipe/productr5.png" class="wow fadeInUp" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s;">
			</div>
            <div class="col-lg-4 col-md-3">
			    <div class="wow fadeInUp" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s;">					
                <h2>Square Tube Product Range</h2>                       
                   <p>(Section Tubes- Square)(1.2 to 1.6 THK, 31.75 to 101.60 OD)S</p>                   
                </div>
			</div> 			
		 </div>
         <div class="loupe"></div>
       </div>
    </section>


</main>



<?php include './inc/footer.php';?>