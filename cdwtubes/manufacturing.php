<!doctype html>
<html class="no-js" lang="eng">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Goodluck Industries  CDW Tubes (Precision Tube Division) :: Manufacturing </title>
<meta name="description" content="">

<?php include './inc/header.php'; ?>


<main>
   
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                <div class="carousel-item active">
                    <img src="../assets/cwdpipe/banner/manbanner1.jpg" class="d-block w-100" alt="...">
                    <h5 class="wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">Manufacturing</h5>
                    <div class="carousel-caption d-none d-md-block bg_tr wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">
                        <h4>High-tech machines for productivity and customization</h4>
                        <p>Goodluck Industries draws its strength and garners the trust of its customers through a meticulously planned manufacturing setup.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="../assets/cwdpipe/banner/manbanner2.jpg" class="d-block w-100" alt="...">
                    <h5 class="wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">Manufacturing</h5>
                    <div class="carousel-caption d-none d-md-block bg_tr wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">
                        <h4>High-tech machines for productivity and customization</h4>
                        <p>Goodluck Industries draws its strength and garners the trust of its customers through a meticulously planned manufacturing setup.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="../assets/cwdpipe/banner/manbanner3.jpg" class="d-block w-100" alt="...">
                    <h5 class="wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">Manufacturing</h5>
                    <div class="carousel-caption d-none d-md-block bg_tr wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">
                        <h4>High-tech machines for productivity and customization</h4>
                        <p>Goodluck Industries draws its strength and garners the trust of its customers through a meticulously planned manufacturing setup.</p>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
               <span class="carousel-control-prev-icon" aria-hidden="true"></span>
               <span class="visually-hidden">Previous</span>
               </button>
               <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
               <span class="carousel-control-next-icon" aria-hidden="true"></span>
               <span class="visually-hidden">Next</span>
               </button>
        </div>
    </section>
    <!-- hero -->
   
   <section class="p60 leadership light-grey">
	   <div class="container">
	    <div class="row justify-content-center align-items-center"> 	
	        <div class="col-lg-5 col-md-6">
              <img src="../assets/cwdpipe/manf1.png" class="wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
			</div>
            <div class="col-lg-6 col-md-6">
			    <div class="wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">					
                   <h2>Quick roll changeover considerably increases the production uptime.</h2>                       
                   <p>The mill has been specifically designed to roll carbon steel tubes. Quick roll changeover considerably increases the production uptime and contributes positively to the overall capacity. Further, features like, online edge preparation and external & internal bead removal within the mill ensure maximum tube strength and superior quality.</p>                   
                </div>
			</div> 			
		 </div>

         <div class="row row-reverse justify-content-center align-items-center mt-40"> 	
	        <div class="col-lg-5 col-md-6">
              <img src="../assets/cwdpipe/manf2.png" class="wow fadeInUp" data-wow-delay=".4s" style="visibility: visible; animation-delay: 0.4s;">
			</div>
            <div class="col-lg-6 col-md-6">
			    <div class="wow fadeInUp" data-wow-delay=".4s" style="visibility: visible; animation-delay: 0.4s;">					
                   <h2>The tube mills operate on high-frequency induction welding technology.   </h2>                       
                   <p>The tube mills at Goodluck Industries are dedicated to Carbon steel tube manufacturing operating on High-Frequency induction welding technologies. It is an edge that speaks volumes of the company’s command in the industry and capability to induce innovative manufacturing practices for optimal product quality.</p>                   
                </div>
			</div> 			
		 </div>

         <div class="row row-design  justify-content-center align-items-center mt-40"> 	
	        <div class="col-lg-5 col-md-6">
              <img src="../assets/cwdpipe/manf3.png" class="wow fadeInUp" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s;">
			</div>
            <div class="col-lg-6 col-md-6">
			    <div class="wow fadeInUp" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s;">					
                <h2>A heat treatment furnace tempering a material would help to reduce the brittleness and remove any stresses. </h2>                       
                   <p>In this process, the tube is heated to a specific temperature and then cooled as per customer specific requirement and supply condition for tube like SRA, Normalized , Annealed , Hardening , Induction Hardening and Quenching.</p>                   
                </div>
			</div> 			
		 </div>

         <div class="row row-reverse justify-content-center align-items-center mt-40"> 	
	        <div class="col-lg-5 col-md-6">
              <img src="../assets/cwdpipe/manf4.png" class="wow fadeInUp" data-wow-delay=".6s" style="visibility: visible; animation-delay: 0.6s;">
			</div>
            <div class="col-lg-6 col-md-6">
			    <div class="wow fadeInUp" data-wow-delay=".6s" style="visibility: visible; animation-delay: 0.6s;">					
                   <h2>Our weld quality is further ensured by online Eddy Current Tester, Ultrasonic test & Magnetic resonance test as per the customer specific requirement</h2>                       
                   <p>The mill uses variable high-frequency induction welding technology for welding tubes, practices that render great welding strength to the tubes without using any kind of filler material. The weld quality is further ensured by the online Eddy Current Tester, UT Test, MRT test for inspection of any possible welding flaws & OD, ID defect.</p>                   
                </div>
			</div> 			
		 </div>
         <div class="row   justify-content-center align-items-center mt-40"> 	
	        <div class="col-lg-6 col-md-6 image">
              <img src="../assets/cwdpipe/banner/productcapabilities.jpg" class="wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
			</div>
            <div class="col-lg-6 col-md-6">
            <div class="wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">					
                   <h2>BLM Cutting Machine</h2>                       
                   <p>BC80 Fully automated cutting and chamfering solution for bushings production CNC sawing and end-machining for tubes, certified production of bushes Non-stop production 24 hours a day of bushings up to <strong>80 mm in diameter</strong> and from <br><strong>32 to 300 mm long</strong> and thickness <strong>range is 0.89 to 6.80 mm.</strong> BC80 is the CNC tube cutting machine for sawing, chamfering and measuring parts.. Dynamic part washing flushes the part prior to measurement. Process reliability is assured. Produce up to 2,200 parts/hour from there the machine automatically applies the perfect cutting conditions to maximize productivity</p>                   
                </div>
			</div> 			
		 </div>

         <div class="row row-design row-reverse justify-content-center align-items-center mt-40"> 	
	        <div class="col-lg-5 col-md-6">
              <img src="../assets/cwdpipe/manf5.png" class="wow fadeInUp" data-wow-delay=".7s" style="visibility: visible; animation-delay: 0.7s;">
			</div>
            <div class="col-lg-6 col-md-6">
			    <div class="wow fadeInUp" data-wow-delay=".8s" style="visibility: visible; animation-delay: 0.8s;">					
                <h2>The tolerances achieved for the ‘first time’ within the industry.</h2>                       
                   <p>Precision cutting of the tubes is done using an in-built rotary cut-off system at tolerances that have never been achieved before in the industry. The true-sized tubes are then passed on to the re-cut machine, if required, for cutting of tubes into multiple smaller length tubes as per specifications.</p>                   
                </div>
			</div> 			
		 </div>

         <div class="row   justify-content-center align-items-center mt-40"> 	
	        <div class="col-lg-5 col-md-6">
              <img src="../assets/cwdpipe/manf6.png" class="wow fadeInUp" data-wow-delay=".9s" style="visibility: visible; animation-delay: 0.9s;">
			</div>
            <div class="col-lg-6 col-md-6">
			    <div class="wow fadeInUp" data-wow-delay=".9s" style="visibility: visible; animation-delay: 0.9s;">					
                   <h2>Once the desired sizes have been achieved, the cut tubes are subjected to end-finishing techniques like deburring and chamfering, inspected finally, and then moved to the packing section. </h2>                       
                   <p>The finished & approved tubes are bundled and then Suitably Packed to avoid any in-transit damage.</p>                   
                </div>
			</div> 			
		 </div>

       </div>
    </section>


</main>



<?php include './inc/footer.php';?>