<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India CDW Tubes (Precision Tube Division) :: Applications </title>
    <meta name="description" content="">

    <?php include './inc/header.php'; ?>


    <main>

        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item active">
                        <img src="../assets/cwdpipe/about/banner.jpg" class="d-block w-100" alt="...">
                        <h5 class="wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">Applications</h5>
                        <div class="carousel-caption d-none d-md-block  bg_tr wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">

                            <p>Established in the year 1986,<br> Goodluck India Limited is an ISO 9001:2008
                                certified organization, engaged in manufacturing and exporting of a wide range of
                                galvanized
                                sheets & coils, towers, hollow sections, CR coils CRCA and pipes & tubes.</p>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- hero -->

        <section class="brand__area p60">
            <div class="container">
            <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <!-- <img src="../assets/cwdpipe/product/1.png" alt="" class="res"> -->
                                        <img src="../assets/cwdpipe/product/Two-wheelerframetubes.png"
                                            alt="Two wheeler Frame Tubes" class="res">
                                    </div>
                                    <h3>Two wheeler Frame Tubes</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/squire.png" alt="Squire" class="res">
                                    </div>
                                    <h3>Squire</h3>
                                </div>
                            </a>
                        </div>
                    </div> -->
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/HydraulicCylinderTube.png"
                                            alt="Hydraulic Cylinder Tube" class="res">
                                    </div>
                                    <h3>Hydraulic Cylinder Tube </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- end -->
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/BoilerTube.png" alt="Boiler Tube"
                                            class="res">
                                    </div>
                                    <h3>Boiler Tube </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/CBQPressureOuter.png"
                                            alt="CBQ Pressure Outer" class="res">
                                    </div>
                                    <h3>CBQ Pressure Outer </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/FrontForkTube.png" alt="TFF - Front Fork"
                                            class="res">
                                    </div>
                                    <h3>TFF - Front Fork </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/DriveShaft.png"
                                            alt="Drive Shaft - Drive Shaft " class="res">
                                    </div>
                                    <h3> Drive Shaft - Drive Shaft </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- end -->
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/RearAxle.jpg" alt="Axle - Rear  Axle"
                                            class="res">
                                    </div>
                                    <h3>Axle - Rear Axle</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/product/TransformerTubes.jpg"
                                            alt="Section  (Rectangle / Square / Oval)" class="res">
                                    </div>
                                    <h3>Section (Rectangle / Square / Oval) </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- end -->
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/3.jpg" alt="Control Arms" class="res">
                                    </div>
                                    <h3>Control Arms </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- end -->
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/4.jpg" alt="Crash Bumper Tube"
                                            class="res">
                                    </div>
                                    <h3>Crash Bumper Tube</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/5.jpg"
                                            alt="Cycle frames, forks, hub tubes" class="res">
                                    </div>
                                    <h3>Cycle frames, forks, hub tubes </h3>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/7.jpg" alt="Engine Mounting Tube"
                                            class="res">
                                    </div>
                                    <h3> Engine Mounting Tube </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- end -->
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/8.jpg" alt="Exhaust tubes" class="res">
                                    </div>
                                    <h3>Exhaust tubes</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/9.jpg" alt="Fuel Lines" class="res">
                                    </div>
                                    <h3>Fuel Lines </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/10.jpg" alt="Furniture industry"
                                            class="res">
                                    </div>
                                    <h3>Furniture industry</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/11.jpg" alt="GAS STRUT TUBE"
                                            class="res">
                                    </div>
                                    <h3>GAS STRUT TUBE</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/12.jpg" alt="Gear Shit Lever"
                                            class="res">
                                    </div>
                                    <h3>Gear Shit Lever </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/13.jpg" alt="Handle Bar" class="res">
                                    </div>
                                    <h3>Handle Bar</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/16.jpg" alt="Propeller shafts"
                                            class="res">
                                    </div>
                                    <h3>Propeller shafts</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/2.jpg" alt="Construction industry"
                                            class="res">
                                    </div>
                                    <h3>Construction industry</h3>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/14.jpg" alt="Machinery manufacture"
                                            class="res">
                                    </div>
                                    <h3>Machinery manufacture </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/15.jpg" alt="OIL INDUSTRY  Line pipes"
                                            class="res">
                                    </div>
                                    <h3>OIL INDUSTRY Line pipes </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/6.jpg" alt="Transformers" class="res">
                                    </div>
                                    <h3>Transformers</h3>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/17.jpg" alt="Railway coaches tubes"
                                            class="res">
                                    </div>
                                    <h3>Railway coaches tubes </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/18.jpg" alt="Seat Frame" class="res">
                                    </div>
                                    <h3>Seat Frame </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/19.jpg" alt="Side Impact Beam"
                                            class="res">
                                    </div>
                                    <h3>Side Impact Beam </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/20.jpg" alt="Steering stem new"
                                            class="res">
                                    </div>
                                    <h3>Steering stem new</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/21.jpg" alt="Steering Stem" class="res">
                                    </div>
                                    <h3>Steering Stem </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/22.jpg" alt="SWING ARM" class="res">
                                    </div>
                                    <h3>SWING ARM</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <img src="../assets/cwdpipe/application/23.jpg" alt="Tie rods and drag links"
                                            class="res">
                                    </div>
                                    <h3>Tie rods and drag links</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                        <div class="brand__slider-item swiper-slide mb-3 pr_b">
                            <a href="javascript:void(0)">
                                <div class="produ_ct">
                                    <div class="p_im_g">
                                        <!-- <img src="../assets/cwdpipe/product/1.png" alt="" class="res"> -->
                                        <img src="../assets/cwdpipe/application/1.jpg"
                                            alt="Chassis Cross Member & long Member tube" class="res">
                                    </div>
                                    <h3>Chassis Cross Member & long Member tube</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- end -->
                    <!-- end -->
                </div>

            </div>
        </section>


    </main>



    <?php include './inc/footer.php';?>