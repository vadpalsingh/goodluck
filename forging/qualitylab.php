 
<?php include './inc/header.php'; ?>
<div class="contant">
 

<section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
           

                <div class="carousel-item  active">
                    <img src="../assets/forging/banner/q_lab.jpg" class="d-block w-100" alt="...">
                </div>
                
            </div>
          
        </div>
    </section>
<section class="QualityLab">
   <div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="heat_treatment mt-4">
            <span class="s_csr mt-4">Quality Lab </span>
            <p class="mb-0 mt-4">Goodluck Engineering Co. ensures on Quality product to our Global partners. In our relentless pursuit of excellence, we consistently meet the stringent requirements of global OEMs. To ensure the highest standards of quality, our laboratory is accredited by NABL as per ISO 17025, enabling us to conduct both chemical and mechanical testing. As evidence of our commitment to quality, we hold certifications in ISO 9001:2015, ISO 14001, and ISO 45001. Some of our in house chemical and mechanical labs can perform:  </p>
            <ul class="csr_aboutq  ">
            <li>Spectro Analysis</li>
            <li>Impact Testing</li>
            <li>King Brinell Hardness</li>
            <li>Hot Tensile Test with 0.2% proof stress</li>
            <li>Radiation Checking Parameters</li>
            <li>100% Ultrasonic Testing</li>
            <li>Surface Testing</li>
            <li>Macro/Micro Structure</li>
            <li>Optical Emission Spectrometer</li>
            <li>100% PMI for all products</li>
            <li>3rd Party Inspection option</li>
                </ul>
        </div>
        </div>
        <div class="col-md-6">
            <div class="himage">
                <img src="../assets/forging/q10.jpg" alt="q1.png" class="w100 r_5" style="height: 500px;">
            </div>
            </div>
            <div class="col-md-12">
            <p class="mt-5">Complete NDT Capabilities with In-house Level 3 Qualified Engineers and the very best Metallurgists, the Industry has…  </p>
            <p>Precision is pivotal in describing Quality and Goodluck stands committed to continuously invest in state of the art instruments to measure each and every dimension. Hence we have invested in three state of the art machinery like :</p>
                <ul class="csr_aboutq">
          <li>VMM Inspection (Vision Measuring Machine)   </li>
          <li>Contracer (Contour Measuring System)   </li>
          <li> 2D & 3D Cam Modelling - With state of the art Forging Simulation software.   </li>
                </ul>
            </div>
            

    </div>
    <div class="row">
    <div class="col-md-6">
                <div class="q_banner mt-5">
                    <img src="../assets/forging/quality/q1.jpg" alt="" class="w100 r_5">
                </div>
            </div>
            <div class="col-md-6">
                <div class="q_banner mt-5">
                    <img src="../assets/forging/quality/q2.jpg" alt="" class="w100 r_5">
                </div>
            </div>
    </div>
    <!-- end -->
    
   </div>
</section>
</div>
<?php include './inc/footer.php';?>