<section class="brand__area1  title1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sectiontitle1 mt-0 mb-3">
                    <h2>Manufacturing
process</h2>
                    <span class="headerLine"></span>

                </div>
            </div>
        </div>
    </div>

</section>
<section class=" ptb  pt-5 pb-5" style="background-color: #eee;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="precision card p-4 mt-5">
                    <img src="../assets/cwdpipe/process/process1.png" alt="industries" class="w100">
                </div>
            </div>
            <div class="col-md-6">
                <div class="precision card p-3 mt-5">
                    <h4>ERW Tube Process Sequence</h4>
                    <p>Goodluck India Group is a manufacturer and exporter of wide range of ERW Hot Dip Galvanized Pipes, Black Pipes, Black &amp; GI Hollow Sections, CR Coils, CRCA, Galvanized Plain &amp; Corrugated Sheets, ERW Precision &amp; CDW Tubes, Power &amp; Telecom Towers, Solar Structures, Forged Flanges and Custom Forgings. The company was established three decades ago.</p>
               
             <ul class="sequ">
                <li>Goodluck India Group </li>
                <li>Goodluck  Group </li>
                <li>Goodluck India  </li>
                <li>Manufacturer India Group </li>
             </ul>
             </div>
            </div>
        </div>
    </div>
</section>