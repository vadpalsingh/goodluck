

 
<?php include './inc/header.php'; ?>
<div class="contant">
 
<div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
           

                <div class="carousel-item  active">
                    <img src="../assets/forging/banner/mechin_shop.jpg" class="d-block w-100" alt="...">
                    <!-- <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>MACHINE SHOP</p>
                    </div> -->
                </div>
                
            </div>
          
        </div>
    </section>


 
<section class="Machine_Shop1 ptb">
   <div class="container">
    
    <!-- end -->
    <div class="row   align-items-center ">
    <div class="col-md-6">
            <div class="himage">
                <img src="../assets/forging/banner/Machining_p.jpg" alt="" class="w100 r_5">
            </div>
            </div>
        <div class="col-md-6">
            <div class="heat_treatment">
            <h2 class="s_csr">Fully State of the art Machine Shop</h2>
            <p class="mt-3">Driven by our commitment to continuous improvement and product development, our machining portfolio has significantly expanded since the inception of our company. As customer demand for machined parts grew, so did our capabilities. Presently, we boast a robust collection of over 90 CNC, VMC, and VTL machines, all equipped with 4-axis functionality. This comprehensive range of machining equipment enables us to cater to a wide spectrum of machining requirements, ensuring precise and efficient manufacturing processes.</p>
           <p>Complete Inhouse Machining Facility for roughing and finishing operations with CNC, VMC, VTL machining facilities.</p>
           <p>We can supply Forgings and Flanges based on Semi Finished or Fully Finished conditions</p>
       
        </div>
        </div>
        

    </div>
   </div>
</section>


<?php include './inc/footer.php'; ?>