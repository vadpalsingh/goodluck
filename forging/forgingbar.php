 
<?php include './inc/header.php'; ?>
<div class="contant">
<!-- <section class="brand__area1  title1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div class="sectiontitle1 mt-0 mb-3">
                    <h2>Forging</h2>
                    <span class="headerLine"></span>
             
                </div>
            </div>
        </div>
    </div>
    
</section> -->

<section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
           

                <div class="carousel-item  active">
                    <img src="../assets/forging/banner/forgingbar.jpg" class="d-block w-100" alt="...">
                    <!-- <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Forging</p>
                    </div> -->
                </div>
                
            </div>
          
        </div>
    </section>





<!-- <section class="ptb Forg_ing   ">
<div class="container">
<div class="row    ">
    
        <div class="col-md-12">
            <div class="forging_main">
        
            <div class="forgung w40">
            <div class="c_enterd">
            <span class="s_csr">Fully State of the art Machine Shop</span> -
            <p><b>Caters to Diversified material Grades right from Carbon, Alloy, Stainless, PH, Duplex and Super Duplex and Nickel Alloy Materials under one roof</b> </p>
          <ul class="forg_ung">
            <li>Customized Forgings upto 80 (2000mm)</li>
            <li>Single Pcs Forging upto 5 tons per Pcs</li>
          </ul>
            </div>
        </div>
        <div class="himage w60">
                <img src="../assets/forging/forg_p.jpg" alt="" class="w100 r_5">
            </div>
        </div>
        </div>
    </div>
</div>
</section> -->
<section class="flanges1 ptb">
   <div class="container">
    <div class="row row-design align-items-center">
        <div class="col-md-6 ">
            <div class="heat_treatment mt-4">
            <h2 class="s_csr mt-4">Forging </h2>
            <p class="mb-0 mt-4">Caters to Diversified material Grades right from Carbon, Alloy, Stainless, PH, Duplex and Super Duplex and Nickel Alloy Materials under one roof.</p>
            
            <div class="types mt-3">
          
         <div class="f_type text-dark"><b style="font-size:18px">Products:</b> 
            <ul class="csr_about">
          <li> Forged Bars / Shafts</li>
          <li>Hollow Bars</li>
          <li>Shape Forgings</li>
          <li>Blocks</li>
          <li>Body & Bonnets</li>
          <li>Cross Block, BOP Body, </li>
          </ul>
         </div>
          
         <div class="mt_gread mt-3">
                <ul>
                    <!-- <li class="production"><b>Product</b></li> -->
                    <li>
                        <div class="left_sp text-dark">Size Range</div>: 
                        <div class="right_sp"> 1 / 2” to 114” and upto 3 Mtr Rings   </div>
                    </li>
                    <li>
                        <div class="left_sp text-dark">Weight Range  </div> :
                        <div class="right_sp"> 1 kg to 20,000 Kgs   </div>
                    </li>

                    <li>
                        <div class="left_sp text-dark">Pressure Rating	  </div> :
                        <div class="right_sp"> #150 to 2500, also as per API 6A and more…  </div>
                    </li>
                    <li>
                        <div class="left_sp text-dark">Length  </div> :
                        <div class="right_sp"> upto 6 Mtr  </div>
                    </li>
                    
                   
                </ul>
                
            </div>
        </div>
        </div>
        </div>
        <div class="col-md-6">
            <div class="himage image-slider">
                <!-- <img src="../assets/forging/f1.png" alt="" class="w100 r_5" style="height: 500px;"> -->
                                        
                <div class="wow fadeInUp gallery-container" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    <img src="../assets/forging/product/forging_p1.png" alt="Quality1" class="r_5">
                    <img src="../assets/forging/product/forging_p2.png" alt="Quality2" class="r_5">
                    <img src="../assets/forging/product/forging_p3.png" alt="Quality3" class="r_5">
                    <img src="../assets/forging/product/forging_p4.png" alt="Quality4" class="r_5">
                   
                </div> 
                <!-- <img src="../assets/forging/forging_p.png" alt="" class="w100 r_5" style="height: 500px;"> -->
            </div>
            </div>

    </div>
    <!-- end -->
    
   </div>
</section>


</div>
<?php include './inc/footer.php';?>