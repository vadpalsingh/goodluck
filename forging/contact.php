 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.6.2/animate.min.css">


 <?php include './inc/header.php'; ?>
 <div class="contant">
     <!-- <section class="contact p-0  title1">
         <img src="../assets/forging/banner/contact_b.jpg" alt="" class="w100">

     </section> -->
   
     <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item  active ">
                        <img src="../assets/img/about/cont.png" class="d-block w-100" alt="...">
                        <h5>Forging</h5>
                   
                    </div>
                </div>

            </div>
        </section>
 
     <style>
     .contact_mail {
         /* background: #92bde7; */
         color: #485e74;
         line-height: 1.6;
         /* font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; */
         padding: 1em;
     }

     .container {
         max-width: 1170px;
         margin-left: auto;
         margin-right: auto;
         padding: 1em;
     }

     ul {
         list-style: none;
         padding: 0;
     }

     .brand {}

     .brand span {
         color: #fff;
     }

     .wrapper {
        box-shadow: 0 0 20px 0 rgb(72 94 116 / 13%);
    border-radius: 8px;
    overflow: hidden;
     }

     .wrapper>* {
         padding: 1em;
     }

     .company-info {
         background: #f4f4f4;
     }

     .address {
         font-size: 16px;
     }

     .company-info h3,
     .company-info ul {

         margin: 0 0 1rem 0;
     }

     .contact {
         background: #fff;
     }

     /* Form Styles */

     .contact form {
         display: grid;
         grid-template-columns: 1fr 1fr;
         grid-gap: 20px;
     }

     .icon {
         padding-top: 8px;
     }

     .contact form label {
         display: block;
     }

     .contact form p {
         margin: 0;
     }

     .contact form .full {
         grid-column: 1/3;
     }

     .contact form button,
     .contact form input,
     .contact form textarea {
         width: 100%;
         padding: 1em;
         border: 1px solid #c9e6ff;
         color: #000;
         font-weight: 700;
     }

     .contact form button {
         background: #b2b2b2;
         border: 0;
         text-transform: uppercase;
         cursor: pointer;
     }

     .contact form button:hover,
     .contact form butto:focus {
         background: #92bde7;
         color: #fff;
         outline: 0;
         transition: background-color 1.5s ease-out;
     }

     

     /* responsive css for larger screeens */
     </style>


<section class="contact_mail">
         <div class="container-fluid">
             <div class="wrapper animated bounceInLeft">
                 <div class="company-info">
                     <h3>Address :-</h3>
                     <ul>
                         <li>
                             <div class="deail mb-3">
                                 <div class="icon">
                                     <i class="fa fa-home"></i>
                                 </div>
                                 <div class="address">
                                     <b> Head Office:</b><br>
                                     Good Luck Engineering Co.
                                     166-167, 2nd Floor,
                                     Good Luck House,
                                     Nehru Nagar, Ambedkar Road,
                                     Ghaziabad, UP
                                     India - 201 001.
                                     Tel: +91 120 4196700

                                 </div>
                             </div>
                         </li>
                         <li>
                             <div class="deail">
                                 <div class="icon">
                                     <i class="fa fa-home"></i>
                                 </div>
                                 <div class="address">
                                     <b> Good Luck Engineering (Works) :</b><br>
                                     Khasra No. 2839, Gram Dhoom Manikpur,
                                     G.T. Road , Gautam Budh Nagar,
                                     Dadri, (U.P.) INDIA

                                 </div>
                             </div>
                         </li>
                         <!-- <li>
                    <div class="deail mt-3 mb-2">
                <div class="icon">
                    <i class="fa fa-envelope "></i>
                </div> 
                <div class="address">
               <b> More Information on:</b><br>
               <b>Web:</b> www.goodlucksteel.com
               <br>
               <b>Email:</b> Hccpl@goodlucksteel.com
                </div>
            </div>
                    </li> -->
                         <li>
                             <div class="deail mt-3 mb-2">
                                 <div class="icon">
                                     <i class="fa fa-phone "></i>
                                 </div>
                                 <div class="address">
                                     <b> Contact Us :</b><br>
                                     <b>08046043795 </b>


                                 </div>
                             </div>
                         </li>
                     </ul>
                 </div>
                 <div class="contact">
                     <h3>Contact Us</h3>
                     <form action="">
                         <p>
                             <label>Name</label>
                             <input type="text" name="name">
                         </p>
                         <p>
                             <label>Company</label>
                             <input type="text" name="company">
                         </p>
                         <p>
                             <label>Email Address</label>
                             <input type="email" name="email">
                         </p>
                         <p>
                             <label>Phone Number</label>
                             <input type="tel" name="phone">
                         </p>
                         <p class="full">
                             <label>Message</label>
                             <textarea name="Message" rows="5"></textarea>
                         </p>
                         <p class="full">
                             <button>Submit</button>
                         </p>
                     </form>
                 </div>
             </div>
         </div>
     </section>
     <section>
         <div class="contact_map">

             <div class="Info w50"><iframe
                     src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3500.8545326390777!2d77.4320974752269!3d28.664073982648084!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cf1c862260ad5%3A0x81d0ee38c2a27ddc!2sGoodluck%20India%20Limited!5e0!3m2!1sen!2sin!4v1690373115327!5m2!1sen!2sin"
                     width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy"
                     referrerpolicy="no-referrer-when-downgrade"></iframe>
             </div>
             <div class="Info w50">

                 <iframe
                     src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3503.775520197179!2d77.52833777522379!3d28.57650278662327!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sKhasra%20No.%202839%2C%20Gram%20Dhoom%20Manikpur%2C%20G.T.%20Road%20%2C%20Gautam%20Budh%20Nagar%2C%20Dadri%2C%20(U.P.)%20INDIA!5e0!3m2!1sen!2sin!4v1691477497664!5m2!1sen!2sin"
                     width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy"
                     referrerpolicy="no-referrer-when-downgrade"></iframe>
             </div>
         </div>
     </section>


 </div>
 <?php include './inc/footer.php';?>