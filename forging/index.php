<?php include './inc/header.php'; ?>


<main>
    <!-- hero -->

    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide banner__slider" data-bs-ride="carousel" data-pause="false">
            <div class="carousel-inner cdwtubesbanner">
                <div class="carousel-item active">
                    <img src="../assets/forging/banner/slide1.png" class="d-block w-100" alt="...">
                    <!-- <div class="carousel-caption d-none d-md-block c2 bg_tr">
                 <h3 class="h_forging">Slide1 Note Give Sir </h3>
                        <p class="m-0">Our manufacturing facility boasts of state-of-the-art equipment with both Open Die and Close Die capabilities.</p>
                    </div> -->
                </div>
                <div class="carousel-item">
                    <img src="../assets/forging/banner/slide2.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                 <h3 class="h_forging">Manufacturer of Open & Close Die Forgings    </h3>
                        <p class="m-0">Our manufacturing facility boasts of state-of-the-art equipment with both Open Die and Close Die capabilities.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="../assets/forging/banner/slide3.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                 <h3 class="h_forging">3000 Ton Press </h3>
                        <p class="m-0">3000 ton Press is equipped to forge wide range from Oil & Gas to Power Generation Partss.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="../assets/forging/banner/slide4.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                 <h3 class="h_forging">Industries  </h3>
                        <p class="m-0">Oil & Gas   |    Power    |    Chemical    |    Defence    |      Aerospace</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="../assets/forging/banner/slide5.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                       <h3 class="h_forging">We offer  </h3>
                        <p class="m-0">Shape Forging, Profile Forging, Forged Bars in Solid & Hollow, Forged Flanges, Spectacle Blind, SRN Nozzles, Custom Forgings as per drawings</p>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>
    <!-- hero -->


    <!-- about -->
    <section class="p60 abou_t">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="comman-head">
                        <h2 class="pb3">WELCOME TO FORGINGS </h2>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="g_bout">


                        <div class="home-about wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">
                            <blockquote>
                            Goodluck Engineering Co. Is a unit of the parent Goodluck India Ltd., specializing in Nickel Alloy, Stainless Steel, Duplex, Carbon and Alloy Steel Forging and Flanges. Our commitment to excellence and innovation sets us apart in the industry

                            </blockquote>
                            <p class="wow fadeInUp" data-wow-delay=".8s"
                                style="visibility: visible; animation-delay: 0.8s;">
                                
                                Our Unit, Goodluck Engineering Co., has evolved into a powerhouse of capabilities,
encompassing in-house Research & Development, state-of-the-art Forging,
Heat treatment and machining expertise.<br><br> We cater to diverse industries, including Oil & Gas, Power, Aerospace and Defense, Chemical, Food & Dairy 
among many.

                                  

                            </p>
                            <p class="wow fadeInUp" data-wow-delay=".9s"
                                style="visibility: visible; animation-delay: 0.8s;"> 
Our specialization now includes in Nickel alloy, Titanium and Aluminum forgings which sets us apart.</p>
                            <div class="read-more">
                                <a href="about.php">Know more</a>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-md-5">
                   <div class="forging">
                    <img src="../assets/forging/forging.jpg" alt="forginf" class="r_5">
                   </div>
                </div>
            </div>
        </div>
    </section>

    <!-- counter -->


 <!-- expites -->
 <section class="p60 ex_pites">
        <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle">
                        <h2>Application & Products</h2>
                        <span class="headerLine"></span>
                        
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-md-3">
                   
                        <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/oilgas.png" alt="">
                        <h3 class="exp_h">Oil & Gas</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Oil & Gas</h3></a>
                        </div>
                         </div>
                        
                </div>
                <div class="col-md-9">
                <div class="product_slider p-0 swiper-container">
                        <div class="swiper-wrapper">
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/f.png" alt="">
                        <h3 class="exp_h a_p">Flanges</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">Flanges</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/sc.png" alt="">
                        <h3 class="exp_h a_p">Stub-ends / Collors</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">Stub-ends / Collors</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/sb.png" alt="">
                        <h3 class="exp_h a_p">Spectacle Blind</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">Spectacle Blind</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/vs.png" alt="">
                        <h3 class="exp_h a_p">Valve / SRN Nozzle</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">Valve / SRN Nozzle</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/ts.png" alt="">
                        <h3 class="exp_h a_p">Tube Sheet</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">Tube Sheet</h3></a>
                        </div>
                         </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-md-3">
                     
                        <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/ol.png" alt="">
                        <h3 class="exp_h">Oil Field Service</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Oil Field Service</h3></a>
                        </div>
                         </div>
                        
                </div>
                <div class="col-md-9">
                <div class="product_slider p-0 swiper-container">
                        <div class="swiper-wrapper">
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/OilFieldService/ShapeForging.png" alt="">
                        <h3 class="exp_h a_p">Shape Forging</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">Shape Forging</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/OilFieldService/ForgedBody.png" alt="">
                        <h3 class="exp_h a_p ">Forged Body</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">Forged Body</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/OilFieldService/ForgedBlock.png" alt="">
                        <h3 class="exp_h a_p">Forged Block</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">Forged Block</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/OilFieldService/ForgedBars.png" alt="">
                        <h3 class="exp_h a_p">Forged Bars</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">Forged Bars</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/OilFieldService/body.png" alt="">
                        <h3 class="exp_h a_p">body</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">body</h3></a>
                        </div>
                         </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                 
            </div>
            <div class="row mb-3">
                <div class="col-md-3">
                    
                        <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/e.png" alt="">
                        <h3 class="exp_h">Energy</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Energy</h3></a>
                        </div>
                         </div>
                      
                </div>
                <div class="col-md-9">
                <div class="product_slider p-0 swiper-container">
                        <div class="swiper-wrapper">
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/energy/ICV.png" alt="">
                        <h3 class="exp_h a_p">ICV</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">ICV</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/energy/MSVValve.png" alt="">
                        <h3 class="exp_h a_p ">MSV Valve</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">MSV Valve</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/energy/NonStandard.png" alt="">
                        <h3 class="exp_h a_p">Non Standard</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">Non Standard</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/energy/Strainer.png" alt="">
                        <h3 class="exp_h a_p">Strainer</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">Strainer</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/energy/ValveForging.png" alt="">
                        <h3 class="exp_h a_p">Valve Forging</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">Valve Forging</h3></a>
                        </div>
                         </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-md-3">
                  
                        <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/aro.png" alt="">
                        <h3 class="exp_h">Defense and Aerospace	</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Defense and Aerospace	</h3></a>
                        </div>
                         </div>
                       
                </div>
                <div class="col-md-9">
                <div class="product_slider p-0 swiper-container">
                        <div class="swiper-wrapper">
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/aro/df1.png" alt="">
                       
                        </div>
                        
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/aro/df2.png" alt="">
                        
                        </div>
                        
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/aro/df3.png" alt="">
                       
                        </div>
                        
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/aro/df4.png" alt="">
                         
                        </div>
                       
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/aro/df5.png" alt="">
                        
                        </div>
                        
                         </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->
            <div class="row mb-3">
                <div class="col-md-3">
                  
                        <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/food.png" alt="">
                        <h3 class="exp_h">Chemical, Mining, Food and Dairy	</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Chemical, Mining, Food and Dairy	</h3></a>
                        </div>
                         </div>
                       
                </div>
                <div class="col-md-9">
                <div class="product_slider p-0 swiper-container">
                        <div class="swiper-wrapper">
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/oil_product/flush_ring.png" alt="">
                        <h3 class="exp_h a_p">FLUSH RING, NUT-NPT</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">FLUSH RING,  NUT-NPT</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/oil_product/nipple.png" alt="">
                        <h3 class="exp_h a_p ">NIPPLE  </h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">NIPPLE</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/oil_product/taper_u.png" alt="">
                        <h3 class="exp_h a_p">TAPER UNION, NUT</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">TAPER UNION, NUT </h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/oil_product/assmbley.png" alt="">
                        <h3 class="exp_h a_p">ASSEMBLY</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">ASSEMBLY</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/oil_product/felang.png" alt="">
                        <h3 class="exp_h a_p">FLANGE</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">FLANGE</h3></a>
                        </div>
                         </div>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                            <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/forging/product/oil_product/h_fleng.png" alt="">
                        <h3 class="exp_h a_p">HYGIENE FLANGE</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h a_p">HYGIENE FLANGE</h3></a>
                        </div>
                         </div>
                            </div>

                          




                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

       </section>
    <!-- expites -->

  

 
 
   

    <!-- brand__area start -->
    <section class="brand__area1 p60" style="    background-color: #fdfdfd;">
        <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle">
                        <h2>Approval</h2>
                        <span class="headerLine"></span>
                      
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="brand__slider p-0 swiper-container">
                        <div class="swiper-wrapper">
                            <div class=" approvial swiper-slide">
                                <a href="#"><img src="../assets/forging/brand/1.png" alt=""></a>
                            </div>
                            <div class="approvial  swiper-slide">
                                <a href="#"><img src="../assets/forging/brand/2.png" alt=""></a>
                            </div>
                            <div class="approvial  swiper-slide">
                                <a href="#"><img src="../assets/forging/brand/3.png" alt=""></a>
                            </div>
                            <div class="approvial swiper-slide">
                                <a href="#"><img src="../assets/forging/brand/4.png" alt=""></a>
                            </div>
                            <div class="approvial swiper-slide">
                                <a href="#"><img src="../assets/forging/brand/5.png" alt=""></a>
                            </div>
                            <div class="approvial swiper-slide">
                                <a href="#"><img src="../assets/forging/brand/6.png" alt=""></a>
                            </div>
                            <div class=" approvial swiper-slide">
                                <a href="#"><img src="../assets/forging/brand/7.png" alt=""></a>
                            </div>
                            <div class="approvial swiper-slide">
                                <a href="#"><img src="../assets/forging/brand/8.png" alt=""></a>
                            </div>
                            <div class="approvial swiper-slide">
                                <a href="#"><img src="../assets/forging/brand/9.png" alt=""></a>
                            </div>
                            <div class="approvial swiper-slide">
                                <a href="#"><img src="../assets/forging/brand/10.png" alt=""></a>
                            </div>
                            <div class=" approvial swiper-slide">
                                <a href="#"><img src="../assets/forging/brand/11.png" alt=""></a>
                            </div>
                            <div class="approvial swiper-slide">
                                <a href="#"><img src="../assets/forging/brand/12.png" alt=""></a>
                            </div>
                            <div class="approvial  swiper-slide">
                                <a href="#"><img src="../assets/forging/brand/13.png" alt=""></a>
                            </div>
                            <div class="approvial swiper-slide">
                                <a href="#"><img src="../assets/forging/brand/14.png" alt=""></a>
                            </div>
                            <div class="approvial  swiper-slide">
                                <a href="#"><img src="../assets/forging/brand/15.png" alt=""></a>
                            </div>
                           
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- brand__area end -->
   
</main>



<?php include './inc/footer.php';?>
