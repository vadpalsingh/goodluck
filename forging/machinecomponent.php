 
<?php include './inc/header.php'; ?>
<div class="contant">
<section class="brand__area1  title1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div class="sectiontitle1 mt-0 mb-3">
                    <h2>Machine Component</h2>
                    <span class="headerLine"></span>
             
                </div>
            </div>
        </div>
    </div>
     
</section>
<section class="flanges">
   <div class="container">
    <div class="row align-items-center">
        <div class="col-md-6  ">
            <div class="heat_treatment mt-4">
            <span class="s_csr mt-4"><b>Condition:</b> <br>As Forged, Heat Treated and Rough Turned </span>
             
            <div class="types mt-3">
            <ul class="m_comp">
          <li>Size Range – upto 24” (upto 600mm)</li>
          <li>Length – 1 Mtr upto 4.5 Mtrs</li>
          <li>Heat Treatment – Solution Annealed, Normalized, Q&T, H&T, PH related Heat Treatments</li>
          <li>Cut to Length solutions can be provided below 1 Mtr</li>
   
          </ul>
        </div>
        </div>
        </div>
        <div class="col-md-6">
            <div class="himage">
                <img src="../assets/forging/mc_p.png" alt="" class="w100 r_5"  >
            </div>
            </div>

    </div>
    <!-- end -->
    
   </div>
</section>
</div>
<?php include './inc/footer.php';?>