
<!doctype html>
         <html class="no-js" lang="zxx">
            
         <head>
               <meta charset="utf-8">
               <meta http-equiv="x-ua-compatible" content="ie=edge">
               <title>  Goodluckindia  </title>
               <meta name="description" content="">
               <meta name="viewport" content="width=device-width, initial-scale=1">
         
               <!-- Place favicon.ico in the root directory -->
               <link rel="shortcut icon" type="image/x-icon" href="../assets/favicon.jpg">
         
               <!-- CSS here -->
               <!-- <link rel="stylesheet" href="../assets/css/bootstrap.css"> -->
               <link rel="stylesheet" href="../assets/css/bootstrapmin.css">
               
               <link rel="stylesheet" href="../assets/css/meanmenu.css">
               <link rel="stylesheet" href="../assets/css/animate.css">
               <link rel="stylesheet" href="../assets/css/owl-carousel.css">
               <link rel="stylesheet" href="../assets/css/swiper-bundle.css">
               <link rel="stylesheet" href="../assets/css/backtotop.css">
               <link rel="stylesheet" href="../assets/css/magnific-popup.css">
               <link rel="stylesheet" href="../assets/css/nice-select.css">
               <link rel="stylesheet" href="../assets/css/flaticon.css">
               <link rel="stylesheet" href="../assets/css/font-awesome-pro.css">
               <link rel="stylesheet" href="../assets/css/spacing.css">
               <link rel="stylesheet" href="../assets/css/style.css">
               <link rel="stylesheet" href="../assets/css/custom.css">
               <script>
                  function myFunction() {
                    var x = document.getElementById("myVideo").autoplay;   
                  }
                  </script>
                  <style>
   

</style>
            </head>
            <body>
               <!-- Preloader -->
               <!-- <div class="preloader"></div> -->
               <!-- pre loader area end -->
               <!-- back to top start -->
               <div class="progress-wrap">
                  <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
                     <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
                  </svg>
               </div>
               <!-- back to top end -->
               
               <!-- header-area-start -->
               <header id="header-sticky" class="header-area">
                  <div class="container-fluid">
                        <div class="row align-items-center">
                           <div class="col-xl-4 col-lg-2 col-md-6 col-8">
                              <div class="logo-area">
                                    <div class="logo" style="    display: flex;
    align-items: center;">
                                       <a href="../index.php"><img src="../assets/img/logo/logo-white.png" alt=""></a>
                                       <h3 class="h_for" style=""><a href="index.php">GoodLuck Engineering<br>(Forging Division)</a></h3>
                                    </div>
                              </div>
                           </div>
                           <div class="col-xl-7 col-lg-8 col-md-6 col-4">
                              <div class="menu-area menu-padding">
                                    <div class="main-menu text-center">
                                       <nav id="mobile-menu">
                                          <ul>
                                             <li class="nav-item  "> <a class="nav-link  " href="index.php">Home</a>  </li>
                                             <li class="nav-item dropdown">
                           <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                           About us
                           </a>
                           <ul class="dropdown-menu">
                        
                              <li><a class="dropdown-item" href="industries.php">Industries</a></li>
                     
                              <li><a class="dropdown-item" href="approval.php">Approvals </a></li>
                           
                              <!-- <li><a class="dropdown-item" href="#">Corporate Video </a></li> -->
                           </ul>
                        </li>
                                             <li class="nav-item dropdown">
                           <a class="nav-link dropdown-toggle" href="infrastructure.php" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                           Infrastructure
                           </a>
                           <ul class="dropdown-menu">
                              <li><a class="dropdown-item" href="flangescapabilities.php">FORGING PLANT</a></li>
                              <li><a class="dropdown-item" href="heateatment.php">Heat Treatment</a></li>
                              <li><a class="dropdown-item" href="machine.php">Machining </a></li>
                           </ul>
                        </li>
                                             <li class="nav-item dropdown">
                           <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                           Products
                           </a>
                           <ul class="dropdown-menu">
                              <li><a class="dropdown-item" href="rowmetrial.php"> MATERIAL GRADES</a></li>
                              <li><a class="dropdown-item" href="forgingbar.php">Forging</a></li>
                              <li><a class="dropdown-item" href="flanges.php">Flanges</a></li>
                          
                              <li><a class="dropdown-item" href="packing.php">Packaging</a></li>
                           </ul>
                        </li>
                          

                        <li > <a  href="qualitylab.php">Quality Lab</a>  </li>
                        <li class="nav-item dropdown">
                           <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                           Download
                           </a>
                           <ul class="dropdown-menu">
                           <li><a class="dropdown-item" href="../assets/forging/certification/GLEECatalogue.pdf" download>E-Catalogue</a></li>
                              <li><a class="dropdown-item" href="certification.php" >Certificate</a></li>
                              <li><a class="dropdown-item" href="https://www.youtube.com/watch?v=RCqTgphaT8g" >Corporate Video</a></li>
                          
                           </ul>
                        </li>
                        <li > <a  href="contact.php">Contact</a>  </li>
                                          </ul>
                                       </nav>
                                    </div>
                              </div>
                              <div class="side-menu-icon d-lg-none text-end">
                                 <a href="javascript:void(0)" class="info-toggle-btn f-right sidebar-toggle-btn"><img src="../assets/img/humberger-icon.svg" alt=""></a>
                              </div>
                           </div>
                           <div class="col-xl-1 col-lg-1 d-none d-lg-block">
                              <div class="header__sm-action">
                                 <div class="header__sm-action-item right-border1">
                                    <a href="javascript:void(0)" class="info-toggle-btn f-right sidebar-toggle-btn"><img src="../assets/img/humberger-icon.svg" alt=""></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                  </div>
               </header>
               <!-- header-area-end -->
                   <!-- sidebar area start -->
                   <div class="sidebar__area">
                  <div class="sidebar__wrapper">
                     <div class="sidebar__close">
                        <button class="sidebar__close-btn" id="sidebar__close-btn">
                           <i class="fal fa-times"></i>
                        </button>
                     </div>
                     <div class="sidebar__content">
                        <div class="sidebar__logo  mb-2">
                           <a href="index.html">
                           <img src="../assets/img/logo/logo-black.png" alt="logo">
                           </a>
                        </div>
                        <div class="mobile-menu fix"></div>
                        <div class="sidebar__contact mt-0 mb-20">
                           <!-- <h4>Contact Info</h4> -->
                           <ul>
                           
                              <li class="d-flex align-items-center">
                              <a href="../index.php">GoodLuck Home</a>
                              </li>
                              <li class="d-flex align-items-center">
                               <a href="#">Corporate Video</a>
                              </li>
                              <li class="d-flex align-items-center">
                                 <a href="#">Brochure</a>
                              </li>
                            
                             
                              <li class="d-flex align-items-center">
                                 <a href="contact.php">Contact Us</a>
                              </li>
                           </ul>
                        </div>
                        <div class="sidebar__social">
                           <ul>
                              <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                              <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                              <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                              <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- sidebar area end -->     
               <div class="body-overlay"></div>
               <!-- sidebar area end --> 
          
         