 
<?php include './inc/header.php'; ?>
<div class="contant">
 
<section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
           

                <div class="carousel-item  active">
                    <img src="../assets/forging/banner/slide2.jpg" class="d-block w-100" alt="...">
                    <!-- <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Heat Treatment</p>
                    </div> -->
                </div>
                
            </div>
          
        </div>
    </section>

 
<section class=" ptb">
   <div class="container">
    <div class="row align-items-center">
        <div class="col-md-12">
            <div class="heat_treatment mb-4 wow fadeInUp mb-2" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.3s;">
            <h2>Adopting Goodluck’s 4 Pillars of sustainable</h2>
            <p class="justify mt-3">Heat Treatment is a vital process that requires precise control of temperature and homogeneity, so by considering these key points, our facility is well equipped with Electric and Gas operated Reheating and Heat Treatment Furnaces. The Furnaces are capable of Normalizing, Hardening, Tempering, Annealing, Solution Annealing and Hydrogen Flanking. GLE has the capabilities to Heat Treat all the major alloys from Ferrous to Non-Ferrous materials including the likes of Drop Bottom Furnace for Aluminum. </p>
          <div><b>Heat Treatment facilities with Oil and Water Quenching Facility</b></div>
            
        </div>
        </div>
        <div class="col-md-6">
            <div class="himage">
                <img src="../assets/forging/h1.png" alt="" class="w100 r_5 wow fadeInUp mb-2" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s;">
            </div>
            </div>
            <div class="col-md-6">
            <div class="himage">
                <img src="../assets/forging/ht2.png" alt="" class="w100 r_5 wow fadeInUp mb-2" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s;">
            </div>
            </div>

    </div>
    <!-- end -->
    
   </div>
</section>
</div>
<?php include './inc/footer.php';?>