 
<?php include './inc/header.php'; ?>
<div class="contant">
<section class="brand__area1  title1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div class="sectiontitle1 mt-0 mb-3">
                    <h2>About Us</h2>
                    <span class="headerLine"></span>
             
                </div>
            </div>
        </div>
    </div>
    
</section>
<section class="ptb">
    <div class="container">
        <div class="row">
           <div class="col-md-6">
           <h2>About Us</h2>
<p>Goodluck Engineering Co. Is a group company of the parent Goodluck India Ltd. specialising in stainless steel, duplex, carbon and Alloy steel customised forging and flanges. Our commitment to excellence and innovation sets us apart in the industry
<br><br>
Our Unit, Goodluck Engineering Co., has evolved into a powerhouse of capabilities, encompassing in-house Research & Development, state-of-the-art machining, heat treatment, and forging expertise. We cater to diverse industries, including Oil & Gas, Aerospace and Defense, Chemical, Food & Dairy, Power, among others. Our specialization in handling stainless steel, duplex, carbon & alloy steel, as well as nickel alloy, titanium, and aluminum forgings, sets us apart.<br> <br>
In 2007, our CEO, Mr. Shyam Aggarwal, recognized the essential role of forged products in machinery and plants, leading to the inception of Goodluck Engineering Co. as a sub-division within Goodluck India. Today, Goodluck Engineering Co. proudly stands as one of the leading manufacturers of Open and Closed Die Ferrous & Non-Ferrous Forgings, holding ISO 9001:2008 & AS 9100 D certifications. <br>

</p>

         </div>
           <div class="col-md-6">
                <div class="csrimg">
                    <img src="http://www.delhibusiness.in/design/goodluckindia/final/final/assets/cwdpipe/clean/c1.png" alt="" class="r_5">
                </div>
           </div>
           <div class="col-md-12">
            
           <p>   <b>Our company’s mission is to be :- </b><br>
            “To be a leading global forging company, delivering exceptional value to our customers through innovative solutions, superior quality products, and reliable services. We strive to be the partner of choice for industries seeking high-performance forgings, while fostering a culture of integrity, collaboration, and continuous improvement.” <br>
 
           </p>
           <p>With an Annual installed capacity of 42,000 Tons of Forgings, we are exporting our products to more than 80 Countries Worldwide. We specialize in production of Forgings and Flanges in special Alloys in PH, Duplex, Inconel and Hi Nickel Alloys</p>
           <h3>Why GOODLUCK</h3>
              <p>Our Business Principal is based on customer orientation in our product’s development and service</p>
              <ul class="csr_about">
                <li>Customer Perspective – Our Priority</li>
                <li>Consistent Quality Product</li>
                <li>Cost Effective Product</li>
                <li>One stop shop for wide range of products</li>
                <li>Commitment in delivery</li>
                <li>Value Added Customer Service</li>
                </ul>
                <p>Strong understanding of Techno - Commercial requirements of our Strategic Partners regarding their end application.</p>
           
           </div>
        </div>
    </div>
</section>
</div>
<?php include './inc/footer.php';?>