 <?php include './inc/header.php'; ?>
 <div class="contant">
     
     <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
           

                <div class="carousel-item  active">
                    <img src="../assets/forging/banner/forging_p.jpg" class="d-block w-100" alt="...">
                    <!-- <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Forging Plant</p>
                    </div> -->
                </div>
                
            </div>
          
        </div>
    </section>
     
     <section class="ptb  ">
         <div class="container">

             <!-- end -->
             <div class="row row-reverse align-items-center mb-5">
                 <div class="col-md-6">
                     <div class="himage">
                         <img src="../assets/forging/forging.jpg" alt="q1.png" class="w100 r_5">
                     </div>
                 </div>
                 <div class="col-md-6 mb-4">
                     <div class="heat_treatment  mt-4">
                         <h2 class="s_csr mt-4">Forging Plant </h2>
                         <p class="mb-0 mt-4"><b>With Total capacity of 42,000 Tons per annum, our facility is fully
                                 equipped to manufacture a complete range of Grades in Nickel Alloys, Stainless, Duplex,
                                 Carbon and Alloy Steel. </b>
                         </p>
                         <p class="mt-4">Our manufacturing facility boasts of state of the art equipments with comprehensive & fully
                             Integrated cutting facility, Forging Pneumatic machine & Press unit. This offers the
                             flexibility in both, type & volume of production. The Pneumatic machine & Press units are
                             regularly modernized to keep with the production needs & keeping the plant energy
                             efficient. We boast of one of the complete in-house Forging capabilities covering both,
                             Open and Close Die machines, where we can make a forgings ranging from 1 kg to 20,000 kgs.
                             The equipment's facilities includes,
                         </p>
                     </div>
                 </div>
             </div>
             <!-- end -->
             <div class="row row-reverse align-items-center mt-5 mb-5">
                
                 <div class="col-md-6">
                     <div class="heat_treatment mt-4">
                         <ul class=" for_gbar  mb-5">
                             <li><span>	&#10029;</span>3 Open Die Hydraulic Machine of 10 Tonne each. </li>
                             <li><span>	&#10029;</span>6 Tonne, 3 Tonne and 1.5 Tonne Close Die Pneumatic Machine. </li>
                             <li><span>	&#10029;</span>650 Tonne Close Die Forging Press for Customized Parts. </li>
                             <li><span>	&#10029;</span>3000 Ton Open Die Hydraulic Press integrated with 25Ton rail bound manipulator and 10T
                                 Tire bound charger </li>
                             <li><span>	&#10029;</span>We have 15 running cranes </li>

                         </ul>
                     </div>
                 </div>
                 <div class="col-md-6">
                     <div class="himage">
                         <img src="../assets/forging/industry.png" alt="q1.png" class="w100 r_5">
                     </div>
                 </div>
             </div>

         </div>
     </section>
 </div>
 <?php include './inc/footer.php';?>