 
<?php include './inc/header.php'; ?>
<div class="contant">
<section class="brand__area1  title1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div class="sectiontitle1 mt-0 mb-3">
                    <h2>CSR WITH</h2>
                    <span class="headerLine"></span>
             
                </div>
            </div>
        </div>
    </div>
    
</section>
<section class="ptb">
    <div class="container">
        <div class="row">
           <div class="col-md-6">
                <span class="s_csr">Adopting Goodluck’s 4 Pillars of sustainable</span><br> practices is essential to address the pressing global challenges of
                climate change, resource depletion, and social inequality, and to create a more just and sustainable future for people
                  and the planet.The four pillars are:
                <ul class="csr_about">
                <li>Net to near shape, generating less usage and wastage</li>
                <li>Scrap purchase benefits to be passed</li>
                <li>Social equality and justice across group companies</li>
                <li>Use of Natural resources in plant</li>
                </ul>
                <p>Clean Energy and Climate Action has never been more urgent than ever before. Goodluck as a Group is
                striving to bringing new means for long term sustainable solutions in manufacturing locations.</p>
                <p><b>Solar :</b> In process of installing Solar panel on the rooftop giving a long term sustainable solution for lightning
                for plants and offices.</p>
                <p><b>Water :</b> With abundance of rain, rainwater harvesting is one of the most important solutions to the water issue
                of the future particularly in the developing world.</p>
           </div>
           <div class="col-md-6">
                <div class="csrimg">
                    <img src="http://www.delhibusiness.in/design/goodluckindia/final/final/assets/cwdpipe/clean/c1.png" alt="" class="r_5">
                </div>
           </div>
           <div class="col-md-12">
           
                <p>Climate Neutral Design of Offices have been completed for use of more natural lights and increasing green
                space across all Factories.</p>
                <p><b>Sustainability :</b> Working with EcoVadis to manage risks, reduce costs, and drive innovation and new revenue.</p>
           </div>
        </div>
    </div>
</section>
</div>
<?php include './inc/footer.php';?>