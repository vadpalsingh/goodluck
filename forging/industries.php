<?php include './inc/header.php'; ?>
<div class="contant">
    
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
           

                <div class="carousel-item  active">
                    <img src="../assets/forging/banner/indu.jpg" class="d-block w-100" alt="...">
                    <!-- <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Industries</p>
                    </div> -->
                </div>
                
            </div>
          
        </div>
    </section>

    

    <section class="ptb" style="">
        <div class="container">

        <div class="row row-design justify-content-center align-items-center"> 	
	        <div class="col-lg-6 col-md-6 image-slider">                        
                <div class="wow fadeInUp gallery-container" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    <img src="../assets/forging/ind/industris.png" alt="Quality">
                    <img src="../assets/forging/ind/plant1.png" alt="Quality">
                    <img src="../assets/forging/ind/power.png" alt="Quality">
                    <img src="../assets/forging/ind/tank.png" alt="Quality">
                    <img src="../assets/forging/ind/plant2.png" alt="Quality">
                </div>              
			</div>
            <div class="col-lg-6 col-md-6">
			    <div class="wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">					
                   <h2>In Focus</h2>                       
                   <p class="text-dark mt-3">Forging is a very versatile process and the Forging Parts play a vital role in high pressure and long lasting products.   </p>
                      
                   <div class="indes mt-3">
                                <div class="oil_gas">
                                    <div class="plant_n">
                                    Oil & Gas
                                    </div>:
                                    <div class="plant_used">
                                    Upstream    |     Midstream  |   Downstream activities
                                        </div>
                                </div>
                                <div class="oil_gas">
                                    <div class="plant_n">
                                    Power Generation
                                    </div>:
                                    <div class="plant_used">
                                    Upstream    |     Midstream  |   Downstream activities
                                        </div>
                                </div>
                            </div>
                            <div class="indes mt-4">
                                <!-- <h4 class="mb-0 ">Defence & Aerospace</h4> -->
                                <ul class="ind_us">
                                    <li>Defence & Aerospace</li>
                                    <li>Chemical</li>
                                    <li>Food & Dairy</li>
                                    <li>Mining, Marine, Paper & Pulp and many more… </li>
                                </ul>
                                <p class="text-dark mt-3">We are now entering into the cleaner Energy areas like Hydrogen and Geo-Thermal.</p>
                            </div>
                               


                </div>
			</div> 			
		 </div>

            
        </div>
</div>
</div>
</section>
</div>
<?php include './inc/footer.php'; ?>