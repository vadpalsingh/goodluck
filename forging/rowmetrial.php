 
<?php include './inc/header.php'; ?>
<div class="contant">
 
<section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
           

                <div class="carousel-item  active">
                    <img src="../assets/forging/banner/met_g.jpg" class="d-block w-100" alt="...">
                    <!-- <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>MATERIAL GRADES</p>
                    </div> -->
                </div>
                
            </div>
          
        </div>
    </section>
<section class="ptb ">
   <div class="container">
    <div class="row">
        
            <div class="col-md-12">
            <div class="row_metrial  p-4" style="    background-color: #fbfbfb66;">
            <p class=" mt_b  justify">We have a comprehensive & dedicated Raw Material selection and storage. All the raw materials for our Forgings are only sourced from recognized and approved mills. All Forgings are supplied with the Material Test Certificate and every material is tested in our Laboratory, Which is certified as per ISO 17025 and to ensure it is conforming to the Standard Material Chemical Composition and Mechanical properties to meet the consumer demands. The company has been able to develop a comprehensive package to manufacture a complete range of Grades in Nickel Alloys, Super Duplex, Stainless, Carbon and Alloy Steel and in more recent years in Titanium and Aluminium. </p>
            <div class="mt_gread">
                <ul>
                    <li class="production"><b>More than 100 Grades in Production</b></li>
                    <li>
                        <div class="left_sp">Stainless Steel</div>: 
                        <div class="right_sp"> 304/304L, 316/316L, 316Ti, 321, 303, 403, 410, 420 A/B/C, 430F, 416, 431,  347/347H,   <br>317L, 310, 904L…. & more  
                        <br>
                        * All Grades in EN Specs as well
                        </div>
                    </li>
                    <li>
                        <div class="left_sp">Duplex & Nickel Alloys </div> :
                        <div class="right_sp"> 2205 / F51 / F60, F53, F55, 1.4462, 254 SMO
                            Alloy 625, 718, 800H/HT, 825, Alloy 20, Alloy 22, 
                            Alloy C276 & Alloy 400   
                        <br>
                        * based on UNS Std
                        </div>
                    </li>
                    <li>
                        <div class="left_sp">Titanium | Aluminum </div> :
                        <div class="right_sp"> Ti-6AL-4V, Ti-6AL-1.5Mo-1.5V  |  AL Alloy 2014, 7075 
                       
                        </div>
                    </li>
                    <li>
                        <div class="left_sp">Zirconium Copper </div> :
                        <div class="right_sp"> C15000, C15100 & more… 
                       
                        </div>
                    </li>
                    <li>
                        <div class="left_sp"> High Temperature | Low Alloy </div> :
                        <div class="right_sp"> 17-4PH, F22, F12, A182 F91, F92 </div>
                    </li>
                    <li>
                        <div class="left_sp">Carbon Steel & Alloy Steel Grades </div> :
                        <div class="right_sp"> A105, P250GH, SA266, LF2, LF3, EN8, EN9, EN26, 4130, 4340, 4140, 8620, 8630, A 182 F1, F5, F9, F11, F6NM, 42CrMo4, 16Mo3, 25CrMo4 & more….    
                        <br>
                       <b>Speciality in :-</b><br>
                       A694 F52, F60, F65 & 15CDV6
                        </div>
                    </li>
                   
                </ul>
                <div class="mt-3">
                    <p><b>Specifications:-</b></p>
                    <p>As per ASTM, EN, DIN, JIS, BS, ASME, NACE, QQS, AFNOR and more…</p>
                </div>
            </div>
            

            </div>
            </div>

    </div>
    <!-- end -->
    
   </div>
</section>
</div>
<?php include './inc/footer.php';?> 