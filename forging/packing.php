 
<?php include './inc/header.php'; ?>
<div class="contant">
<section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
           

                <div class="carousel-item  active">
                    <img src="../assets/forging/banner/pack_ing.jpg" class="d-block w-100" alt="...">
                    <h5>Packing</h5>
                </div>
                
            </div>
          
        </div>
    </section>
<section class="packing1 ptb">
   <div class="container">
   <div class="row">
        <div class="col-md-6">
            <div class="heat_treatment">
            <!-- <span class="s_csr">Packaging</span> -->
            <h2>Packaging</h2>
            <p>Packaging has a very pivotal role to ensure that the material with stands the transit and reaches the client in good condition. </p>
         
            <ul class="csr_about pack">
          <li>All Forged Flanges are packed in Euro Pallets  </li>
          <li>Seaworthy export packing bundles for Forged Bars</li>
          <li>All loose cargo shipments (LCL) are shipped in wooden crates</li>
          <li>All Wooden crates and blocks are well fumigated as per specific country  requirements </li>
          <li>All weather proof labels used in each bundles</li>
          </ul>
        </div>
        </div>
        <div class="col-md-6">
            <div class="himage  image-slider">
                <!-- <img src="../assets/forging/p1.png" alt="" class="w100 r_5"> -->
                                  
                <div class="wow fadeInUp gallery-container" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    <img src="../assets/forging/ind/pack1.png" alt="Quality" class="r_5">
                    <img src="../assets/forging/ind/pack2.png" alt="Quality" class="r_5">
                    <img src="../assets/forging/ind/pack3.png" alt="Quality" class="r_5">
                </div>              
            </div>
            </div>

    </div>
    <!-- end -->
    
   </div>
</section>
</div>
<?php include './inc/footer.php';?>