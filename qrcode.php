
<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India </title>
    <meta name="description" content="">

<?php include './inc/investorheader.php';?>
 
    <section class="  inves_tor">
        <div class="logo_t" style="display: flex;
    justify-content: space-between;padding-bottom: 5rem;    margin-bottom: 6%;
    padding: 0 2%;">
            <img src="./assets/img/about/q_rigjt.png" alt="" class="w100" style="    width: 20%;">
            <img src="./assets/img/about/qr_left.png" alt="" class="w100" style="    width: 20%;">
        </div>
        <div class="container-fluid">
            <div class="row justify-content-center align-items-center">

                <h2 class="invstor mt-5 mb-5">Investors Relation  </h2>
                <!-- Product Tabes -->
                <ul class="nav in_tab nav-pills  justify-content-center" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link  bg_trans" id="pills-one-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-one" type="button" role="tab" aria-controls="pills-one"
                            aria-selected="true"><span> Investors Presentation</span><img src="assets/img/investor/1.png"
                                alt=""></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link bg_trans" id="pills-two-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-two" type="button" role="tab" aria-controls="pills-two"
                            aria-selected="false"><span>Financial Performance</span><img src="assets/img/investor/2.png"
                                alt=""></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link bg_trans" id="pills-two-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-three" type="button" role="tab" aria-controls="pills-two"
                            aria-selected="false"><span>Annual Report</span><img src="assets/img/investor/3.png"
                                alt=""></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link bg_trans" id="pills-two-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-four" type="button" role="tab" aria-controls="pills-two"
                            aria-selected="false"><span> Shareholding Pattern</span><img src="assets/img/investor/4.png"
                                alt=""></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <!-- <button class="nav-link bg_trans" id="pills-two-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-five" type="button" role="tab" aria-controls="pills-two"
                            aria-selected="false"></button> -->
                            <a href="" class="bg_trans" style="margin-top: 8px;
    display: inline-block;
    text-align: center;"><span>  Our Website</span><img src="assets/img/investor/1.png"
                                alt="" style="    width: 61%;"></a>
                    </li>
                </ul>
                <div class="tab-content mb-5" id="pills-tabContent">
                    <div class="tab-pane fade  " id="pills-one" role="tabpanel"
                        aria-labelledby="pills-one-tab">
                        <div class="row">
                            <!-- client Start -->
                            <div class="col-md-12">
                                <div class="cloent">
                                    <div class="ins-det">
                                        <div class="dn-info b_bone"><a
                                                href="pdf/investor-presentation-q1-fy24.pdf"
                                                target="_new"><i class="fa fa-file-pdf-o"></i> Investor Presentation Q1 FY 2023-24</a></div>

                                      

                                    </div>
                                </div>
                            </div>
                            <!-- client End -->
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-two" role="tabpanel" aria-labelledby="pills-two-tab">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="cloent">
                                    <!-- domistck -->
                                    <div class="ins-det">
                                        <div class="dn-info b_bone"><a
                                                href="pdf/q1-result-2023.pdf"
                                                target="_new"><i class="fa fa-file-pdf-o"></i> Financial Performance –   Q1 FY 2023-24 </a>
                                            </div>
                                            <div class="dn-info b_bone"><a
                                                href="pdf/q4-result-2023.pdf"
                                                target="_new"><i class="fa fa-file-pdf-o"></i> Financial Performance -    Q4 FY 2022-23 </a>
                                            </div>
                                            <div class="dn-info b_bone"><a
                                                href="pdf/q3-result-2022.pdf"
                                                target="_new"><i class="fa fa-file-pdf-o"></i> Financial Performance -    Q3 FY 2022-23 </a>
                                            </div>
                                            

                                       

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="coo">
                                <div class="dn-info b_bone">
                                    <a  href="pdf/q2-result-2022.pdf"
                                                target="_new"><i class="fa fa-file-pdf-o"></i> Financial Performance -    Q2 FY 2022-23 </a>
                                            </div>
                                            <div class="dn-info b_bone"><a
                                                href="pdf/q1-result-2022.pdf"
                                                target="_new"><i class="fa fa-file-pdf-o"></i> Financial Performance -    Q1 FY 2022-23</a>
                                            </div>

                                        </div>
                            </div>

                        </div>
                    </div>
                    <!--end  -->
                    <div class="tab-pane fade" id="pills-three" role="tabpanel" aria-labelledby="pills-two-tab">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="cloent">
                                <div class="ins-det">
                                        <div class="dn-info b_bone"><a
                                                href="pdf/goodluck-annual-report-2021-22.pdf"
                                                target="_new"><i class="fa fa-file-pdf-o"></i> Annual Report -  FY 2021-22</a></div>

                                                <div class="dn-info b_bone"><a
                                                href="pdf/goodluck-annual-report-2020-21-new.pdf"
                                                target="_new"><i class="fa fa-file-pdf-o"></i> Annual Report -  FY 2020-21</a></div>
                                                <div class="dn-info b_bone"><a
                                                href="pdf/goodluck-annual-report-2019-20-new.pdf"
                                                target="_new"><i class="fa fa-file-pdf-o"></i> Annual Report -  FY 2019-20</a></div>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!--end  -->
                    <div class="tab-pane fade" id="pills-four" role="tabpanel" aria-labelledby="pills-two-tab">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="cloent">
                                <div class="ins-det">
                                        <div class="dn-info b_bone"><a
                                                href="pdf/shp-jun23.pdf"
                                                target="_new"><i class="fa fa-file-pdf-o"></i> Shareholding pattern - June 2023</a></div>

                                       

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!--end  -->
                

                </div>
                <!-- Product Tabes End -->

            </div>
        </div>
    </section>
 
 