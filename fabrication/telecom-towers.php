<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India Limited (Infrastructure & Energy Division) :: Telecom Towers</title>
    <meta name="description" content="">

    <?php include './inc/header.php'; ?>


    <main>

        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item active">
                        <img src="../assets/fabrication/telecom-towers-banner.jpg" class="d-block w-100" alt="...">
                        <h5 class="wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">Telecom Towers</h5>
                        <div class="carousel-caption d-none d-md-block bg_tr wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">

                        </div>
                    </div>
                </div>
                <!-- <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button> -->
            </div>
        </section>
        <!-- hero -->

        <section class="p60 leadership light-grey">
            <div class="container">
                <div class="row row-reverse justify-content-center align-items-center">                   
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Specialization</h3>
                            <p>We specialize in a wide range of products for the telecommunications sector including Towers, Monopoles, Iron fittings and accessories; all tailored to the specific needs and regulations of the customer.</p>

                        </div>
                    </div>
                </div>             

                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Product Offering</h3>                           

                            <div class="row row-design light-grey justify-content-center box-shadow-none ptb-0 align-items-center mt-2">
                                <div class="col-lg-6 col-md-6">                                    
                                    <img src="../assets/fabrication/telecom-towers/row2/1.jpg"
                                        class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                        <p class="text-left font-20"><strong>Legged Tubular Tower</strong></p>
                                        <p>The recommended height of this structure is 30m, 40m, 60m and 70m. This tower is suitable for 1 to 3 operator usage.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-reverse light-grey box-shadow-none ptb-0 justify-content-center align-items-center mt-4">
                                <div class="col-lg-6 col-md-6">                                    
                                    <img src="../assets/fabrication/telecom-towers/row2/2.jpg"
                                        class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                        <p class="text-left font-20"><strong> Legged Angular Towers</strong></p>
                                        <p>The tower is designed for heights from 20m up to 140m</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row row-design light-grey justify-content-center box-shadow-none ptb-0 align-items-center mt-2">
                                <div class="col-lg-6 col-md-6">                                    
                                    <img src="../assets/fabrication/telecom-towers/row2/3.jpg"
                                        class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                        <p class="text-left font-20"><strong>Legged Narrow Base Tubular Tower</strong></p>
                                        <p>The recommended height of this structure is 30m, 45m and 60m. <br>They are suitable for single
operator usage</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Facility</h3>                           
                            <ul class="row boxview">
                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">Manufacturing Capacity 48000MT per annum </li>

                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">In-House Designing & PreEngineering Department</li>

                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">In-House Galvanizing Plant with up to 10.1m length, 1.2m width and 2.5m depth</li>

                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">Abides to international norms of packaging like proper lashing, choking and fumigation </li>

                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">In-house manufacturing facility of pipes up to 273NB.</li>
                            </ul>
                        </div>
                    </div>
                </div>

  

            </div>
        </section>


    </main>



    <?php include './inc/footer.php';?>