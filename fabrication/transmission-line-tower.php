<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India Limited (Infrastructure & Energy Division) :: Transmission Line Tower</title>
    <meta name="description" content="">

    <?php include './inc/header.php'; ?>


    <main>

        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item active">
                        <img src="../assets/fabrication/transmission-line-tower-banner.jpg" class="d-block w-100" alt="...">
                        <h5 class="wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">Transmission Line Tower</h5>
                        <div class="carousel-caption d-none d-md-block bg_tr wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">

                        </div>
                    </div>
                </div>
                <!-- <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button> -->
            </div>
        </section>
        <!-- hero -->

        <section class="p60 leadership light-grey">
            <div class="container">
                <div class="row row-reverse justify-content-center align-items-center">                   
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Who are we</h3>
                            <p>We are the most experienced and reliable supplier of lattice steel structures for transmission Line towers in the region. Our design, manufacturing, supply and installation capabilities give us an unmatched ability to be a comprehensive and competitive supplier in the power transmission market. </p>

                        </div>
                    </div>
                </div>
                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Consider us</h3>                           
                            <ul class="row boxview">
                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">State of the Art Engineering, Fabrication and Galvanizing facilities in-house, Sikandrabad<br> (Uttar Pradesh)</li>

                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">Dedicated vertical for (EPC) services has been established with specialized skill set to execute turnkey projects in the towers’ domestic market.</li>

                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">Health and safety is one of the priority areas at Goodluck. We are ISO 14001 and OHSAAS 18001 certified</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Product Offering</h3>
                            <p>We offer our customers a full-range-cum-diverse product basket ranging from 66kV to 800kV Towers Single Circuit, Double Circuit, Multi-Circuit Towers suitable for Twin, Quad and Hex Conductor configurations</p>

                            <div class="row row-design light-grey justify-content-center box-shadow-none ptb-0 align-items-center mt-2">
                                <div class="col-lg-6 col-md-6">                                    
                                    <img src="../assets/fabrication/transmission-line-tower/row3/1.jpg"
                                        class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                        <p class="text-left font-20"><strong>Suspension/Tangent Towers</strong></p>
                                        <p>These are the towers that are only intended to carry the conductors and that are used in the linear routes or small angled corners, where the conductors are bound with suspension insulator.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-reverse light-grey box-shadow-none ptb-0 justify-content-center align-items-center mt-4">
                                <div class="col-lg-6 col-md-6">                                    
                                    <img src="../assets/fabrication/transmission-line-tower/row3/2.jpg"
                                        class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                        <p class="text-left font-20"><strong>Tension/Angle Towers</strong></p>
                                        <p>These are the towers that are intended to carry the line conductors and fix the same with a tension insulator and that are used in the linear route or corners.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row row-design light-grey justify-content-center box-shadow-none ptb-0 align-items-center mt-2">
                                <div class="col-lg-6 col-md-6">                                    
                                    <img src="../assets/fabrication/transmission-line-tower/row3/3.jpg"
                                        class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                        <p class="text-left font-20"><strong>Crossing Towers</strong></p>
                                        <p>TThese are the towers that are specially designed for the locations, where a significant level of traffic prevails, such as highways, watercourses or railways or long distance river/sea crossings.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row row-reverse light-grey box-shadow-none ptb-0 justify-content-center align-items-center mt-4">
                                <div class="col-lg-6 col-md-6">                                    
                                    <img src="../assets/fabrication/transmission-line-tower/row3/4.jpg"
                                        class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                        <p class="text-left font-20"><strong>Terminal/Dead End Towers</strong></p>
                                        <p>These are the towers that are used at the beginning or end of the lines for terminating the transmission line purposes.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

  

            </div>
        </section>


    </main>



    <?php include './inc/footer.php';?>