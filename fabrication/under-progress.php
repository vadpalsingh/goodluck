<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India Limited (Infrastructure & Energy Division) :: Under Progress Projects</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <main>
        <!-- hero -->
        <section class="good_luck">
            <div id="carouselExample" class="carousel slide banner__slider" data-bs-ride="carousel" data-pause="false">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item active">
                        <h5>Under Progress Projects</h5>
                        <img src="../assets/fabrication/complete-banner.jpg" class="d-block w-100"
                            alt="Mumbai–Ahmadabad High Speed Rail Corridor (MAHSR)">
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                            <h4></h4>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- hero -->

        <!-- counter -->
        <!-- service -->
        <section class="brand__area tabproduct p60">
            <div class="container">
                <!-- Product Tabes -->
                <ul class="nav nav-pills  justify-content-center" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pills-one-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-one" type="button" role="tab" aria-controls="pills-one"
                            aria-selected="true">Fabricated Structure</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-two-tab" data-bs-toggle="pill" data-bs-target="#pills-two"
                            type="button" role="tab" aria-controls="pills-two" aria-selected="false">Transmission &
                            Telecom</button>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-one" role="tabpanel"
                        aria-labelledby="pills-one-tab">
                        <div class="row row-reverse justify-content-center align-items-center mt-4">
                            <div class="col-lg-5 col-md-6">
                                <img src="../assets/fabrication/underproject/A/1.jpg" class="wow fadeInUp"
                                    data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;width:100%">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="wow fadeInUp" data-wow-delay=".3s"
                                    style="visibility: visible; animation-delay: 0.3s;">
                                    <ul class="liststyle text-left">
                                        <li><strong>PROJECT: </strong> Rampur Dumra - Tal - Rajendra Pul : Additional Bridge and Doubling (Design and Construction of New Double Track Railway Bridge (25T Axle Load) Over River Ganga, Upstream of Existing Rajendra Pul Near Mokama Through EPC Contract)</li>
                                        <li><strong>Total PO Wt: </strong>2400 MT</li>
                                        <li><strong>Inspection Authority: </strong> AFCONS, IRCON and RDSO</li>                                      
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="row row-reverse justify-content-center align-items-center mt-4">
                            <div class="col-lg-5 col-md-6">
                                <img src="../assets/fabrication/underproject/A/2.jpg" class="wow fadeInUp"
                                    data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;width:100%">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="wow fadeInUp" data-wow-delay=".3s"
                                    style="visibility: visible; animation-delay: 0.3s;">
                                    <ul class="liststyle text-left">
                                        <li><strong>PROJECT:</strong> STEEL PLANT EXPANSION PROJECT AT HAZIRA, GUJARAT</li>
                                        <li><strong>Total PO Wt:</strong>&nbsp; 3000 MT</li>
                                        <li><strong>Inspection Authority:</strong> L&T and AMNS India</li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="row row-reverse justify-content-center align-items-center mt-4">
                            <div class="col-lg-5 col-md-6">
                                <img src="../assets/fabrication/underproject/A/3.jpg" class="wow fadeInUp"
                                    data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;width:100%">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="wow fadeInUp" data-wow-delay=".3s"
                                    style="visibility: visible; animation-delay: 0.3s;">
                                    <ul class="liststyle text-left">
                                        <li><strong>PROJECT: </strong> MSIL-P-3-IMT-KHARKHODA SONIPAT HARYANA</li>
                                        <li><strong>Total PO Wt:</strong> 3000 MT</li>
                                        <li><strong>Inspection Authority:</strong>  L&T and MSIL</li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="row row-reverse justify-content-center align-items-center mt-4">
                            <div class="col-lg-5 col-md-6">
                                <img src="../assets/fabrication/underproject/A/4.jpg" class="wow fadeInUp"
                                    data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;width:100%">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="wow fadeInUp" data-wow-delay=".3s"
                                    style="visibility: visible; animation-delay: 0.3s;">
                                    <ul class="liststyle text-left">
                                        <li><strong>PROJECT:</strong> Patratu (3x800 MW) STTP.</li>
                                        <li><strong>Total PO Wt:</strong> 3200 MT</li>
                                        <li><strong>Inspection Authority:</strong> PCTL, BHEL and NTPC.</li>
                                    </ul>

                                </div>
                            </div>
                        </div>                       
                    </div>
                    <div class="tab-pane fade" id="pills-two" role="tabpanel" aria-labelledby="pills-two-tab">
                        <div class="row row-design justify-content-center align-items-center mt-4">
                            <div class="col-lg-5 col-md-6">
                                <img src="../assets/fabrication/underproject/B/1.jpg" style="visibility: visible; animation-delay: 0.1s; width:100%">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="">
                                    <p class="text-left font-20"><strong>PROJECTS: </strong>SAMPRADHA POWER, HYDRO POWER PROJECT NEPAL</p>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="row row-design justify-content-center align-items-center mt-4">
                            <div class="col-lg-5 col-md-6">
                                <img src="../assets/fabrication/underproject/B/2.jpg"  style="visibility: visible; animation-delay: 0.1s; width:100%">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="">
                                    <p class="text-left font-20"><strong>PROJECT:</strong> RELIANCE</p>
                                    <p class="text-left font-20"><strong>QTY:</strong> 5000 MT</p>
                                    <p class="text-left font-20"><strong>INSPECTION AUTHORITY:</strong> RAMBOL</p>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Product Tabes End -->
            </div>
        </section>
        <!-- service -->

    </main>
    <?php include './inc/footer.php';?>