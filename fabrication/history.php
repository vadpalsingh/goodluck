<!doctype html>
<html class="no-js" lang="eng">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Goodluck India Limited (Infrastructure & Energy Division) :: History </title>
<meta name="description" content="">

<?php include './inc/header.php'; ?>


<main>
   
    <section class="good_luck">
        <div class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                <div class="carousel-item active">            
                <img src="../assets/fabrication/history-banner.jpg" class="d-block w-100" alt="...">        
                    <h5 class="wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">History</h5>
                    
                </div>
            </div>
          
        </div>
    </section>
    <!-- hero -->
   <section class="innerpagenav">
	 <div class="container">
		<div class="row">
			<ul class="pagenav">
				<li><a href="facilities.php">Facilities</a></li>
				<li><a href="history.php" class="active">History</a></li>
			</ul>
		</div>
	 </div>   
   </section>
   <!-- about -->	
	 <!-- <section class="p60 light-grey wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">
	   <div class="container">      
	    <div class="row align-items-center">             
            <div class="col-lg-6 col-md-6">
			    <div class="home-about wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">
				  
					<p class="row-design">Established in the year 1986, Goodluck India Limited is an EMS 14001, OHSAS and ISO 9001-2016 certified organization, engaged in manufacturing and exporting of a wide range of galvanized sheets & coils, towers, hollow sections, CR coils CRCA and pipes & tubes. We also specialize in providing Telecommunication Structures, ERW Steel Tubes, ERW Steel Pipes, and Galvanized Black Steel Tubes. These are acclaimed for high tensile strength, long service life and higher efficiency. </p>

                    <p class="row-design">We have grown leaps and bounds under the aegis of our mentor Chairman Shri M. C. Garg. His rich industry experience and entrepreneurial zeal have enabled us to surge ahead in the competitive market Along with him is a team of talented pool of Engineering Experts who have excelled in managing Business verticals since last 3 decades.</p>  
                    <p class="row-design">A diversified portfolio of products coming out of state of art individual workshops through a team of scrupulous quality control professionals who strictly monitor every stage of production to ensure International standards of quality. Our products after internal quality checks are verified and witnessed by team of Expert External auditors and inspectors from SGS, DNV, Tuv, BIS, Railways, RITES, RDSO, Core, Power Grid Corp. and various reputed international agencies.</p>
                </div>
			</div>
            <div class="col-lg-6 col-md-6">
			    <div class="home-about wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">
                <p class="row-design">We work out of 4 facilities in Delhi National Capital Region catering to production of ERW/CDW Pipes and Tubes, Structural fabricated Products and Forged Items. 
Our Recent Capacity addition in Western region of India based at Bhuj, Gujrat caters to CDW tubes /Pipes and Structural Fabrication products. We Deliver to nooks and crannies of Indian geography as well as Sail our products through Ports in Mumbai, Mundra, Chennai and Haldia. We are a global ready company as 40% of our annual turnover is through exports. </p>

<p class="row-design">It was in the year 2001 that a State of The Art facility (approved by Power Grid Corp, BHEL NTPC and Other Power distribution and telecom Utilities ) started its Foray into manufacturing and erection of Transmission Tower, Substation Structures, telecom towers. The Workshop was further upgraded in 2013 to fabricate Steel Superstructure Bridges , Building Structures for Power Plant , Oil & Gas, Energy Structures for Utilities and Mechanical Items. Today this Unit is one of India’s Top recognised workshop for Engineering , Manufacturing & Erection Services of Critical Super Structure Bridges , Primary & Secondary Steel Structures , Products & Services for Roads and Highways , Structures for Renewable Energy Projects , Electrification Products for Railways, Core,  RDSO. </p>
<p class="row-design">Today we are a  70000 MTPA capacity ushering to the India Infrastructure Growth Story and we have built in capacity of another 40% output. </p>
                </div>
			</div> 			 			
		 </div>
       </div>
    </section> -->
    <div class="timeline">
    <section class="timeline light-grey">
        <ul>
          <li class="wow fadeInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s;">
            <div class="content">                
                <div class="timeline-content-info">
                    <span class="timeline-content-info-title">
                        <i class="fa fa-certificate" aria-hidden="true"></i>
                        Established in the year 1986
                    </span>                   
                </div>
                <p>Goodluck India Limited is an EMS 14001, OHSAS and ISO 9001-2016 certified organization, engaged in manufacturing and exporting of a wide range of galvanized sheets & coils, towers, hollow sections, CR coils CRCA and pipes & tubes. We also specialize in providing Telecommunication Structures, ERW Steel Tubes, ERW Steel Pipes, and Galvanized Black Steel Tubes. These are acclaimed for high tensile strength, long service life and higher efficiency. </p>
                
            </div>
          </li>
          <li class="wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
            <div class="content">                
                <div class="timeline-content-info">
                    <span class="timeline-content-info-title">
                        <i class="fa fa-certificate" aria-hidden="true"></i>
                        Chairman Shri M. C. Garg. His Rich Industry Experience
                    </span>                   
                </div>
                <p>We have grown leaps and bounds under the aegis of our mentor Chairman Shri M. C. Garg. His rich industry experience and entrepreneurial zeal have enabled us to surge ahead in the competitive market Along with him is a team of talented pool of Engineering Experts who have excelled in managing Business verticals since last 3 decades.</p>
                
            </div>
          </li>
          <li class="wow fadeInUp" data-wow-delay=".4s" style="visibility: visible; animation-delay: 0.4s;">
            <div class="content">                
                <div class="timeline-content-info">
                    <span class="timeline-content-info-title">
                        <i class="fa fa-certificate" aria-hidden="true"></i>
                        Diversified Portfolio
                    </span>                  
                </div>
                <p>A diversified portfolio of products coming out of state of art individual workshops through a team of scrupulous quality control professionals who strictly monitor every stage of production to ensure International standards of quality. Our products after internal quality checks are verified and witnessed by team of Expert External auditors and inspectors from SGS, DNV, Tuv, BIS, Railways, RITES, RDSO, Core, Power Grid Corp. and various reputed international agencies.</p>
            </div>
          </li>
          <li class="wow fadeInUp" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s;">
            <div class="content">                
                <div class="timeline-content-info">
                    <span class="timeline-content-info-title">
                        <i class="fa fa-certificate" aria-hidden="true"></i>
                        Global Ready Company as 40% of our Annual Turnover
                    </span>                    
                </div>
                <p>We work out of 4 facilities in Delhi National Capital Region catering to production of ERW/CDW Pipes and Tubes, Structural fabricated Products and Forged Items.</p><p>
Our Recent Capacity addition in Western region of India based at Bhuj, Gujrat caters to CDW tubes /Pipes and Structural Fabrication products. We Deliver to nooks and crannies of Indian geography as well as Sail our products through Ports in Mumbai, Mundra, Chennai and Haldia. We are a global ready company as 40% of our annual turnover is through exports.</p>
                
            </div>
          </li>
          <li class="wow fadeInUp" data-wow-delay=".6s" style="visibility: visible; animation-delay: 0.6s;">
            <div class="content">                
                <div class="timeline-content-info">
                    <span class="timeline-content-info-title">
                        <i class="fa fa-certificate" aria-hidden="true"></i>
                        The Workshop was further upgraded in 2013
                    </span>                  
                </div>
                <p>It was in the year 2001 that a State of The Art facility (approved by Power Grid Corp, BHEL NTPC and Other Power distribution and telecom Utilities ) started its Foray into manufacturing and erection of Transmission Tower, Substation Structures, telecom towers. The Workshop was further upgraded in 2013 to fabricate Steel Superstructure Bridges , Building Structures for Power Plant , Oil & Gas, Energy Structures for Utilities and Mechanical Items. Today this Unit is one of India’s Top recognised workshop for Engineering , Manufacturing & Erection Services of Critical Super Structure Bridges , Primary & Secondary Steel Structures , Products & Services for Roads and Highways , Structures for Renewable Energy Projects , Electrification Products for Railways, Core,  RDSO. </p>
<p>Today we are a  70000 MTPA capacity ushering to the India Infrastructure Growth Story and we have built in capacity of another 40% output. </p>
            </div>
          </li>
        </ul>
      </section>
  </div>
   
</main>



<?php include './inc/footer.php';?>
