<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India Limited (Infrastructure & Energy Division) :: Primary & Secondary Boiler Structure </title>
    <meta name="description" content="">

    <?php include './inc/header.php'; ?>


    <main>

        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item active">
                        <img src="../assets/fabrication/primary-and-secondary-boiler-structure-banner.jpg"
                            class="d-block w-100" alt="primary-and-secondary-boiler-structure">
                        <h5 class="wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">Primary & Secondary Boiler Structure
                        </h5>
                        <div class="carousel-caption d-none d-md-block bg_tr wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">

                        </div>
                    </div>
                </div>
                <!-- <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button> -->
            </div>
        </section>
        <!-- hero -->

        <section class="p60 leadership light-grey">
            <div class="container">
                <div class="row row-reverse justify-content-center align-items-center">

                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Primary and Secondary Support Structures for Boilers and Other Equipments</h3>
                            <p>We fabricate structures from plates, channels and rolled sections to columns, bracings
                                and hangers. We use standard sections and plates of reputed makes and have inventory of
                                similar stock for easy delivery. Custom-made columns can be assembled upto a length of
                                90-140 mtrs.</p>
                            <p>We have mock assembled lengths of around 90mtr in our bay. We also have available space
                                to carry mock assemblies of larger lengths. Efficient and qualified welders following
                                approved Welding procedures of Power grid, NTPC, BHEL, EIL are involved in fabricating
                                these items. Ultrasonic and radiography tests are also carried out on these products.
                            <p>We have in-house facilities for same Built-up beams of web height around 1200 2000 mtrs
                                can be fabricated at our
                                workshop. We have executed similar jobs of specific web heights and thickness. We have
                                developed indigenous fixtures and modules to handle built up T&rsquo;s and rolled
                                sections of thickness around 40mm to 52mm. Our supplies presently has been mainly to
                                Boiler Support structures for major power plants and refineries in India. We are
                                registered vendors for Engineers India Limited.
                            </p>
                            </ul>
                        </div>
                    </div>
                </div>



                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3 class="text-left">Our expertise lies in the fabrication of</h3>
                            <ul class="row boxview">
                                <li class="col-lg-6 col-md-6 col-sm-6 col-12"><img src="../assets/fabrication/boiler-structure/1.jpg">
                                    <strong>Plus Columns</strong>
                                </li>
                                <li class="col-lg-6 col-md-6 col-sm-6 col-12"><img src="../assets/fabrication/boiler-structure/2.jpg">
                                <strong>Primary columns of web height over 3 mtrs.</strong></li>

                                <li class="col-lg-6 col-md-6 col-sm-6 col-12"><img src="../assets/fabrication/boiler-structure/3.jpg">
                                <strong>Double Bevel Welded joints checked in-house with RT and UT NDT procedures</strong></li>
                                
                                <li class="col-lg-6 col-md-6 col-sm-6 col-12"><img src="../assets/fabrication/boiler-structure/4.jpg">
                                <strong>Cran Tressels and galleries on layout fabrication</strong></li>

                                <!-- <li class="col-lg-4 col-md-4 col-sm-6 col-12"><img src="../assets/fabrication/boiler-structure/5.jpg">
                                <strong>Secondary Structures</strong></li>

                                <li class="col-lg-4 col-md-4 col-sm-6 col-12">
                                <img src="../assets/fabrication/boiler-structure/6.jpg">
                                <strong>Monorail Beams</strong></li> -->

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3 class="text-left">Our Projects</h3>
                            <ul class="row boxview">
                                
                                <li class="col-lg-3 col-md-6 col-sm-6 col-12"><img src="../assets/fabrication/boiler-structure/project/1.jpg">
                                    <strong >HMEL Bhatinda</strong>
                                </li>
                                <li class="col-lg-3 col-md-6 col-sm-6 col-12"><img src="../assets/fabrication/boiler-structure/project/2.jpg">
                                <strong >NTPC – Tanda</strong></li>

                                <li class="col-lg-3 col-md-6 col-sm-6 col-12"><img src="../assets/fabrication/boiler-structure/project/3.jpg">
                                <strong>NTPC – Khargone</strong></li>
                                
                                <li class="col-lg-3 col-md-6 col-sm-6 col-12"><img src="../assets/fabrication/boiler-structure/project/4.jpg">
                                <strong >Shree Singhaji – Malwa</strong></li>

                            </ul>
                        </div>
                    </div>
                </div>


            </div>
        </section>


    </main>



    <?php include './inc/footer.php';?>