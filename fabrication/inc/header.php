<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Place favicon.ico in the root directory -->
<link rel="shortcut icon" type="image/x-icon" href="../assets/favicon.jpg">
<!-- CSS here -->
<!-- <link rel="stylesheet" href="../assets/css/bootstrap.css"> -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/css/meanmenu.css">
<link rel="stylesheet" href="../assets/css/animate.css">
<link rel="stylesheet" href="../assets/css/owl-carousel.css">
<link rel="stylesheet" href="../assets/css/swiper-bundle.css">
<link rel="stylesheet" href="../assets/css/backtotop.css">
<link rel="stylesheet" href="../assets/css/magnific-popup.css">
<link rel="stylesheet" href="../assets/css/nice-select.css">
<link rel="stylesheet" href="../assets/css/flaticon.css">
<link rel="stylesheet" href="../assets/css/font-awesome-pro.css">
<link rel="stylesheet" href="../assets/css/spacing.css">
<link rel="stylesheet" href="../assets/css/style.css">
<link rel="stylesheet" href="../assets/css/custom.css">
<script>
function myFunction() {
    var x = document.getElementById("myVideo").autoplay;
}
</script>

</head>

<body>
    <!-- Preloader -->
    <!-- <div class="preloader"></div> -->
    <!-- pre loader area end -->
    <!-- back to top start -->
    <div class="progress-wrap">
        <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
            <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
        </svg>
    </div>
    <!-- back to top end -->
    <!-- header-area-start -->
    <header id="header-sticky" class="header-area">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-xl-4 col-lg-4 col-md-6 col-8">
                    <div class="logo-area">
                        <div class="logo" style="display: flex; align-items: center;">
                            <a href="../index.php" title="Back to Goodluck India Home"><img src="../assets/img/logo/logo-white.png" alt="Goodluck India Home"></a>
                            <h3 class="h_for"><a href="index.php" title="Back to Infrastructure & Energy Division Home">Goodluck India Limited<br>(Infrastructure & Energy Division)</a></h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-7 col-md-6 col-4">
                    <div class="menu-area menu-padding">
                        <div class="main-menu text-center">
                            <nav id="mobile-menu" style="display: block;">
                                <ul>
                                    <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            About US
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="history.php">History</a></li>
                                            <li><a class="dropdown-item" href="facilities.php">Facilities</a></li>                                           
                                        </ul>
                                    </li>                                   
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" role="button"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            Products
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="nav-item dropdown">
                                                <a class="nav-link quali_ty dl_f" href="product.php" role="button"
                                                    data-bs-toggle="dropdown" aria-expanded="false">
                                                    Fabricated Structure<i class="fa fa-angle-right"></i>
                                                </a>
                                                <ul class="dropdown-menu sub_minu">
                                                    <li><a class="dropdown-item" href="railway-and-road-bridges.php">Railway & Road Bridges</a></li>
                                                    <li><a class="dropdown-item" href="building-structures.php">Building Structures</a></li>
                                                    <li><a class="dropdown-item" href="structure-for-roads-and-expressways.php">Structure for Roads & Expressways</a></li>
                                                    <li><a class="dropdown-item" href="launching-girder.php">Launching Girder</a></li>
                                                    <li><a class="dropdown-item" href="primary-and-secondary-boiler-structure.php">Primary & secondary Boiler Structure </a></li>
                                                    <li><a class="dropdown-item" href="defense-fabrication.php">Defense Fabrication </a></li>
                                                </ul>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link quali_ty dl_f" href="product.php" role="button"
                                                    data-bs-toggle="dropdown" aria-expanded="false">
                                                    Transmission & Telecom<i class="fa fa-angle-right"></i>
                                                </a>
                                                <ul class="dropdown-menu sub_minu">
                                                    <li><a class="dropdown-item" href="transmission-line-tower.php">Transmission Line Tower</a>                                                    </li>
                                                    <li><a class="dropdown-item" href="telecom-towers.php">Telecom Towers</a></li>                                                   
                                                    <li><a class="dropdown-item" href="over-head-electrification.php">Over Head Electrification</a>                                                    </li>
                                                    <li><a class="dropdown-item" href="substation-structure.php">Substation Structure</a></li>
                                                </ul>
                                            </li>
                                            <li><a  href="../assets/fabrication/Goodluck_Solar.pdf" target="_blank">Solar Mounting Structure</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            Projects
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="completed.php">Completed</a></li>
                                            <li><a class="dropdown-item" href="under-progress.php">Under Process</a></li>
                                        </ul>
                                    </li>
                                    <li><a class="nav-link" href="approvals.php">Approvals</a></li>
                                    <li><a href="contact.php">COntact Us</a> </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="side-menu-icon d-lg-none text-end">
                        <a href="javascript:void(0)" class="info-toggle-btn f-right sidebar-toggle-btn"><img
                                src="../assets/img/humberger-icon.svg" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-1 col-lg-1 d-none d-lg-block">
                    <div class="header__sm-action">
                        <div class="header__sm-action-item right-border1">
                            <a href="javascript:void(0)" class="info-toggle-btn f-right sidebar-toggle-btn"><img
                                    src="../assets/img/humberger-icon.svg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-area-end -->
    <!-- sidebar area start -->
    <div class="sidebar__area">
        <div class="sidebar__wrapper">
            <div class="sidebar__close">
                <button class="sidebar__close-btn" id="sidebar__close-btn">
                    <i class="fal fa-times"></i>
                </button>
            </div>
            <div class="sidebar__content">
                <div class="sidebar__logo  mb-2">
                    <a href="index.html">
                        <img src="../assets/img/logo/logo-black.png" alt="logo">
                    </a>
                </div>
                <div class="mobile-menu fix"></div>
                <div class="sidebar__contact mt-0 mb-20">
                    <!-- <h4>Contact Info</h4> -->
                    <ul>
                        <li class="d-flex align-items-center">
                            <a href="../index.php">GoodLuck Home</a>
                        </li>
                        <li class="d-flex align-items-center">
                            <a href="#">Corporate Video</a>
                        </li>
                        <li class="d-flex align-items-center">
                            <a href="#">Brochure</a>
                        </li>
                        <li class="d-flex align-items-center">
                            <a href="#">Careers</a>
                        </li>
                        <li class="d-flex align-items-center">
                            <a href="#">Enquiry</a>
                        </li>
                        <li class="d-flex align-items-center">
                            <a href="#">Contact Us</a>
                        </li>
                    </ul>
                </div>
                <div class="sidebar__social">
                    <ul>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- sidebar area end -->
    <div class="body-overlay"></div>
    <!-- sidebar area end -->