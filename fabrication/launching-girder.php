<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India Limited (Infrastructure & Energy Division) :: Launching Girder </title>
    <meta name="description" content="">

    <?php include './inc/header.php'; ?>


    <main>

        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item active">
                        <img src="../assets/fabrication/launching-grider-banner.jpg" class="d-block w-100" alt="...">
                        <h5 class="wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">Launching Girder</h5>
                        <div class="carousel-caption d-none d-md-block bg_tr wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">

                        </div>
                    </div>
                </div>
                <!-- <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button> -->
            </div>
        </section>
        <!-- hero -->

        <section class="p60 leadership light-grey">
            <div class="container">
                <div class="row row-reverse justify-content-center align-items-center">

                    <div class="col-lg-11 col-md-11">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Introduction</h3>
                            <p>top-notch launching girders for PSC (Pre-Stressed Concrete) girders across major rivers
                                in India. Our commitment to precision engineering ensures the right fit and matching of
                                components for these critical structures. With the ability to check fabrication
                                correctness through trial assembly of major lengths, our launching girders are a perfect
                                solution for various bridge construction projects.</p>

                        </div>
                    </div>
                </div>
                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="ribbon red"><span>Type</span></div>
                    <div class="col-lg-5 col-md-6">
                        <img src="../assets/fabrication/launching-grider/2.webp" class="wow fadeInUp"
                            data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Segmental Launching Girders</h3>
                            <ul class="liststyle">
                                <li>Our segmental launching girders are designed to incrementally push or pull PSC
                                    segments into position during bridge construction</li>
                                <li>These girders facilitate the controlled and safe assembly of precast segments,
                                    ensuring seamless construction
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3 class="text-left">Uses</h3>
                            <ul class="row boxview">
                                <li class="col-lg-3 col-md-3 col-sm-6 col-12"><img
                                        src="../assets/fabrication/launching-grider/uses/1.jpg"><strong>River
                                        Crossings</strong>Our launching girders are specifically tailored for erecting
                                    PSC girders across major rivers, enabling efficient and safe crossings.</li>

                                <li class="col-lg-3 col-md-3 col-sm-6 col-12"><img
                                        src="../assets/fabrication/launching-grider/uses/2.jpg"><strong>Long-Span
                                        Bridges</strong>For projects requiring long spans, our segmental launching
                                    girders ensure precise and smooth segment alignment, supporting the construction of
                                    large bridges.</li>

                                <li class="col-lg-3 col-md-3 col-sm-6 col-12"> <img
                                        src="../assets/fabrication/launching-grider/uses/3.jpg"><strong>Highway
                                        Overpasses</strong>Launching girders play a vital role in building highway
                                    overpasses, allowing systematic assembly and minimizing traffic disruption.</li>
                                    
                                <li class="col-lg-3 col-md-3 col-sm-6 col-12"><img
                                        src="../assets/fabrication/launching-grider/uses/4.jpg"><strong>Railway
                                        Viaducts</strong>Our girders are indispensable for constructing railway
                                    viaducts, providing a controlled and reliable approach for installing PSC segments.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3 class="text-left">Advantages</h3>
                            <ul class="row boxview">
                                <li class="col-lg-3 col-md-3 col-sm-6 col-12"><strong>Precision Engineering</strong>Our
                                    launching girders are engineered with utmost precision, ensuring seamless
                                    integration of PSC segments during assembly.</li>
                                <li class="col-lg-3 col-md-3 col-sm-6 col-12"><strong>Trial Assembly</strong>he ability
                                    to perform trial assemblies with major lengths allows us to verify fabrication
                                    correctness, reducing on-site errors and saving valuable time.</li>
                                <li class="col-lg-3 col-md-3 col-sm-6 col-12"><strong>Safety and Efficiency</strong>Our
                                    launching girders prioritize safety during construction, offering controlled and
                                    reliable installation of PSC segments, leading to efficient project execution.</li>
                                <li class="col-lg-3 col-md-3 col-sm-6 col-12"><strong>Customization</strong>We adapt our
                                    launching girders to specific bridge configurations and project requirements,
                                    offering tailor-made solutions for each application.</li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3 class="text-left">In conclusion</h3>
                            <p> Goodluck India's launching girders are synonymous with expertise, technology, and
                                precision engineering. With a focus on safety, efficiency, and customization, our
                                segmental launching girders provide a perfect fit for a variety of bridge construction
                                projects, delivering seamless crossings and efficient transportation solutions across
                                major rivers and challenging terrains in India.</p>
                        </div>
                    </div>
                </div>


            </div>
        </section>


    </main>



    <?php include './inc/footer.php';?>