<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India Limited (Infrastructure & Energy Division)</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <main>
        <!-- hero -->
        <section class="good_luck">
            <div id="carouselExample" class="carousel slide banner__slider" data-bs-ride="carousel" data-pause="false">
                <div class="carousel-inner cdwtubesbanner">
                    <h2 class="freeze">our contributions to the milestone projects</h2> 
                   <div class="carousel-item active">
                       <img src="../assets/fabrication/banner/1.jpg" class="d-block w-100" alt="Mumbai–Ahmadabad High Speed Rail Corridor (MAHSR)">
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                            <h4>Mumbai–Ahmadabad High Speed Rail Corridor (MAHSR)</h4>
                            <p>Mumbai–Ahmadabad HSR, is an under-construction high-speed rail line, which will connect India's economic and financial hub and Maharashtra’s capital, Mumbai, with the largest city of the state of Gujarat, Ahmadabad</p>
                        </div>
                    </div>        
                    <div class="carousel-item">
                        <img src="../assets/fabrication/banner/4.jpg" class="d-block w-100" alt="Delhi-Vadodara Expressway">
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                            <h4>Delhi-Vadodara Expressway</h4>
                            <p class="text-white">A major highway connecting Delhi to Vadodara, enhancing road transportation between the two cities
                            </p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="../assets/fabrication/banner/2.jpg" class="d-block w-100" alt="Delhi-Meerut Rapid Rail Elevated corridor">
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                            <h4>Delhi-Meerut Rapid Rail Elevated corridor </h4>
                            <p class="text-white">High-speed rail project connecting Delhi and Meerut with an elevated corridor for efficient transportation</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="../assets/fabrication/banner/5.jpg" class="d-block w-100" alt="Dwarka Expressway">
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                            <h4>Dwarka Expressway</h4>
                            <p class="text-white">Road project in Delhi connecting Dwarka to other parts of the city, improving connectivity and reducing congestion
                            </p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="../assets/fabrication/banner/3.jpg" class="d-block w-100" alt="Katra-Qazikund rail link with Chenab arch bridge">
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                            <h4>Katra-Qazikund rail link with Chenab arch bridge</h4>
                            <p class="text-white">Rail link in Kashmir featuring the world's highest arch bridge, connecting Katra to Qazikund for improved connectivity.</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                    <img src="../assets/fabrication/banner/6.jpg" class="d-block w-100" alt="Mumbai-Vadodara Expressway">
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                            <h4>Mumbai-Vadodara Expressway</h4>
                            <p class="text-dark text-white">Expressway connecting Mumbai and Vadodara, enabling faster and smoother travel between the two cities.</p>
                        </div>
                    </div>                   
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </section>
        <!-- hero -->
        <!-- about -->
        <section class="p60 abou_t">
            <div class="container">
                <div class="row">                   
                    <div class="col-md-8">
                    <div class="comman-head">
                            <h2 class="pb3">Welcome To Infra & Energy Division</h2>
                        </div>
                        <div class="g_bout">
                            <div class="home-about wow fadeInUp" data-wow-delay=".2s"
                                style="visibility: visible; animation-delay: 0.2s;">
                                <blockquote>
                                GoodLuck India has Specialized Fabrication facilities in Two locations of Northern and Western India. These facilities are home to skilled manpower and efficient machineries that have an established process based on international ISO standards of quality, sustainable manufacturing and occupational health considerations.
</blockquote>
                                <p class="wow fadeInUp" data-wow-delay=".8s"
                                    style="visibility: visible; animation-delay: 0.8s;">
                                    A total capacity of 90,000 MT per annum of fabricated steel for Infrastructure and Energy sectors globally through EPC contractors, Concessioners, Consultants, OEMS and Licensors.


                                </p>
                                <p class="wow fadeInUp" data-wow-delay=".9s"
                                    style="visibility: visible; animation-delay: 0.8s;">Our products have following approvals & Licenses - Euro Certificate Class EXC2/ISO/RDSO <br>(Ministry of Indian Railways)/ Indian Railway Divisions.
                                </p>
                                <div class="read-more">
                                    <a href="history.php">Know more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4 style="border-bottom: 1px solid #eee;
                        padding-bottom: 5px;">FACTS</h4>
                        <div class="a_im_g">
                            <div class="row g-4 align-items-center">
                                <div class="col-sm-12">                                  
                                    <div class="gray cunt p-3  wow fadeIn ">
                                        <!-- <i class="fa fa-briefcase"></i> -->
                                        <p class="number" style="display:inline-block">60,000 </p>
                                        <span style="color: #838080;font-size: 21px;font-weight: 600;">MT</span>
                                        <span></span>
                                        <p>Galvanizing Installed Capacity</p>
                                    </div>                                  
                                    <div class="gray cunt p-3  wow fadeIn">                                      
                                        <p class="number">5.2 Lac</p>
                                        <span></span>
                                        <p>Transmission Tower Supplied</p>
                                    </div>
                                    <div class="gray cunt p-3  wow fadeIn">                                      
                                        <p class="number">1.5 Lac</p>
                                        <span></span>
                                        <p>1.5 lac Telecom Tower Supplied</p>
                                    </div>
                                    <div class="gray cunt p-3  wow fadeIn">                                      
                                        <p class="number">300 </p>
                                        <span></span>
                                        <p>Steel Bridges Supplied</p>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- counter -->       
        <!-- service -->
        <section class="brand__area tabproduct p60">
            <div class="container">
                <div class="row">
                <div class="sectiontitle mb-4">
                            <h2>Our Products</h2>
                            <span class="headerLine"></span>                            
                        </div>
                </div>
                <!-- Product Tabes -->
                <ul class="nav nav-pills  justify-content-center" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pills-one-tab" data-bs-toggle="pill" data-bs-target="#pills-one" type="button" role="tab" aria-controls="pills-one" aria-selected="true">Fabricated Structure</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-two-tab" data-bs-toggle="pill" data-bs-target="#pills-two" type="button" role="tab" aria-controls="pills-two" aria-selected="false">Transmission & Telecom</button>
                    </li>                   
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-one" role="tabpanel" aria-labelledby="pills-one-tab">                        
                        <div class="row">
                            <!-- Product Start -->
                            <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                    <a href="railway-and-road-bridges.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">                                 
                                            <img src="../assets/fabrication/product/A/1.2.jpg" alt="Railway & Road Bridges" class="res">
                                        </div>
                                        <h3>Railway & Road Bridges</h3>
                                    </div>
                                    </a>
                                </div>
                            </div>  
                            <!-- Product End -->
                            <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                    <a href="building-structures.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">                                 
                                            <img src="../assets/fabrication/product/A/2.1.jpg" alt="Building Structures" class="res">
                                        </div>
                                        <h3>Building Structures</h3>
                                    </div>
                                    </a>
                                </div>
                            </div>  
                            <!-- Product End -->                
                            <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                    <a href="structure-for-roads-and-expressways.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">                                 
                                            <img src="../assets/fabrication/product/A/3.jpg" alt="Structure for Roads & Expressways" class="res">
                                        </div>
                                        <h3>Structure for Roads & Expressways</h3>
                                    </div>
                                    </a>
                                </div>
                            </div>  
                            <!-- Product End -->                
                            <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                    <a href="launching-girder.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">                                 
                                            <img src="../assets/fabrication/product/A/4.jpg" alt="Launching Girder" class="res">
                                        </div>
                                        <h3>Launching Girder</h3>
                                    </div>
                                    </a>
                                </div>
                            </div>  
                            <!-- Product End -->                
                            <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                    <a href="primary-and-secondary-boiler-structure.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">                                 
                                            <img src="../assets/fabrication/product/A/5.jpg" alt="Primary & secondary Boiler Structure " class="res">
                                        </div>
                                        <h3>Primary & secondary Boiler Structure </h3>
                                    </div>
                                    </a>
                                </div>
                            </div>  
                            <!-- Product End -->                
                            <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                    <a href="defense-fabrication.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">                                 
                                            <img src="../assets/fabrication/product/A/6.jpg" alt="Defense Fabrication" class="res">
                                        </div>
                                        <h3>Defense Fabrication </h3>
                                    </div>
                                    </a>
                                </div>
                            </div>  
                            <!-- Product End -->                                
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-two" role="tabpanel" aria-labelledby="pills-two-tab">                   
                    <div class="row">
                            <!-- Product Start -->
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                    <a href="transmission-line-tower.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">                                 
                                            <img src="../assets/fabrication/product/B/1.jpg" alt="Transmission Line Tower" class="res">
                                        </div>
                                        <h3>Transmission Line Tower</h3>
                                    </div>
                                    </a>
                                </div>
                            </div>  
                            <!-- Product End -->
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                    <a href="telecom-towers.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">                                 
                                            <img src="../assets/fabrication/product/B/2.jpg" alt="Telecom Towers" class="res">
                                        </div>
                                        <h3>Telecom Towers</h3>
                                    </div>
                                    </a>
                                </div>
                            </div>  
                            <!-- Product End -->                
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                    <a href="over-head-electrification.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">                                 
                                            <img src="../assets/fabrication/product/B/3.jpg" alt="Over Head Electrification" class="res">
                                        </div>
                                        <h3>Over Head Electrification</h3>
                                    </div>
                                    </a>
                                </div>
                            </div>  
                            <!-- Product End -->                
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                    <a href="substation-structure.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">                                 
                                            <img src="../assets/fabrication/product/B/4.jpg" alt="Substation Structure" class="res">
                                        </div>
                                        <h3>Substation Structure</h3>
                                    </div>
                                    </a>
                                </div>
                            </div>  
                            <!-- Product End -->               
                                                     
                        </div>
                    </div>                       
                </div>
                <!-- Product Tabes End -->
            </div>
        </section>
        <!-- service -->
        <!-- brand__area start -->
        <section class="brand__area1 p60" style="background-color: #fdfdfd;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sectiontitle">
                            <h2>Approvals</h2>
                            <span class="headerLine"></span>
                        </div>
                    </div>
                </div>                
                <div class="row">
                  <div class="col-xl-12">
                     <div class="brand__slider swiper-container">
                        
                        <div class="swiper-wrapper">
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl1.png" alt="approvals"></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl3.png" alt="approvals"></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl4.png" alt="approvals"></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl5.png" alt="approvals"></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl6.png" alt="approvals"></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl7.png" alt="approvals"></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl8.png" alt="approvals"></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl9.png" alt="approvals"></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl10.png" alt="approvals"></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl11.png" alt="approvals"></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl12.png" alt="approvals"></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl13.png" alt="approvals"></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl14.png" alt="approvals"></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl15.png" alt="approvals"></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl16.png" alt="approvals"></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                             <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl17.png" alt="approvals"></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
        </section>
        <!-- brand__area end -->
    </main>
    <?php include './inc/footer.php';?>