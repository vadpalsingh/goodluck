<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India Limited (Infrastructure & Energy Division) :: Over Head Electrification</title>
    <meta name="description" content="">

    <?php include './inc/header.php'; ?>


    <main>

        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item active">
                        <img src="../assets/fabrication/over-head-electrification-banner.jpg" class="d-block w-100"
                            alt="...">
                        <h5 class="wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">Over Head Electrification</h5>
                        <div class="carousel-caption d-none d-md-block bg_tr wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">

                        </div>
                    </div>
                </div>
                <!-- <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button> -->
            </div>
        </section>
        <!-- hero -->

        <section class="p60 leadership light-grey">
            <div class="container">
                <div class="row row-reverse justify-content-center align-items-center">
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">

                            <p>Overhead Electrification is designed on the principle of one or more overhead wires (or
                                rails, particularly in tunnels) situated over rail tracks, raised to a high electrical
                                potential by connection to feeder stations at regular intervals. Goodluck is among the
                                very few companies in India having end-to-end capabilities in the Railway infrastructure
                                segment. We are one of the leading railway consulting companies in India in Railway
                                Over-Head-Electrification (OHE) segment drawings</p>

                                <p><strong>OHE Manufacturing Capacity 12000 MT per annum</strong></p>

                        </div>
                    </div>
                </div>

                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Product offering</h3>
                            <ul class="row boxview justify-content-center">
                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">
                                    <img src="../assets/fabrication/over-head-electrification/row2/1.jpg"><strong>Rolled Mast</strong></li>

                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">
                                    <img src="../assets/fabrication/over-head-electrification/row2/2.jpg"><strong>B-series Mast</strong></li>

                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">
                                    <img src="../assets/fabrication/over-head-electrification/row2/3.jpg"><strong>Special Fabricated Masts
Portals</strong></li>

                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">
                                    <img src="../assets/fabrication/over-head-electrification/row2/4.jpg"><strong>Two Track Cantilever (TTC)</strong></li>

                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">
                                    <img src="../assets/fabrication/over-head-electrification/row2/5.jpg"><strong>Traction Sub-station</strong></li>

                                    <li class="col-lg-4 col-md-6 col-sm-6 col-12">
                                    <img src="../assets/fabrication/over-head-electrification/row2/6.jpg"><strong>Small Part Steel (SPS)</strong></li>
                            </ul>
                        </div>
                    </div>
                </div>



            </div>
        </section>


    </main>



    <?php include './inc/footer.php';?>