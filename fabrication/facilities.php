<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India Limited (Infrastructure & Energy Division) :: Facilities </title>
    <meta name="description" content="">

    <?php include './inc/header.php'; ?>


    <main>

        <section class="good_luck">
            <div class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item active">

                        <h5 class="wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">Facilities</h5>

                    </div>
                </div>

            </div>
        </section>
        <!-- hero -->
        <section class="innerpagenav">
            <div class="container">
                <div class="row">
                    <ul class="pagenav">
                        <li><a href="facilities.php" class="active">Facilities</a></li>
                        <li><a href="history.php">History</a></li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- about -->
        <section class="p60  dark-grey wow fadeInUp" data-wow-delay=".2s"
            style="visibility: visible; animation-delay: 0.2s;">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="sectiontitle mb-4 text-center">
                        <h2>Our Facility</h2>
                        <span class="headerLine"></span>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>
                                            <p><strong>Description</strong></p>
                                        </th>
                                        <th>
                                            <p><strong>Sikandrabad </strong></p>
                                            <p><strong>(Delhi NCR)</strong></p>
                                        </th>
                                        <th>
                                            <p><strong>GoodLuck Metallics </strong></p>
                                            <p><strong>(Bhuj, Gujrat)</strong></p>
                                        </th>
                                    </tr>
                                <tbody>
                                    <tr>
                                        <td>
                                            <p>Total Area in Sq mtrs</p>
                                        </td>
                                        <td>
                                            <p>45000</p>
                                        </td>
                                        <td>
                                            <p>5,50000</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Covered Shed Area</p>
                                        </td>
                                        <td>
                                            <p>38000</p>
                                        </td>
                                        <td>
                                            <p>50000</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Trial Assembly &amp; proto type area</p>
                                            <p>(Mtr x mtr)</p>
                                        </td>
                                        <td>
                                            <p>200 x 15</p>
                                        </td>
                                        <td>
                                            <p>130 mtr x 20 mtr &ndash; 2 nos</p>
                                            <p>Open Area :</p>
                                            <p>400 Mtr x 30 mtrs</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Galvanising Bath<br>(L x B X H) in Mtr</p>
                                        </td>
                                        <td>
                                            <p>10.1 x 1.5 x 2.5</p>
                                        </td>
                                        <td>
                                            <p>Closed Bath only for Pipes</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Blasting Painting facility<br>(mtr x mtr)</p>
                                        </td>
                                        <td>
                                            <p>120 x 10</p>
                                            <p>200 x 100</p>
                                        </td>
                                        <td>
                                            <p>100 x 16</p>
                                            <p>20 x 20</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <img src="../assets/fabrication/facility/banner.jpg" class="d-block w-100 mt-3"
                                alt="Our Facility">
                        </div>

                    </div>
                    <div class="col-md-5 col-lg-5">
                        <img src="../assets/fabrication/facility//map.jpg" class=" wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible;animation-delay: 0.2s;width: 100%;filter: hue-rotate(204deg);border-radius: 5px;box-shadow: 0px 0px 20px rgb(0 0 0 / 12%);">
                    </div>
                </div>

            </div>
        </section>
        <section class="light-grey tabproduct p60">
            <div class="container-fluid">
                <div class="row">
                    <div class="sectiontitle mb-3">
                        <h2>Our Extended Facility</h2>
                        <span class="headerLine"></span>
                    </div>
                </div>
                <!-- Product Tabes -->
                <ul class="nav nav-pills  justify-content-center" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pills-one-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-one" type="button" role="tab" aria-controls="pills-one"
                            aria-selected="true">Gujarat- Bhuj </button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-two-tab" data-bs-toggle="pill" data-bs-target="#pills-two"
                            type="button" role="tab" aria-controls="pills-two" aria-selected="false">Sikandrabad
                        </button>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-one" role="tabpanel"
                        aria-labelledby="pills-one-tab">
                        <div class="row">
                            <div class="col-lg-5 col-md-5">
                            <h4>Plant Area</h4>
                                <div class="table-responsive table-height">                                   
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <p>Area</p>
                                                </th>
                                                <th>
                                                    <p>Dimension</p>
                                                </th>
                                                <th>
                                                    <p>Nos</p>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th colspan="3">
                                                    <p><strong>Fabrication Area</strong></p>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p><strong>&nbsp;Covered Shed 1: </strong></p>
                                                </td>
                                                <td>&nbsp;</td>
                                                <td>
                                                    <p>One</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Covered Shed 1 :</p>
                                                </td>
                                                <td>
                                                    <p>276 mtr x 24 mtr</p>
                                                </td>
                                                <td>
                                                    <p>One</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Covered shed 2:</p>
                                                </td>
                                                <td>
                                                    <p>90 mtr x 24 mtr</p>
                                                </td>
                                                <td>
                                                    <p>One</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Covered Shed 3 : (Under construction)</p>
                                                </td>
                                                <td>
                                                    <p>235 mtr x 20 mtr</p>
                                                </td>
                                                <td>
                                                    <p>One</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th colspan="3">
                                                    <p><strong>Finishing Area</strong></p>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Blasting shed- 20 mtr x 16 mtr Three Booths</p>
                                                </td>
                                                <td>
                                                    <p>20 mtr x 16 mtr</p>
                                                </td>
                                                <td>
                                                    <p>Three Booths</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Painting shed 125 mtr x 20 mtr One</p>
                                                </td>
                                                <td>
                                                    <p>125 mtr x 20 mtr</p>
                                                </td>
                                                <td>
                                                    <p>One</p>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <p>Trial assembly area</p>
                                                </td>
                                                <td>
                                                    <p>130 mtr x 20 mtr</p>
                                                </td>
                                                <td>
                                                    <p>One</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Open Area for Trial Assembly</p>
                                                </td>
                                                <td>
                                                    <p>230 mtr x 20 mtr</p>
                                                </td>
                                                <td>
                                                    <p>One</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th colspan="3">
                                                    <p><strong>Open Area for Further Development</strong></p>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Open Area for New development</p>
                                                </td>
                                                <td>
                                                    <p>330 mtrs x 200 mtrs</p>
                                                </td>
                                                <td>
                                                    <p>One</p>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th colspan="3">
                                                    <p><strong>Crane and Handling Facility</strong></p>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>EOT Crane in Shed 1 </p>
                                                </td>
                                                <td colspan="2">
                                                    <p>25 MT – 3 no. </p>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>EOT Crane in Shed 2 </p>
                                                </td>
                                                <td colspan="2">
                                                    <p>15 Mt &ndash; 2 no</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Goliath Crane in Shed 3 </p>
                                                </td>
                                                <td colspan="2">
                                                    <p>40 Mt &ndash; 1no. 20 Mt &ndash; 1 noo</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Hydra </p>
                                                </td>
                                                <td colspan="2">
                                                    <p>10 nos</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Mobile crane </p>
                                                </td>
                                                <td colspan="2">
                                                    <p>300 MT &ndash; 2 no.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Manlift</p>
                                                </td>
                                                <td colspan="2">
                                                    <p>2nos</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-7 col-md-7">
                            <h4>Process & Capacity</h4>
                                <div class="table-responsive table-height">                                    
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <p><strong>Process</strong></p>
                                                </th>
                                                <th>
                                                    <p><strong>Installed</strong></p>
                                                </th>
                                                <th>
                                                    <p><strong>Capacity </strong></p>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <p>Cutting</p>
                                                </td>
                                                <td>
                                                    <p>Multi torch Oxyfuel CNC Cutting 4 mtr x 14 mtr</p>
                                                    <p>Multi Torch Oxyfuel CNC Cutting 3 mtr x 14 mtrs </p>
                                                    <p>Multi Torch Plasma / Oxyfuel CNC Cutting 4 mtr x 14 mtr</p>
                                                </td>
                                                <td>
                                                    <p>&nbsp;3000 MT</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Drilling</p>
                                                </td>
                                                <td>
                                                    <p>High Speed Plate drilling M/c &ndash; 1no.</p>
                                                    <p>Radial Drill machines &ndash; 3 nos</p>
                                                </td>
                                                <td>
                                                    <p>6000 holes/day + 8000 holes/day</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Welding &ndash; SAW</p>
                                                </td>
                                                <td>
                                                    <p>Gantry type Double Boom CNC SAW- 2 no</p>
                                                    <p>Portable Type SAW CNC machine - 12 nos</p>
                                                </td>
                                                <td>
                                                    <p>50,000 m per month</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Welding GMAW</p>
                                                </td>
                                                <td>
                                                    <p>IGBT based welding machines - 20 nos</p>
                                                </td>
                                                <td>
                                                    <p>15,000 mtr per month</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Straightening Machine</p>
                                                </td>
                                                <td>
                                                    <p>Roller hydraulic based straightening machine &ndash; 1 no</p>
                                                </td>
                                                <td>
                                                    <p>2500 MT per month</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>End Face Milling Machine</p>
                                                </td>
                                                <td>
                                                    <p>Vertical and horizontal milling machine</p>
                                                </td>
                                                <td>
                                                    <p>2500 MT per month</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>7 AXIS Drilling machine</p>
                                                </td>
                                                <td>
                                                    <p>2 nos Special High Speed 12 axis and 6 axis machine.</p>
                                                </td>
                                                <td>
                                                    <p>1000 MT per month</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Trial Assembly</p>
                                                </td>
                                                <td>
                                                    <p>2 spans of 130 mtr each at one time</p>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Blasting</p>
                                                </td>
                                                <td>
                                                    <p>Blasting shed with reclaimer</p>
                                                    <p>Blasting guns &ndash; 8 nos.</p>
                                                    <p>Hopper &ndash; 8 nos</p>
                                                </td>
                                                <td>
                                                    <p>2 spans of 100 mtr each at one time</p>
                                                    <p>&nbsp;2500 MT per month</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Metallizing</p>
                                                </td>
                                                <td>
                                                    <p>Oxygen based machines &ndash; 2nos</p>
                                                    <p>Air based machines &ndash; 4 nos</p>
                                                </td>
                                                <td>
                                                    <p>1200 sq mtrs a month</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Painting</p>
                                                </td>
                                                <td>
                                                    <p>Closed shed = 2 nos.</p>
                                                    <p>Painting guns</p>
                                                </td>
                                                <td>
                                                    <p>2000 sq mtr a month</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-two" role="tabpanel" aria-labelledby="pills-two-tab">
                        <div class="row justify-content-center">
                            <div class="col-lg-6 col-md-6">
                            <h4>Plant Area</h4>
                                <div class="table-responsive table-height">
                                   
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <p><strong>Area</strong></p>
                                                </th>
                                                <th>
                                                    <p><strong>Dimension</strong></p>
                                                </th>
                                                <th>
                                                    <p><strong>Capacity of production</strong></p>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th colspan="3">
                                                    <p><strong>Covered Shed and facility</strong></p>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Total Area covered under shed Fabrication</p>
                                                </td>
                                                <td>
                                                    <p>196 mtrs x 25 mtrs</p>
                                                    <p>90 mtr x 20 mtr</p>
                                                    <p>64 mtr x 20 mtr</p>
                                                </td>
                                                <td>
                                                    <p>2500 MT per month</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Blasting and painting Area</p>
                                                </td>
                                                <td>
                                                    <p>60 mtrs x 20 mtrs</p>
                                                    <p>40 mtrs x 30 mtrs</p>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <th colspan="3">
                                                    <p><strong>Trial Assembly Area for Bridges</strong></p>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Open web span</p>
                                                </td>
                                                <td>
                                                    <p>70 mtr x 15 mtr</p>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Open web / Bowstring girder span</p>
                                                </td>
                                                <td>
                                                    <p>130 mtr x 12 mtr</p>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <th colspan="3">
                                                    <p><strong>Crane and Handling Facility</strong></p>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Bay 1</p>
                                                </td>
                                                <td colspan="2">
                                                    <p>EOT Crane in Shed 1: 25 MT &ndash; 3 no. / 20 MT &ndash; 1 no.
                                                    </p>
                                                    <p>Trial area under Goliath crane &ndash; 40 MT</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Bay 2</p>
                                                </td>
                                                <td colspan="2">
                                                    <p>EOT Crane 15 mtr span &ndash; 4 no.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Bay 3</p>
                                                </td>
                                                <td colspan="2">
                                                    <p>EOT crane 25 mtr span &ndash; 2nos</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Blasting area</p>
                                                </td>
                                                <td colspan="2">
                                                    <p>Trolley &ndash; 30 MT &ndash; 2 nos + 2nos. 25 Mtr crane</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Painting Areao</p>
                                                </td>
                                                <td colspan="2">
                                                    <p>Mobile hydra - 15 mtr span- 30 MT &ndash; 6 no</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Mobile Handling </p>
                                                </td>
                                                <td>
                                                    <p>4 nos Gen II Hydra - 15 MT</p>
                                                </td>
                                                <td>
                                                    <p></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Crawler Crane</p>
                                                </td>
                                                <td colspan="2">
                                                    <p>40 mt &ndash; 1no. on hire</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                            <h4>Process & Capacity</h4>
                                <div class="table-responsive table-height">
                                    
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <p><strong>Machinery</strong></p>
                                                </th>
                                                <th>
                                                    <p><strong>Capacity</strong></p>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <p>Oxy fuel Cutting Systems &ndash; 2 nos</p>
                                                </td>
                                                <td>
                                                    <p>14 mtrs x 4 mtrs &ndash; 8 torch. 1200 MT/month x 2</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>HD Plasma cutting System &ndash; 3 nos</p>
                                                </td>
                                                <td>
                                                    <p>24 mtr x 4 mtr</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Pug Cutting</p>
                                                </td>
                                                <td>
                                                    <p>6 nos &ndash; 800 MT/month</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>H beam/ Box Beam Formation</p>
                                                </td>
                                                <td>
                                                    <p>1 no. line &ndash; 1500 MT fit up.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Submerged Arc Welding for H Beam and Box Beam</p>
                                                </td>
                                                <td>
                                                    <p>2 no. Gantry type</p>
                                                    <p>16 nos tractor type &ndash; Net 2000 MT /month</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>GMAW Welding Stations</p>
                                                </td>
                                                <td>
                                                    <p>30 nos</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Straightening Machine</p>
                                                </td>
                                                <td>
                                                    <p>1 nos</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>End Face Milling</p>
                                                </td>
                                                <td>
                                                    <p>1 nos.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>High Speed Plate Drilling Machine</p>
                                                </td>
                                                <td>
                                                    <p>2 nos &ndash; 2000 MT Drilling</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Core Cutters and Radial Drills</p>
                                                </td>
                                                <td>
                                                    <p>15 nos / 4 nos</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Plate Bending Machine</p>
                                                </td>
                                                <td>
                                                    <p>3.5 mtrs window. 3 mtrs long</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Pipe Bending Machine</p>
                                                </td>
                                                <td>
                                                    <p>5 nos</p>
                                                </td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Product Tabes End -->
            </div>
        </section>





    </main>



    <?php include './inc/footer.php';?>