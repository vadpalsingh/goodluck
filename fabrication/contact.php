<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India :: Contact</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>

    <style>

 

.contact_form {
  /* height: 100vh; */
  display: flex;
  justify-content: center;
  align-items: flex-start;
  background: #f8f8f8;
}
.contact_det {
    padding: 15px;
    margin-bottom: 30px;
    height: 180px;
}
.br_fight{
    border-right: 2px solid #dbdbdb5c;
}
.contact-form {
  width: 80vw;
  display: flex;
  justify-content: space-between;
  background: #fff;
  margin: 50px 0;
}
.contact-form > * {
  width: 50%;
}
.contact-form .first-container {
  /* background: linear-gradient(45deg, rgba(0, 0, 0, 0.8), rgba(0, 0, 0, 0.8)), url("https://colorlib.com/etc/cf/ContactFrom_v17/images/bg-01.jpg") center center/cover no-repeat;
  */
  background:linear-gradient(45deg, rgb(255 255 255 / 89%), rgb(255 255 255 / 98%)), url(https://colorlib.com/etc/cf/ContactFrom_v17/images/bg-01.jpg);
  display: flex;
  justify-content: center;
  align-items: center;
}
.contact-form .first-container .info-container div {
  margin: 24px 0;
}
.contact-form .first-container .info-container div h3 {
  color: #000;
  font-size: 22px;
  font-weight: 400;
  line-height: 1.2;
  padding-bottom: 00px;
  margin-bottom: 5px;
}
.contact-form .first-container .info-container div:first-of-type p {
  /* max-width: 260px; */
  color: #000;
}
.contact-form .first-container .info-container div p {
  font-size: 16px;
  line-height: 1.6;
  color: #000;
}
.contact-form .second-container {
  padding: 30px;
}
.contact-form .second-container h2 {
  font-size: 30px;
  font-weight: 400;
  color: #333;
  line-height: 1.2;
  /* text-align: center; */
  margin-bottom: 20px;
}
.contact-form .second-container form {
  display: flex;
  flex-direction: column;
}
.contact-form .second-container form .form-group {
  margin-bottom: 10px;
}
.contact-form .second-container form .form-group * {
  min-height: 55px;
  border: 1px solid #e6e6e6;
  padding: 0 20px;
}
.contact-form .second-container form .form-group label {
  display: flex;
  align-items: center;
  width: 100%;
  border: 1px solid #e6e6e6;
  font-size: 16px;
  color: #333;
  text-transform: uppercase;
  margin-top: -1px;
}
.contact-form .second-container form .form-group:first-of-type input {
  width: 50.1%;
  margin-right: -5px;
}
.contact-form .second-container form .form-group input {
  width: 100%;
  font-size: 15px;
  margin-top: -2px;
}
.contact-form .second-container form .form-group input::placeholder, .contact-form .second-container form .form-group textarea::placeholder {
  color: #999;
}
.contact-form .second-container form .form-group textarea {
  width: 100%;
  min-height: 80px;
  resize: none;
  padding: 10px 20px;
  margin-top: -1px;
}
.contact-form .second-container form button {
  width: 200px;
  height: 50px;
  background: #ff8b01;
  color: #fff;
  font-size: 17px;
  font-weight: 600;
  text-transform: uppercase;
  border: 0;
  position: relative;
  left: calc(50% - 100px);
  cursor: pointer;
}
.info-container {
    width: 60%;
}
.contact-form .second-container form button:hover {
  background: #333;
}
.contact_det:hover{
    background-color: #000;
    box-shadow: 0px 0px 0px 1px #000;
}
.contact_det:hover {
    background-color: #fff;
    box-shadow: 0px 0px 4px 1px #00000021;
    transition: all 0.3s ease-out 0s;
    transform: translate(0px, -10px);
}
.contact_det h3 {
    border-bottom: 1px solid #cecece;
    padding-bottom: 8px;
    margin-bottom: 12px;
    display: inline-block;
    font-size: 1.3rem;
}
@media screen and (max-width: 800px) {
  .contact-form {
    width: 90vw;
  }
}
@media screen and (max-width: 700px) {
  .contact-form {
    flex-direction: column-reverse;
  }
  .contact-form > * {
    width: 100%;
  }
  .contact-form .first-container {
    padding: 40px 0;
  }
}
    </style>
    <main>
    <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item  active ">
                        <img src="../assets/img/about/cont.png" class="d-block w-100" alt="...">
                        <h5>Infrastructure & Energy Division </h5>
                   
                    </div>
                </div>

            </div>
        </section>

        <section>
            <div class="contact_form">
        <div class="contact-form">
      <div class="first-container">
        <div class="info-container">
            <!-- <div><h2 class="text-dark">Goodluck India Limited</h2></div> -->
          <div><img class="icon"/>
            <h3> Corporate Office :-</h3>
            <p class="m-0">Goodluck India Limited</p>
<p class="m-0">CIN No. :  L74899DL1986PLC050910</p>
            <p> Good Luck House, II F, 166-167, Nehru Nagar 2, Ambedkar Road, Phone Number: 01204196600, 01204196700
Ghaziabad - 201001, Uttar Pradesh, India</p>

          </div>
          <div> <img class="icon"/>
            <h3>Contact Us:-</h3>
            <p>+91-0120-4196600, 4196700</p>
          </div>
          <div><img class="icon"/>
            <h3>Email :-</h3>
            <p>goodluck@goodluckindia.com</p>
          </div>
        </div>
      </div>
      <div class="second-container">
        <h2 class="text-left">Business Enquiry Form</h2>
        <form>
          <div class="form-group">
            <label for="name-input">Tell us your name*</label>
            <input id="name-input" type="text" placeholder="First name" required="required"/>
            <input type="text" placeholder="Last name" required="required"/>
          </div>
          <div class="form-group">
            <label for="email-input">Enter your email*</label>
            <input id="email-input" type="text" placeholder="Enter Your Email" required="required"/>
          </div>
          <div class="form-group">
            <label for="phone-input">Enter phone number*</label>
            <input id="phone-input" type="text" placeholder="Enter Your Number" required="required"/>
          </div>
          <div class="form-group">
            <label for="message-textarea">Message</label>
            <textarea id="message-textarea" placeholder="Write us a message"></textarea>
          </div>
          <button>Send message</button>
        </form>
      </div>
    </div>
    </div>
        </section>
        <section class="pt-5 pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-lg-6 col-xl-4">
                        <div class="contact_det br_fight">
                            <h3><b>Regd. Office :</b> <span style="color:#ff8b01">(Goodluck India Ltd)</span></h3>
                            <p>509, Arunachal Building, Barakhamba Road,
                                Connaught Place, -2pxNew Delhi - 110001 (INDIA)</p>
                                <p><b>CIN No. :</b> L74899DL1986PLC050910</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-6 col-xl-4">
                        <div class="contact_det br_fight">
                            <h3><b>Goodluck India Limited. <span style="color:#ff8b01">(Works)</span></b></span></h3>
                            <p>A-42/45, Industrial Area, Sikandrabad,<br>
Distt. Bulandshahr - 203205 (U.P.) INDIA</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-6 col-xl-4">
                        <div class="contact_det  ">
                            <h3><b>Goodluck India Limited. <span style="color:#ff8b01">(Works II)</span></b> </h3>
                            <p>D-2/3/4, Industrial Area, Sikandrabad,<br>
Distt. Bulandshahr - 203205 (U.P.) INDIA</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-6 col-xl-4">
                        <div class="contact_det  br_fight">
                            <h3><b>Good Luck Industries <span style="color:#ff8b01">(Works)</span></b> </h3>
                            <p>A-51, Industrial Area, Sikandrabad,<br>
Distt.-Bulandshahr - 203205 (U.P.) INDIA</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-6 col-xl-4">
                        <div class="contact_det br_fight ">
                            <h3><b>Good Luck Industries-II <span style="color:#ff8b01">(Works)</span> </b> </h3>
                            <p>A-59, Industrial Area, Sikandrabad,<br>
Distt.-Bulandshahr - 203205 (U.P.) INDIA</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-6 col-xl-4">
                        <div class="contact_det  ">
                            <h3><b>Good Luck Engineering <span style="color:#ff8b01">(Works)</span></b> </h3>
                            <p>Khasra No. 2839, Gram Dhoom Manikpur,
G.T. Road , Gautam Budh Nagar, Dadri, (U.P.) INDIA</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-6 col-xl-4">
                        <div class="contact_det br_fight ">
                            <h3><b>Goodluck Metallics <span style="color:#ff8b01">(Works)</span></b>  </span></h3>
                            <p>Survey No. 495,497 to 502, Bachau Sikra Road, Village-Sikra, T A Bachau, Gujarat - 370140, INDIA</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

     </main>
     <?php include './inc/footer.php';?>