<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India Limited (Infrastructure & Energy Division) :: Substation Structure</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <main>
        <!-- hero -->
        <section class="good_luck">
            <div id="carouselExample" class="carousel slide banner__slider" data-bs-ride="carousel" data-pause="false">
                <div class="carousel-inner cdwtubesbanner">
                    
                    <div class="carousel-item active">
                        <h5>Substation Structure</h5>
                        <img src="../assets/fabrication/substation-structure-banner.jpg" class="d-block w-100"
                            alt="Mumbai–Ahmadabad High Speed Rail Corridor (MAHSR)">
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        </div>
                    </div>

                </div>

            </div>
        </section>
        <!-- hero -->
       
     
        <!-- brand__area start -->
        <section class="brand__area light-grey p60" style="
    background: #315f81;
">
            <div class="container">                
            <div class="row justify-content-center">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/substationstructure.jpg"
                                            alt="substation"></a>
                                </div>
                               
                            </div>
                </div>
                    
              
            </div>
        </section>
        <!-- brand__area end -->
    </main>
    <?php include './inc/footer.php';?>