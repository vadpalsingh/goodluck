<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India Limited (Infrastructure & Energy Division) :: Building Structure </title>
    <meta name="description" content="">

    <?php include './inc/header.php'; ?>


    <main>

        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item active">
                        <img src="../assets/fabrication/buildingstructure-banner.jpg" class="d-block w-100" alt="...">
                        <h5 class="wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">Building Structure</h5>
                        <div class="carousel-caption d-none d-md-block bg_tr wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">
                            <h4>STRUCTURING YOUR BUILDING STRUCTURES </h4>
                        </div>
                    </div>
                </div>
                <!-- <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button> -->
            </div>
        </section>
        <!-- hero -->

        <section class="p60 leadership light-grey">
            <div class="container">
                <div class="row row-design justify-content-center align-items-center">
                    <div class="col-lg-5 col-md-6">
                        <img src="../assets/fabrication/buildingstructure/1.jpg" class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>AIRPORTS AND CANOPIES</h3>
                            <ul class="liststyle">
                                <li><strong>Building &amp; Factories</strong></li>
                                <li><strong>Power Boiler Primary and Secondary Structures</strong></li>
                                <li><strong>Roof Trusses for Airports and Bus Stands</strong></li>
                                <li><strong>Solar Module Mounted Renewable structures </strong></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row row-reverse justify-content-center align-items-center mt-4">
                    <div class="col-lg-5 col-md-6">
                        <img src="../assets/fabrication/buildingstructure/2.jpg" class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>COMMERCIAL BUILDING</h3>
                            <p>Today’s buildings need structural strength because of development of structures in
                                seismic zones and also need architectural brilliance. Built up sections of beams are
                                hence in demand. At Goodluck we make tailor made sections with finishes as required.
                                Prefabricated concretes have been replaced with steel structures in many places. Today
                                in India there are buildings made of complete steel frames, tubes, pipes and beams.
                                Builders are promoting such structures based on better stability and form as well as
                                aesthetic looks. Goodluck India provides to your such requirements. From pipes, tubes,
                                and beams.</p>
                        </div>
                    </div>
                </div>

                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="col-lg-5 col-md-6">
                        <img src="../assets/fabrication/buildingstructure/3.jpg" class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>PLATFORM</h3>
                            <p>Platforms and landing need strong supports, such structures are in form of I sections or
                                Box Girders to support the heavy foundation above. Goodluck Steels takes care of the
                                primary support needs.</p>
                        </div>
                    </div>
                </div>

                <div class="row row-reverse justify-content-center align-items-center mt-4">
                    <div class="col-lg-5 col-md-6">
                        <img src="../assets/fabrication/buildingstructure/4.jpg" class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>ROOF TRUSS OF CONVENTION CENTRES & EXHIBITION</h3>
                            <p>Roof Trusses are made from tubular structures and pipes with good finish. Goodluck Steels
                                quality manufacturing process ensures your requirement of such trusses. We can even
                                provide you galvanized trusses. Building scaffolding trusses as well as usage of tubular
                                or bent sections are very extensively used. At GoodLuck we make our own ERW pipes .</p>
                        </div>
                    </div>
                </div>

                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="col-lg-5 col-md-6">
                        <img src="../assets/fabrication/buildingstructure/5.jpg" class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>BOILER SUPPORT STRUCTURE</h3>
                            <ul class="liststyle">
                                <li><strong>Primary Structures </strong></li>
                                <li><strong>Secondary Structures </strong></li>
                                <li><strong>Equipment structures</strong></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row row-reverse justify-content-center align-items-center mt-4">
                    <div class="col-lg-5 col-md-6">
                        <img src="../assets/fabrication/buildingstructure/6.jpg" class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>CONVEYOR & OTHER HANDLING STRUCTURE</h3>
                            <p>Conveyor Galleries, Trestles, Gantries and several other equipment structures are fabricated at Goodluck India. We are approved vendors for NTPC and all State Boards .</p>
                        </div>
                    </div>
                </div>

                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="col-lg-5 col-md-6">
                        <img src="../assets/fabrication/buildingstructure/9.jpg" class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>STATION BUILDINGS FOR HIGH SPEED RAIL & MRTS </h3>
                            <p>We are executing the Coveted National High Speed railway Project by fabrication of Special Super Structure Open Web Steel Bridges designed by Japanese engineering team.</p><p>Other major areas of development are Defense artillery and launching vehicle systems, Aeronautical and aerospace requirements of  Fabricated structure. </p>
                        </div>
                    </div>
                </div>

              
            </div>
        </section>


    </main>



    <?php include './inc/footer.php';?>