<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India Limited (Infrastructure & Energy Division) :: Approvals</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <main>
        <!-- hero -->
        <section class="good_luck">
            <div id="carouselExample" class="carousel slide banner__slider" data-bs-ride="carousel" data-pause="false">
                <div class="carousel-inner cdwtubesbanner">
                    
                    <div class="carousel-item active">
                        <h5>Approvals</h5>
                        <img src="../assets/fabrication/approvals-banner.jpg" class="d-block w-100"
                            alt="Mumbai–Ahmadabad High Speed Rail Corridor (MAHSR)">
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        </div>
                    </div>

                </div>

            </div>
        </section>
        <!-- hero -->
       
     
        <!-- brand__area start -->
        <section class="brand__area light-grey p60">
            <div class="container">                
            <div class="row">
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl1.png"
                                            alt="approvals"></a>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl3.png"
                                            alt="approvals"></a>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl4.png"
                                            alt="approvals"></a>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl5.png"
                                            alt="approvals"></a>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl6.png"
                                            alt="approvals"></a>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl7.png"
                                            alt="approvals"></a>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl8.png"
                                            alt="approvals"></a>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl9.png"
                                            alt="approvals"></a>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl10.png"
                                            alt="approvals"></a>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl11.png"
                                            alt="approvals"></a>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl12.png"
                                            alt="approvals"></a>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl13.png"
                                            alt="approvals"></a>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl14.png"
                                            alt="approvals"></a>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl15.png"
                                            alt="approvals"></a>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl16.png"
                                            alt="approvals"></a>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="javascript:void(0)"><img src="../assets/fabrication/approvals/aprvl17.png"
                                            alt="approvals"></a>
                                </div>
                            </div>
                </div>
                    
              
            </div>
        </section>
        <!-- brand__area end -->
    </main>
    <?php include './inc/footer.php';?>