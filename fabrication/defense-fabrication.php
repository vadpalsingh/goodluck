<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India Limited (Infrastructure & Energy Division) :: Defense Fabrication </title>
    <meta name="description" content="">

    <?php include './inc/header.php'; ?>


    <main>

        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item active">
                        <img src="../assets/fabrication/defense-fabrication-banner.jpg" class="d-block w-100" alt="...">
                        <h5 class="wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">Defense Fabrication</h5>
                        <div class="carousel-caption d-none d-md-block bg_tr wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">

                        </div>
                    </div>
                </div>
                <!-- <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button> -->
            </div>
        </section>
        <!-- hero -->

        <section class="p60 leadership light-grey">
            <div class="container">
                <div class="row row-reverse justify-content-center align-items-center">
                    <div class="col-lg-5 col-md-6">
                        <img src="../assets/fabrication/defense-fabrication/1.jpg" class="wow fadeInUp"
                            data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Defense Fabrication</h3>
                            <p>At Goodluck India , we are passionate about manufacturing state-of-the-art military
                                equipment and systems that fortify nations and protect the future. Our dedication to
                                excellence, innovation, and precision engineering sets us apart as a trusted leader in
                                the defense industry.</p>

                        </div>
                    </div>
                </div>
                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Types of Defense Fabrication</h3>
                            <p><strong>Discover the diversity of our defense fabrication solutions, ranging
                                    from</strong></p>
                            <ul class="row boxview">
                                <li class="col-lg-3 col-md-6 col-sm-6 col-12"><img
                                        src="../assets/fabrication/defense-fabrication/type/1.jpg"><strong>Cutting-edge
                                        Armored vehicles</strong></li>
                                <li class="col-lg-3 col-md-6 col-sm-6 col-12"><img
                                        src="../assets/fabrication/defense-fabrication/type/2.jpg"><strong>aircraft
                                        components</strong></li>
                                <li class="col-lg-3 col-md-6 col-sm-6 col-12"><img
                                        src="../assets/fabrication/defense-fabrication/type/3.jpg"><strong>and naval
                                        vessels to advanced electronic systems</strong></li>
                                <li class="col-lg-3 col-md-6 col-sm-6 col-12"><img
                                        src="../assets/fabrication/defense-fabrication/type/4.jpg"><strong>firearms, and
                                        munitions</strong></li>
                            </ul>
                            <p>We specialize in developing support systems for radar , communication equipment, and
                                surveillance devices that redefine military capabilities.</p>
                        </div>
                    </div>
                </div>

                <div class="row row-reverse justify-content-center align-items-center mt-4">
                    <div class="col-lg-5 col-md-6">
                        <img src="../assets/fabrication/defense-fabrication/2.jpg" class="wow fadeInUp"
                            data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3 class="text-left">Applications and Uses</h3>
                            <p>Our defense fabrication products find applications across diverse military operations,
                                from land-based to air and naval missions. By incorporating state-of-the-art
                                technologies, we enhance military efficiency, situational awareness, and overall defense
                                effectiveness, empowering our clients to defend with confidence.</p>
                        </div>
                    </div>
                </div>

                <div class="row row-design justify-content-center align-items-center mt-4">

                    <div class="col-lg-5 col-md-6">
                        <img src="../assets/fabrication/defense-fabrication/3.jpg" class="wow fadeInUp"
                            data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Indian Defense Perspective</h3>
                            <p>With a deep-rooted commitment to India’s defense aspirations, actively supports ‘Make in
                                India’ initiatives, bolstering the nation’s indigenous defense capabilities. Our
                                contributions are integral to securing India’s sovereignty and equipping our armed
                                forces with the best-in-class defense solutions.</p>
                        </div>
                    </div>
                </div>

                <div class="row row-reverse justify-content-center align-items-center mt-4">
                    <div class="col-lg-5 col-md-6">
                        <img src="../assets/fabrication/defense-fabrication/4.jpg" class="wow fadeInUp"
                            data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3 class="text-left">Export Opportunities for Our Company</h3>
                            <p>As an industry leader in defense fabrication, Goodluck is positioned to explore global
                                defense opportunities. We proudly showcase India’s capabilities on the international
                                stage, driving the nation’s defense exports and making a mark as a global player in the
                                defense sector.</p>

                            <p><strong>Explore Goodluck India - Your Trusted Defense Fabrication Partner</strong></p>
                        </div>
                    </div>
                </div>


            </div>
        </section>


    </main>



    <?php include './inc/footer.php';?>