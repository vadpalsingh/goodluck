<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India Limited (Infrastructure & Energy Division) :: Railway and Road Bridges </title>
    <meta name="description" content="">

    <?php include './inc/header.php'; ?>


    <main>

        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item active">
                        <img src="../assets/fabrication/railway-and-road-bridges-banner.jpg" class="d-block w-100"
                            alt="...">
                        <h5 class="wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">Railway and Road Bridges</h5>
                        <div class="carousel-caption d-none d-md-block bg_tr wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">
                            <h4></h4>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- hero -->

        <section class="p60 leadership light-grey">
            <div class="container">
                <div class="row row-design justify-content-center align-items-center">

                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Steel Girder/ROB/Foot Over Bridge/ Metro Stations</h3>
                            <p>The use of Plate Girders gives scope to vary the girder sections to suit the loads
                                carried at different positions along the bridge. The designer is free to choose the
                                thickness of web and size of flange to suit the internal forces at different positions
                                along the length of the span, though it must be remembered that too many changes may not
                                lead to economy, because of the additional fabrication work. Splices are expensive,
                                whether bolted or welded. Often, the girders have parallel flanges, that is, they have a
                                constant depth. However, with plate girders, the designer can also choose to vary the
                                depth of the girder along its length. For longer spans it is quite common to increase
                                the girder depth over intermediate supports. For spans below about 50m, the choice
                                (constant or varying depth) is often governed by aesthetics. Above 50m, varied depth may
                                offer economy because of the weight savings possible in mid span regions. The variation
                                in depth can be achieved either by straight launching (tapered girders) or by curving
                                the bottom flange. The shaped web, either for a variable depth or constant depth girder
                                with a vertical camber, is easily achieved by profile cutting during fabrication.
                                Generally, webs have a high depth/thickness ratio and this leads to the need for
                                intermediate transverse web stiffeners in regions of high shear (near supports)</p>

                            <p><strong>AT Goodluck we have fabricated over 10000 MT of Steel Plate girders for the
                                    following applications</strong></p>
                            <ul class="row boxview mb-2">
                                <li class="col-lg-3 col-md-6 col-sm-6 col-12">
                                    <img src="../assets/fabrication/railway-and-road-bridges/row1/1.jpg"><strong>Road
                                        Over Bridges from Spans 5 mtr to 49.5 mtrs </strong>
                                </li>
                                <li class="col-lg-3 col-md-6 col-sm-6 col-12">
                                    <img src="../assets/fabrication/railway-and-road-bridges/row1/2.jpg"><strong>Railway
                                        Over Bridges from Spans 5 mtr tov 49 mtrs </strong>
                                </li>
                                <li class="col-lg-3 col-md-6 col-sm-6 col-12">
                                    <img src="../assets/fabrication/railway-and-road-bridges/row1/3.jpg"><strong>Foot
                                        Over Bridges of Spans 30 mtrs to 40 on railway stations and roads</strong>
                                </li>
                                <li class="col-lg-3 col-md-6 col-sm-6 col-12">
                                    <img src="../assets/fabrication/railway-and-road-bridges/row1/4.jpg"><strong>Link
                                        Bridges for Metros</strong>
                                </li>
                            </ul>
                            <p>Primary Structures like Crane Beams and Buck Stays. Composite Plate Girders made
                                of Special Steels of grade 250 and 350 C and 410 B0 with impact values of 54
                                Joules at Minus 20 Deg C.</p>
                            <p>Normal Plate Girders of RDSO Spans of Raw Material Grade 350 B0 are regularly
                                being used.<br>Our Supplies include topographies of altitude above 2500 mtr from
                                Sea level and across river Roads Over Bridges have been supplied over canals and
                                viaduct.</p>

                        </div>
                    </div>
                </div>


                <div class="row row-reverse justify-content-center align-items-center mt-4">
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Open Web through Truss Bridges </h3>

                            <p><strong>Following Open Web Through Truss Girders are designed by RDSO </strong></p>
                            <ul class="row boxview mb-2">
                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">
                                    <img src="../assets/fabrication/railway-and-road-bridges/row2/1.jpg">
                                    <strong>Open Web Girders (B.G)</strong>
                                </li>
                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">
                                    <img src="../assets/fabrication/railway-and-road-bridges/row2/2.jpg">
                                    <strong>Open Web Girders (M.G)</strong>
                                </li>
                                <li class="col-lg-4 col-md-6 col-sm-6 col-12">
                                    <img src="../assets/fabrication/railway-and-road-bridges/row2/3.jpg">
                                    <strong>Open Web Girders (Welded)</strong>
                                </li>
                            </ul>
                            <p>Open Web Girders are light weight and the latest in technology of Girders using less
                                steel weight yet absorbing double axle loads for MBG loading.</br>
                                At GoodLuck India we have fabricated more than 30 spans of OWG for spans ranging from 31
                                mtrs to 63 mtrs.</p>
                            <p>We at GoodLuck have expert manpower and machinery to achieve perfectness in the component
                                fabrication of OWG - like Bottom Chord and Top Chord straightness and Correctly aligned
                                Stringers and Verticals. Our Expertise is in providing right fit and camber correct OWG.
                                Same is verified at trial assembly at our works . A large trial assembly area makes us
                                capable to hold spans upto 100 mtrs.</p>
                            <p>Bow String Girders We have Orders for 10 Bow strip girders to be supplied and erected .
                                We have expertise to fabricate bolted and welded designs from our workshop, Our Expert
                                erection team can execute critical projects which require site welding of sections</p>
                        </div>
                    </div>
                </div>

                <div class="row row-design justify-content-center align-items-center mt-4">
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>Bow String Steel Girder</h3>
                            <p>As a reputable steel girder manufacturer, we take pride in offering innovative solutions
                                for bridge construction projects.<br> Our expertise lies in crafting bowstring steel
                                girders, a distinct and efficient structural system that combines strength, stability,
                                and aesthetic appeal.</p>

                            <div class="row row-design light-grey justify-content-center box-shadow-none ptb-0 align-items-center mt-2">
                                <div class="col-lg-6 col-md-6">
                                    <div class="ribbon imgrbnleft red"><span>Type</span></div>
                                    <img src="../assets/fabrication/railway-and-road-bridges/row3/1.jpg"
                                        class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                        <p class="text-left font-20"><strong>Riveted Bow string Girder</strong>
                                        </p>
                                        <ul class="liststyle">
                                            <li>Embodying the legacy of bridge-building craftsmanship, our riveted
                                                bowstring girders showcase classic charm and historical significance.
                                            </li>
                                            <li>Each riveted connection is meticulously crafted, reflecting the artistry
                                                and attention to detail of the past.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div
                                class="row row-reverse light-grey box-shadow-none ptb-0 justify-content-center align-items-center mt-4">
                                <div class="col-lg-6 col-md-6">
                                    <div class="ribbon imgrbnright red"><span>Type</span></div>
                                    <img src="../assets/fabrication/railway-and-road-bridges/row3/2.jpg"
                                        class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">
                                        <p class="text-left font-20"><strong>Welded Bow string Girder</strong></p>
                                        <ul class="liststyle">
                                            <li>Leveraging cutting-edge welding techniques, our welded bowstring girders
                                                exemplify modern engineering standards.</li>
                                            <li>Welded connections ensure superior structural integrity, durability, and
                                                ease of fabrication, reducing maintenance costs over the bridge's
                                                lifespan.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="row row-reverse light-grey  box-shadow-none ptb-0 justify-content-center align-items-center mt-4">
                                <div class="col-lg-12 col-md-12">
                                    <div class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">

                                        <p class="font-20"><strong>Uses </strong></p>
                                        <p>Our bowstring steel girders find versatile applications in bridge projects,
                                            catering to various transportation needs</p>
                                        <ul class="row boxview mb-2">
                                            <li class="col-lg-4 col-md-6 col-sm-6 col-12 bg-white border-1-grey">
                                                <img src="../assets/fabrication/railway-and-road-bridges/row4/1.jpg">
                                                <strong>Long Span Bridges</strong>For projects requiring long spans, our
                                                bowstring girders deliver optimal load distribution and minimize
                                                deflection, ensuring robust and stable structures.
                                            </li>
                                            <li class="col-lg-4 col-md-6 col-sm-6 col-12 bg-white border-1-grey">
                                                <img src="../assets/fabrication/railway-and-road-bridges/row4/2.jpg">
                                                <strong>Highway Overpasses</strong>Constructing lightweight and durable
                                                highway overpasses is made possible with our bowstring girders,
                                                providing efficient load-bearing capabilities.
                                            </li>
                                            <li class="col-lg-4 col-md-6 col-sm-6 col-12 bg-white border-1-grey">
                                                <img src="../assets/fabrication/railway-and-road-bridges/row4/3.jpg">
                                                <strong>Railway Viaducts</strong>We provide the strength and reliability
                                                needed for railway viaducts crossing vast distances and challenging
                                                terrains, ensuring safe and smooth train passage.
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-reverse light-grey  box-shadow-none ptb-0 justify-content-center align-items-center mt-4">
                                <div class="col-lg-12 col-md-12">
                                    <div class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">

                                        <p class="font-20"><strong>Advantages </strong></p>
                                        <ul class="row boxview mb-2">
                                            <li class="col-lg-3 col-md-3 col-sm-6 col-12 bg-white border-1-grey"><strong>Seamless
                                                    Aesthetics</strong>Our bowstring girders deliver an elegant, arching
                                                profile that enhances the bridge's visual appeal, adding value to the
                                                surrounding landscape.</li>

                                            <li class="col-lg-3 col-md-3 col-sm-6 col-12 bg-white border-1-grey"><strong>Optimized Load
                                                    Distribution</strong>The arched design efficiently distributes loads
                                                along the span, reducing stress concentrations and enhancing overall
                                                structural performance.</li>

                                            <li class="col-lg-3 col-md-3 col-sm-6 col-12 bg-white border-1-grey"><strong>Stability and
                                                    Durability</strong>Our girders are engineered for inherent
                                                stability, minimizing the need for additional bracing and ensuring
                                                long-lasting, resilient structures.</li>

                                            <li class="col-lg-3 col-md-3 col-sm-6 col-12 bg-white border-1-grey"><strong>Prefabrication
                                                    Excellence</strong>We excel in off-site prefabrication, enabling
                                                streamlined assembly, faster construction, and reduced on-site
                                                disruption.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="row row-reverse light-grey  box-shadow-none ptb-0 justify-content-center align-items-center mt-4">
                                <div class="col-lg-12 col-md-12">
                                    <div class="wow fadeInUp" data-wow-delay=".3s"
                                        style="visibility: visible; animation-delay: 0.3s;">

                                        <p class="font-20"><strong>Challenges </strong></p>
                                        <ul class="row boxview mb-2">
                                            <li class="col-lg-6 col-md-6 col-sm-6 col-12 bg-white border-1-grey"><strong>Precision Fabrication</strong>Our skilled team embraces the challenges of crafting precise bowstring girders, ensuring each piece meets exact specifications for a seamless fit during assembly.</li>

                                            <li class="col-lg-6 col-md-6 col-sm-6 col-12 bg-white border-1-grey"><strong>Efficient Transportation and Erection</strong>We employ meticulous planning and cutting-edge equipment to overcome logistical challenges during transportation and erection, ensuring efficient project execution.</li>
                                           
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row row-reverse justify-content-center align-items-center mt-4">
                    <div class="col-lg-12 col-md-12">
                        <div class="wow fadeInUp" data-wow-delay=".3s"
                            style="visibility: visible; animation-delay: 0.3s;">
                            <h3>In summary</h3>
                            <p>our bowstring steel girders embody a perfect blend of tradition and innovation, making them an ideal choice for your bridge construction projects. As a manufacturer committed to quality and craftsmanship, we offer reliable solutions that exemplify engineering excellence, durability, and captivating aesthetics.</p>
                        </div>
                    </div>
                </div>

            </div>
        </section>


    </main>



    <?php include './inc/footer.php';?>