<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India Limited (Infrastructure & Energy Division) :: Complete Projects</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <main>
        <!-- hero -->
        <section class="good_luck">
            <div id="carouselExample" class="carousel slide banner__slider" data-bs-ride="carousel" data-pause="false">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item active">
                        <h5>Completed Projects</h5>
                        <img src="../assets/fabrication/complete-banner.jpg" class="d-block w-100"
                            alt="Mumbai–Ahmadabad High Speed Rail Corridor (MAHSR)">
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                            <h4></h4>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- hero -->

        <!-- counter -->
        <!-- service -->
        <section class="brand__area tabproduct p60">
            <div class="container">
                <!-- Product Tabes -->
                <ul class="nav nav-pills  justify-content-center" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pills-one-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-one" type="button" role="tab" aria-controls="pills-one"
                            aria-selected="true">Fabricated Structure</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-two-tab" data-bs-toggle="pill" data-bs-target="#pills-two"
                            type="button" role="tab" aria-controls="pills-two" aria-selected="false">Transmission &
                            Telecom</button>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-one" role="tabpanel"
                        aria-labelledby="pills-one-tab">
                        <div class="row row-reverse justify-content-center align-items-center mt-4">
                            <div class="col-lg-5 col-md-6">
                                <img src="../assets/fabrication/complete-project/A/1.jpg" class="wow fadeInUp"
                                    data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;width:100%">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="wow fadeInUp" data-wow-delay=".3s"
                                    style="visibility: visible; animation-delay: 0.3s;">
                                    <ul class="liststyle text-left">
                                        <li><strong>PROJECT: </strong> Delhi - Meerut Regional Rapid Transit System
                                            (RRTS) Corridor.</li>
                                        <li><strong>Client: </strong> National Capital Region Transport Corporation.
                                        </li>
                                        <li><strong>Contractor: </strong> Afcons Infrastructure ltd.</li>
                                        <li><strong>Type of bridge: </strong> Bow String Girder (M.S. High tensile Steel
                                            Bridge).</li>
                                        <li><strong>Bridge length: </strong> 70Mtr, 50Mtr.</li>
                                        <li><strong>Total weight: </strong> 2806 MT.</li>
                                        <li><strong>Duration: </strong> 2022-2023</li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="row row-reverse justify-content-center align-items-center mt-4">
                            <div class="col-lg-5 col-md-6">
                                <img src="../assets/fabrication/complete-project/A/2.jpg" class="wow fadeInUp"
                                    data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;width:100%">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="wow fadeInUp" data-wow-delay=".3s"
                                    style="visibility: visible; animation-delay: 0.3s;">
                                    <ul class="liststyle text-left">
                                        <li><strong>PROJECT:&nbsp;</strong> L&amp;T -CMRL-C5-ECV 02 (Chennai Metro)</li>
                                        <li><strong>Client:</strong>&nbsp; Chennai Metro Rail Ltd.</li>
                                        <li><strong>Contractor:</strong> L&amp;T Construction</li>
                                        <li><strong>Type of bridge:</strong> composite girder Curve span (M.S. High
                                            tensile Steel Bridge).</li>
                                        <li><strong>Bridge length:</strong> 56Mtr, 49.9Mtr, 44.39Mtr, 50.9Mtr, 32.7Mtr,
                                            37.9Mtr, 38.454Mtr, 37.9Mtr.</li>
                                        <li><strong>Total weight:</strong> 1891MT.</li>
                                        <li><strong>Duration:</strong> - 023-2024</li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="row row-reverse justify-content-center align-items-center mt-4">
                            <div class="col-lg-5 col-md-6">
                                <img src="../assets/fabrication/complete-project/A/3.jpg" class="wow fadeInUp"
                                    data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;width:100%">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="wow fadeInUp" data-wow-delay=".3s"
                                    style="visibility: visible; animation-delay: 0.3s;">
                                    <ul class="liststyle text-left">
                                        <li><strong>PROJECT: </strong> Rail Project Sonnagar to Japla PKG-1 in Mughal
                                            Sarai Division)</li>
                                        <li><strong>Client:</strong> Rail Vikas Nigam Ltd.</li>
                                        <li><strong>Contractor:</strong> - Ashoka buildcon Ltd.</li>
                                        <li><strong>Type of bridge:</strong> Open Web Girder (M.S. Steel Bridge).</li>
                                        <li><strong>Bridge length:</strong> 72Mtr</li>
                                        <li><strong>Total weight:</strong> 540MT.</li>
                                        <li><strong>Duration:</strong> 2022-2023.</li>

                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="row row-reverse justify-content-center align-items-center mt-4">
                            <div class="col-lg-5 col-md-6">
                                <img src="../assets/fabrication/complete-project/A/4.jpg" class="wow fadeInUp"
                                    data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;width:100%">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="wow fadeInUp" data-wow-delay=".3s"
                                    style="visibility: visible; animation-delay: 0.3s;">
                                    <ul class="liststyle text-left">
                                        <li><strong>PROJECT:</strong> Project Lucknow Ring Road CH. 29+800 amausi (AMS)
                                            and piparasand (PPR) station.</li>
                                        <li><strong>Client:</strong> &nbsp;National highway authority of India.</li>
                                        <li><strong>Contractor:</strong> &nbsp;Pnc Infratech Ltd.</li>
                                        <li><strong>Type of bridge:</strong> Bowstring Girder (M.S. High tensile Steel
                                            Bridge).</li>
                                        <li><strong>Bridge length:</strong> 56Mtr</li>
                                        <li><strong>Total weight:</strong> 1290MT.</li>
                                        <li><strong>Duration:</strong> &nbsp;2022-2023.</li>

                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="row row-reverse justify-content-center align-items-center mt-4">
                            <div class="col-lg-5 col-md-6">
                                <img src="../assets/fabrication/complete-project/A/5.jpg" class="wow fadeInUp"
                                    data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;width:100%">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="wow fadeInUp" data-wow-delay=".3s"
                                    style="visibility: visible; animation-delay: 0.3s;">
                                    <ul class="liststyle text-left">
                                        <li><strong>PROJECT:</strong> &nbsp;Delhi - Meerut Regional Rapid Transit System
                                            (RRTS) Corridor.</li>
                                        <li><strong>Client:</strong> &nbsp;National Capital Region transport
                                            Corporation.</li>
                                        <li><strong>Contractor:</strong> &nbsp;Apco Infratech Ltd.</li>
                                        <li><strong>Type of bridge:</strong> Composite Girder (M.S. High tensile Steel
                                            Bridge).</li>
                                        <li><strong>Bridge length:</strong> &nbsp;45Mtr.</li>
                                        <li><strong>Total weight:</strong> &nbsp;758MT.</li>
                                        <li><strong>Duration:</strong> &nbsp;2021-2022</li>

                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-two" role="tabpanel" aria-labelledby="pills-two-tab">
                        <div class="row row-design justify-content-center align-items-center mt-4">
                            <div class="col-lg-5 col-md-6">
                                <img src="../assets/fabrication/complete-project/B/1.jpg" style="visibility: visible; animation-delay: 0.1s; width:100%">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="">
                                    <p class="text-left font-20"><strong>PROJECTS: </strong> HARTEK AMPULS SOLAR BIKANER RAJASTHAN</p>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="row row-design justify-content-center align-items-center mt-4">
                            <div class="col-lg-5 col-md-6">
                                <img src="../assets/fabrication/complete-project/B/2.jpg"  style="visibility: visible; animation-delay: 0.1s; width:100%">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="">
                                    <p class="text-left font-20"><strong>PROJECT:</strong> BAJAJ HVPNL SONIPAT</p>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Product Tabes End -->
            </div>
        </section>
        <!-- service -->

    </main>
    <?php include './inc/footer.php';?>