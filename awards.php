<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India :: awards</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>

    <div class="contant">

    <section class="services__area pt-60 pb-60" data-background="assets/img/services/services-bg.jpg" style="background-image: url(&quot;assets/img/services/services-bg.jpg&quot;);">
            <div class="container">
                <div class="row mb-4">
                    <div class="col-xl-12">
                        <div class="comman-head">
                            <h2> Awards</h2>
                        </div>

                    </div>
                </div>
                <div class="row mt-30">
               

                    <div class="col-xl-12">
                <div class="related__product swiper-container">
                        
                        <div class="swiper-wrapper">
                           <div class="brand__slider-item swiper-slide">
                              <a href="#"><img src="./assets/img/about/award1.png" class="w100 r__product" alt=""></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                              <a href="#"><img src="./assets/img/about/award2.png" class="w100 r__product" alt=""></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                              <a href="#"><img src="./assets/img/about/award3.png" class="w100 r__product" alt=""></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                              <a href="#"><img src="./assets/img/blog/1event.png" class="w100 r__product" alt=""></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                              <a href="#"><img src="./assets/img/about/award5.png" class="w100 r__product" alt=""></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                              <a href="#"><img src="./assets/img/about/award6.png" class="w100 r__product" alt=""></a>
                           </div>
                        </div>
                     </div>
                </div>
                                   

                </div>
            </div>
        </section>



</div>

<?php include './inc/footer.php'; ?>