<?php include './inc/header.php';?>
<main>
    <!-- hero -->
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">

                <!-- <div class="carousel-item active">
                    <img src="../assets/PipesAndTubes/pipe/quality_b.png" class="d-block w-100"
                        alt="...">
                    <div class="carousel-caption   d-md-block cp  ">
                        <h5 class="text-white mb_font p_ro">Offers the most exclusive range both in terms of capacity
                            and capability</h5>
                    </div>
                </div> -->

                <div class="carousel-item  active">
                    <img src="./assets/img/about/csr.jpg" class="d-block w-100" alt="...">

                </div>

            </div>

        </div>
    </section>
    <!-- hero -->



    <section class="coials bg-light pt-5 pb-5 ">
        <div class="container">
            <div class="row  row-design align-items-center">
                <div class="col-md-12">
                    <h4><b>Businesses must reconnect company success with social progress. Shared value is not social
                            responsibility, philanthropy, or even sustainability, but a new way to achieve economic
                            success. It is not on the margin of what companies do but at the center. We believe that it
                            can give rise to the next major transformation of business thinking.”</b></h4>
                </div>
                <div class="col-md-6">
                    <div class="sectiontitl">
                        <p class="p_color" style="    font-size: 18px;">
                            Goodluck India is committed to minimizing pollution, reducing its environmental footprint
                            and optimizing resource consumption by planning and carrying out operations through
                            environmentally responsible
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="qlabe">
                        <img src="./assets/img/about/csr1.png" alt="" class="w-100 r_5">
                    </div>
                </div>
            </div>
            <div class="row row-design mt-5 align-items-center row-revers">
            <div class="col-md-6">
                    <div class="qlabe">
                        <img src="./assets/img/about/csr2.png" alt="" class="w-100 r_5">
                    </div>
                </div>
            <div class="col-md-6">
                <div class="sectiontitl">
                    <p class="p_color" style="    font-size: 18px;">
                    Giving back to society is at the heart of what we do.  Goodluck strive towards improving quality of life of local communities and society at large by providing education facilities and study materials to the under privileged students
                    </p>
                </div>
            </div>
          
        </div>
        <!-- end -->
        <div class="row row-design mt-5 align-items-center">
                 
                <div class="col-md-6">
                    <div class="sectiontitl">
                        <p class="p_color" style="    font-size: 18px;">
                        Goodluck India has a deep rooted tradition of acting in a responsible manner for improving health of our workforce & their families and society at large.
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="qlabe">
                        <img src="./assets/img/about/csr3.png" alt="" class="w-100 r_5">
                    </div>
                </div>
            </div>



        </div>
    </section>




</main>
<?php include './inc/footer.php';?>