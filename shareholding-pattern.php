<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India :: Shareholding Pattern</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <main>
        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item  active ">
                        <img src="./assets/PipesAndTubes/banner/common.jpg" class="d-block w-100" alt="Financial">
                        <h5>Shareholding Pattern </h5>
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="innerpagenav">
            <div class="container">
                <div class="row">
                    <ul class="pagenav">
                        <li><a href="investors.php">Investors</a></li>
                        <li><a href="financial.php">Financial</a></li>
                        <li><a href="press-release.php">Press Release</a></li>
                        <li><a href="investors-news.php">Investors News</a></li>
                        <li><a href="shareholding-pattern.php" class="active">Shareholding Pattern</a></li>
                        <li><a href="shareholder-information.php">Shareholder Information</a></li>
                        <li><a href="downloads.php">Downloads</a></li>
                        <li><a href="corporate-governance.php">Corporate Governance</a></li>
                    </ul>
                </div>
            </div>
        </section>

        <!-- end -->

        <section class="export pb-60 p60  wow fadeInUp   animated" data-wow-delay="0.5s"
            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
            <div class="container">
                <div class="row">
                    <div class="sec_title">
                        <h1 class="h_padding pt-0 wow fadeInRight   animated" data-wow-delay="0s"
                            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">Shareholding Pattern
                        </h1>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th align="CENTER" width="20%" class="text-center"><b>Year</b></th>
                                    <th align="CENTER" width="20%" class="text-center"><b>June</b></th>
                                    <th align="CENTER" width="20%" class="text-center"><b>September</b></th>
                                    <th align="CENTER" width="20%" class="text-center"><b>December</b></th>
                                    <th align="CENTER" width="20%" class="text-center"><b>March</b></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="CENTER">2023-24</td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-jun23.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER">-</td>
                                    <td align="CENTER">-</td>
                                    <td align="CENTER">-</td>

                                </tr>
                                <tr>
                                    <td align="CENTER">2022-23</td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-jun22.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-sep22-new.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-dec22.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-mar23.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>

                                </tr>
                                <tr>
                                    <td align="CENTER">2021-22</td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-jun21.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-sep21.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-dec21.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-mar22.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>

                                </tr>
                                <tr>
                                    <td align="CENTER">2020-21</td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-jun20.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-sep20.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-dec20.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-mar21.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>

                                </tr>
                                <tr>
                                    <td align="CENTER">2019-20</td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-jun19.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-sep19.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-december2019.pdf"><img
                                                width="30" height="30" align="absmiddle" alt="pdf"
                                                src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-march2020.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>

                                </tr>

                                <tr>
                                    <td align="CENTER">2018-19</td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-jun18.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-sep18.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-dec2018.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-march2019.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                </tr>








                                <tr>
                                    <td align="CENTER">2017-18</td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-jun2017-18.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/SHPsep17.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-dec17.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/shp-march18.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                </tr>

                                <tr>
                                    <td align="CENTER">2016-17</td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-jun2016-17.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-sep2016-17.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-dec2016-17.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-shp-march17.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                </tr>
                                <tr>
                                    <td align="CENTER">2015-16</td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-jun2015-16.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-neaps-sh-30-09-15.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/SHP-31-12-2015.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank" href="pdf/SHP-31-03-2016.pdf"><img width="30"
                                                height="30" align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                </tr>
                                <tr>
                                    <td align="CENTER">2014-15</td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-jun2014-15.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-sep2014-15.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-dec2014-15.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-mar2014-15.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                </tr>
                                <tr>
                                    <td align="CENTER">2013-14</td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-jun2013-14.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-sep2013-14.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-dec2013-14.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-mar2013-14.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                </tr>
                                <tr>
                                    <td align="CENTER">2012-13</td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-jun2012-13.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-sep2012-13.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-dec2012-13.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-mar2012-13.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                </tr>
                                <tr>
                                    <td align="CENTER">2011-12</td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-jun2011-12.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-sep2011-12.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-dec2011-12.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-mar2011-12.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                </tr>
                                <tr>
                                    <td align="CENTER">2010-11</td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-jun2010-11.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-sep2010-11.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-dec2010-11.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-mar2010-11.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                </tr>
                                <tr>
                                    <td align="CENTER">2009-10</td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-jun2009-10.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-sep2009-10.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-dec2009-10.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                    <td align="CENTER"><a target="_blank"
                                            href="pdf/shareholing-pattern-mar2009-10.pdf"><img width="30" height="30"
                                                align="absmiddle" alt="pdf" src="gifs/pdf.png"></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        </section>
        <!-- end -->
    </main>
    <?php include './inc/footer.php';?>