<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Goodluck India Limited (Pipes And Tubes) :: Capability </title>
    <meta name="description" content="">
    <?php include './inc/header.php';?>
    <div class="contant">
        <!-- <section class="brand__area1  title1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle1 mt-0 mb-3">
                        <h2>Over-View</h2>
                        <span class="headerLine"></span>

                    </div>
                </div>
            </div>
        </div>

    </section> -->

        <!-- hero -->

        <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
            <div class="carousel-item active">
                    <img src="./assets/img/about/slide1.png" class="d-block w-100" alt="...">
                    <h5 class="">Sikandarabad Plant </h5>
                </div>
                <div class="carousel-item ">
                    <img src="./assets/img/about/slid2.png" class="d-block w-100" alt="...">
                    <h5 class=""> Dadri Plant </h5>
                </div>
                <div class="carousel-item">
                    <img src="./assets/img/about/slide.jpg" class="d-block w-100" alt="...">
                   
                    <h5 class="">Gujarat Plant </h5>
                </div>
               
                
                 
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>
    <!-- hero -->
        <!-- hero -->


        <section class="ptb     " style="padding-top:0px!important">

            <div class="innerpagenav pb-4">
                <ul class="nav nav-tabs units mb-3 pagenav" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#sikandrabad"
                            type="button" role="tab" aria-controls="home" aria-selected="true">Sikandrabad</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#Darri"
                            type="button" role="tab" aria-controls="contact" aria-selected="false">Dadri</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#gujarat"
                            type="button" role="tab" aria-controls="contact" aria-selected="false">Gujarat</a>
                    </li>



                </ul>
            </div>

            <div class="container">
                <div class="orw">
                    <div class="col-md-12">

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="sikandrabad" role="tabpanel"
                                aria-labelledby="home-tab">
                                <div class="container">
                                    <div class="row ">
                                        <div class="col-md-8">
                                            <div class="himage un_it  ">
                                                <img src="./assets/img/about/sikndrabadmap.png" alt=""
                                                    class="w100  r_5">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="forgung_m mt-5 p-0">
                                                <h2 class="unit_color"><img src="./assets/img/about/location.png" alt=""
                                                        style="width: 7%;"> Unit 1</h2>
                                                <div class="indes mt-3">
                                                    <p class="text-dark"><b>Goodluck India Limited. <br> </b>A-42/45,
                                                        Industrial Area, Sikandrabad, Distt. Bulandshahr - 203205 (U.P.)
                                                        INDIA</p>
                                                </div>
                                            </div>
                                            <div class="forgung_m p-0">
                                                <h2 class="unit_color"><img src="./assets/img/about/location.png" alt=""
                                                        style="width: 7%;">Unit 2</h2>
                                                <div class="indes mt-3">
                                                    <p class="text-dark"><b>Good Luck Industries<br> </b> A-51,
                                                        Industrial Area, Sikandrabad,
                                                        Distt.-Bulandshahr - 203205 (U.P.) INDIA</p>
                                                </div>
                                            </div>
                                            <div class="forgung_m p-0">
                                                <h2 class="unit_color"><img src="./assets/img/about/location.png" alt=""
                                                        style="width: 7%;">Unit 3 </h2>
                                                <div class="indes mt-3">
                                                    <p class="text-dark"><b>GoodLuck Industries-II <br> </b> A-59,
                                                        Industrial Area, Sikandrabad,
                                                        Distt.-Bulandshahr - 203205 (U.P.) INDIA</p>
                                                </div>
                                            </div>
                                            <div class="forgung_m p-0">
                                                <h2 class="unit_color"><img src="./assets/img/about/location.png" alt=""
                                                        style="width: 7%;"> Unit 4</h2>
                                                <div class="indes mt-3">
                                                    <p class="text-dark"><b>Goodluck India Limited.<br></b>D-2/3/4,
                                                        Industrial Area, Sikandrabad,
                                                        Distt. Bulandshahr - 203205 (U.P.) INDIA</p>
                                                </div>
                                            </div>



                                        </div>
                                    </div>

                                </div>



                            </div>
                            <!-- end -->
                             <!-- start -->
                             <div class="tab-pane fade" id="Darri" role="tabpanel" aria-labelledby="contact-tab">

                                    <div class="container">
                                        <div class="row  ">
                                        <div class="col-md-8">
                                            <div class="himage un_it  ">
                                                <img src="./assets/img/about/dadri.png" alt=""
                                                    class="w100  r_5">
                                            </div>
                                        </div>
                                            <div class="col-md-4">

                                            <div class="forgung_m p-0 mt-5">
                                                <!-- <h2 class="unit_color"><img src="./assets/img/about/location.png" alt=""
                                                        style="width: 7%;"> Unit 1</h2> -->
                                                <div class="indes mt-3">
                                                    <p class="text-dark"><b>Good Luck Engineering <br> </b>
                                                    Khasra No. 2839, Gram Dhoom Manikpur,
G.T. Road , Gautam Budh Nagar,
Dadri, (U.P.) INDIA
                                                </p>
                                                </div>
                                            </div>
                                            </div>

                                        </div>
                                        

                                    </div>
                                </div>
                                <!-- end -->
                                <!-- end -->
                                <div class="tab-pane fade" id="gujarat" role="tabpanel" aria-labelledby="contact-tab">

                                <div class="container">
                                        <div class="row  ">
                                        <div class="col-md-8">
                                            <div class="himage un_it  ">
                                                <img src="./assets/img/about/gugrat.png" alt=""
                                                    class="w100  r_5">
                                            </div>
                                        </div>
                                            <div class="col-md-4">

                                            <div class="forgung_m p-0 mt-5">
                                                <!-- <h2 class="unit_color"><img src="./assets/img/about/location.png" alt=""
                                                        style="width: 7%;"> Unit 1</h2> -->
                                                <div class="indes mt-3">
                                                    <p class="text-dark"><b>Goodluck Metallics <br> </b>
                                                    Survey No. 495,497 to 502, Bachau Sikra Road,
Village-Sikra, T A Bachau,
Gujarat - 370140, INDIA</p>
                                                </div>
                                            </div>
                                            </div>

                                        </div>
                                        

                                    </div>
                                </div>
                                <!-- end -->



                            </div>
                        </div>
                    </div>
                </div>

        </section>
    </div>
    <?php include './inc/footer.php';?>