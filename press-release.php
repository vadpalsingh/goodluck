<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India :: Press Release</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <main>
        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item  active ">
                        <img src="./assets/PipesAndTubes/banner/common.jpg" class="d-block w-100" alt="Financial">
                        <h5>Press Release </h5>
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="innerpagenav">
            <div class="container">
                <div class="row">
                    <ul class="pagenav">
                        <li><a href="investors.php">Investors</a></li>
                        <li><a href="financial.php">Financial</a></li>
                        <li><a href="press-release.php" class="active">Press Release</a></li>
                        <li><a href="investors-news.php">Investors News</a></li>
                        <li><a href="shareholding-pattern.php">Shareholding Pattern</a></li>
                        <li><a href="shareholder-information.php">Shareholder Information</a></li>
                        <li><a href="downloads.php">Downloads</a></li>
                        <li><a href="corporate-governance.php">Corporate Governance</a></li>
                    </ul>
                </div>
            </div>
        </section>

        <!-- end -->

        <section class="export pb-60 p60  wow fadeInUp   animated" data-wow-delay="0.5s"
            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
            <div class="container">
                <div class="row">
                    <div class="sec_title">
                        <h1 class="h_padding pt-0 wow fadeInRight   animated" data-wow-delay="0s"
                            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">Press Release
                        </h1>
                    </div>
                    <div class="ins-det">


                        <div class="pre-rel"><a href="pdf/press-release-q4fy22.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Financial Performance Highlights Q4 and FY22</a></div>
                        <div class="pre-rel"><a href="pdf/press-release-04-04-2022.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Business Update Release</a></div>
                        <div class="pre-rel"><a href="pdf/press-release-01-03-2022.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Highest Ever Export of the Company</a></div>
                        <div class="pre-rel"><a href="pdf/financial-performance-highlights-q3fy22.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Financial Performance Highlights Q3FY22</a></div>
                        <div class="pre-rel"><a href="pdf/financial-performance-highlights-q2fy22.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Financial Performance Highlights Q2FY22</a></div>
                        <div class="pre-rel"><a href="pdf/press-release-Q4FY16.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Financial Performance Highlights Q4 and FY16 </a>
                        </div>
                        <div class="pre-rel"><a href="pdf/award-press-release-11-03-16.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Good Luck won award for Export Excellence</a></div>
                        <div class="pre-rel"><a href="pdf/achieved-ebitda-growth.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Achieved EBITDA growth of 38.5 %</a></div>
                        <div class="pre-rel"><a href="pdf/approval-from-rdso.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Approval from RDSO, Ministry of Railway </a></div>
                        <div class="pre-rel"><a href="pdf/press-release-credit-rating-02-12-15.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Credit Rating Upgraded by ICRA</a></div>
                        <div class="pre-rel"><a href="pdf/press-release-order-67cr-17-11.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Rs. 67 crores order for structure vertical </a></div>
                        <div class="pre-rel"><a href="pdf/pressrelease-26-10-2015.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Financial Performance - Q2 FY16</a></div>
                        <div class="pre-rel"><a href="pdf/update-29.04.15.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Update 29.04.15</a></div>
                        <div class="pre-rel"><a href="pdf/goodluck-q4-press-release.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Goodluck India Limited Limited announces Financial
                                Results for FY 15</a></div>
                        <div class="pre-rel"><a href="pdf/q3-press-release.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> GLST's Stellar Shows Continues</a></div>
                        <div class="pre-rel"><a href="pdf/nse-trading-press-release-final.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> NSE Trading Press Release Final - 08.01.15</a></div>
                        <div class="pre-rel"><a href="pdf/nse-listing-press-release-final.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Listing on National Stock Exchange (NSE)</a></div>
                        <div class="pre-rel"><a href="pdf/inauguration-press-release-final.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Inauguration of New Plant</a> </div>
                        <div class="pre-rel"><a href="pdf/q2-press-release.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Q2 FY15 Result</a></div>
                        <div class="pre-rel"><a href="pdf/press-release-award-ceremony-eepc.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Company Won Award For Export Excellence</a></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
        </section>
        <!-- end -->
    </main>
    <?php include './inc/footer.php';?>