<?php include './inc/header.php'; ?>


<main>
    <!-- hero -->

    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide banner__slider" data-bs-ride="carousel" data-pause="false">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="../assets/CoilsSheets/banner/s1.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block c1 new">
                        <h5 class="text-dark text-white"> Cold Rolled Coils & Sheets</h5>
                        <p class="text-dark text-white">The Galvanized Pipes are manufactured using steel and then coated using the process of galvanizing.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="../assets/CoilsSheets/banner/s2.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block  bl">
                        <h5 class="b_pipe">Cold Rolled Coils & Sheets</h5>
                        <p class="text-white">The Black Pipes are made from steel as well as iron and are widely suited for water supply and flammable gases.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="../assets/CoilsSheets/banner/s3.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block c3 yl">
                        <h5 class="text-white"> CR COILS & SHEETS</h5>
                        <p>We are offering excellent CR (Cold Rolled) Coils that are made from finest steel metal and produced from hot rolled, before being pickled Previous</p>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>
    <!-- hero -->


    <!-- about -->
    <section class="p60 abou_t">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="comman-head">
                        <h2 class="pb3">Welcome To INFRA Fabrication</h2>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="g_bout">


                        <div class="home-about wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">
                            <blockquote>
                                With Total capacity of 18,000 Tons per annum, our facility is equipped to manufacture a
                                complete range of Grades in Carbon and Alloy Steel, Stainless, Duplex, Super Duplex,
                                Nickel Alloys, Titanium & Aluminium Alloys.

                            </blockquote>
                            <p class="wow fadeInUp" data-wow-delay=".8s"
                                style="visibility: visible; animation-delay: 0.8s;">
                                The Galvanized Pipes are manufactured<br> using steel and then coated using the
                                process of galvanizing. <br>These pipes are thoroughly tested
                                and examined in accordance with international standards.

                            </p>
                            <p class="wow fadeInUp" data-wow-delay=".9s"
                                style="visibility: visible; animation-delay: 0.8s;">Our manufacturing facility boasts of
                                state of the art equipment with Close Die and Open Die Forging Machines - 6 Machines
                                covering complete range of Forging from 1/2" to 80" under one roof with Manipulator to
                                handle larger single piece Forging</p>
                            <div class="read-more">
                                <a href="about-us/company-profile.html?mpgid=2&amp;pgidtrail=3">Know more</a>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-md-4">
                    <h4 style="border-bottom: 1px solid #eee;
                    padding-bottom: 5px;">FACTS</h4>
                    <div class="a_im_g">
                        <div class="row g-4 align-items-center">
                            <div class="col-sm-12">
                                <!-- <div class="bg-primary p-4 mb-4 wow fadeIn" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeIn;"><i class="fa fa-users fa-2x text-white mb-3"></i>
                                <h2 class="text-white mb-2" data-toggle="counter-up">1234</h2>
                                <p class="text-white mb-0">Happy Clients</p>
                                </div> -->
                                <div class="gray cunt p-3  wow fadeIn " 
                                 >
                                    <!-- <i class="fa fa-briefcase"></i> -->
                                    <p   class="number" style="display:inline-block">100 </p> <span style="    color: #838080;
                                    font-size: 21px;
                                    font-weight: 600;">  Tonnes</span>
                                    <span></span>
                                    <p>Annual Installed Capacity</p>
                                </div>
                                <!-- <div class="bg-secondary p-4 wow fadeIn" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeIn;"><i class="fa fa-ship fa-2x text-white mb-3"></i>
                                <h2 class="text-white mb-2" data-toggle="counter-up">1234</h2>
                                <p class="text-white mb-0">Complete Shipments</p>
                                </div> -->
                                <div class="gray cunt p-3  wow fadeIn" >

                                    <!-- <i class="fa fa-user"></i> -->
                                    <p   class="number">510+</p>
                                    <span></span>
                                    <p>Counters  Export</p>
                                </div>
                                <div class="gray cunt p-3 wow fadeIn" >
                                    <!-- <i class="fa fa-users"></i> -->
                                    <!-- <p id="number3" class="number">3059</p> -->
                                    <span></span>
                                    <p>Nickel Alloys Titanium & Aluminum  Forging (Specialization)</p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- counter -->
    <!-- expites -->
       <section class="p60 ex_pites">
        <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle">
                        <h2>APPLICATION</h2>
                        <span class="headerLine"></span>
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <ul class="experts">
                        <li>
                        <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/goodluck/area/a1.png" alt="">
                        <h3 class="exp_h">Railway & Road Bridges</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Railway & Road Bridges</h3></a>
                        </div>
                         </div>
                        </li>
                        <li>
                        <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/goodluck/area/a2.png" alt="">
                        <h3 class="exp_h">Structure for Roads & Expressways.</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Structure for Roads & Expressways.</h3></a>
                        </div>
                         </div>
                        </li>
                        <li>
                        <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/goodluck/area/a3.png" alt="">
                        <h3 class="exp_h">Launching Girder & Superstructure.</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Launching Girder & Superstructure.</h3></a>
                        </div>
                         </div>
                        </li>
                        <li>
                        <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/goodluck/area/a4.png" alt="">
                        <h3 class="exp_h">Transmission Live Tower</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Transmission Live Tower</h3></a>
                        </div>
                         </div>
                        </li>
                        <li>
                        <div class="expites pos">
                        <div class="exp_img  pos">
                        <img src="../assets/goodluck/area/telecom_touber.png" alt="">
                        <h3 class="exp_h">Telecom Towers</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Telecom Towers</h3></a>
                        </div>
                         </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

       </section>
    <!-- expites -->
    <!-- service -->

    <section class="brand__area p60">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle">
                        <h2>Product</h2>
                        <span class="headerLine"></span>
                        <p class="p_color">Established in the year 1986, Goodluck India Limited is an ISO 9001:2008
                            certified
                            organization,<br> engaged in manufacturing and exporting of a wide range of galvanized
                            sheets &amp; coils, <br>towers, hollow sections, CR coils CRCA and pipes &amp; tubes.</p>
                    </div>
                </div>
            </div>
            <div class="row  ">
                <div class="col-md-3">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/CoilsSheets/product/cold.png" alt="" class="res">
                                        </div>
                                        <h3>Cold Rolled Coils & Sheets </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/CoilsSheets/product/galvanised.png" alt="" class="res">
                                        </div>
                                        <h3>Galvanized Coils </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/CoilsSheets/product/galvanised2.png" alt="" class="res">
                                        </div>
                                        <h3>Galvanized Sheets </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/CoilsSheets/product/crca.png" alt="" class="res">
                                        </div>
                                        <h3>CRCA </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/CoilsSheets/product/hrpo.png" alt="" class="res">
                                        </div>
                                        <h3>HRPO </h3>
                                    </div>
                                </a>
                            </div>
                </div>

                
                <!-- <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/goodluck/product/GE28.png" alt="" class="res">
                                        </div>
                                        <h3>GE28 </h3>
                                    </div>
                                </a>
                            </div>
                </div> -->
                <!-- end -->


                
                  
                <!-- end -->
            </div>
            <!-- <div class="col-md-12">
                    <div class="ViewAll text-center mt-4">
                        <button class="v_all"><i class="fa fa-plus"></i> ViewAll</button>
                    </div>
                  </div> -->
        </div>
    </section>

    <!-- service -->
<!-- brand__area start -->
<section class="brand__area1 p60" style="    background-color: #fdfdfd;">
        <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle">
                        <h2>Approval</h2>
                        <span class="headerLine"></span>
                      
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="brand__slider p-0 swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events">
                        <div class="swiper-wrapper" id="swiper-wrapper-9b6dff0c1f7e3c20" aria-live="off" style="transition-duration: 300ms; transform: translate3d(-2121.6px, 0px, 0px);"><div class="brand__slider-item swiper-slide swiper-slide-duplicate swiper-slide-duplicate-prev" data-swiper-slide-index="2" role="group" aria-label="1 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b3.png" alt=""></a>
                            </div><div class="brand__slider-item swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active" data-swiper-slide-index="3" role="group" aria-label="2 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b4.png" alt=""></a>
                            </div><div class="brand__slider-item swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next" data-swiper-slide-index="4" role="group" aria-label="3 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b5.png" alt=""></a>
                            </div><div class="brand__slider-item swiper-slide swiper-slide-duplicate" data-swiper-slide-index="5" role="group" aria-label="4 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b6.png" alt=""></a>
                            </div><div class="brand__slider-item swiper-slide swiper-slide-duplicate" data-swiper-slide-index="6" role="group" aria-label="5 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b7.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide" data-swiper-slide-index="0" role="group" aria-label="6 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b1.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide" data-swiper-slide-index="1" role="group" aria-label="7 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b2.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide swiper-slide-prev" data-swiper-slide-index="2" role="group" aria-label="8 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b3.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide swiper-slide-active" data-swiper-slide-index="3" role="group" aria-label="9 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b4.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide swiper-slide-next" data-swiper-slide-index="4" role="group" aria-label="10 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b5.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide" data-swiper-slide-index="5" role="group" aria-label="11 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b6.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide" data-swiper-slide-index="6" role="group" aria-label="12 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b7.png" alt=""></a>
                            </div>
                        <div class="brand__slider-item swiper-slide swiper-slide-duplicate" data-swiper-slide-index="0" role="group" aria-label="13 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b1.png" alt=""></a>
                            </div><div class="brand__slider-item swiper-slide swiper-slide-duplicate" data-swiper-slide-index="1" role="group" aria-label="14 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b2.png" alt=""></a>
                            </div><div class="brand__slider-item swiper-slide swiper-slide-duplicate swiper-slide-duplicate-prev" data-swiper-slide-index="2" role="group" aria-label="15 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b3.png" alt=""></a>
                            </div><div class="brand__slider-item swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active" data-swiper-slide-index="3" role="group" aria-label="16 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b4.png" alt=""></a>
                            </div><div class="brand__slider-item swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next" data-swiper-slide-index="4" role="group" aria-label="17 / 17" style="width: 235.2px; margin-right: 30px;">
                                <a href="#"><img src="../assets/img/brand/b5.png" alt=""></a>
                            </div></div>
                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                </div>
            </div>
        </div>
    </section>
 
  
    
 
    <!-- brand__area end -->
   
</main>



<?php include './inc/footer.php';?>
