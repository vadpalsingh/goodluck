<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India :: Corporate Governance</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <main>
        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item  active ">
                        <img src="./assets/PipesAndTubes/banner/common.jpg" class="d-block w-100" alt="Financial">
                        <h5>Corporate Governance</h5>
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="innerpagenav">
            <div class="container">
                <div class="row">
                    <ul class="pagenav">
                        <li><a href="investors.php">Investors</a></li>
                        <li><a href="financial.php">Financial</a></li>
                        <li><a href="press-release.php">Press Release</a></li>
                        <li><a href="investors-news.php">Investors News</a></li>
                        <li><a href="shareholding-pattern.php">Shareholding Pattern</a></li>
                        <li><a href="shareholder-information.php">Shareholder Information</a></li>
                        <li><a href="downloads.php">Downloads</a></li>
                        <li><a href="corporate-governance.php" class="active">Corporate Governance</a></li>
                    </ul>
                </div>
            </div>
        </section>

        <!-- end -->

        <section class="export pb-60 p60  wow fadeInUp   animated" data-wow-delay="0.5s"
            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
            <div class="container">
                <div class="row">
                <div class="sec_title">
                        <h1 class="h_padding pt-0 wow fadeInRight   animated" data-wow-delay="0s"
                            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">Corporate
                            Governance
                        </h1>
                    </div>
                    
                    <div class="ins-det mt-2">
                        <h3>Code &amp; Policies</h3>

                        <div class="pre-rel"><a
                                href="pdf/Anti-CorruptionBriberyPolicy.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i>Anti-Corruption & Bribery Policy and Code of Ethics & Business Conduct</a> </div>
                                <div class="pre-rel"><a
                                href="pdf/DisclosurecontactdetailsofKeyManagerial.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i>Disclosure of contact details of Key Managerial Personnel as required under Regulation 30(5) of SEBI (LODR) Regulations, 2015</a> </div>
 
                        <div class="pre-rel"><a
                                href="pdf/policy-for-determination-of-materiality-of-events-information.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Policy for determination of materiality
                                of events / Information</a> </div>
                        <div class="pre-rel"><a href="pdf/composition-of-board-and-committees.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Composition of the Board and Committees</a> </div>
                        <div class="pre-rel"><a href="pdf/dividend-distribution-policy.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Dividend Distribution Policy</a> </div>
                        <div class="pre-rel"><a href="pdf/records-archives-management-policy.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Archival Policy</a> </div>
                        <div class="pre-rel"><a href="pdf/rpt-policy-goodluck.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Policies on Related Party</a></div>
                        <div class="pre-rel"><a href="pdf/policy-for-material-subsidiary.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Policy for Material Subsidiary</a></div>
                        <div class="pre-rel"><a href="pdf/terms-conditions-of-independent-directors.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Terms &amp; Conditions of Independent
                                Directors</a> </div>
                        <div class="pre-rel"><a href="pdf/code-of-fair-disclosure-of-upsi.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Code of Fair Disclosure of UPSI</a></div>
                        <div class="pre-rel"><a href="pdf/csr-policy-2015.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> CSR Policy - 2015</a></div>
                        <div class="pre-rel"><a href="pdf/vigil-mechanism.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Vigil Mechanism</a></div>
                        <div class="pre-rel"><a href="pdf/nomination-and-remuneration-policy.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Remuneration Policy</a> </div>
                        <div class="pre-rel"><a href="pdf/familiarization-programme.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Familarisation Program for Directors </a></div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="sec_title">
                        <h1 class="h_padding pt-0 wow fadeInRight  mt-3  animated A_rchive" data-wow-delay="0s"
                            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">Archive     </h1>
                    </div>
                    <div class="table-responsive archive hide" >
                        <table class="table">
                            <thead>
                                <tr>
                                    <th align="CENTER" width="20%" class="text-center"><strong>Year</strong></th>
                                    <th align="CENTER" width="20%" class="text-center"><strong>Q1</strong></th>
                                    <th align="CENTER" width="20%" class="text-center"><strong>Q2</strong></th>
                                    <th align="CENTER" width="20%" class="text-center"><strong>Q3</strong></th>
                                    <th align="CENTER" width="20%" class="text-center"><strong>Q4</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="CENTER">2015-16</td>
                                    <td align="CENTER"><a href="pdf/Q1-2015.pdf" target="_new"><img src="gifs/pdf.png"
                                                width="30" height="30" alt="pdf" align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/cgr-Q2-30-09-15.pdf" target="_new"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER">--</td>
                                    <td align="CENTER">--</td>
                                </tr>
                                <tr>
                                    <td align="CENTER">2014-15</td>
                                    <td align="CENTER"><a href="pdf/Q1-2014.pdf" target="_new"><img src="gifs/pdf.png"
                                                width="30" height="30" alt="pdf" align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/Q2-2014.pdf" target="_new"><img src="gifs/pdf.png"
                                                width="30" height="30" alt="pdf" align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/Q3-2014.pdf" target="_new"><img src="gifs/pdf.png"
                                                width="30" height="30" alt="pdf" align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/Q4-2015.pdf" target="_new"><img src="gifs/pdf.png"
                                                width="30" height="30" alt="pdf" align="absmiddle"></a></td>
                                </tr>
                                <tr>
                                    <td align="CENTER">2013-14</td>
                                    <td align="CENTER"><a href="pdf/Q1-2013.pdf" target="_new"><img src="gifs/pdf.png"
                                                width="30" height="30" alt="pdf" align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/Q2-2013.pdf" target="_new"><img src="gifs/pdf.png"
                                                width="30" height="30" alt="pdf" align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/Q3-2013.pdf" target="_new"><img src="gifs/pdf.png"
                                                width="30" height="30" alt="pdf" align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/Q4-2014.pdf" target="_new"><img src="gifs/pdf.png"
                                                width="30" height="30" alt="pdf" align="absmiddle"></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

        </section>
        <!-- end -->
    </main>
   
    <?php include './inc/footer.php';?>
    <script>
//   $(document).ready(function(){
//     $(".A_rchive").click(function(){
//         alert('safb')
//   $(".archive").removeClass(hide);
// });
//   })
        </script>