<?php include './inc/header.php'; ?>


<main>
    <!-- hero -->

    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="./assets/goodluck/product/s1.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block c1">
                        <h5>CDW  TUBES PIPES</h5>
                        <p>The Galvanized Pipes are manufactured using steel and then coated using the process of
                            galvanizing.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="./assets/goodluck/product/s2.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block c2">
                        <h5>BLACK PIPES</h5>
                        <p>The Black Pipes are made from steel as well as iron and are widely suited for water supply
                            and flammable gases.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="./assets/goodluck/product/s3.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block c3">
                        <h5>CR COILS & SHEETS</h5>
                        <p>We are offering excellent CR (Cold Rolled) Coils that are made from finest steel metal and
                            produced from hot rolled, before being pickled.</p>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>
    <!-- hero -->

    </section>
    <!-- about -->
    <section class="p60 abou_t">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="comman-head">
                        <h2 class="pb3">WELCOME TO GOODLUCK INDIA LIMITED</h2>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="g_bout">


                        <div class="home-about wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">
                            <blockquote>


                                Good Luck, A 36 Years Old group
                                promoted by IITians, a steel
                                processor, converting basic steel
                                to quality engineering products.
                                The products are being used
                                world wide byend-customers
                                like automobile manufacturers,

                            </blockquote>
                            <p class="wow fadeInUp" data-wow-delay=".8s"
                                style="visibility: visible; animation-delay: 0.8s;">


                                The Galvanized Pipes are manufactured<br> using steel and then coated using the
                                process of galvanizing. <br>These pipes are thoroughly tested
                                and examined in accordance with international standards.
                            </p>
                            <div class="read-more">
                                <a href="about-us/company-profile.html?mpgid=2&amp;pgidtrail=3">Know more</a>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-md-4">
                    <div class="a_im_g">
                        <img src="./assets/goodluck/product/pipe1.png" alt="res" class>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- about -->
    <!-- counter -->
    <!-- <div class="sectiontitle">
    <h2>Projects statistics</h2>
    <span class="headerLine"></span>
</div> -->
    <div id="projectFacts" class="sectionClass">
        <div class="fullWidth eight columns">
            <div class="projectFactsWrap ">
                <!-- <div class="item wow fadeInUpBig animated animated" data-number="12" style="visibility: visible;"> -->

                <div class="item wow  animated animated" data-number="2025" style="visibility: visible;"><i
                        class="fa fa-briefcase"></i>
                    <p id="number1" class="number">2025</p>
                    <span></span>
                    <p>Projects done</p>
                </div>
                <div class="item wow   animated animated" data-number="1055" style="visibility: visible;">
                    <i class="fa fa-user"></i>
                    <p id="number2" class="number">1055</p>
                    <span></span>
                    <p>Happy clients</p>
                </div>
                <div class="item wow   animated animated" data-number="3059" style="visibility: visible;">
                    <i class="fa fa-users"></i>
                    <p id="number3" class="number">3059</p>
                    <span></span>
                    <p>Employee</p>
                </div>
                <div class="item wow   animated animated" data-number="8000" style="visibility: visible;">
                    <i class="fa fa-building"></i>
                    <p id="number4" class="number">8000</p>
                    <span></span>
                    <p>MT/PA Capacity</p>
                </div>
            </div>
        </div>
    </div>

    <!-- counter -->

    <!-- service -->

    <section class="brand__area p60">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle">
                        <h2>Product</h2>
                        <span class="headerLine"></span>
                        <p class="p_color">Established in the year 1986, Goodluck India Limited is an ISO 9001:2008 certified
                            organization,<br> engaged in manufacturing and exporting of a wide range of galvanized
                            sheets & coils, <br>towers, hollow sections, CR coils CRCA and pipes & tubes.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="service__slider swiper-container">
                        <div class="swiper-wrapper">
                            <div class="brand__slider-item swiper-slide">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="./assets/goodluck/product/p1.png" alt="" class="res">
                                        </div>
                                        <h3>ERW Galvanized Pipes</h3>
                                    </div>
                                </a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="./assets/goodluck/product/erw_black.png" alt="" class="res">
                                        </div>
                                        <h3>Black and Painted Pipes</h3>
                                    </div>
                                </a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="./assets/goodluck/product/GALVANISED_COILS.png" alt=""
                                                class="res">
                                        </div>
                                        <h3>Galvanised Coils</h3>
                                    </div>
                                </a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="./assets/goodluck/product/Galvanised_Sheets.png" alt=""
                                                class="res">
                                        </div>
                                        <h3>Galvanised Sheets</h3>
                                    </div>
                                </a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="./assets/goodluck/product/squre_pipe.png" alt="" class="res">
                                        </div>
                                        <h3>Square & Rectangular Pipes</h3>
                                    </div>
                                </a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="./assets/goodluck/product/CRCoils&Sheets.png" alt="" class="res">
                                        </div>
                                        <h3>CR Coils & Sheets </h3>
                                    </div>
                                </a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="./assets/goodluck/product/febreaction.png" alt="" class="res">
                                        </div>
                                        <h3>Fabricated Steel Structure </h3>
                                    </div>
                                </a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="./assets/goodluck/product/tabur.png" alt="" class="res">
                                        </div>
                                        <h3>Transmission & Telecom Towers </h3>
                                    </div>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- service -->

    <!-- tab -->
    <section class="capabilities_tab wow fadeInUp animated p60" data-wow-delay=".4s"
        style="visibility: visible;-webkit-animation-delay: .4s; -moz-animation-delay: .4s; animation-delay: .4s;">

        <div class="container">
            <div class="row">

            <div class="col-md-12">
            <div class="n_tabs ews text-center">
                        <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active " id="home-tab" data-bs-toggle="tab"
                                    data-bs-target="#home-tab-pane" type="button" role="tab"
                                    aria-controls="home-tab-pane" aria-selected="true">Manufacturing
                                </button>
                            </li>
            
                            <li class="nav-item" role="presentation">
                                <button class="nav-link " id="profile-tab" data-bs-toggle="tab"
                                    data-bs-target="#profile-tab-pane" type="button" role="tab"
                                    aria-controls="profile-tab-pane" aria-selected="false"> Engineering  </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="contact-tab" data-bs-toggle="tab"
                                    data-bs-target="#contact-tab-pane" type="button" role="tab"
                                    aria-controls="contact-tab-pane" aria-selected="false">Quality</button>
                            </li>
                            <!-- <li class="nav-item" role="presentation">
                        <button class="nav-link" id="disabled-tab" data-bs-toggle="tab" data-bs-target="#disabled-tab-pane" type="button" role="tab" aria-controls="disabled-tab-pane" aria-selected="false"  >Disabled</button>
                        </li> -->
                        </ul>
                    </div>
            </div>
            <!-- end -->
            <div class="col-md-12">
                <div class="c_0ntan_t">
                <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel"
                            aria-labelledby="home-tab" tabindex="0">
                            <!-- start -->
                            <div class="container">
                                <div class="row">

                                    <div class="col-md-5">
                                        <div class="g_bout_e">


                                            <div class="home-about h_l   "  >
                                                <blockquote>


                                                    Good Luck, A 36 Years Old group
                                                    promoted by IITians, a steel
                                                    processor,


                                                </blockquote>
                                                <p class="  "  >


                                                We manufacture an extensive range of Welded<br> Tubes & Pipes that are available in different<br> Structural shapes such as round, square and <br>Rectangular hollow sections,<br> which can be easily welded into tube, <br>tubing and structures.
                                                </p>
                                                <p class="  "  >


We manufacture an extensive range of WeldedTubes & Pipes that are available in different<br> Structural shapes such as round, square and Rectangular hollow <br>sections,  which can be easily welded into tube, <br>tubing and structures.
</p>
                                                
                                                <div class="read-more">
                                                    <a href="#">Know
                                                        more</a>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="a_im_g">
                                            <img src="./assets/goodluck/product/erw_p.png" alt="res" class="res">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- \end -->
                        <div class="tab-pane fade  " id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab"
                            tabindex="0">
                            <div class="container">
                                <div class="row">

                                    <div class="col-md-5">
                                        <div class="g_bout_e">


                                            <div class="home-about h_l   "  >
                                                <blockquote>


                                                    Good Luck, A 36 Years Old group
                                                    promoted by IITians, a steel
                                                    processor,


                                                </blockquote>
                                                <p class="  "  >


                                                    The Galvanized Pipes are manufactured<br> using steel and then
                                                    coated using the
                                                    process of galvanizing. <br>These pipes are thoroughly tested
                                                    and examined in accordance with international standards.
                                                </p>
                                                <div class="read-more">
                                                    <a href="#">Know
                                                        more</a>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="a_im_g">
                                            <img src="./assets/goodluck/product/eng.png" alt="res" class="res">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end -->
                        <div class="tab-pane fade" id="contact-tab-pane" role="tabpanel" aria-labelledby="contact-tab"
                            tabindex="0">
                            <div class="container">
                                <div class="row">

                                    <div class="col-md-5">
                                        <div class="g_bout_e">


                                            <div class="home-about h_l   "  >
                                                <blockquote>


                                                    Good Luck, A 36 Years Old group
                                                    promoted by IITians, a steel
                                                    processor,


                                                </blockquote>
                                                <p class="  "  >


                                                    The Galvanized Pipes are manufactured<br> using steel and then
                                                    coated using the
                                                    process of galvanizing. <br>These pipes are thoroughly tested
                                                    and examined in accordance with international standards.
                                                </p>
                                                <div class="read-more">
                                                    <a href="#">Know
                                                        more</a>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="a_im_g">
                                            <img src="./assets/goodluck/product/qulity.png" alt="res" class="res">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="tab-pane fade" id="disabled-tab-pane" role="tabpanel" aria-labelledby="disabled-tab" tabindex="0">.4.</div> -->
                    </div>
                </div>
            </div>

               
            </div>
        </div>


    </section>
    <!-- tab -->
      <!-- Gallery start---->
      <section class="brand__area1 p60" style="    background-color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                     
                    <div class="sectiontitle">
                        <h2>Our Gallery</h2>
                        <span class="headerLine"></span>
                     
                    </div>
                </div>
            </div>
            
        </div>
    </section>
    <section id="gallery" class="bg-white p-0 ">
        <div class="container">
             

            <!--  -->

            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/img/gallery/g9.png"><img src="./assets/img/gallery/g9.png"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                        <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/img/gallery/g1.jpg"><img src="./assets/img/gallery/g1.jpg"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                        <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/img/gallery/g2.jpg"><img src="./assets/img/gallery/g2.jpg"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                        <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/img/gallery/g3.jpg"><img src="./assets/img/gallery/g3.jpg "
                                class="img-responsive"></a>
                        <div class="img-overlay">
                           <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/img/gallery/g4.jpg"><img src="./assets/img/gallery/g4.jpg"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                        <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/img/gallery/g5.png"><img src="./assets/img/gallery/g5.png"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                        <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/img/gallery/g6.png"><img src="./assets/img/gallery/g6.png"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                        <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/img/gallery/g7.png"><img src="./assets/img/gallery/g7.png"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                        <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>

            </div><!-- End row -->
        </div><!-- End image gallery -->
        </div><!-- End container -->
    </section>
<!-- gallery -->
     <!-- brand__area start -->
     <section class="brand__area1 p60" style="    background-color: #fdfdfd;">
                  <div class="container">
                     <div class="row">
                        <div class="col-xl-12">
                           <div class="brand__title text-center">
                              <h1> Customer </h1>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xl-12">
                           <div class="brand__slider p-0 swiper-container">
                              <div class="swiper-wrapper">
                                 <div class="brand__slider-item swiper-slide">
                                    <a href="#"><img src="assets/img/brand/1.jpg" alt=""></a>
                                 </div>
                                 <div class="brand__slider-item swiper-slide">
                                    <a href="#"><img src="assets/img/brand/2.jpg" alt=""></a>
                                 </div>
                                 <div class="brand__slider-item swiper-slide">
                                    <a href="#"><img src="assets/img/brand/3.jpg" alt=""></a>
                                 </div>
                                 <div class="brand__slider-item swiper-slide">
                                    <a href="#"><img src="assets/img/brand/4.jpg" alt=""></a>
                                 </div>
                                 <div class="brand__slider-item swiper-slide">
                                    <a href="#"><img src="assets/img/brand/5.jpg" alt=""></a>
                                 </div>
                                 <div class="brand__slider-item swiper-slide">
                                    <a href="#"><img src="assets/img/brand/6.jpg" alt=""></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <!-- brand__area end -->

</main>



<?php include './inc/footer.php';?>