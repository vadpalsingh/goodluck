<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India </title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <main>
        <header class="video-hero">
            <div class="video-bg">
                <video id="myVideo" poster="./assets/img/awards/poster.jpg" loop autoplay="true" controls>
                    <source src="./assets/img/services/shortvideo/v1.mp4" type="video/mp4">
                </video>
            </div>
        </header>
        <!-- Swiper Dot start -->
        <!-- Swiper Dot end -->
        <!-- about__area start -->
        <section class="about-us wow light-grey  pt-60 pb-60 fadeInUp animated" data-wow-delay=".3s"
            style="visibility: visible;-webkit-animation-delay: .3s; -moz-animation-delay: .3s; animation-delay: .3s;">
            <div class="container">

                <div class="row">
                    <div class="comman-head">
                        <h2>Company Facts</h2>
                    </div>
                    <div class="col-md-12 col-lg-4 col-sm-6">

                        <div class="row mt-2 mb-3">

                            <div class="col-sm-4  col-lg-7 col-md-4 col-4">
                                <div class="counter-container">
                                    <div class="counter" data-target="364000"></div>
                                    <span class="shorts left-long" style="right: 42px;">MTPA</span>
                                    <span>Capacity</span>
                                </div>
                            </div>
                            <div class="col-sm-4 col-lg-5 col-md-4 col-4">
                                <div class="counter-container">
                                    <div class="counter" data-target="06"></div>
                                    <span>Plants</span>
                                </div>
                            </div>
                            <div class="col-sm-4 col-lg-7 col-md-4 col-4">
                                <div class="counter-container">
                                    <div class="counter" data-target="06"></div>
                                    <span>Verticals</span>
                                </div>
                            </div>
                            <div class="col-sm-4 col-lg-5 col-md-4 col-4">
                                <div class="counter-container">
                                    <div class="counter" data-target="3100"></div>
                                    <span class="shorts left-long">+cr</span>
                                    <span>Turnover</span>
                                </div>
                            </div>
                            <div class="col-sm-4 col-lg-7 col-md-4 col-4">
                                <div class="counter-container">
                                    <div class="counter" data-target="100"></div>
                                    <span>Countries Export</span>
                                </div>
                            </div>
                            <div class="col-sm-4 col-lg-5 col-md-4 col-4">
                                <div class="counter-container">
                                    <div class="counter" data-target="4000"></div>
                                    <span class="shorts left-short">+</span>
                                    <span>Workforce</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-8 col-lg-4 col-sm-6">
                        <div class="home-about wow fadeInUp animated" data-wow-delay=".6s"
                            style="visibility: visible;-webkit-animation-delay: .6s; -moz-animation-delay: .6s; animation-delay: .6s;">

                            <p class="wow fadeInUp animated" data-wow-delay=".8s"
                                style="visibility: visible;-webkit-animation-delay: .8s; -moz-animation-delay: .8s; animation-delay: .8s;">
                                Goodluck, a group founded by IIT alumni with 37 years of experience, specializes in manufacturing and exporting a diverse range of products, including ERW Precision & CDW Tubes, Forged Flanges, and Custom Forgings, Power & Telecom Towers, Solar Structures, ERW Hot Dip Galvanized Pipes, Black Pipes, Black & GI Hollow Sections, CR Coils, CRCA, Road Safety Product Galvanized Plain & Corrugated Sheets.
                                <br>
                                Established three decades ago, the company has established itself as a pioneering and rapidly advancing presence in the Steel Industry through its innovative and progressive approach. As an ISO-9001, AS 9100D, IATF-16949, ISO-14001 & OH&SMS-45001 & CE certified organization, Goodluck is proud to uphold high standards of quality and excellence.
                             </p>


                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="lagacy wow fadeInUp animated" data-wow-delay=".9s"
                            style="visibility: visible;-webkit-animation-delay: .9s; -moz-animation-delay: .9s; animation-delay: .9s;">
                            <!-- <img alt="lagacy" class="img-fluid" src="assets/img/about/lagacy.png"> -->
                            <p>LEGACY Of</p>
                            <h1 style="font-family: 'EB Garamond', serif;">37</h1>
                            <p>Years</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- about__area end -->
        <!-- services__area start -->
        <!-- <section class="services__area pt-60 pb-60" data-background="assets/img/services/services-bg.jpg">
            <div class="container">
                <div class="row mb-4">
                    <div class="col-xl-12">
                        <div class="comman-head">
                            <h2>Our Verticals</h2>
                        </div>

                    </div>
                </div>
                <div class="row mt-30">
                    <div class="col-xl-4 col-lg-4 col-md-6 roundbox mb-40">
                        <span> </span>
                        <span> </span>
                        <span> </span>
                        <span> </span>
                        <div class="services__item services__item-tp text-center mb-40">
                            <div class="services__item-content">
                                <div class="ser__icon mb-30">
                                    <img src="./assets/img/blog/4.jpg" alt="">
                                </div>
                                <h5 class="ser__title mb-10">Pipes And Tubes</h5>
                                <div class="ser__more-option mt-10">
                                    <a href="PipesAndTubes/index.php">View Details <i
                                            class="fal fa-long-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-40">
                        <div class="services__item services__item-tp text-center mb-40">
                            <div class="services__item-content">
                                <div class="ser__icon mb-30">
                                    <img src="./assets/img/blog/1.jpg" alt="">
                                </div>
                                <h5 class="ser__title mb-10">Forgings</h5>
                                <div class="ser__more-option mt-10">
                                    <a href="forging/index.php">View Details <i class="fal fa-long-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-40">
                        <div class="services__item services__item-tp  text-center mb-40">
                            <div class="services__item-content">
                                <div class="ser__icon mb-30">
                                    <img src="./assets/img/about/cwdpipe.jpg" alt="">
                                </div>
                                <h5 class="ser__title mb-10">CDW Tubes</h5>
                                <div class="ser__more-option mt-10">
                                    <a href="cdwtubes/index.php">View Details <i
                                            class="fal fa-long-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="services__item services__item-tp text-center mb-40">
                            <div class="services__item-content">
                                <div class="ser__icon mb-30">
                                    <img src="./assets/img/blog/3.jpg" alt="">
                                </div>
                                <h5 class="ser__title mb-10"> Coils & Sheets</h5>
                                <div class="ser__more-option mt-10">
                                    <a href="CoilsSheets/index.php">View Details <i
                                            class="fal fa-long-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="services__item services__item-tp text-center mb-40">
                            <div class="services__item-content">
                                <div class="ser__icon mb-30">
                                    <img src="./assets/img/about/feb.png" alt="">
                                </div>
                                <h5 class="ser__title mb-10">Infra & Energy</h5>
                                <div class="ser__more-option mt-10">
                                    <a href="fabrication/index.php">View Details <i
                                            class="fal fa-long-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 roundbox">
                        <span> </span>
                        <span> </span>
                        <span> </span>
                        <span> </span>
                        <div class="services__item services__item-tp text-center mb-40">
                            <div class="services__item-content">
                                <div class="ser__icon mb-30">
                                    <img src="./assets/img/about/road.png" alt="">
                                </div>
                                <h5 class="ser__title mb-10">Road Safety Division</h5>
                                <div class="ser__more-option mt-10">
                                    <a href="roadsafetydivision/index.php">View Details <i
                                            class="fal fa-long-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <!-- services__area end -->
        <section class="services__area pt-60 pb-60" data-background="assets/img/services/services-bg.jpg">
            <div class="container">
                <div class="row mb-4">
                    <div class="col-xl-12">
                        <div class="comman-head">
                            <h2>Our Verticals</h2>
                        </div>

                    </div>
                </div>
                <div class="row mt-30">
                <div class="col-xl-4 col-lg-4 col-md-6 col-6 mb-40">
                        <a href="cdwtubes" title="Checkout Details">
                            <div class="services__item services__item-tp  text-center mb-40">
                                <div class="services__item-content">
                                    <div class="ser__icon mb-30">
                                        <img src="./assets/img/product/cwdpipe.jpg" alt="CDW Tubes">
                                    </div>
                                    <h5 class="ser__title mb-10">CDW/DOM Tubes</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-6 mb-40">
                        <a href="forging" title="Checkout Details">
                            <div class="services__item services__item-tp text-center mb-40">
                                <div class="services__item-content">
                                    <div class="ser__icon mb-30">
                                        <img src="./assets/img/product/forging.jpg" alt="Forgings">
                                    </div>
                                    <h5 class="ser__title mb-10">Forgings</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-6">
                        <a href="fabrication" title="Checkout Details">
                            <div class="services__item services__item-tp text-center mb-40">
                                <div class="services__item-content">
                                    <div class="ser__icon mb-30">
                                        <img src="./assets/img/product/infra.jpg" alt="Infra & Energy">
                                    </div>
                                    <h5 class="ser__title mb-10">Infra & Energy</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                       
                    <div class="col-xl-4 col-lg-4 col-md-6 col-6" title="Checkout Details">
                        <a href="roadsafetydivision">
                            <div class="services__item services__item-tp text-center mb-40">
                                <div class="services__item-content">
                                    <div class="ser__icon mb-30">
                                        <img src="./assets/img/product/roadsafety.jpg" alt="Road Safety">
                                    </div>
                                    <h5 class="ser__title mb-10">Road Safety</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-6  mb-40">
                        <a href="PipesAndTubes" title="Checkout Details">
                            <div class="services__item services__item-tp text-center mb-40">
                                <div class="services__item-content">
                                    <div class="ser__icon mb-30">
                                        <img src="./assets/img/product/galvanized-steel-pipes.jpg"
                                            alt="Pipes And Tubes">
                                    </div>
                                    <h5 class="ser__title mb-10">Pipes And Tubes</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                   
                    
                    <div class="col-xl-4 col-lg-4 col-md-6 col-6">
                        <a href="CoilsSheets" title="Checkout Details">
                            <div class="services__item services__item-tp text-center mb-40">
                                <div class="services__item-content">
                                    <div class="ser__icon mb-30">
                                        <img src="./assets/img/product/coil-sheets.jpg" alt=" Coils & Sheets">
                                    </div>
                                    <h5 class="ser__title mb-10"> Coils & Sheets</h5>

                                </div>
                            </div>
                        </a>
                    </div>

                 

                </div>
            </div>
        </section>
        <!-- <section class="light-grey">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <img src="assets/img/MyVideo.gif" alt="broucher" class="w100">
                    </div>
                </div>
            </div>
        </section> -->
        <!-- end -->

        <!-- sd-banner-area start -->
        <!-- <section class="sd-banner-area pb-60 pt-60" data-background="assets/img/banner/banner-01.jpg">
            <div class="container">
                <div class="row ">
                    <div class="col-md-3  ">
                        <div class="slide-border1">
                            <div class="inner-slides card-1">
                                <img src="./assets/img/benifet/1.gif" alt="" class="s-icon-big" srcset="">
                                <p class="benefits-para"> 364000+ <br>
                                    MTPA
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3  ">
                        <div class="slide-border1">
                            <div class="inner-slides card-1">
                                <img src="./assets/img/benifet/2.gif" alt="" class="s-icon-big" srcset="">
                                <p class="benefits-para">100+ Export <br>
                                    Destination
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3  ">
                        <div class="slide-border1">
                            <div class="inner-slides card-1">
                                <img src="./assets/img/benifet/3.gif" alt="" class="s-icon-big" srcset="">
                                <p class="benefits-para">Rs. 3100+ Cr. <br>
                                    Revenue
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3  ">
                        <div class="slide-border1">
                            <div class="inner-slides card-1">
                                <img src="./assets/img/benifet/4.gif" alt="" class="s-icon-big" srcset="">
                                <p class="benefits-para"> 4000+ <br>
                                    Workforce
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <!-- end -->
        </div>
        <!----end---->
        </div>
        </div>
        </section>
        <!--- -end -->
        <!-- process__area start -->
        <section class="process__area light-grey pb-60 pt-60 ">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4 wow fadeInRight animated" data-wow-delay=".6s"
                                style="visibility: visible;-webkit-animation-delay: .6s; -moz-animation-delay: .6s; animation-delay: .6s;">
                                <div class="comman-head  ">
                                    <h2 class="csr">CSR </h2>
                                </div>
                                <div class="news-1">
                                    <div class="news-detail">
                                        <p>Businesses must reconnect company success with social progress. Shared value
                                            is not social responsibility, philanthropy, or even sustainability, but a
                                            new way to achieve economic success. It is not on the margin of what
                                            companies do but at the center. We believe that it can give rise to the next
                                            major transformation of business thinking.” - Good India Ltd
                                        </p>
                                    </div>
                                    <div class="read-more"><a href="#">Know more</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 wow fadeInLeft animated" data-wow-delay=".5s"
                                style="visibility: visible;-webkit-animation-delay: .5s; -moz-animation-delay: .5s; animation-delay: .5s;">
                                <img src="./assets/img/blog/28evlg_home_img.jpg" id="repnews1_ctl00_newsimage"
                                    class="img-fluid" alt="1l">
                            </div>
                        </div>
                    </div>
                    <!---start---->
                    <div class="col-md-12 mt-4">
                        <div class="row">
                            <div class="col-md-4 wow fadeInLeft animated" data-wow-delay=".5s"
                                style="visibility: visible;-webkit-animation-delay: .5s; -moz-animation-delay: .5s; animation-delay: .5s;">
                                <div class="home-video">
                                    <!-- <button type="button" class="video-btn" data-toggle="modal"
                                 data-src='https://www.youtube.com/embed/_Lyet3ElYL0' data-target='#m22'>
                                 <div class="icon-yuoutube"><img src="images/you-tube-icon.svg" alt="Youtube">
                                 </div>
                                 <img src="Uploads/SmallImages/22event_video-img.jpg" id="repnews2_ctl00_newsimage" class="img-fluid"
                                   alt="Caparo Wins International Business Of The Year Award - TOI" />
                                 </button> -->
                                    <img src="./assets/img/blog/1event.png" id="repnews2_ctl00_newsimage"
                                        class="img-fluid"
                                        alt="Caparo Wins International Business Of The Year Award - TOI">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6 pl-0 pr-0 wow fadeInLeft animated" data-wow-delay=".6s"
                                        style="visibility: visible;-webkit-animation-delay: .6s; -moz-animation-delay: .6s; animation-delay: .6s;">
                                        <div class="news-1 blue-box">
                                            <div class="news-detail">
                                                <h3>Welcome To</h3>
                                                <p> This congregation of the family.As the head of the family of several
                                                    thousand stakelholdersit gives me great pleasure to apprise you with
                                                    what has been a ver y eventful year gone by.</p>
                                            </div>
                                            <div class="read-more"><a href="#">Know
                                                    more</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pl-0 pr-0 wow fadeInLeft animated" data-wow-delay=".7s"
                                        style="visibility: visible;-webkit-animation-delay: .7s; -moz-animation-delay: .7s; animation-delay: .7s;">
                                        <div class="news-pic">
                                            <img src="./assets/img/blog/22event_video-img.png"
                                                id="repnews3_ctl00_newsimage" class="img-fluid"
                                                alt="Lord Swraj Paul donates $5 million to MIT">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!----end---->
                </div>
            </div>
        </section>
        <!-- process__area end -->
        <!-- testimonial__area start -->
        <section class="testimonial__area grey-bg-5 pt-60 pb-60 fix">
            <div class="testimonial__right-bg">
                <img src="assets/img/testimonial/testimonial-bg-1.jpg" alt="">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="section__wrapper mb-45">
                            <h4 class="section__title">Best innovations in the metallurgy today</h4>
                            <div class="r-text">
                                <span>feedback</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="testimonial__slider swiper-container">
                            <div class="testimonial__wrapper swiper-wrapper">
                                <div class="testimonial__item swiper-slide">
                                    <p class="review__text">“ A user review is a review conducted by any person who has
                                        access to the internet and publishes their ux to a review site or social media
                                        platform ”</p>
                                    <div class="review__info mt-30">
                                        <a href="#"><img src="assets/img/author/client-1.jpg" alt=""></a>
                                        <div class="client__content">
                                            <h5 class="client__name"><a href="#">Alonso Dowson</a></h5>
                                            <div class="client__designation">
                                                <p>Head Of Idea , <a href="#">Alonso Co.</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial__item swiper-slide">
                                    <p class="review__text">“ A user review is a review conducted
                                        by any person who has access to the
                                        internet and publishes their ux to a
                                        review site or social media platform ”
                                    </p>
                                    <div class="review__info mt-30">
                                        <a href="#"><img src="assets/img/author/client-2.jpg" alt=""></a>
                                        <div class="client__content">
                                            <h5 class="client__name"><a href="#">Rosalina William</a></h5>
                                            <div class="client__designation">
                                                <p>CEO , <a href="#">Rosa Corporation</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial__item swiper-slide">
                                    <p class="review__text">“ Design quality superlative
                                        account on its cloud platform. Think Dikons is the best theme I ever
                                        seen this year. Amazing design, easy to
                                        customize a ”
                                    </p>
                                    <div class="review__info mt-30">
                                        <a href="#"><img src="assets/img/author/client-3.jpg" alt=""></a>
                                        <div class="client__content">
                                            <h5 class="client__name"><a href="#">Iqbal Hossain</a></h5>
                                            <div class="client__designation">
                                                <p>Founder , <a href="#">Gazi Company</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial__item swiper-slide">
                                    <p class="review__text">“ Amazing design, easy to
                                        customize a design quality superlative
                                        account on its cloud platform. Think Dikons is the best theme I ever
                                        seen this year. ”
                                    </p>
                                    <div class="review__info mt-30">
                                        <a href="#"><img src="assets/img/author/client-4.jpg" alt=""></a>
                                        <div class="client__content">
                                            <h5 class="client__name"><a href="#">Josep D. William</a></h5>
                                            <div class="client__designation">
                                                <p>Head Of Idea , <a href="#">William Co.</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- testimonial__area end -->
        <!-- brand__area start -->
        <section class="brand__area pb-60 pt-0" style="background: white;">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="comman-head">
                            <h2>Our Clients</h2>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="brand__slider swiper-container">

                            <div class="swiper-wrapper">
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/forging/brand/1.png" alt="1" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/forging/brand/2.png" alt="2" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/forging/brand/3.png" alt="3" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/forging/brand/4.png" alt="4" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/forging/brand/5.png" alt="5" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/forging/brand/6.png" alt="6" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/forging/brand/7.png" alt="7" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/forging/brand/8.png" alt="8" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/forging/brand/9.png" alt="9" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/forging/brand/10.png" alt="10" class="w100">
                                </div>

                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/forging/brand/11.png" alt="11" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/forging/brand/12.png" alt="12" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/forging/brand/13.png" alt="13" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/forging/brand/14.png" alt="14" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/forging/brand/15.png" alt="15" class="w100">
                                </div>
                                <!-- end -->
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/1.png" alt="15" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/2.png" alt="15" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/3.png" alt="15" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/4.png" alt="15" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/5.png" alt="15" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/6.png" alt="15" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/7.png" alt="15" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/8.png" alt="15" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/9.png" alt="15" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/10.png" alt="15" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/11.png" alt="15" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/12.png" alt="15" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/13.png" alt="15" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/14.png" alt="15" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/15.png" alt="15" class="w100">
                                </div>
                                <div class="brand__slider-item swiper-slide">
                                <img src="./assets/PipesAndTubes/brand/16.png" alt="15" class="w100">
                                </div>
                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- brand__area end -->
    </main>
    <?php include './inc/footer.php';?>