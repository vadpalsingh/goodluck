<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India :: Downloads</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <main>
        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item  active ">
                        <img src="./assets/PipesAndTubes/banner/common.jpg" class="d-block w-100" alt="Financial">
                        <h5>Downloads</h5>
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="innerpagenav">
            <div class="container">
                <div class="row">
                    <ul class="pagenav">
                        <li><a href="investors.php">Investors</a></li>
                        <li><a href="financial.php">Financial</a></li>
                        <li><a href="press-release.php">Press Release</a></li>
                        <li><a href="investors-news.php">Investors News</a></li>
                        <li><a href="shareholding-pattern.php">Shareholding Pattern</a></li>
                        <li><a href="shareholder-information.php">Shareholder Information</a></li>
                        <li><a href="downloads.php" class="active">Downloads</a></li>
                        <li><a href="corporate-governance.php">Corporate Governance</a></li>
                    </ul>
                </div>
            </div>
        </section>

        <!-- end -->

        <section class="export pb-60 p60  wow fadeInUp   animated" data-wow-delay="0.5s"
            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
            <div class="container">
                <div class="row">
                    <div class="sec_title">
                        <h1 class="h_padding pt-0 wow fadeInRight   animated" data-wow-delay="0s"
                            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">Downloads
                        </h1>
                    </div>
                    <div class="ins-det">
                        <div class="dn-info"><a
                                href="pdf/request-for-registering-pan-kyc-details-or-changes-updation-thereof-form-isr-1.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Request for
                                registering PAN, KYC details or changes updation thereof - Form
                                ISR-1</a></div>

                        <div class="dn-info "><a href="pdf/change-of-nominee-form-no-sh-14.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Change of Nominee -
                                Form No. SH-14</a></div>
                        <div class="dn-info"><a
                                href="pdf/confirmation-of-signature-of-securities-holder-by-the-banker-form-isr-2.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Confirmation of
                                Signature of securities holder by the Banker - Form ISR-2</a></div>
                        <div class="dn-info"><a href="pdf/declaration-form-for-opting-out-of-nomination-form-isr-3.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Declaration Form for
                                Opting-out of Nomination - Form ISR-3</a></div>
                        <div class="dn-info"><a
                                href="pdf/isr-4-request-for-issue-of-duplicate-certificate-and-other-service-requests.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> ISR-4 Request for
                                issue of Duplicate Certificate and other Service Requests</a></div>
                        <div class="dn-info"><a href="pdf/nomination-form-form-no-sh-13.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Nomination Form -
                                Form no. SH-13</a></div>
                        <div class="dn-info"><a
                                href="pdf/sebi-circular-common-and-simplified-norms-for-processing-investors-service-requests-by-rtas-and-norms-for-furnishing-pan-kyc-details-and-nomination.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> SEBI Circular -
                                Common and simplified norms for processing investor’s service
                                requests by RTAs and norms for furnishing PAN, KYC details and
                                Nomination</a></div>
                    </div>
                    <!-- end -->
                    
                </div>
        </section>
        <!-- end -->
    </main>
    <?php include './inc/footer.php';?>