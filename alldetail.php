<!doctype html>
<html class="no-js" lang="eng">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Goodluck Industries  CDW Tubes (Precision Tube Division) :: Quality </title>
<meta name="description" content="">

<?php include './inc/header.php'; ?>


<main>
  
    <section class="p60 leadership">
	   <div class="container">
	    <div class="row justify-content-center align-items-center"> 
           <h2>Download Quality  Certificate</h2>      	
	        <div class="col-lg-3 col-md-3 col-sm-6">                        
                <div class="wow fadeInUp pdfbox" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    <a href="../assets/cwdpipe/certificate/Good-Luck-Industrie-EMS-607465-ISO-14001-Valid-16-11-2025.pdf" target="_blank">
                       EMS-607465 ISO-14001<span class="Date">Valid : 16-NOV-2025</span>
                       <img src="../assets/cwdpipe/certificate/pdficon.png">
                    </a>   
                </div>              
			</div>
            <div class="col-lg-3 col-md-3 col-sm-6">      
                <div class="wow fadeInUp pdfbox" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    <a href="../assets/cwdpipe/certificate/Good-Luck-Industries-OHS-607466-ISO-45001-Valid-16-11-2025.pdf" target="_blank">
                    OHS-607466 ISO-45001<span class="Date">Valid : 16-NOV-2025</span>
                    <img src="../assets/cwdpipe/certificate/pdficon.png">
                    </a>   
                </div>  
			</div> 			
            <div class="col-lg-3 col-md-3 col-sm-6">      
                <div class="wow fadeInUp pdfbox" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    <a href="../assets/cwdpipe/certificate/Goodluck-industries-IATF-16949-Valid-9-4-24 .pdf" target="_blank">
                      IATF-16949<span class="Date">Valid : 9-MARCH-2024</span>
                      <img src="../assets/cwdpipe/certificate/pdficon.png">
                    </a>   
                </div>  
			</div> 		
            <div class="col-lg-3 col-md-3 col-sm-6">      
                <div class="wow fadeInUp pdfbox" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s;">
                    <a href="../assets/cwdpipe/certificate/Goodluck-Industries-00.12.0737-ISO9001-Valid-4-11-2023.pdf" target="_blank">
                      00.12.0737 EN ISO-9001<span class="Date">Valid : 4-NOV-2023</span>
                      <img src="../assets/cwdpipe/certificate/pdficon.png">
                    </a>   
                </div>  
			</div>     	
             			
		 </div>        
       </div>
    </section>


</main>



<?php include './inc/footer.php';?>