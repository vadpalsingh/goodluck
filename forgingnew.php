<?php include './inc/header.php'; ?>


<main>
    <!-- hero -->

    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="./assets/forging/banner/b1.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block c1 blue">
                        <h5>manufacturer of die forgings</h5>
                        <p>Our manufacturing facility boasts of state of the art equipment with Close Die and Open Die
                            Forging Machines </p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="./assets/forging/banner/b2.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block c2 blue">
                        <h5>HEAT TREATMENT</h5>
                        <p>Our manufacturing facility boasts of state of the art equipment with Close Die and Open Die
                            Forging Machines</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="./assets/forging/banner/b3.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block c3 blue">
                        <h5> Quality and all our operations</h5>
                        <p>Our manufacturing facility boasts of state of the art equipment with Close Die and Open Die
                            Forging Machines</p>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>
    <!-- hero -->


    <!-- about -->
    <section class="p60 abou_t">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="comman-head">
                        <h2 class="pb3">Welcome To Forgings </h2>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="g_bout">


                        <div class="home-about wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">
                            <blockquote>
                                With Total capacity of 18,000 Tons per annum, our facility is equipped to manufacture a
                                complete range of Grades in Carbon and Alloy Steel, Stainless, Duplex, Super Duplex,
                                Nickel Alloys, Titanium & Aluminium Alloys.

                            </blockquote>
                            <p class="wow fadeInUp" data-wow-delay=".8s"
                                style="visibility: visible; animation-delay: 0.8s;">
                                The Galvanized Pipes are manufactured<br> using steel and then coated using the
                                process of galvanizing. <br>These pipes are thoroughly tested
                                and examined in accordance with international standards.

                            </p>
                            <p class="wow fadeInUp" data-wow-delay=".9s"
                                style="visibility: visible; animation-delay: 0.8s;">Our manufacturing facility boasts of
                                state of the art equipment with Close Die and Open Die Forging Machines - 6 Machines
                                covering complete range of Forging from 1/2" to 80" under one roof with Manipulator to
                                handle larger single piece Forging</p>
                            <div class="read-more">
                                <a href="about-us/company-profile.html?mpgid=2&amp;pgidtrail=3">Know more</a>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-md-4">
                    <div class="a_im_g">
                        <div class="row g-4 align-items-center">
                            <div class="col-sm-6">
                                <!-- <div class="bg-primary p-4 mb-4 wow fadeIn" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeIn;"><i class="fa fa-users fa-2x text-white mb-3"></i>
                                <h2 class="text-white mb-2" data-toggle="counter-up">1234</h2>
                                <p class="text-white mb-0">Happy Clients</p>
                                </div> -->
                                <div class="bg-dark cunt p-3 mb-4 wow fadeIn " data-number="2025"
                                    style="visibility: visible;"><i class="fa fa-briefcase"></i>
                                    <p id="number1" class="number">2025</p>
                                    <span></span>
                                    <p>Projects done</p>
                                </div>
                                <!-- <div class="bg-secondary p-4 wow fadeIn" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeIn;"><i class="fa fa-ship fa-2x text-white mb-3"></i>
                                <h2 class="text-white mb-2" data-toggle="counter-up">1234</h2>
                                <p class="text-white mb-0">Complete Shipments</p>
                                </div> -->
                                <div class="bg-success cunt p-3 wow fadeIn" data-number="1055"
                                    style="visibility: visible;">

                                    <i class="fa fa-user"></i>
                                    <p id="number2" class="number">1055</p>
                                    <span></span>
                                    <p>Happy clients</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <!-- <div class="bg-success p-4 wow fadeIn" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeIn;"><i class="fa fa-star fa-2x text-white mb-3"></i>
                                <h2 class="text-white mb-2" data-toggle="counter-up">1234</h2>
                                <p class="text-white mb-0">Customer Reviews</p>
                                </div> -->
                                <div class="bg-danger cunt p-3 wow fadeIn" data-number="3059"
                                    style="visibility: visible;">
                                    <i class="fa fa-users"></i>
                                    <p id="number3" class="number">3059</p>
                                    <span></span>
                                    <p>Employee</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- counter -->
    <!-- service -->

    <section class="brand__area p60">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle">
                        <h2>Product</h2>
                        <span class="headerLine"></span>
                        <p class="p_color">Established in the year 1986, Goodluck India Limited is an ISO 9001:2008
                            certified
                            organization,<br> engaged in manufacturing and exporting of a wide range of galvanized
                            sheets & coils, <br>towers, hollow sections, CR coils CRCA and pipes & tubes.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="service__slider swiper-container">
                        <div class="swiper-wrapper">
                            <div class="brand__slider-item swiper-slide">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="./assets/forging/product/p1.png" alt="" class="res">
                                        </div>
                                        <h3>Name </h3>
                                    </div>
                                </a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="./assets/forging/product/p2.png" alt="" class="res">
                                        </div>
                                        <h3>Name </h3>
                                    </div>
                                </a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="./assets/forging/product/p3.png" alt="" class="res">
                                        </div>
                                        <h3>Name </h3>
                                    </div>
                                </a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="./assets/forging/product/p4.png" alt="" class="res">
                                        </div>
                                        <h3>Name </h3>
                                    </div>
                                </a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="./assets/forging/product/p5.png" alt="" class="res">
                                        </div>
                                        <h3>Name </h3>
                                    </div>
                                </a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="./assets/forging/product/p6.png" alt="" class="res">
                                        </div>
                                        <h3>Name </h3>
                                    </div>
                                </a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="./assets/forging/product/p7.png" alt="" class="res">
                                        </div>
                                        <h3>Name </h3>
                                    </div>
                                </a>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- service -->

    <!-- tab -->
    <section class="capabilities_tab wow fadeInUp animated p60" data-wow-delay=".4s"
        style="visibility: visible;-webkit-animation-delay: .4s; -moz-animation-delay: .4s; animation-delay: .4s;">

        <div class="container">
            <div class="row">

                <div class="col-md-3 bg-white">
                    <div class="n_tabs mt-5 pt5">
                        <br>
                        <ul class="nav pt5 nav-tabs justify-content-center" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active " id="home-tab" data-bs-toggle="tab"
                                    data-bs-target="#home-tab-pane" type="button" role="tab"
                                    aria-controls="home-tab-pane" aria-selected="true">Manufacturing
                                </button>
                            </li>

                            <li class="nav-item" role="presentation">
                                <button class="nav-link " id="profile-tab" data-bs-toggle="tab"
                                    data-bs-target="#profile-tab-pane" type="button" role="tab"
                                    aria-controls="profile-tab-pane" aria-selected="false"> Engineering </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="contact-tab" data-bs-toggle="tab"
                                    data-bs-target="#contact-tab-pane" type="button" role="tab"
                                    aria-controls="contact-tab-pane" aria-selected="false">Quality</button>
                            </li>
                            <!-- <li class="nav-item" role="presentation">
                        <button class="nav-link" id="disabled-tab" data-bs-toggle="tab" data-bs-target="#disabled-tab-pane" type="button" role="tab" aria-controls="disabled-tab-pane" aria-selected="false"  >Disabled</button>
                        </li> -->
                        </ul>
                    </div>
                </div>
                <!-- end -->
                <div class="col-md-9">
                    <div class="c_0ntan_t">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel"
                                aria-labelledby="home-tab" tabindex="0">
                                <!-- start -->
                                <div class="container">
                                    <div class="row">

                                        <div class="col-md-5">
                                            <div class="g_bout_e">


                                                <div class="home-about h_l   ">
                                                    <blockquote>


                                                        Good Luck, A 36 Years Old group
                                                        promoted by IITians, a steel
                                                        processor,


                                                    </blockquote>
                                                    <p class="wow  ">


                                                        We manufacture an extensive range of Welded<br> Tubes & Pipes
                                                        that are available in different<br> Structural shapes such as
                                                        round, square and <br>Rectangular hollow sections,<br> which can
                                                        be easily welded into tube, <br>tubing and structures.
                                                    </p>
                                                    <div class="read-more">
                                                        <a href="#">Know
                                                            more</a>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="a_im_g">
                                                <img src="./assets/forging/product/manu_f.png" alt="res" class="res">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- \end -->
                            <div class="tab-pane fade  " id="profile-tab-pane" role="tabpanel"
                                aria-labelledby="profile-tab" tabindex="0">
                                <div class="container">
                                    <div class="row">

                                        <div class="col-md-5">
                                            <div class="g_bout_e">


                                                <div class="home-about h_l   ">
                                                    <blockquote>


                                                        Good Luck, A 36 Years Old group
                                                        promoted by IITians, a steel
                                                        processor,


                                                    </blockquote>
                                                    <p class="  ">


                                                        The Galvanized Pipes are manufactured<br> using steel and then
                                                        coated using the
                                                        process of galvanizing. <br>These pipes are thoroughly tested
                                                        and examined in accordance with international standards.
                                                    </p>
                                                    <div class="read-more">
                                                        <a href="#">Know
                                                            more</a>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="a_im_g">
                                                <img src="./assets/forging/product/eng.png" alt="res" class="res">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end -->
                            <div class="tab-pane fade" id="contact-tab-pane" role="tabpanel"
                                aria-labelledby="contact-tab" tabindex="0">
                                <div class="container">
                                    <div class="row">

                                        <div class="col-md-5">
                                            <div class="g_bout_e">


                                                <div class="home-about h_l   ">
                                                    <blockquote>


                                                        Good Luck, A 36 Years Old group
                                                        promoted by IITians, a steel
                                                        processor,


                                                    </blockquote>
                                                    <p class="  ">


                                                        The Galvanized Pipes are manufactured<br> using steel and then
                                                        coated using the
                                                        process of galvanizing. <br>These pipes are thoroughly tested
                                                        and examined in accordance with international standards.
                                                    </p>
                                                    <div class="read-more">
                                                        <a href="#">Know
                                                            more</a>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="a_im_g">
                                                <img src="./assets/forging/product/quelity.png" alt="res" class="res">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="tab-pane fade" id="disabled-tab-pane" role="tabpanel" aria-labelledby="disabled-tab" tabindex="0">.4.</div> -->
                        </div>
                    </div>
                </div>


            </div>
        </div>


    </section>
    <!-- tab -->
   
    <!-- Gallery start---->
    
    <section id="gallery" class="bg-white p60 ">
        <div class="container">
        <div class="row">
                <div class="col-xl-12">
                     
                    <div class="sectiontitle">
                        <h2>Our Gallery</h2>
                        <span class="headerLine"></span>
                     
                    </div>
                </div>
            </div>
            <!--  -->
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/forging/gallery/p1.png"><img src="./assets/forging/gallery/p11.png"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                        <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/forging/gallery/p22.png"><img src="./assets/forging/gallery/p2.png"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                        <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/forging/gallery/p33.png"><img src="./assets/forging/gallery/p3.png"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                        <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/forging/gallery/p44.png"><img src="./assets/forging/gallery/p4.png "
                                class="img-responsive"></a>
                        <div class="img-overlay">
                           <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/forging/gallery/p55.png"><img src="./assets/forging/gallery/p5.png"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                        <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/forging/gallery/p66.png"><img src="./assets/forging/gallery/p6.png"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                        <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/forging/gallery/p77.png"><img src="./assets/forging/gallery/p7.png"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                        <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/forging/gallery/p88.png"><img src="./assets/forging/gallery/p8.png"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                        <span>
                           <i class="fa fa-plus-circle" aria-hidden="true"></i><br>
                            <p class="text-cener text-white">   Name</p>
                           </span>
                        </div>
                    </div>
                </div>

            </div><!-- End row -->
        </div><!-- End image gallery -->
        </div><!-- End container -->
    </section>
<!-- gallery -->
    

    <!-- brand__area start -->
    <section class="brand__area1 p60" style="    background-color: #fdfdfd;">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="brand__title text-center">
                        <h1> Customer </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="brand__slider p-0 swiper-container">
                        <div class="swiper-wrapper">
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="assets/img/brand/1.jpg" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="assets/img/brand/2.jpg" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="assets/img/brand/3.jpg" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="assets/img/brand/4.jpg" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="assets/img/brand/5.jpg" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="assets/img/brand/6.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- brand__area end -->
   
</main>



<?php include './inc/footer.php';?>
