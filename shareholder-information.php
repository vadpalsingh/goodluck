<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India :: Shareholder Information</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <main>
        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item  active ">
                        <img src="./assets/PipesAndTubes/banner/common.jpg" class="d-block w-100" alt="Financial">
                        <h5>Shareholder Information</h5>
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="innerpagenav">
            <div class="container">
                <div class="row">
                    <ul class="pagenav">
                        <li><a href="investors.php">Investors</a></li>
                        <li><a href="financial.php">Financial</a></li>
                        <li><a href="press-release.php">Press Release</a></li>
                        <li><a href="investors-news.php">Investors News</a></li>
                        <li><a href="shareholding-pattern.php">Shareholding Pattern</a></li>
                        <li><a href="shareholder-information.php" class="active">Shareholder Information</a></li>
                        <li><a href="downloads.php">Downloads</a></li>
                        <li><a href="corporate-governance.php">Corporate Governance</a></li>
                    </ul>
                </div>
            </div>
        </section>

        <!-- end -->

        <section class="export pb-60 p60  wow fadeInUp   animated" data-wow-delay="0.5s"
            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
            <div class="container">
                <div class="row">
                    <div class="sec_title">
                        <h1 class="h_padding pt-0 wow fadeInRight   animated" data-wow-delay="0s"
                            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">Shareholder
                            Information
                        </h1>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th align="center" class="text-center">AGM</th>
                                    <th align="center" class="text-center">AGM Outcome</th>
                                    <th align="center" class="text-center">Postal Ballot</th>
                                    <th align="center" class="text-center">EGM</th>
                                    <th align="center" class="text-center">Amalgamation</th>
                                    <th align="center" class="text-center">Annual Return</th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td align="center"><a href="pdf/goodluck-notice-agm.pdf" target="_blank"><strong>
                                                Good Luck AGM Notice 2014</strong></a></td>
                                    <td align="center"> <a href="pdf/outcome-agm-2014.pdf" target="_blank"><strong>
                                                Outcome AGM 2014</strong></a></td>
                                    <td align="center"><a href="pdf/postal-ballet.pdf"
                                            target="_blank"><strong>Notice</strong></a></td>
                                    <td align="center"><a href="pdf/egm-notice-20-11-14.pdf" target="_blank"><strong>EGM
                                                Notice</strong></a></td>
                                    <td align="center"><a href="pdf/amalgamation-scheme.pdf"
                                            target="_blank"><strong>Scheme</strong></a></td>
                                    <td align="center"><a href="pdf/annual-return-2020-21.pdf"
                                            target="_blank"><strong>Annual Return 2020-21</strong></a></td>

                                </tr>

                                <tr>
                                    <td align="center"><a href="pdf/goodluck-notice-agm1.pdf" target="_blank"><strong>
                                                Good Luck AGM Notice 2015</strong></a></td>
                                    <td align="center"><a href="pdf/outcome-agm-2015.pdf" target="_blank"><strong>
                                                Outcome AGM 2015</strong></a></td>
                                    <td align="center"><a href="pdf/postal-ballot-result.pdf"
                                            target="_blank"><strong>Postal Ballot Result</strong></a></td>
                                    <td align="center"><a href="pdf/outcome-egm-35a.pdf" target="_blank"><strong>
                                                Outcome EGM 35A</strong></a></td>
                                    <td align="center"><a href="pdf/complaint-report.pdf" target="_blank"><strong>
                                                Complaint Report</strong></a></td>
                                    <td align="center"><a href="pdf/annual-return-2021-22.pdf"
                                            target="_blank"><strong>Annual Return 2021-22</strong></a></td>
                                </tr>

                                <tr>
                                    <td align="center"><a href="pdf/goodluck-notice-agm-2016.pdf"
                                            target="_blank"><strong> Good Luck AGM Notice 2016</strong></a></td>
                                    <td align="center"><a href="pdf/outcome-agm-2016.pdf" target="_blank"><strong>
                                                Outcome AGM 2016</strong></a></td>
                                    <td align="center"><a href="pdf/postal-ballot-notice-07.05.2016.pdf"
                                            target="_blank"><strong> Postal Ballot Notice 07.05.2016</strong></a></td>
                                    <td align="center"> <strong> -</strong></td>
                                    <td align="center"><a href="pdf/observation-letter.pdf" target="_blank"><strong>
                                                Observation Letter</strong></a></td>
                                    <td align="center"> <strong> -</strong></td>
                                </tr>


                                <tr>
                                    <td align="center"><a href="pdf/goodluck-agm-notice-2017.pdf"
                                            target="_blank"><strong>GOODLUCK AGM NOTICE 2017<br> (Goodluck AGM
                                                Notice)</strong></a></td>
                                    <td align="center"><a href="pdf/voting-result-2017.pdf" target="_blank"><strong>
                                                Outcome AGM 2017</strong></a></td>
                                    <td align="center"><a href="pdf/postal-ballot-result-11-06-16.pdf"
                                            target="_blank"><strong>Postal Ballot Result 11.06.16</strong></a></td>
                                    <td align="center"> <strong> -</strong></td>
                                    <td align="center"><a href="pdf/dasti-order.pdf" target="_blank"><strong> 1st Motion
                                                Order</strong></a></td>
                                    <td align="center"> <strong> -</strong></td>
                                </tr>

                                <tr>
                                    <td align="center"> <a href="pdf/goodluck-agm-notice-2018.pdf"
                                            target="_blank"><strong>GOODLUCK AGM NOTICE 2018</strong></a></td>
                                    <td align="center"><a href="pdf/voting-result-2018.pdf" target="_blank"><strong>
                                                Outcome AGM 2018</strong></a></td>
                                    <td align="center"><a href="pdf/postal-ballot-notice-11-11-16.pdf"
                                            target="_blank"><strong>Postal Ballot Notice 11.11.16</strong></a></td>
                                    <td align="center"> <strong> -</strong></td>
                                    <td align="center"><a href="pdf/dasti-order.pdf" target="_blank"><strong> Court
                                                Order</strong></a></td>
                                    <td align="center"> <strong> -</strong></td>
                                </tr>

                                <tr>
                                    <td align="center"><a href="pdf/goodluck-agm-notice-2019.pdf"
                                            target="_blank"><strong>GOODLUCK AGM NOTICE 2019</strong></a></td>
                                    <td align="center"><a href="pdf/outcome-agm-2019.pdf" target="_blank"><strong>
                                                Outcome AGM 2019</strong></a></td>
                                    <td align="center"><a href="pdf/Postalballotresult151216.pdf"
                                            target="_blank"><strong>Postal Ballot Result 15.12.16</strong></a></td>
                                    <td align="center"> <strong> -</strong></td>
                                    <td align="center"><a href="pdf/formal-order-5-11-16.pdf" target="_blank"><strong>
                                                Court Order (Formal Order 05.11.16)</strong></a></td>
                                    <td align="center"> <strong> -</strong></td>
                                </tr>

                                <tr>
                                    <td align="center"><a href="pdf/goodluck-agm-notice-2020-new.pdf"
                                            target="_blank"><strong>GOODLUCK AGM NOTICE 2020</strong></a></td>
                                    <td align="center"><a href="pdf/outcome-agm-2020.pdf" target="_blank"><strong>
                                                Outcome AGM 2020</strong></a></td>
                                    <td align="center"><a href="pdf/postal-ballot-notice-13-08-2019.pdf"
                                            target="_blank"><strong>Postal Ballot Notice 13.08.2019</strong></a></td>
                                    <td align="center"><strong> -</strong></td>
                                    <td align="center"><a href="pdf/amalgamation-scheme-31-07-2019.pdf"
                                            target="_blank"><strong>Scheme 31.07.2019</strong></a></td>
                                    <td align="center"> <strong> -</strong></td>
                                </tr>
                                <tr>
                                    <td align="center"><a href="pdf/goodluck-agm-notice-2021.pdf"
                                            target="_blank"><strong>GOODLUCK AGM NOTICE 2021</strong></a></td>
                                    <td align="center"><a href="pdf/outcome-agm-2021.pdf" target="_blank"><strong>
                                                Outcome AGM 2021</strong></a></td>
                                    <td align="center"><a href="pdf/postal-ballot-result-17-09-2019.pdf"
                                            target="_blank"><strong>Postal Ballot Result 17.09.2019</strong></a></td>
                                    <td align="center"></td>
                                    <td align="center"><a href="pdf/notice-to-shareholders.pdf"
                                            target="_blank"><strong>Notice to Shareholders</strong></a></td>
                                    <td align="center"> <strong> -</strong></td>
                                </tr>
                                <tr>
                                    <td align="center"><a href="pdf/goodluck-agm-notice-2022.pdf"
                                            target="_blank"><strong>GOODLUCK AGM NOTICE 2022</strong></a></td>
                                    <td align="center"><a href="pdf/outcome-agm-2022.pdf" target="_blank"><strong>
                                                Outcome AGM 2022</strong></a></td>
                                    <td align="center"><a href="pdf/postal-ballot-notice-19-04-2021.pdf"
                                            target="_blank"><strong>Postal Ballot Notice 19.04.2021</strong></a></td>
                                    <td align="center"></td>
                                    <td align="center"><a href="pdf/notice-to-secured-creditor.pdf"
                                            target="_blank"><strong>Notice to Secured Creditor</strong></a></td>
                                    <td align="center"> <strong> -</strong></td>
                                </tr>
                                <tr>
                                    <td align="center"><strong> -</strong></td>
                                    <td align="center"><strong> -</strong></td>
                                    <td align="center"><a href="pdf/postal-ballot-result-24-05-2021.pdf"
                                            target="_blank"><strong>Postal Ballot Result 24.05.2021</strong></a></td>
                                    <td align="center"><strong>-</strong></td>
                                    <td align="center"><a href="pdf/notice-to-un-secured-creditor.pdf"
                                            target="_blank"><strong>Notice to Un Secured Creditor</strong></a></td>
                                    <td align="center"> <strong> -</strong></td>
                                </tr>
                                <tr>
                                    <td align="center"><strong> -</strong></td>
                                    <td align="center"><strong> -</strong></td>
                                    <td align="center"><a href="pdf/postal-ballot-notice-06-06-2022.pdf"
                                            target="_blank"><strong>Postal Ballot Notice 06.06.2022</strong></a></td>
                                    <td align="center"><strong>-</strong></td>
                                    <td align="center"><a href="pdf/final-order-20-02-2020.pdf"
                                            target="_blank"><strong>Final Order - 20.02.2020</strong></a></td>
                                    <td align="center"> <strong> -</strong></td>
                                </tr>
                                <tr>
                                    <td align="center"><strong> -</strong></td>
                                    <td align="center"><strong> -</strong></td>
                                    <td align="center"><a href="pdf/postal-ballot-result-07-07-2021.pdf"
                                            target="_blank"><strong>Postal Ballot Result 07.07.2022</strong></a></td>
                                    <td align="center"><strong>-</strong></td>
                                    <td align="center"><strong>-</strong></td>
                                    <td align="center"> <strong> -</strong></td>
                                </tr>
                                <tr>
                                    <td align="center"><strong> -</strong></td>
                                    <td align="center"><strong> -</strong></td>
                                    <td align="center"><a href="pdf/postal-ballot-notice-04-11-2022-new.pdf"
                                            target="_blank"><strong>Postal Ballot Notice 04.11.2022</strong></a></td>
                                    <td align="center"><strong>-</strong></td>
                                    <td align="center"><strong>-</strong></td>
                                    <td align="center"> <strong> -</strong></td>
                                </tr>
                                <tr>
                                    <td align="center"><strong> -</strong></td>
                                    <td align="center"><strong> -</strong></td>
                                    <td align="center"><a href="pdf/corrigendum-to-postal-ballot-notice-04-11-2022.pdf"
                                            target="_blank"><strong>Corrigendum to postal ballot Notice
                                                04.11.2022</strong></a></td>
                                    <td align="center"><strong>-</strong></td>
                                    <td align="center"><strong>-</strong></td>
                                    <td align="center"> <strong> -</strong></td>
                                </tr>
                                <tr>
                                    <td align="center"><strong> -</strong></td>
                                    <td align="center"><strong> -</strong></td>
                                    <td align="center"><a href="pdf/postal-ballot-results-06-12-2022.pdf"
                                            target="_blank"><strong>Postal Ballot Results 06.12.2022</strong></a></td>
                                    <td align="center"><strong>-</strong></td>
                                    <td align="center"><strong>-</strong></td>
                                    <td align="center"> <strong> -</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="ins-det board mt-4">



 
                    <div class="sh-info"><a
                                href="pdf/NEWSPAPERADVERTISEMENt2023.pdf"   target="_new">
                                <i class="fa fa-file-pdf-o"></i>  NEWSPAPER ADVERTISEMENT OF UNAUDITED FINANCIAL RESULTS FOR THE Q1 JUNE 2023</a></div>
                        <a href="pdf/NEWSPAPERADVERTISEMENt2023.pdf"  target="_new">
                        </a>
                        <div class="sh-info"><a
                                href="pdf/board-meeting-to-consider-unaudited-financial-results-for-q1fy2023-24.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Board Meeting to consider Unaudited
                                Financial Results for Q1FY2023-24</a></div>
                        <a href="pdf/board-meeting-to-consider-unaudited-financial-results-for-q1fy2023-24.pdf"
                            target="_new">
                        </a>
                        <div class="sh-info"><a
                                href="pdf/board-meeting-to-consider-unaudited-financial-results-for-q1fy2023-24.pdf"
                                target="_new"></a><a href="pdf/annual-secretarial-compliance-report-2022-23.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Annual Secretarial Compliance Report
                                2022-23</a></div>
                        <a href="pdf/annual-secretarial-compliance-report-2022-23.pdf" target="_new">
                        </a>
                        <div class="sh-info"><a href="pdf/annual-secretarial-compliance-report-2022-23.pdf"
                                target="_new"></a><a href="pdf/iepf-unpaid-dividend-financial-year-2015-16-interim.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> LIST OF SHAREHOLDERS WHOSE SHARES TO BE
                                TRANSFERRED TO IEPF IN RELATION TO UNPAID DIVIDEND FOR THE FINANCIAL YEAR 2015-16
                                (INTERIM)</a></div>
                        <a href="pdf/iepf-unpaid-dividend-financial-year-2015-16-interim.pdf" target="_new">
                        </a>
                        <div class="sh-info"><a href="pdf/iepf-unpaid-dividend-financial-year-2015-16-interim.pdf"
                                target="_new"></a><a
                                href="pdf/newspaper-advertisement-of-audited-financial-results-2022-23.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Newspaper Advertisement of Audited
                                Financial Results 2022-23</a></div>
                        <a href="pdf/newspaper-advertisement-of-audited-financial-results-2022-23.pdf" target="_new">
                        </a>
                        <div class="sh-info"><a
                                href="pdf/newspaper-advertisement-of-audited-financial-results-2022-23.pdf"
                                target="_new"></a><a
                                href="pdf/board-meeting-to-consider-audited-financial-results-and-final-dividend-2022-23.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Board Meeting to consider Audited
                                Financial Results and Final Dividend 2022-23</a></div>
                        <div class="sh-info"><a
                                href="pdf/newspaper-advertisement-for-record-date-of-interim-dividend-2022-23.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Newspaper Advertisement for Record date
                                of Interim Dividend 2022-23</a></div>
                        <div class="sh-info"><a
                                href="pdf/newspaper-advertisement-for-transfer-of-interim-dividend-2015-16-to-iepf.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Newspaper Advertisement for Transfer of
                                Interim Dividend(2015-16) to IEPF</a> </div>
                        <div class="sh-info"><a href="pdf/outcome-of-board-meeting-dated-08-04-2023.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Outcome of Board Meeting dated
                                08.04.2023</a></div>
                        <div class="sh-info"><a
                                href="pdf/declaration-of-interim-dividend-for-the-fy-2022-23-and-fixation-of-record-date.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Declaration of Interim Dividend for the
                                FY 2022-23 and fixation of record date</a></div>
                        <div class="sh-info"><a
                                href="pdf/board-meeting-to-consider-interim-dividend-for-the-fy-2022-23.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Board Meeting to consider Interim
                                Dividend for the FY 2022-23</a></div>
                        <div class="sh-info"><a
                                href="pdf/board-meeting-to-consider-unaudited-financial-results-for-q3-for-the-fy-2022-23.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Board Meeting to consider Unaudited
                                Financial Results for Q3 for the FY 2022-23</a></div>
                        <div class="sh-info"><a
                                href="pdf/subsidiaries-secretarial-compliance-report-for-the-fy-2021-22.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Subsidiaries Secretarial Compliance
                                Report for the FY 2021-22</a></div>
                        <div class="sh-info"><a href="pdf/subsidiaries-audited-balance-sheet-for-the-fy-2021-22.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Subsidiaries Audited Balance Sheet for
                                the FY 2021-22</a></div>
                        <div class="sh-info"><a href="pdf/subsidiaries-audited-balance-sheet-for-the-fy-2020-21.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Subsidiaries Audited Balance Sheet for
                                the FY 2020-21</a></div>
                        <div class="sh-info"><a
                                href="pdf/subsidiaries-secretarial-compliance-report-for-the-fy-2020-21.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Subsidiaries Secretarial Compliance
                                Report for the FY 2020-21</a></div>
                        <div class="sh-info"><a href="pdf/subsidiaries-audited-balance-sheet-for-the-fy-2019-20.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Subsidiaries Audited Balance Sheet for
                                the FY 2019-20</a></div>
                        <div class="sh-info"><a href="pdf/shareholding-pattern-as-on-19-12-2022.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Shareholding Pattern as on 19.12.2022</a></div>
                        <div class="sh-info"><a href="pdf/valuation-report-dated-04-11-2022-for-preferential-issue.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Valuation Report dated 04.11.2022 for
                                Preferential Issue</a></div>
                        <div class="sh-info"><a
                                href="pdf/pcs-certificate-29-10-2022-under-regulation-163-of-sebi-icdr-regulation-2018.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> PCS Certificate dt. 29.10.2022 under
                                regulation 163 of SEBI (ICDR) Regulation 2018</a></div>
                        <div class="sh-info"><a href="pdf/bm-consider-unaudited-financial-results-q2-fy-2022-23.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Board Meeting to consider Unaudited
                                Financial Results for Q2 for the FY 2022-23</a></div>
                        <div class="sh-info"><a href="pdf/iepf-financial-year-2014-15-final.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> List of shareholders whose shares to be transferred to
                                IEPF in relation to unpaid dividend for the financial year 2014-15 (Final)</a></div>
                        <div class="sh-info"><a href="pdf/iepf-financial-year-2014-15-interim.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> List of shareholders whose shares to be transferred to
                                IEPF in relation to unpaid dividend for the financial year 2014-15 (Interim)</a></div>
                        <div class="sh-info"><a href="pdf/bm-consider-unaudited-financial-results-q1-2022-23.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Board Meeting to consider Unaudited
                                Financial Results for Q1 2022-23</a></div>
                        <div class="sh-info"><a href="pdf/cc-06-06-2022-ur-163-icdr-regulation.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Compliance Certificate dt. 06.06.2022 Under Regulation
                                163 Of ICDR Regulation</a></div>
                        <div class="sh-info"><a href="pdf/bm-consider-audited-financial-results-for-fy-2021-22.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Board Meeting to consider Audited
                                Financial Results for FY 2021-22</a></div>
                        <div class="sh-info"><a href="pdf/reschedule-bm-consider-q3-fy-2021-22-financial-result.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Reschedule Board Meeting to consider Q3
                                FY 2021-22 Financial Result</a></div>
                        <div class="sh-info"><a href="pdf/bm-consider-q3-fy-2021-22-financial-result.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Board Meeting to consider Q3 FY 2021-22
                                Financial Result</a></div>
                        <div class="sh-info"><a href="pdf/bm-consider-q1-fy-2021-22-financial-result-1id.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Board Meeting to consider Q1 FY 2021-22
                                Financial Result &amp; 1st Interim Dividend</a></div>
                        <div class="sh-info"><a href="pdf/iepf-advertisement-2013-14.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> List of shareholders whose shares to be transferred to
                                iepf in relation to unpaid dividend for the year 2013-14</a></div>
                        <div class="sh-info"><a href="pdf/bm-audited-annual-financial-result-fy-2020-21.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Board Meeting to consider Audited Annual
                                Financial Result of FY 2020-21</a></div>
                        <div class="sh-info"><a href="pdf/bm-consider-q3-fy-2020-21-financial-result.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Board Meeting to consider Q3 FY 2020-21
                                Financial Result</a></div>
                        <div class="sh-info"><a href="pdf/bm-consider-q2-fy-2020-21-financial-result.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Board Meeting to consider Q2 FY 2020-21
                                Financial Result</a></div>
                        <div class="sh-info"><a href="pdf/iepf-advertisement.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> List of shareholders whose shares to be transferred to
                                IEPF in relation to unpaid dividend for the year 2012-13</a></div>
                        <div class="sh-info"><a href="pdf/bm-audited-annual-financial-result-fy-2019-20.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Board Meeting to consider Audited Annual
                                Financial Result of FY 2019-20</a></div>
                        <div class="sh-info"><a href="pdf/bod-notice-q3-fy19-20.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to consider Q3 FY19-20 Financial
                                Results</a></div>
                        <div class="sh-info"><a href="pdf/voting-results-scrutinizers-reports.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Voting Results &amp; Scrutinizer's Reports</a></div>
                        <div class="sh-info"><a href="pdf/bod-notice-q2-fy19-20.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to consider Q2 FY19-20 Financial
                                Results</a></div>
                        <div class="sh-info"><a href="pdf/audited-financial-statements-fy-2018-19.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Audited Financial statements FY 2018-19</a></div>
                        <div class="sh-info"><a href="pdf/bmnotice-20-09-2019.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to consider issue and allotment of
                                warrant, convertible into equity on preferential basis.</a></div>
                        <div class="sh-info"><a
                                href="pdf/corrigendum-dt-28-08-2019-to-postal-ballot-notice-13-08-2019.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Corrigendum DT 28.08.2019 to Postal
                                Ballot Notice 13.08.2019</a></div>
                        <div class="sh-info"><a href="pdf/corrigendum-to-postal-ballot-notice-13-08-2019.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Corrigendum to Postal Ballot Notice
                                13.08.2019</a></div>
                        <div class="sh-info"><a href="pdf/iepf.pdf" target="_new"><i class="fa fa-file-pdf-o"></i> List
                                of shareholders whose shares to be transferred to IEPF in relation to unpaid dividend
                                for the year 2011-12</a></div>
                        <div class="sh-info"><a href="pdf/bod-notice-13082019.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to Consider Q1 FY19-20 Financial
                                Results</a></div>
                        <div class="sh-info"><a href="pdf/outcome31072019.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Outcome of Board Meeting DT 31.07.2019</a></div>
                        <div class="sh-info"><a href="pdf/bm-audited-annual-financial-result-fy-2018-19.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Board Meeting to Consider Audited Annual
                                Financial Result of FY 2018-19</a></div>
                        <div class="sh-info"><a href="pdf/audited-financial-statements-fy2017-18.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Audited Financial Statements FY 2017-18</a></div>
                        <div class="sh-info"><a href="pdf/bod-notice-280119.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to Consider Q3 FY18-19 Financial
                                Results</a></div>
                        <div class="sh-info"><a href="pdf/bod-notice-241018.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to Consider Q2 FY18-19 Financial
                                Results</a></div>
                        <div class="sh-info"><a href="pdf/bod-notice-04082018.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to Consider Q1 FY18-19 Financial
                                Results</a></div>
                        <div class="sh-info"><a href="pdf/outcome31072018.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Outcome of Board Meeting dt 31.07.2018</a></div>
                        <div class="sh-info"><a href="pdf/commissioning-greenfield-project-at-kutch.pdf"
                                target="_new"><i class="fa fa-file-pdf-o"></i> Commissioning of Greenfield Project at
                                Kutch, Gujarat</a></div>
                        <div class="sh-info"><a href="pdf/board-meeting-notice-19.05.2018.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to consider Audited Annual Financial
                                Result FY17-18</a></div>
                        <div class="sh-info"><a href="pdf/bod-notice-1718.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to consider Q3 FY17-18 Financial
                                Results</a></div>
                        <div class="sh-info"><a href="pdf/financial-statement.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Audited Financial Statements FY 2016-17</a></div>
                        <div class="sh-info"><a href="pdf/post-shp-dec17.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Shareholding Pattern Post Conversion Of Share
                                Warrants</a></div>
                        <div class="sh-info"><a href="pdf/outcome.pdf" target="_new"><i class="fa fa-file-pdf-o"></i>
                                Allotment of 1000000 Equity Shares Upon Conversion Of Share Warrants</a></div>
                        <div class="sh-info"><a href="pdf/bodnotice1.pdf" target="_new"><i class="fa fa-file-pdf-o"></i>
                                Board Meeting to consider conversion of share warrants</a></div>
                        <div class="sh-info"><a href="pdf/bodnotice.pdf" target="_new"><i class="fa fa-file-pdf-o"></i>
                                Board Meeting to consider Q2 FY17-18 Financial Results</a></div>
                        <div class="sh-info"><a href="pdf/AGMNotice.pdf" target="_new"><i class="fa fa-file-pdf-o"></i>
                                Notice for AGM, E-voting and Book Closures (AGMNotice)</a></div>
                        <div class="sh-info"><a href="pdf/BODNotice.pdf" target="_new"><i class="fa fa-file-pdf-o"></i>
                                Board Meeting to consider Q1 FY17-18 Results (BODNotice)</a></div>
                        <div class="sh-info"><a href="pdf/bod-notice-annualresults-fy16-17.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to consider Audited Annual Financial
                                Result FY16-17</a></div>
                        <div class="sh-info"><a href="pdf/bod-notice-10-02-2017.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to consider Q3 FY16-17 Results</a>
                        </div>
                        <div class="sh-info"><a href="pdf/bod-notice-01-11-2016.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to consider H1/Q2FY16-17 Results</a>
                        </div>
                        <div class="sh-info"><a href="pdf/public-notoice-agm.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Notice For AGM, E-Voting &amp; Book Closures</a></div>
                        <div class="sh-info"><a href="pdf/bod-meeting-notice-02-08-16.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to consider Q1FY16-17</a></div>
                        <div class="sh-info"><a href="pdf/change-of-name.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Change of Name of Company</a></div>
                        <div class="sh-info"><a href="pdf/bod-notice-24-05-16.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to consider FY16</a></div>
                        <div class="sh-info"><a href="pdf/shifting-of-registered-office.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Shifting of Registered Office</a> </div>
                        <div class="sh-info"><a href="pdf/bodoutcome1400316.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> BOD outcome for Interim Dividend and Record Date</a>
                        </div>
                        <div class="sh-info"><a href="pdf/board-meeting-to-consider-dividend.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to consider Dividend</a></div>
                        <div class="sh-info"><a href="pdf/bod-notice-13-02-16.pdf" target="_new"><i
                                    class="fa fa-file-pdf-o"></i> Board Meeting to consider Q3FY16</a></div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- end- -->

                </div>
        </section>
        <!-- end -->
    </main>
    <?php include './inc/footer.php';?>