<!doctype html>
<html class="no-js" lang="eng">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India :: About Us</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <main>

 <!-- hero -->

 <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
            <div class="carousel-item active">
                    <img src="./assets/img/about/slide1.png" class="d-block w-100" alt="...">
                    <h5 class="">Sikandarabad Plant </h5>
                </div>
                <div class="carousel-item ">
                    <img src="./assets/img/about/slid2.png" class="d-block w-100" alt="...">
                    <h5 class=""> Dadri Plant </h5>
                </div>
                <div class="carousel-item">
                    <img src="./assets/img/about/slide.jpg" class="d-block w-100" alt="...">
                   
                    <h5 class="">Gujarat Plant </h5>
                </div>
               
                
                 
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>
    <!-- hero -->


        <section class="bream_banner pb-2">
            <div class="container">
                <div class="sec_title">
                    <h1 class="h_padding wow fadeInRight   animated" data-wow-delay="0s"
                        style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">ABOUT
                        GOODLUCK INDIA LIMITED</h1>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-6 pos">
                        <!-- <img src="./assets/img/about/trustseal.gif" alt="" class="res trust r5"> -->
                        <p>Established in the year 1986, Goodluck India Limited is an ISO 9001:2008 certified
                            organization,
                            engaged in manufacturing and exporting of a wide range of

                            ERW Steel Pipes (Black, Red Painted & Galvanized) , Hollow Sections, CR Coils, CRCA Sheets
                            and Pipes, Galvanized Coils, CDW Tubes, Forgings & Flanges, Telecom & Transmission Line
                            Towers, Substation Structure, Bridges for Road & Railways and Road Safety Equipments </p>
                        <p> We also specialize in providing Telecommunication & Transmission Tower and Structures
                            Leveraging on advanced manufacturing facilities and engineering expertise, we have been
                            successfully catering to the needs of clients from Public sector, Private  Sector OEMs
                            and Central & State  Government Departments. <br>Our team of scrupulous quality control professionals strictly monitor every
                            stage of production
                            to ensure International standards of quality. In addition, we also offer third party
                            inspections from reputed
                            agencies such as DGS & D, RITES, BHEL and SGS before the products are dispatched at the
                            client's end.</p>
                        <p>
                            We have grown leaps and bounds under the aegis of our mentor Mr. M. C. Garg. His
                            rich
                            industry experience
                            and entrepreneurial zeal have enabled us to surge ahead in the competitive market. Today, we
                            have successfully
                            garnered the trust of most reputed global clients from across 100 countries of the globe.

                    </div>
                    <div class="col-md-6">
                        <div class="about_img">
                            <img src="./assets/img/about/a1.png" alt="" class="res r5">

                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- end -->

        <section class="export pb-0 p60  wow fadeInUp   animated" data-wow-delay="0.5s"
            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ex_p_h">
                            <h1 class="left_heading pos text-dark  "> OUR EXPORTS </h1>
                            <p>Our exemplary products and services have enabled us to leap geographical bounds and
                                register a strong presence in 100 countries across the globe. We have extended our reach
                                to the customers worldwide with a well knit network of stock holders distributors and
                                agents.</p>
                            <p>GOODLUCK IS EXPORTING TO EUROPE, NORTH AND SOUTH AMERICA, AUSTRALIA,MIDDLE EAST, SOUTH
                                EAST ASIA AND AFRICA.
                            </p>

                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- end -->

        <!-- end -->
        <section class="mt-5 mb-5">
            <div class="container">

                <div class="insprir">
                    <h1 class="left_heading pos mb-4">Explore Our Diverse Application Areas</h1>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="app_lication">
                            <h4 class="mb-0"><b>1. Automotive Excellence</b></h4>
                            <ul class="area_list">
                                <li>Crafting precision components for the Automotive industry, including frames, forks, shock absorber and Hydraulic line tubing. </li>
                                <li>Supplying reliable chains for automotive, industrial, and boiler applications</li>
                                <li>Serving the automotive sector with tubes for two/three wheelers, cars, LCVs, HCVs, and chassis</li>
                                <li>Providing essential components such as bus body building main beam tubes, automobile axle tubes, and hydraulic cylinder tubes</li>
                                <li>Delivering critical parts like propeller shafts, steering columns, tie rods, drag links, and exhaust tubes</li>
                                <li>Offering specialized solutions like aluminized tubes and hydraulic line tubing for two-wheelers</li>

                            </ul>
                            </div>
                        </div>
                        <div class="col-md-6">

                        <div class="app_lication">
                            <h4 class="mb-0"><b>2. Oil and Gas Expertise</b></h4>
                            <ul class="area_list">
                                <li>Specializing in flanges and forged products for the oil and gas industry</li>
                                <li>Meeting the unique demands of the oil and gas, defense and aerospace sector with precision and quality</li>
                            </ul>
                        </div>
                        <div class="app_lication">
                            <h4 class="mb-0"><b>3. Infrastructure and Power Solutions</b></h4>
                            <ul class="area_list">
                                <li>Contributing to infrastructure and power projects with innovative tube applications</li>
                                <li>Providing reliable and efficient solutions for various infrastructure needs</li>
                            </ul>
                        </div>
                        <div class="app_lication">
                            <h4 class="mb-0"><b>4. Cutting-Edge Heat Management</b></h4>
                            <ul class="area_list">
                                <li>Leading the way in producing boiler heat exchangers for diverse industries</li>
                                <li>Ensuring optimal heat exchange solutions for chemical, sugar, paper, and process industries</li>
                            </ul>
                        </div>
                            
                        </div>

                    </div>

                    <!-- end -->

                </div>
            </div>
        </section>




    </main>
    <?php include './inc/footer.php';?>