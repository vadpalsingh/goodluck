<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Place favicon.ico in the root directory -->
<link rel="shortcut icon" type="image/x-icon" href="assets/favicon.jpg">
<!-- CSS here -->
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/meanmenu.css">
<link rel="stylesheet" href="assets/css/animate.css">
<link rel="stylesheet" href="assets/css/owl-carousel.css">
<link rel="stylesheet" href="assets/css/swiper-bundle.css">
<link rel="stylesheet" href="assets/css/backtotop.css">
<link rel="stylesheet" href="assets/css/magnific-popup.css">
<link rel="stylesheet" href="assets/css/nice-select.css">
<link rel="stylesheet" href="assets/css/flaticon.css">
<link rel="stylesheet" href="assets/css/font-awesome-pro.css">
<link rel="stylesheet" href="assets/css/spacing.css">
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/custom.css">
<script>
function myFunction() {
    var x = document.getElementById("myVideo").autoplay;
}
</script>
</head>

<body>
    <!-- Preloader -->
    <!-- <div class="preloader"></div> -->
    <!-- pre loader area end -->
    <!-- back to top start -->
    <div class="progress-wrap">
        <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
            <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
        </svg>
    </div>
    <!-- back to top end -->
    <!-- header-area-start -->
    <header id="header-sticky" class="header-area">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-xl-2 col-lg-2 col-md-6 col-6">
                    <div class="logo-area">
                        <div class="logo">
                            
                            <a href="index.php"><img src="assets/img/logo/logo-white.png" alt="Goodluck India"></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-9 col-md-6 col-6">
                    <div class="menu-area menu-padding">
                        <div class="main-menu text-center">
                            <nav id="mobile-menu">
                                <ul>
                                    <li class="nav-item"> <a class="nav-link" href="index.php">Home</a> </li>
                                    <!-- <li> <a href="about.php">About Us</a>  </li> -->
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" role="button"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            About Us
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="about.php">Company Overview</a></li>
                                            <li><a class="dropdown-item" href="leadership.php">Leadership</a></li>
                                            <li><a class="dropdown-item" href="manufacturingunit.php">Manufacturing
                                                    Units</a></li>
                                            <li><a class="dropdown-item" href="csr.php">CSR</a></li>
                                            <li><a class="dropdown-item" href="awards.php ">Awards And Recognition</a></li>

                                        </ul>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            Our Verticals
                                        </a>
                                        <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="./cdwtubes">GOODLUCK INDUSTRIES  (CDW/DOM TUBES )</a></li>
                                        <li><a class="dropdown-item" href="./forging">GOODLUCK ENGINEERING CO. (FORGINGS)</a></li>
                                        <li><a class="dropdown-item" href="./fabrication">GOODLUCK INDIA LTD. (INFRA & ENERGY)</a></li>
                                        <li><a class="dropdown-item" href="./roadsafetydivision">GOODLUCK INDIA LTD. (ROAD SAFETY)</a></li>
                                         <li><a class="dropdown-item" href="./PipesAndTubes">GOODLUCK INDIA LTD. (PIPES & TUBES)</a> </li>
                                        <li><a class="dropdown-item" href="./PipesAndTubes">GOODLUCK INDIA LTD. (COILS AND SHEETS)</a></li>
                                            
                                           
                                           
                                            
                                        </ul>
                                    </li>
                                    <li class="nav-item dropdown"><a href="javascript:void(0)"
                                            class="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown"
                                            aria-expanded="false">Product</a>

                                        <div class="dropdown-menu megamenu">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="row">

                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <h4 class="headingmenu"><a href="PipesAndTubes">Pipes & Tubes</a></h4>
                                                            <ul class="menumega">
                                                                <li><a href="PipesAndTubes/product.php"> ERW Hot dipped Galvanized
                                                                        Pipes</a></li>
                                                                <li><a href="PipesAndTubes/blackpipe.php">ERW Black Round Pipes</a>
                                                                </li>
                                                                <li><a href="PipesAndTubes/sqaure.php">Square Tubes</a> </li>
                                                                <li><a href="PipesAndTubes/rectangular.php">Rectangular Tubes</a>
                                                                </li>
                                                                <li><a href="PipesAndTubes/pregalvanizedpipes.php"> Pre Galvanized Pipes</a>
                                                                </li>
                                                                <li><a href="PipesAndTubes/redpipe.php">Painted Pipes ( Black, Red
                                                                        and Blue )</a></li>
                                                                <li><a href="PipesAndTubes/conduitsul6.php">Rigid Conduits - UL6</a>
                                                                </li>
                                                                <li><a href="PipesAndTubes/conduitsul797.php">EMT Conduits UL797</a></li>
                                                                <li><a href="PipesAndTubes/conduits1242.php">Intermediate Metal Conduit
                                                                        UL1242</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                        <h4 class="headingmenu"><a href="forging"> Forging</a>
                                                            </h4>
                                                            <ul class="menumega">
                                                                <li><a href="forging/rowmetrial.php"> MATERIAL GRADES</a></li>
                                                                <li><a href="forging/forgingbar.php">Forging</a></li>
                                                                <li><a href="forging/flanges.php">Flanges</a></li>
                                                                <li><a href="forging/packing.php">Packaging</a> </li>
                                                            </ul>   
                                                        <h4 class="headingmenu"><a href="PipesAndTubes"> Coils & Sheets</a></h4>
                                                            <ul class="menumega">
                                                                <li><a href="PipesAndTubes/crcoils.php">CR Coils and Sheets</a>
                                                                </li>
                                                                <li><a href="PipesAndTubes/galvanisedcoils.php">Galvanized Coils</a></li>
                                                                <li><a href="PipesAndTubes/corrugated.php">Corrugated Sheets</a> </li>
                                                            </ul>
                                                           
                                                        </div>

                                                    </div>
                                                </div>
                                                <!-- end-col6 -->
                                                <div class="col-lg-6">
                                                    <div class="row">

                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                        <h4 class="headingmenu"><a href="cdwtubes"> CDW/DOM Tubes </a></h4>
                                                            <ul class="menumega">
                                                                <li><a href="cdwtubes/product-&-capability.php">ERW Tube</a>
                                                                </li>
                                                                <li><a href="cdwtubes/product-&-capability.php">CDW Tube</a></li>
                                                                
                                                            </ul>
                                                            
                                                            <h4 class="headingmenu"><a href="roadsafetydivision"> Road Safety </a></h4>
                                                            <ul class="menumega">
                                                                <li><a href="roadsafetydivision/approach.php">Approach End Terminals</a> </li>
                                                                <li><a href="roadsafetydivision/crashcushions.php">Crash Cushions / Impact Attenuators</a></li>
                                                                <li><a href="roadsafetydivision/bridge.php">Bridge/Vehicle Parapets</a></li>
                                                                <li><a href="roadsafetydivision/crashtested.php">Crash Tested/ Conventional Metal Beam Crash Barriers</a></li>
                                                                <li><a href="roadsafetydivision/transition.php">Transitions</a></li>
                                                                <li><a href="roadsafetydivision/endterminals.php">End Terminals</a></li>
                                                            </ul>
                                                            
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                        <h4 class="headingmenu"><a href="fabrication">Infra & Energy </a></h4>
                                                        
                                                            <ul class="menumega">
                                                                <li><a href="fabrication/railway-and-road-bridges.php">Railway & Road Bridges</a>
                                                                </li>
                                                                <li><a href="fabrication/building-structures.php">Building Structures</a></li>
                                                                <li><a href="fabrication/structure-for-roads-and-expressways.php">Structure for Roads & Expressways </a></li>
                                                                <li><a href="fabrication/launching-girder.php">Launching Girder </a></li>
                                                                <li><a href="/fabrication/primary-and-secondary-boiler-structure.php">Primary & secondary Boiler Structure </a></li>
                                                                <li><a href="fabrication/defense-fabrication.php">Defense Fabrication </a></li>
                                                               
                                                             
                                                            </ul>
                                                           
                                                          <ul  class="menumega">
                                                          <li><a href="fabrication/transmission-line-tower.php">Transmission Line Tower </a></li>
                                                                <li><a href="fabrication/telecom-towers.php">Telecom Towers </a></li>
                                                                <li><a href="fabrication/over-head-electrification.php">Over Head Electrification </a></li>
                                                                <li><a href="fabrication/substation-structure.php">Substation Structure </a></li>
                                                          </ul>
                                                            
                                                        </div>
                                                        

                                                    </div>
                                                </div>
                                                <!-- end-col6 -->
                                                
                                                
                                            </div>

                                        </div>

                                    </li>
                                    <li> <a href="investors.php">Investor</a> </li>
                                    <li> <a href="client.php">Clients</a> </li>
                                    <li > <a  href="contact.php">Contact Us</a>  </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="side-menu-icon d-lg-none text-end">
                        <a href="javascript:void(0)" class="info-toggle-btn f-right sidebar-toggle-btn"><i
                                class="fal fa-bars"></i></a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-1 d-none d-lg-block">
                    <div class="header__sm-action">
                        <div class="header__sm-action-item right-border1">
                            <a href="javascript:void(0)" class="info-toggle-btn f-right sidebar-toggle-btn"><i
                                    class="fal fa-bars"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-area-end -->
    <!-- sidebar area start -->
    <div class="sidebar__area">
        <div class="sidebar__wrapper">
            <div class="sidebar__close">
                <button class="sidebar__close-btn" id="sidebar__close-btn">
                    <i class="fal fa-times"></i>
                </button>
            </div>
            <div class="sidebar__content">
                <div class="sidebar__logo  mb-2">
                    <a href="index.html">
                        <img src="assets/img/logo/logo-black.png" alt="logo">
                    </a>
                </div>
                <div class="mobile-menu fix"></div>
                <div class="sidebar__contact mt-0 mb-20">
                    <!-- <h4>Contact Info</h4> -->
                    <ul>
                        <li class="d-flex align-items-center">
                            <a href="#">Corporate Video</a>
                        </li>
                        <li class="d-flex align-items-center">
                            <a href="#">Brochure</a>
                        </li>
                        <li class="d-flex align-items-center">
                            <a href="#">Careers</a>
                        </li>
                        <li class="d-flex align-items-center">
                            <a href="#">Enquiry</a>
                        </li>
                        <li class="d-flex align-items-center">
                            <a href="Contact.php">Contact Us</a>
                        </li>
                    </ul>
                </div>
                <!-- <div class="sidebar__social">
                    <ul>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                    </ul>
                </div> -->
            </div>
        </div>
    </div>
    <!-- sidebar area end -->
    <div class="body-overlay"></div>
    <!-- sidebar area end -->