<footer class="footer light-grey wow fadeInUp  animated" data-wow-delay=".3s"
    style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
   
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="footer-detail">
                    <a href="index.php"><img src="assets/img/logo/footer-logo.png" alt=""
                            style="margin-top: -23px;     margin-left: -6px;"></a>
                    <h3 class="heading_fooetr"> Corporate Office</h3>
                    <p><strong>CIN No. :</strong> L74899DL1986PLC050910</p>
                    <p>
                        Good Luck House, II F, 166-167,<br> Nehru Nagar 2 Ambedkar Road <br> Ghaziabad - 201001, Uttar Pradesh, India
                    </p>
                    <!-- <div class="tel-phone"><a href="tel:+91-124-4318222" class="landline"><img src="./assets/img/blog/phone-icon.jpg" class="img-fluid" alt="Phone">+91-8046043795</a>
                       </div> -->
                </div>
            </div>
            <div class="col-md-2">
                <!-- <a href="#">
                       <h5 class="heading_fooetr">GROUP  COMPANIES  </h5>
                     </a> -->
                <ul class="footer_link">
                    <li><a href="../index ">Home</a></li>
                    <li><a href=" ">About Us</a></li>
                    <li><a href=" ">Our Verticals</a></li>
                    <li><a href="#"> Our Products</a></li>
                    <li><a href="#">Investor</a></li>
                    <li><a href="#">Clients</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <!-- <a href="#">
                       <h5 class="heading_fooetr">GROUP  COMPANIES  </h5>
                     </a> -->
                            <ul  class="footer_link">
                            <li><a   href="./cdwtubes">Goodluck Industries (Cdw Tubes)</a></li>
                            <li><a   href="./forging">Goodluck Engineering Co. (Forging)</a></li>
                            <li><a   href="./fabrication">Goodluck India Ltd. (Infra & Energy)</a></li>
                            <li><a   href="./roadsafetydivision">Goodluck India Ltd. (Road Safety)</a></li>
                            <li><a   href="./PipesAndTubes">Goodluck India Ltd. (Pipes & Tubes)</a>  </li>
                            <li><a  href="./PipesAndTubes">Goodluck India Ltd. (Coils and Sheets)</a>  </li>
                             </ul>
                   </div>

                

            <div class="col-md-3">

                <h5 class="text-dark">Connect Us</h5>
                <div style="margin-bottom: 10px;" class="c_now"><a href="tel:+91-8046043795" class="landline"><img
                            src="./assets/img/blog/phone-icon.jpg" class="img-fluid" alt="Phone"
                            style="width: 18%;border-radius:5px"> +91-0120-4196600, 4196700</a></div>
                            <div class="emil mb-2"><b>Email:</b> goodluck@goodluckindia.com</div>
                <div class="ne_w">
                    <button data-bs-toggle="modal" data-bs-target="#exampleModal" class="Enquire">Enquire Now</button>
                    <br>
                
                    <button class="Feed_Back" data-bs-toggle="modal" data-bs-target="#Feed_Back">FeedBack</button>
                </div>
            </div>
        </div>

    </div>
</footer>
<!-- footer start -->
<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-5">
                <div class="copy-text">
                    <p> ©<a href="https://www.goodluckindia.com/">Goodluck India Limited</a>  | All Rights Reserved</p>
                </div>
            </div>
            <div class="col-12 col-md-7">
                <p class="text-end"> Designed by <a href="https://www.webcadenceindia.com/" target="_blank" style="color:rgb(169 4 4);">Web Cadence</a>
                </p>
            </div>
        </div>
    </div>
</div>
<!-- footer end -->



<!-- enquri -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Enquire Now</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="mb-3">
                        <label for="formGroupExampleInput" class="form-label">Name </label>
                        <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Name">
                    </div>
                    <div class="mb-3">
                        <label for="formGroupExampleInput" class="form-label">Phone No. </label>
                        <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Phone Number">
                    </div>
                    <div class="mb-3">
                        <label for="formGroupExampleInput2" class="form-label">Email </label>
                        <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Email">
                    </div>
                    <div class="mb-3 text-center">
                        <button class="btn btn-success">Submit</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
<!-- enquri end---->
<!-- enquri -->
<div class="modal fade" id="Feed_Back" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Feedback Now </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="mb-3">
                        <label for="formGroupExampleInput" class="form-label">Name </label>
                        <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Name">
                    </div>

                    <div class="mb-3">
                        <label for="formGroupExampleInput2" class="form-label">FeedBack Now </label>
                        <textarea name="" id="" cols="30" rows="5" class="form-control"
                            placeholder="Enter feedback"></textarea>
                    </div>
                    <div class="mb-3 text-center">
                        <button class="btn btn-success">Submit</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
<!-- enquri end---->


<!-- JS here -->
<script src="assets/js/vendor/jquery.js"></script>
<script src="assets/js/vendor/waypoints.js"></script>
<script src="assets/js/bootstrap-bundle.js"></script>
<script src="assets/js/meanmenu.js"></script>
<script src="assets/js/swiper-bundle.js"></script>
<script src="assets/js/owl-carousel.js"></script>
<script src="assets/js/magnific-popup.js"></script>
<script src="assets/js/parallax.js"></script>
<script src="assets/js/backtotop.js"></script>
<script src="assets/js/nice-select.js"></script>
<script src="assets/js/counterup.js"></script>
<script src="assets/js/wow.js"></script>
<script src="assets/js/isotope-pkgd.js"></script>
<script src="assets/js/imagesloaded-pkgd.js"></script>
<script src="assets/js/ajax-form.js"></script>
<script src="assets/js/main.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script>
$.fn.jQuerySimpleCounter = function(options) {
    var settings = $.extend({
            start: 0,
            end: 100,
            easing: "swing",
            duration: 400,
            complete: ""
        },
        options
    );

    var thisElement = $(this);

    $({
        count: settings.start
    }).animate({
        count: settings.end
    }, {
        duration: settings.duration,
        easing: settings.easing,
        step: function() {
            var mathCount = Math.ceil(this.count);
            thisElement.text(mathCount);
        },
        complete: settings.complete
    });
};

$("#number1").jQuerySimpleCounter({
    end: 2025,
    duration: 3000
});
$("#number2").jQuerySimpleCounter({
    end: 1055,
    duration: 3000
});
$("#number3").jQuerySimpleCounter({
    end: 3059,
    duration: 2000
});
$("#number4").jQuerySimpleCounter({
    end: 8000,
    duration: 2500
});

/* AUTHOR LINK */
$(".about-me-img").hover(
    function() {
        $(".authorWindowWrapper").stop().fadeIn("fast").find("p").addClass("trans");
    },
    function() {
        $(".authorWindowWrapper")
            .stop()
            .fadeOut("fast")
            .find("p")
            .removeClass("trans");
    }
);
</script>

<script>
// Gallery image hover
$(".img-wrapper").hover(
    function() {
        $(this).find(".img-overlay").animate({
            opacity: 1
        }, 600);
    },
    function() {
        $(this).find(".img-overlay").animate({
            opacity: 0
        }, 600);
    }
);

// Lightbox
var $overlay = $('<div id="overlay"></div>');
var $image = $("<img>");
var $prevButton = $(
    '<div id="prevButton"><i class="fa fa-chevron-left"></i></div>'
);
var $nextButton = $(
    '<div id="nextButton"><i class="fa fa-chevron-right"></i></div>'
);
var $exitButton = $('<div id="exitButton"><i class="fa fa-times"></i></div>');

// Add overlay
$overlay
    .append($image)
    .prepend($prevButton)
    .append($nextButton)
    .append($exitButton);
$("#gallery").append($overlay);

// Hide overlay on default
$overlay.hide();

// When an image is clicked
$(".img-overlay").click(function(event) {
    // Prevents default behavior
    event.preventDefault();
    // Adds href attribute to variable
    var imageLocation = $(this).prev().attr("href");
    // Add the image src to $image
    $image.attr("src", imageLocation);
    // Fade in the overlay
    $overlay.fadeIn("slow");
});

// When the overlay is clicked
$overlay.click(function() {
    // Fade out the overlay
    $(this).fadeOut("slow");
});

// When next button is clicked
$nextButton.click(function(event) {
    // Hide the current image
    $("#overlay img").hide();
    // Overlay image location
    var $currentImgSrc = $("#overlay img").attr("src");
    // Image with matching location of the overlay image
    var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
    // Finds the next image
    var $nextImg = $($currentImg.closest(".image").next().find("img"));
    // All of the images in the gallery
    var $images = $("#image-gallery img");
    // If there is a next image
    if ($nextImg.length > 0) {
        // Fade in the next image
        $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
    } else {
        // Otherwise fade in the first image
        $("#overlay img").attr("src", $($images[0]).attr("src")).fadeIn(800);
    }
    // Prevents overlay from being hidden
    event.stopPropagation();
});

// When previous button is clicked
$prevButton.click(function(event) {
    // Hide the current image
    $("#overlay img").hide();
    // Overlay image location
    var $currentImgSrc = $("#overlay img").attr("src");
    // Image with matching location of the overlay image
    var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
    // Finds the next image
    var $nextImg = $($currentImg.closest(".image").prev().find("img"));
    // Fade in the next image
    $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
    // Prevents overlay from being hidden
    event.stopPropagation();
});

// When the exit button is clicked
$exitButton.click(function() {
    // Fade out the overlay
    $("#overlay").fadeOut("slow");
});
</script>

<!-- <script src="js/wow.min.js"></script> -->
<script>
new WOW().init();
</script>
<!-- counter start -->
<script>
const counters = document.querySelectorAll(".counter");

counters.forEach((counter) => {
    counter.innerText = "0";
    const updateCounter = () => {
        const target = +counter.getAttribute("data-target");
        const count = +counter.innerText;
        const increment = target / 200;
        if (count < target) {
            counter.innerText = `${Math.ceil(count + increment)}`;
            setTimeout(updateCounter, 1);
        } else counter.innerText = target;
    };
    updateCounter();
});
</script>
<!-- counter end -->
</body>

</html>