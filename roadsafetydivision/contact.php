<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Goodluck India Limited (Pipes And Tubes) :: (Road Safety Division ) </title>
    <meta name="description" content="">


    <?php include './inc/header.php'; ?>
    <div class="contant">
    <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item  active ">
                        <img src="../assets/img/about/cont.png" class="d-block w-100" alt="...">
                        <h5>Road Safety Division </h5>
                    </div>
                </div>


        
            </div>
        </section>

        <style>
            
            .contact_mail {
         /* background: #92bde7; */
         color: #485e74;
         line-height: 1.6;
         /* font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; */
         padding: 1em;
     }

     
     ul {
         list-style: none;
         padding: 0;
     }

     .brand {}

     .brand span {
         color: #fff;
     }

     .wrapper {
        box-shadow: 0 0 20px 0 rgb(72 94 116 / 13%);
    border-radius: 8px;
    overflow: hidden;
     }

     .wrapper>* {
         padding: 1em;
     }

     .company-info {
         background: #f4f4f4;
     }

     .address {
         font-size: 16px;
     }

     .company-info h3,
     .company-info ul {

         margin: 0 0 1rem 0;
     }

     .contact {
         background: #fff;
     }

     /* Form Styles */

     .contact form {
         display: grid;
         grid-template-columns: 1fr 1fr;
         grid-gap: 20px;
     }

     .icon {
         padding-top: 8px;
     }

     .contact form label {
         display: block;
     }

     .contact form p {
         margin: 0;
     }

     .contact form .full {
         grid-column: 1/3;
     }

     .contact form button,
     .contact form input,
     .contact form textarea {
         width: 100%;
         padding: 1em;
         border: 1px solid #c9e6ff;
         color: #000;
         font-weight: 700;
     }

     .contact form button {
         background: #b2b2b2;
         border: 0;
         text-transform: uppercase;
         cursor: pointer;
     }

     .contact form button:hover,
     .contact form butto:focus {
         background: #92bde7;
         color: #fff;
         outline: 0;
         transition: background-color 1.5s ease-out;
     }

        /* responsive css for larger screeens */
        </style>

<section class="contact_mail">
            <div class="container-fluid">


                <div class="wrapper animated bounceInLeft">
                    <div class="company-info">
                        <h3>Address :-</h3>
                        <ul>
                            <li>
                                <div class="deail mb-3">
                                    <div class="icon">
                                        <i class="fa fa-home"></i>
                                    </div>
                                    <div class="address">
                                        <b> Head Office:</b><br>
                                        Good Luck India
                                        166-167, 2nd Floor,
                                        Good Luck House,
                                        Nehru Nagar, Ambedkar Road,
                                        Ghaziabad, UP
                                        India - 201 001.
                                        Tel: +91 120 4196700

                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="deail">
                                    <div class="icon">
                                        <i class="fa fa-home"></i>
                                    </div>
                                    <div class="address">
                                        <b> Manufacturing:</b><br>
                                        Goodluck India Limited :-
                                        A - 42 & 45,Industrial Area, Sikandrabad -203205,
                                        Distt. Bulandshahar, (U.P.) India
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="deail mt-3 mb-2">
                                    <div class="icon">
                                        <i class="fa fa-envelope "></i>
                                    </div>
                                    <div class="address">
                                        <b> More Information on:</b><br>
                                        <b>Web:</b> <a href="wwww.goodluckindia.com">wwww.goodluckindia.com</a>
                                        <br>
                                        <b>Email:</b> <a
                                            href="mailto:goodluck@goodluckindia.com">gil.rsp@goodluckindia.com</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="deail mt-3 mb-2">
                                    <div class="icon">
                                        <i class="fa fa-phone "></i>
                                    </div>
                                    <div class="address">
                                        <b>+91-120-4196600, +91-9599065741 </b>


                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="contact p-5">
                        <h3>Contact Us</h3>
                        <form action="">
                            <p>
                                <label>Name</label>
                                <input type="text" name="name">
                            </p>
                            <p>
                                <label>Company</label>
                                <input type="text" name="company">
                            </p>
                            <p>
                                <label>Email Address</label>
                                <input type="email" name="email">
                            </p>
                            <p>
                                <label>Phone Number</label>
                                <input type="tel" name="phone">
                            </p>
                            <p class="full">
                                <label>Message</label>
                                <textarea name="Message" rows="5"></textarea>
                            </p>
                            <p class="full">
                                <button>Submit</button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </section>


        <section>
            <div class="contact_map">
                <div class="Info w50"><iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3500.8545326390777!2d77.4320974752269!3d28.664073982648084!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cf1c862260ad5%3A0x81d0ee38c2a27ddc!2sGoodluck%20India%20Limited!5e0!3m2!1sen!2sin!4v1690373115327!5m2!1sen!2sin"
                        width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy"  referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
                <div class="Info w50">

                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m12!1m8!1m3!1d447948.41382818273!2d77.287463!3d28.7017!3m2!1i1024!2i768!4f13.1!2m1!1sA%20-%2042%20%26%2045%2CIndustrial%20Area%2C%20Sikandrabad%20-203205%2C%20Distt.%20Bulandshahar%2C%20India!5e0!3m2!1sen!2sin!4v1691146303984!5m2!1sen!2sin"
                        width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy"   referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
        </section>

      
    </div>

    <?php include './inc/footer.php'; ?>