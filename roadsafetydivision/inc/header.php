<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Goodluckindia </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="../assets/favicon.jpg">

    <!-- CSS here -->
    <!-- <link rel="stylesheet" href="../assets/css/bootstrap.css"> -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/meanmenu.css">
    <link rel="stylesheet" href="../assets/css/animate.css">
    <link rel="stylesheet" href="../assets/css/owl-carousel.css">
    <link rel="stylesheet" href="../assets/css/swiper-bundle.css">
    <link rel="stylesheet" href="../assets/css/backtotop.css">
    <link rel="stylesheet" href="../assets/css/magnific-popup.css">
    <link rel="stylesheet" href="../assets/css/nice-select.css">
    <link rel="stylesheet" href="../assets/css/flaticon.css">
    <link rel="stylesheet" href="../assets/css/font-awesome-pro.css">
    <link rel="stylesheet" href="../assets/css/spacing.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
    <script>
    function myFunction() {
        var x = document.getElementById("myVideo").autoplay;
    }
    </script>
    <style>
    ul.dropdown-menu.sub_minu {
        left: 99%;
        top: 0;
    }

    .dropdown-toggle.quali_ty::after {
        display: inline-block;
        margin-left: 0.255em;
        vertical-align: 3px;
        content: "";
        border-top: 0.3em solid transparent;
        border-right: 6px;
        border-bottom: 0.3em solid transparent;
        border-left: 0.3em solid;
    }

    ul.dropdown-menu.sub_minu a.dropdown-item {
        white-space: break-spaces;
        font-size: 12px;
    }

    a.quali_ty.dl_f {
        display: flex;
        align-items: center;
        font-size: 12px;
    }

    a.quali_ty.dl_f {
        display: flex;
        align-items: center;
        font-size: 12px;
    }

    ul.dropdown-menu.sub_minu a.dropdown-item::active {
        background-color: #eee;

    }

    ul.dropdown-menu.sub_minu a.dropdown-item::focus {
        background-color: #eee;

    }
    </style>
    <!-- Enquire -->
    <div class="modal fade" id="Enquire" tabindex="1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content b_gc">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Enquire Now</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="container">
                    <form action="">
                        <div class="row">
                           
                            <div class="col-md-6 col-12">
                                <div class="mb-2">
                                    <label for="exampleInputEmail1" class="form-label">Enter Your Name<b>*</b></label>
                                    <input type="email" class="form-control" id="exampleInputEmail1"  required>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="mb-2">
                                    <label for="exampleInputEmail1" class="form-label">Enter Your Email<b>*</b></label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" required >
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="mb-2">
                                    <label for="exampleInputEmail1" class="form-label">Enter Your Phone<b>*</b></label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="mb-2">
                                    <label for="exampleInputEmail1" class="form-label">Enter  Company Name<b>*</b></label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" required >
                                </div>
                            </div>
                            <div class="col-md-12 col-12">
                                <div class="mb-2">
                                    <label for="exampleInputEmail1" class="form-label">Enter Your Project </label>
                                    <input type="email" class="form-control" id="exampleInputEmail1"   >
                                </div>
                            </div>
                            <div class="col-md-12 col-12">
                                <div class="mb-2">
                                    <label for="exampleInputEmail1" class="form-label">Enter Your Requirement</label>
                                    <textarea name="" id=""  class="form-control" cols=" " rows="5"></textarea>
                                </div>
                            </div>
                            
                        </div>
                        </form>
                    </div>
                </div>
                <div class=" pb-3 text-center">
               
                    <button type="button" class="btn btn-success pr-3 pl-3">Submit</button>
                </div>
            </div>
        </div>
    </div>
 <!-- Enquire -->


</head>

<body>
    <!-- Preloader -->
    <!-- <div class="preloader"></div> -->
    <!-- pre loader area end -->
    <!-- back to top start -->
    <div class="progress-wrap">
        <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
            <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
        </svg>
    </div>
    <!-- back to top end -->

    <!-- header-area-start -->
    <header id="header-sticky" class="header-area">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-xl-4 col-lg-3 col-md-6 col-8">
                    <div class="logo-area">
                        <div class="logo" style="    display: flex; align-items: center;">
                            <a href="../index.php" title="Back To Home"><img src="../assets/img/logo/logo-white.png" alt=""></a>
                            <h3 class="h_for" style=""><a href="index.php" title="Back To Road Safety Division">Goodluck India Limited <br>(Road Safety Division )</a></h3>
                            <!-- <h3 class="h_for" style="">Road Safety Equipment<br>( Division)</h3> -->
                        </div>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-8 col-md-6 col-4">
                    <div class="menu-area menu-padding">
                        <div class="main-menu text-center">
                            <nav id="mobile-menu" style="display: block;">
                                <ul>
                                    <li class="nav-item  "> <a class="nav-link  " href="index.php">Home</a> </li>
                                    <!-- <li class="nav-item  "> <a class="nav-link  " href="about.php">About Us</a> </li> -->
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" role="button"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            Products
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="approach.php"> Approach End Terminals</a>
                                            </li>
                                            <li><a class="dropdown-item" href="crashcushions.php"> Crash Cushions /
                                                    Impact Attenuators </a></li>
                                            <li><a class="dropdown-item" href="bridge.php"> Bridge/Vehicle Parapets</a>
                                            </li>
                                            <li><a class="dropdown-item" href="crashtested.php"> Crash Tested/
                                                    Conventional Metal Beam Crash Barriers </a></li>
                                            <li><a class="dropdown-item" href="transition.php"> Transitions</a></li>
                                            <li><a class="dropdown-item" href="endterminals.php"> End Terminals</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" role="button"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            Certification
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="iso9001.php"> ISO 9001 : 2015</a></li>
                                            <li><a class="dropdown-item" href="iso1401.php"> ISO 14001 : 2015  </a></li>
                                            <li><a class="dropdown-item" href="iso4501.php"> ISO 45001 : 2018</a></li>
                                            <li><a class="dropdown-item" href="bms2prl.php"> CE Certification For W Beam Model - BMS2PRL-H1  </a></li>
                                            <li><a class="dropdown-item" href="bms2pls.php"> CE Certification For W Beam Model - BMS2PLS-H1</a></li>
                                            <li><a class="dropdown-item" href="bms4pls.php"> CE Certification For W Beam Model - BMS4PLS-H1</a></li>
                                            <li><a class="dropdown-item" href="bms4prl.php"> CE Certification For W Beam Model - BMS4PRL-H1</a></li>
                                            <li><a class="dropdown-item" href="bms4pr.php"> CE Certification For W Beam Model - BMS4PR-H1</a></li>
                                            <li><a class="dropdown-item" href="cps11.php"> CE Certification For Thrie Beam Model - CPS11 H2 2.66 </a></li>
                                            <li><a class="dropdown-item" href="cps05.php"> CE Certification For Thrie Beam Model -CPS05 H2 2.00</a></li>
                                        </ul>
                                    </li>

                                    <li> <a href="../assets/RoadSafetyEquipment/certification/roadbroucher.pdf " Download  >Download</a> </li>
                                    <li> <a href="contact.php">Contact</a> </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="side-menu-icon d-lg-none text-end">
                        <a href="javascript:void(0)" class="info-toggle-btn f-right sidebar-toggle-btn"><img
                                src="../assets/img/humberger-icon.svg" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-1 col-lg-1 d-none d-lg-block">
                    <div class="header__sm-action">
                        <div class="header__sm-action-item right-border1">
                            <a href="javascript:void(0)" class="info-toggle-btn f-right sidebar-toggle-btn"><img
                                    src="../assets/img/humberger-icon.svg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-area-end -->
    <!-- sidebar area start -->
    <div class="sidebar__area">
        <div class="sidebar__wrapper">
            <div class="sidebar__close">
                <button class="sidebar__close-btn" id="sidebar__close-btn">
                    <i class="fal fa-times"></i>
                </button>
            </div>
            <div class="sidebar__content">
                <div class="sidebar__logo  mb-2">
                    <a href="index.html">
                        <img src="../assets/img/logo/logo-black.png" alt="logo">
                    </a>
                </div>

                <div class="mobile-menu fix"></div>
                <div class="sidebar__contact mt-0 mb-20">
                    <!-- <h4>Contact Info</h4> -->
                    <ul>
                        <li class="d-flex align-items-center">
                            <a href="../index.php">GoodLuck Home</a>
                        </li>
                        <li class="d-flex align-items-center">
                            <a href="#">Corporate Video</a>
                        </li>
                        <li class="d-flex align-items-center">
                            <a href="#">Brochure</a>
                        </li>
                        <li class="d-flex align-items-center">
                            <a href="#">Careers</a>
                        </li>
                        <li class="d-flex align-items-center">
                            <a href="#">Enquiry</a>
                        </li>
                        <li class="d-flex align-items-center">
                            <a href="#">Contact Us</a>
                        </li>
                    </ul>
                </div>
                <div class="sidebar__social">
                    <ul>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- sidebar area end -->
    <div class="body-overlay"></div>
    <!-- sidebar area end -->