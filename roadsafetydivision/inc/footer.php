<footer class="footer light-grey wow fadeInUp  animated" data-wow-delay=".3s"
    style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
   
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="footer-detail">
                    <a href="index.php"><img src="../assets/img/logo/footer-logo.png" alt=""
                            style="margin-top: -23px;     margin-left: -6px;"></a>
                    <h3 class="heading_fooetr"> Corporate Office</h3>
                    <p><strong>CIN No. :</strong> L74899DL1986PLC050910</p>
                    <p>
                        Good Luck House, II F, 166-167,<br> Nehru Nagar 2 Ambedkar Road <br> Ghaziabad - 201001, Uttar Pradesh, India
                    </p>
                    <!-- <div class="tel-phone"><a href="tel:+91-124-4318222" class="landline"><img src="./assets/img/blog/phone-icon.jpg" class="img-fluid" alt="Phone">+91-8046043795</a>
                       </div> -->
                </div>
            </div>
            <div class="col-md-2">
                <!-- <a href="#">
                       <h5 class="heading_fooetr">GROUP  COMPANIES  </h5>
                     </a> -->
                <ul class="footer_link">
                    <li><a href="../index ">Home</a></li>
                    <li><a href=" ">About Us</a></li>
                    <li><a href=" ">Our Verticals</a></li>
                    <li><a href="#"> Our Products</a></li>
                    <li><a href="#">Investor</a></li>
                    <li><a href="#">Clients</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <!-- <a href="#">
                       <h5 class="heading_fooetr">GROUP  COMPANIES  </h5>
                     </a> -->
                     <ul  class="footer_link">
                            <li><a   href="./cdwtubes">Goodluck Industries (Cdw Tubes)</a></li>
                            <li><a   href="./forging">Goodluck Engineering Co. (Forging)</a></li>
                            <li><a   href="./fabrication">Goodluck India Ltd. (Infra & Energy)</a></li>
                            <li><a   href="./roadsafetydivision">Goodluck India Ltd. (Road Safety)</a></li>
                            <li><a   href="./PipesAndTubes">Goodluck India Ltd. (Pipes & Tubes)</a>  </li>
                            <li><a  href="./PipesAndTubes">Goodluck India Ltd. (Coils and Sheets)</a>  </li>
                             </ul>
                   </div>

                

            <div class="col-md-3">

                <h5 class="text-dark">Connect Us</h5>
                <div style="margin-bottom: 10px;" class="c_now"><a href="tel:+91-8046043795" class="landline"><img
                            src="../assets/img/blog/phone-icon.jpg" class="img-fluid" alt="Phone"
                            style="width: 18%;border-radius:5px"> +91-0120-4196600, 4196700</a></div>
                            <div class="emil mb-2"><b>Email:</b> goodluck@goodluckindia.com</div>
                <div class="ne_w">
                    <button data-bs-toggle="modal" data-bs-target="#exampleModal" class="Enquire">Enquire Now</button>
                    <br>
                
                    <button class="Feed_Back" data-bs-toggle="modal" data-bs-target="#Feed_Back">FeedBack</button>
                </div>
            </div>
        </div>

    </div>
</footer>
<!-- footer start -->
<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-5">
                <div class="copy-text">
                    <p> ©<a href="https://www.goodluckindia.com/">Goodluck India Limited</a>  | All Rights Reserved</p>
                </div>
            </div>
            <div class="col-12 col-md-7">
                <p class="text-end"> Designed by <a href="https://www.webcadenceindia.com/" target="_blank" style="color:rgb(169 4 4);">Web Cadence</a>
                </p>
            </div>
        </div>
    </div>
</div>
<!-- footer end -->


    


 
 


               <!-- JS here -->
               <script src="../assets/js/vendor/jquery.js"></script>
               <script src="../assets/js/vendor/waypoints.js"></script>
               <script src="../assets/js/bootstrap-bundle.js"></script>
               <script src="../assets/js/meanmenu.js"></script>
               <script src="../assets/js/swiper-bundle.js"></script>
               <script src="../assets/js/owl-carousel.js"></script>
               <script src="../assets/js/magnific-popup.js"></script>
               <script src="../assets/js/parallax.js"></script>
               <script src="../assets/js/backtotop.js"></script>
               <script src="../assets/js/nice-select.js"></script>
               <script src="../assets/js/counterup.js"></script>
               <script src="../assets/js/wow.js"></script>
               <script src="../assets/js/isotope-pkgd.js"></script>
               <script src="../assets/js/imagesloaded-pkgd.js"></script>
               <script src="../assets/js/ajax-form.js"></script>
               <script src="../assets/js/main.js"></script>
               
               <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

<!-- <script src="js/wow.min.js"></script> -->
              <script>
              new WOW().init();
              </script>
            </body>
         </html> 