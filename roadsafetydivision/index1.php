<?php include './inc/header.php'; ?>


<main>
    <!-- hero -->

    <section calss="good_luck">
   



        <div id="carouselExample" class="carousel slide banner__slider" data-bs-ride="carousel" data-pause="false">
            <div class="carousel-inner cdw">
            <div class="carousel-item active">
                    <img src="../assets/RoadSafetyEquipment/banner/s2.png" class="d-block w-100" alt="...">
                    <!-- <div class="carousel-caption d-none d-md-block c1 new_r">
                        <h5 class="text-white"> Road Safety Division</h5>
                        <p>We are offering excellent CR (Cold Rolled) Coils that are made from finest steel metal and produced from hot rolled, before being pickled.
Previous</p>
                    </div> -->
                </div>
                <div class="carousel-item">
                    <img src=" ../assets/RoadSafetyEquipment/banner/approach_b.png" class="d-block w-100" alt="...">
                    <!-- <div class="carousel-caption d-none d-md-block c1 new_r">
                        <h5 class="text-white"> Road Safety Division</h5>
                        <p>We are offering excellent CR (Cold Rolled) Coils that are made from finest steel metal and produced from hot rolled, before being pickled.
Previous</p>
                    </div> -->
                </div>
                <div class="carousel-item  ">
                    <img src="../assets/RoadSafetyEquipment/banner/s1.png" class="d-block w-100" alt="...">
                    <!-- <div class="carousel-caption d-none d-md-block c1 new_r">
                        <h5 class="text-dark text-white">Road Safety Division</h5>
                        <p class="text-dark text-white">The Galvanized Pipes are manufactured using steel and then coated using the process of galvanizing.</p>
                    </div> -->
                </div>
                <div class="carousel-item">
                    <img src="../assets/RoadSafetyEquipment/banner/s3.png" class="d-block w-100" alt="...">
                    <!-- <div class="carousel-caption d-none d-md-block c1 new_r">
                        <h5 class="b_pipe">Road Safety Division</h5>
                        <p class="text-white">The Black Pipes are made from steel as well as iron and are widely suited for water supply and flammable gases.</p>
                    </div> -->
                </div>
                <div class="carousel-item">
                    <img src="../assets/RoadSafetyEquipment/banner/s4.png" class="d-block w-100" alt="...">
                    <!-- <div class="carousel-caption d-none d-md-block c1 new_r">
                        <h5 class="b_pipe">Road Safety Division</h5>
                        <p class="text-white">The Black Pipes are made from steel as well as iron and are widely suited for water supply and flammable gases.</p>
                    </div> -->
                </div>


               

               
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>
    <!-- hero -->


    <!-- about -->
    <section class="p60 abou_t">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="comman-head">
                        <h2 class="pb3">Welcome To Road Safety Division</h2>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="g_bout">


                        <div class="home-about wow fadeInUp" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;    text-align: justify;">
                            <blockquote>
                            Goodluck India Ltd. has collaborated with Road Steel, a Group Company of Gonvarri Industries. Gonvarri Industries is a Leading Spanish Company in the field of Road Safety having a presence in 19 countries, 45 factories, and 20 distribution centers with a team of over 6000 dedicated members, across the globe. 

                            </blockquote>
                            <p class="wow fadeInUp" data-wow-delay=".8s"
                                style="visibility: visible; animation-delay: 0.8s;    text-align: justify;">
                                Road Steel, Division of Ganvarri Industries, researches, develop, design, manufacture, install and market equipment, product and engineering services for Road Safety. It also offers a wide and complete range of vehicle containment systems for the shoulders and medians, where safety, efficiency, quality and innovation play a very important role.

                            </p>
                            <p class="wow fadeInUp" data-wow-delay=".9s"
                                style="visibility: visible; animation-delay: 0.8s;    text-align: justify;">
                                Road Steel's advanced modelling system and procedures ensure precise results, with continuous involvement from the design department to upgrade systems as per vehicle design, keeping up with changing road conditions. The comprehensive research and development team uses feedback from users along with the in-house latest computerized simulation techniques and actual crash testing as a tool to propose and deliver modern safety barriers.
                            </p>
                            <p class="wow fadeInUp" data-wow-delay=".9s"
                                style="visibility: visible; animation-delay: 0.8s;    text-align: justify;">
                                Road Safety division is providing meaningful and performance/crash-tested solutions for vehicles from passenger cars to heavy trucks with an Installed capacity of 60,000 MT per annum. Performance tested Products manufactured by Goodluck India are <b>“CE Certified”</b>
                            </p>
                            <!-- <div class="read-more">
                                <a href="#">Know more</a>
                            </div> -->
                        </div>


                    </div>
                </div>
                <div class="col-md-4">
                    <h4 style="border-bottom: 1px solid #eee;
                    padding-bottom: 5px;">FACTS</h4>
                    <div class="a_im_g">
                        <div class="row g-4 align-items-center">
                            <div class="col-sm-12">
                                <!-- <div class="bg-primary p-4 mb-4 wow fadeIn" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeIn;"><i class="fa fa-users fa-2x text-white mb-3"></i>
                                <h2 class="text-white mb-2" data-toggle="counter-up">1234</h2>
                                <p class="text-white mb-0">Happy Clients</p>
                                </div> -->
                                <!-- <div class="gray cunt p-3  wow fadeIn " >
                                    <i class="fa fa-briefcase"></i>
                                    <p   class="number" style="display:inline-block"> 60000 MT </p> <span style="    color: #838080;
                                    font-size: 21px;
                                    font-weight: 600;">   </span>
                                    <span></span>
                                    <p>Annual Installed Capacity</p>
                                </div> -->
                                <!-- <div class="bg-secondary p-4 wow fadeIn" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeIn;"><i class="fa fa-ship fa-2x text-white mb-3"></i>
                                <h2 class="text-white mb-2" data-toggle="counter-up">1234</h2>
                                <p class="text-white mb-0">Complete Shipments</p>
                                </div> -->
                                <div class="gray cunt p-3  wow fadeIn" >

                                    <!-- <i class="fa fa-user"></i> -->
                                    <!-- <p   class="number">10+</p> -->
                                    <span></span>
                                    <p>Pioneering the future of Road Safety : Cutting Edge Technology, Reliable, Budget Friendly & Performance Tested Products</p>
                                </div>
                                <div class="gray cunt p-3 wow fadeIn" >
                                    <!-- <i class="fa fa-users"></i> -->
                                    <!-- <p id="number3" class="number">3059</p> -->
                                    <span></span>
                                    <p>Precision Engineering Solutions, Uncompromised Road Safety Products</p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- counter -->

    <!-- service -->

    <section class="brand__area p60">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-12">
                    <div class="sectiontitle">
                        <h2>Product</h2>
                        <span class="headerLine"></span>
                        <p class="p_color">Established in the year 1986, Goodluck India Limited is an ISO 9001:2008
                            certified
                            organization,<br> engaged in manufacturing and exporting of a wide range of galvanized
                            sheets &amp; coils, <br>towers, hollow sections, CR coils CRCA and pipes &amp; tubes.</p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center align-items-center  ">
                <div class="col-md-3">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                <a href="approach.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/RoadSafetyEquipment/product/r1.png" alt="" class="res">
                                        </div>
                                        <h3>Approach End <br> Terminals </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                <a href="crashcushions.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/RoadSafetyEquipment/product/r2.png" alt="" class="res">
                                        </div>
                                        <h3>  Crash Cushions / Impact <br>Attenuators </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                <a href="bridge.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/RoadSafetyEquipment/product/r3.png" alt="" class="res">
                                        </div>
                                        <h3> Bridge/Vehicle<br> Parapets </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                <a href="crashtested.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/RoadSafetyEquipment/product/r4.png" alt="" class="res">
                                        </div>
                                        <h3>Crash Tested/ Conventional Metal <br> Beam Crash Barriers </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                <a href="transition.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/RoadSafetyEquipment/product/r5.png" alt="" class="res">
                                        </div>
                                        <h3>Transitions  </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3 ">
                <div class="brand__slider-item swiper-slide mb-3 pr_b">
                                <a href="endterminals.php">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/RoadSafetyEquipment/product/r6.png" alt="" class="res">
                                        </div>
                                        <h3>End Terminals  </h3>
                                    </div>
                                </a>
                            </div>
                </div>
               


                
                  
                <!-- end -->
            </div>
            <!-- <div class="col-md-12">
                    <div class="ViewAll text-center mt-4">
                        <button class="v_all"><i class="fa fa-plus"></i> ViewAll</button>
                    </div>
                  </div> -->
        </div>
    </section>

    <!-- service -->
<!-- brand__area start -->
<section class="brand__area1 p60" style="    background-color: #fdfdfd;">
        <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle">
                        <h2> Product  Application</h2>
                        <span class="headerLine"></span>
                      
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                <div class="related__product swiper-container">
                        <div class="swiper-wrapper">
                           <div class="brand__slider-item swiper-slide">
                              <a href="#"><img src="../assets/RoadSafetyEquipment/product/related_p/r1.png" class="w100 r__product" alt=""></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                              <a href="#"><img src="../assets/RoadSafetyEquipment/product/related_p/r2.png" class="w100 r__product" alt=""></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                              <a href="#"><img src="../assets/RoadSafetyEquipment/product/related_p/r3.png" class="w100 r__product" alt=""></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                              <a href="#"><img src="../assets/RoadSafetyEquipment/product/related_p/r4.png" class="w100 r__product" alt=""></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                              <a href="#"><img src="../assets/RoadSafetyEquipment/product/related_p/r5.png" class="w100 r__product" alt=""></a>
                           </div>
                           <div class="brand__slider-item swiper-slide">
                              <a href="#"><img src="../assets/RoadSafetyEquipment/product/related_p/r6.png" class="w100 r__product" alt=""></a>
                           </div>
                        </div>
                     </div>

                     

                </div>
            </div>
        </div>
    </section>
 
  
    
 
    <!-- brand__area end -->
   
</main>



<?php include './inc/footer.php';?>
