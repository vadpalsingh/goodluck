<?php include './inc/header.php'; ?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Goodluck India Limited (Pipes And Tubes) :: Quality </title>
    <meta name="description" content="">


    <?php include './inc/header.php'; ?>
    <div class="contant">
        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">

                    <!-- <div class="carousel-item active">
                    <img src="../assets/PipesAndTubes/pipe/quality_b.png" class="d-block w-100"
                        alt="...">
                    <div class="carousel-caption   d-md-block cp  ">
                        <h5 class="text-white mb_font p_ro">Offers the most exclusive range both in terms of capacity
                            and capability</h5>
                    </div>
                </div> -->

                    <div class="carousel-item  active">
                        <img src="../assets/RoadSafetyEquipment/banner/about_b.jpg" class="d-block w-100"
                            alt="...">

                        <div class="carousel-caption d-none d-md-block c2 bg_tr">

                            <p>Stringent and uncompromising Quality Control from Raw Material stage to Finished Goods
                            </p>
                        </div>
                    </div>

                </div>

            </div>
        </section>
        <!-- hero -->

        <section class="coials bg-light pt-5 pb-5 ">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="sectiontitl">
                            <h2> WELCOME TO GOODLUCK ROAD SAFETY DIVISION</h2>
                            <span class="headerLine"></span>
                            <p class="p_color">
                                We also provide conventional end terminals, which
                                are widely used in Indian Market, may/may not
                                confirm to the codal provisions of IRC/MoRTH.<br><br>
                                Various End Terminals Includes Trailing Terminals,
                                Turn Down End Terminals of length varying from 2
                                mtr to 12 mtrs, Fish tail End terminals,<br><br> Bullnose type
                                end terminals and many more both for W Beam
                                and Thrie Beam as per site requirements.
                            </p>
                            <div class="ab-button mb-30 mt-3 ">
                                <button type="button" class="btn   tp-btn-ts bg-dark text-white" data-bs-toggle="modal"
                                    data-bs-target="#Enquire"><b> Enquire Now</b></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="qlabe">
                            <img src="../assets/RoadSafetyEquipment/product/about.png" alt="" class="w-100 r_5">
                        </div>
                    </div>
                </div>



            </div>
        </section>
        <!-- end-sheets -->
        <?php include './inc/footer.php';?>