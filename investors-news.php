<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India :: Investors News</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <main>
        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item  active ">
                        <img src="./assets/PipesAndTubes/banner/common.jpg" class="d-block w-100" alt="Financial">
                        <h5>Investors News </h5>
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="innerpagenav">
            <div class="container">
                <div class="row">
                    <ul class="pagenav">
                        <li><a href="investors.php">Investors</a></li>
                        <li><a href="financial.php">Financial</a></li>
                        <li><a href="press-release.php">Press Release</a></li>
                        <li><a href="investors-news.php" class="active">Investors News</a></li>
                        <li><a href="shareholding-pattern.php">Shareholding Pattern</a></li>
                        <li><a href="shareholder-information.php">Shareholder Information</a></li>
                        <li><a href="downloads.php">Downloads</a></li>
                        <li><a href="corporate-governance.php">Corporate Governance</a></li>
                    </ul>
                </div>
            </div>
        </section>

        <!-- end -->

        <section class="export pb-60 p60  wow fadeInUp   animated" data-wow-delay="0.5s"
            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
            <div class="container">
                <div class="sec_title">
                    <h1 class="h_padding pt-0 wow fadeInRight   animated" data-wow-delay="0s"
                        style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">Investors News
                    </h1>
                </div>
                <div class="row">

                    <div class="col-md-6">
                        <div class="ins-det">
                            <h4 class="mt-4 mb-3">Investors Presentation</h4>
                            <div class="pre-rel"><a href="pdf/investor-presentation-q1-fy24.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Investor Presentation Q1 FY24</a> </div>
                            <div class="pre-rel"><a href="pdf/presentation-march2022.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Investor Presentation March 2022</a> </div>
                            <div class="pre-rel"><a href="pdf/presentation-december2021.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Investor Presentation December 2021</a> </div>
                            <div class="pre-rel"><a href="pdf/presentation-september2021.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Investor Presentation September 2021</a> </div>
                            <div class="pre-rel"><a href="pdf/presentation-june2021.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Investor Presentation June 2021</a> </div>
                            <div class="pre-rel"><a href="pdf/q3fy17-earnings-presentation.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Q3FY17 Earnings Presentation </a> </div>
                            <div class="pre-rel"><a href="pdf/presentation-june2016.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Investor Presentation June 2016 </a> </div>
                            <div class="pre-rel"><a href="pdf/presentation-march2016.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Investor Presentation March 2016 </a> </div>
                            <div class="pre-rel"><a href="pdf/presentation-april2015.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Presentation - April 2015</a> </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="ins-det">
                            <h4 class="mt-4 mb-3">CONFERENCE CALL</h4>
                            <div class="pre-rel"><a href="mp3/ScheduleofAnalystInstitutionalInvestorMeeting.pdf"
                                    target="_new"><i class="fa fa-file-pdf-o"></i> Schedule of Analyst/ Institutional Investor Meeting</a></div>
                            <div class="pre-rel"><a href="mp3/investors-concall-audio-recording-dated-27-05-2022.mp3"
                                    target="_new"><i class="fa fa-file-pdf-o"></i> Investor's Concall Audio Recording
                                    dated 27.05.2022</a></div>

                            <div class="pre-rel"><a href="pdf/conference-call-on-27-05-2022.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Conference Call on 27.05.2022</a></div>
                            <div class="pre-rel"><a href="pdf/transcript-of-conference-call-19-01-2022.pdf"
                                    target="_new"><i class="fa fa-file-pdf-o"></i> Transcript of Conference Call
                                    19.01.2022 </a></div>
                            <div class="pre-rel"><a href="pdf/transcript-of-conference-call-26-10-21.pdf"
                                    target="_new"><i class="fa fa-file-pdf-o"></i> Transcript of Conference Call
                                    26.10.21 </a></div>
                            <div class="pre-rel"><a href="pdf/conference-call-on-26-10-21.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Conference Call on 26.10.2021</a></div>
                            <div class="pre-rel"><a href="pdf/conference-call-on-11-02-19.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Conference Call on 11.02.19</a></div>
                            <div class="pre-rel"><a href="pdf/transcript-conference-call-06-11-18.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Transcript of Conference Call 06.11.18</a></div>
                            <div class="pre-rel"><a href="pdf/conference-call-on-06-11-18.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Conference Call on 06.11.18</a></div>
                            <div class="pre-rel"><a href="pdf/concall-transcript-q1fy19.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Transcript of Conference Call 16.08.18</a></div>
                            <div class="pre-rel"><a href="pdf/q1fy19concall.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Conference Call on 16.08.18</a></div>
                            <div class="pre-rel"><a href="pdf/concall15022018.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Conference Call 15.02.18</a></div>
                            <div class="pre-rel"><a href="pdf/concallinvitationQ2FY18.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Conference Call 15.12.17
                                    ("Q2FY18ConcallInvitation")</a></div>
                            <div class="pre-rel"><a href="pdf/TranscriptconcallSept15-2017.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Transcript of Conference Call 15.09.17</a></div>
                            <div class="pre-rel"><a href="pdf/Q1FY18Concall.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Conference Call 15.09.17 ("Q1FY18Concall")</a>
                            </div>
                            <div class="pre-rel"><a href="pdf/conference-call-30-05-17.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Conference Call 30.05.17</a></div>
                            <div class="pre-rel"><a href="pdf/transcript-of-conference-call-15-02-17.pdf"
                                    target="_new"><i class="fa fa-file-pdf-o"></i> Transcript of Conference Call
                                    15.02.17</a></div>
                            <div class="pre-rel"><a href="pdf/conference-call-15-02-17.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Conference Call 15.02.17</a> </div>
                            <div class="pre-rel"><a href="pdf/concall-invitation-15-11-16.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Concall Invitation 15.11.16 </a> </div>
                            <div class="pre-rel"><a href="pdf/transcript-conCall-16-08-16.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Transcript of Investors &amp; analytics conference
                                    call 16.08.16 </a> </div>
                            <div class="pre-rel"><a href="pdf/concall-invitation-16-08-16.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Concall Invitation 16.08.16 </a> </div>
                            <div class="pre-rel"><a href="pdf/concall-invitation-25-05-16.pdf" target="_new"><i
                                        class="fa fa-file-pdf-o"></i> Concall Invitation 25-05-16 </a> </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
        </section>
        <!-- end -->
    </main>
    <?php include './inc/footer.php';?>