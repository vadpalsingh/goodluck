<!doctype html>
<html class="no-js" lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Goodluck India :: Financial</title>
    <meta name="description" content="">
    <?php include './inc/header.php'; ?>
    <main>
        <section calss="good_luck">
            <div id="carouselExample" class="carousel slide">
                <div class="carousel-inner cdwtubesbanner">
                    <div class="carousel-item  active ">
                        <img src="./assets/PipesAndTubes/banner/common.jpg" class="d-block w-100" alt="Financial">
                        <h5>Financial </h5>
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="innerpagenav">
            <div class="container">
                <div class="row">
                    <ul class="pagenav">
                        <li><a href="investors.php">Investors</a></li>
                        <li><a href="financial.php" class="active">Financial</a></li>
                        <li><a href="press-release.php">Press Release</a></li>
                        <li><a href="investors-news.php">Investors News</a></li>
                        <li><a href="shareholding-pattern.php">Shareholding Pattern</a></li>
                        <li><a href="shareholder-information.php">Shareholder Information</a></li>
                        <li><a href="downloads.php">Downloads</a></li>
                        <li><a href="corporate-governance.php">Corporate Governance</a></li>
                    </ul>
                </div>
            </div>
        </section>

        <!-- end -->

        <section class="export pb-60 p60  wow fadeInUp   animated" data-wow-delay="0.5s"
            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
            <div class="container">
                <div class="row">
                    <div class="sec_title">
                        <h1 class="h_padding pt-0 wow fadeInRight   animated" data-wow-delay="0s"
                            style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">Financial</h1>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th align="CENTER" width="16%" class="text-center"><strong>Year</strong></th>
                                    <th align="CENTER" width="17%" class="text-center"><strong>Q1</strong></th>
                                    <th align="CENTER" width="17%" class="text-center"><strong>Q2</strong></th>
                                    <th align="CENTER" width="17%" class="text-center"><strong>Q3</strong></th>
                                    <th align="CENTER" width="17%" class="text-center"><strong>Q4</strong></th>
                                    <th align="CENTER" width="16%" class="text-center"><strong>Annual Report</strong>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="CENTER">2023-24</td>
                                    <td align="CENTER"><a href="pdf/q1-result-2023.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER">-</td>
                                    <td align="CENTER">-</td>
                                    <td align="CENTER">-</td>
                                    <td align="CENTER">-</td>

                                </tr>
                                <tr>
                                    <td align="CENTER">2022-23</td>
                                    <td align="CENTER"><a href="pdf/q1-result-2022.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/q2-result-2022.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/q3-result-2022.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/q4-result-2023.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER">-</td>

                                </tr>
                                <tr>
                                    <td align="CENTER">2021-22</td>
                                    <td align="CENTER"><a href="pdf/bodoutcome30-06-2021.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/q2-result-2021.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/q3-result-2021.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/q4-result-2022.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/goodluck-annual-report-2021-22.pdf"
                                            target="_blank"><img src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                </tr>
                                <tr>
                                    <td align="CENTER">2020-21</td>
                                    <td align="CENTER"><a href="pdf/q1-result-2020.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/q2-result-2020.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/q3-result-2020.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/q4-result-2021.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/goodluck-annual-report-2020-21-new.pdf"
                                            target="_blank"><img src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>

                                </tr>

                                <tr>
                                    <td align="CENTER">2019-20</td>
                                    <td align="CENTER"><a href="pdf/bmoutcome13082019.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/q2-result-2019.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/q3-result-2019.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/q4-result-2020.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/goodluck-annual-report-2019-20-new.pdf"
                                            target="_blank"><img src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>

                                </tr>

                                <tr>
                                    <td align="CENTER">2018-19</td>
                                    <td align="CENTER"><a href="pdf/result-june2018.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/result-sep2018.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/result-dec2018.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/result-mar2019.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/goodluck-annual-report-2018-19.pdf"
                                            target="_blank"><img src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>

                                </tr>





                                <tr>
                                    <td align="CENTER">2017-18</td>
                                    <td align="CENTER"><a href="pdf/resultsjune2017.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/results-december-14-2017.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/resultdec17.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/results-mar2018.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/goodluck-annual-report-2017-18.pdf"
                                            target="_blank"><img src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>

                                </tr>

                                <tr>
                                    <td align="CENTER">2016-17</td>
                                    <td align="CENTER"><a href="pdf/2016-17-q1.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2016-17-q2.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2016-17-q3.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/ResultMar17.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/ResultMar18.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>

                                </tr>


                                <tr>
                                    <td align="CENTER">2015-16</td>
                                    <td align="CENTER"><a href="pdf/2015-16-q1.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/qfr-q2-fy16.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/qfr-q3-fy16.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/qfr-q4-fy16.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/annual-report-2015-16.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                </tr>
                                <tr>
                                    <td align="CENTER">2014-15</td>
                                    <td align="CENTER"><a href="pdf/2014-15-q1.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2014-15-q2.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2014-15-q3.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2014-15-q4.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/annual-report-2015.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>

                                </tr>
                                <tr>
                                    <td align="CENTER">2013-14</td>
                                    <td align="CENTER"><a href="pdf/2013-14-q1.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2013-14-q2.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2013-14-q3.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2013-14-q4.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/annual-report2013-14.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                </tr>
                                <tr>
                                    <td align="CENTER">2012-13</td>
                                    <td align="CENTER"><a href="pdf/2012-13-q1.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2012-13-q2.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2012-13-q3.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2012-13-q4.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/annual-report2012-13.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                </tr>
                                <tr>
                                    <td align="CENTER">2011-12</td>
                                    <td align="CENTER"><a href="pdf/2011-12-q1.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2011-12-q2.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2011-12-q3.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2011-12-q4.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/annual-report2011-12.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                </tr>
                                <tr>
                                    <td align="CENTER">2010-11</td>
                                    <td align="CENTER"><a href="pdf/2010-11-q1.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2010-11-q2.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2010-11-q3.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2010-11-q4.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/annual-report2010-11.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                </tr>
                                <tr>
                                    <td align="CENTER">2009-10</td>
                                    <td align="CENTER"><a href="pdf/2009-10-q1.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2009-10-q2.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2009-10-q3.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/2009-10-q4.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                    <td align="CENTER"><a href="pdf/annual-report2009-10.pdf" target="_blank"><img
                                                src="gifs/pdf.png" width="30" height="30" alt="pdf"
                                                align="absmiddle"></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        </section>
        <!-- end -->
    </main>
    <?php include './inc/footer.php';?>