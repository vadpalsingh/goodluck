<!doctype html>
         <html class="no-js" lang="zxx">
            
         <head>
               <meta charset="utf-8">
               <meta http-equiv="x-ua-compatible" content="ie=edge">
               <title>  Goodluck India Limited (Pipes And Tubes) :: Corrugated  </title>
               <meta name="description" content="">


<?php include './inc/header.php'; ?>
<div class="contant">
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                
                <div class="carousel-item active">
                    <img src="../assets/PipesAndTubes/pipe/glv_coils.png" class="d-block w-100"
                        alt="...">
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Defined allocation for each Product Category ensures 100% delivery performance </p>
                    </div>
                </div>
                
            </div>
           
        </div>
    </section>
    <!-- hero -->
 
    <section class="coials bg-light pt-5 pb-5 ">
        <div class="container">
            <div class="row align-items-center ">
                <div class="col-md-6">
                    <div class="gj_coial">


                        <h3>Corrugation Details</h3>
                        <div class="gl_detail">
                            <div class="cr_coils w100">
                            <table class="table table-bordered">
                            <thead class="table-dark">
                                <tr>

                                    <th scope="col">Width Before Corrugation (mm)</th>
                                    <th scope="col">Width After Corrugation (mm)</th>
                                    <th scope="col">Pitch (MM)  </th>
                                    <th scope="col">Depth (mm) (±)</th>
                                    <th scope="col">No. of Corrugations</th>


                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>750 / 762</td>
                                    <td>660 / 665 / 720 / 740 /</td>
                                    <td>75</td>
                                    <td> 17.5</td>
                                    <td> 8</td>
                                </tr>
                                <tr>
                                    <td>900 / 914</td>
                                    <td>750 / 760 / 780 / 800 / 810 / 830 /</td>
                                    <td>65 to 85</td>
                                    <td> 12.5 to 20.5</td>
                                    <td> 10</td>
                                </tr>

                                <tr>
                                    <td>100</td>
                                    <td>885/900/930</td>
                                    <td>75</td>
                                    <td> 17.5</td>
                                    <td> 11</td>
                                </tr>
                               
                            <tbody>
                        </table>
                        <h4 class="mt-3">HRPO</h4>
                                 
                        <div class="gl_detail">
                    <div class="gl_cd w50">  
                        
                    <ul>
                        <li><span>Thickness</span> <span> : 1.4 mm to 6.0 mm</span></li>
                        <li><span>Width</span> <span> : 100 mm to 1000 mm </span></li>
                       
                        
                    </ul>
                        </div>
                    <div class="gl_cd w50"> 
                    <ul>
                        <li><span>Length Range</span> <span>: 500 mm to 3500 mm</span></li>
                        <li><span>Specification </span> <span>: IS11513, IS1069</span></li>
                         
                      
                    </ul>   </div>
                    </div>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="gj_img">
                        <img src="../assets/PipesAndTubes/pipe/cru_p.png" alt="" class="w-100 r_5">
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- end-sheets -->
   

</div>
<?php include './inc/footer.php'; ?>