
<!doctype html>
         <html class="no-js" lang="zxx">
            
         <head>
               <meta charset="utf-8">
               <meta http-equiv="x-ua-compatible" content="ie=edge">
               <title>  Goodluck India Limited (Pipes And Tubes) :: conduitul6  </title>
               <meta name="description" content="">
<?php include './inc/header.php';?>
<div class="contant">
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                
                <div class="carousel-item active">
                    <img src="../assets/PipesAndTubes/pipe/rig_coduct.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Defined allocation for each Product Category ensures 100% delivery performance </p>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
    <!-- hero -->

<section class="ptb  ">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>Rigid  Conduits</h2>
                <h4>UL 6 - Galvanized Rigid Metal Conduits</h4>
                <p class="v">Goodluck RIGID CONDUITS are hot dipped galvanized both
inside and outside. The rigid conduits are threaded both
ends with state-of-the-art Automatic threading machines
and supplied with UL approved couplings on one end and
other end protected with color coded plastic thread
protection caps. The external and internal surface of the
conduits is hot dipped galvanized and then a clear anti-
corrosion coating is done for higher protection against
corrosion giving the rigid conduits long life. Rigid conduits 
are for the job that requires physical protection that only
thickest walls can provide and rigid conduits are known for
their thick walls and heaviest weights. Goodluck RMC is
tough enough for use in outdoor applications, energy and
manufacturing plants and corrosive environments.
Goodluck rigid conduits have prestigious UL6 certification.
(File No. E510369). We also make rigid conduits as per American
national Standard for electrical rigid conduit (ANSI C80.1)
</p>
            </div>
            <div class="col-md-6">
                <div class=" text-center">
                <img src="../assets/PipesAndTubes/pipe/rig_coduct_p.png" class=" r_5  " alt="...">
                </div>
            </div>
            <div class="col-md-12">
            <div class="forgung ">

            <div class="c_enterd1">
              
          
          
           <table class="table table-bordered">
  <thead class="table-dark">
    <tr>
    
      <th scope="col">Trade Size</th>
      <th scope="col">Metric Designator</th>
      <th scope="col">Outside Diameter</th>
      <th scope="col">Nominal Wall Thickness**</th>
      <th scope="col">Min. Acceptable Weight of Ten Pcs of Conduits With Couplings</th>
      <th scope="col">QTY In Master Bundle</th>
      <th scope="col">QTY In Inner Bundle</th>
      <th scope="col">End Caps Color</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th></th>
      <td></td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td>In </td>
                <td>Mm</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td>In </td>
                <td>Mm</td>
            </tr>
        </table>
      </td>
      <td>Kg</td>
      <td>Pcs</td>
      <td>Pcs</td>
      <td></td>
    </tr>
    <!-- end -->
    <tr class="alert alert-success ">
      <th>1/2" </th>
      <td>16</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td>0.840 </td>
                <td>21.34</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td>0.104 </td>
                <td>2.64</td>
            </tr>
        </table>
      </td>
      <td>35.8</td>
      <td>250</td>
      <td>10</td>
      <td>Black</td>
    </tr>
    <!-- end -->
    <tr class="alert alert-success ">
      <th> 3/4" </th>
      <td>21</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td> 1.050 </td>
                <td>26.67 </td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td> 0.107 </td>
                <td> 2.72</td>
            </tr>
        </table>
      </td>
      <td> 47.6 </td>
      <td>200 </td>
      <td>5 </td>
      <td> Red</td>
    </tr>
    <!-- end -->
    <tr class="alert alert-success ">
      <th> 1" </th>
      <td>27</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td> 1.315 </td>
                <td> 33.40</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td> 0.126 </td>
                <td> 3.20</td>
            </tr>
        </table>
      </td>
      <td> 69.4 </td>
      <td>125 </td>
      <td>1 </td>
      <td> Blue</td>
    </tr>
    <!-- end -->
    <tr class="alert alert-success ">
      <th> 1 1/4" </th>
      <td>35</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td> 1.660 </td>
                <td> 42.16</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td> 0.133 </td>
                <td> 3.38</td>
            </tr>
        </table>
      </td>
      <td>  91.2</td>
      <td>90 </td>
      <td> </td>
      <td> Red</td>
    </tr>
    <!-- end -->
    <tr class="alert alert-success ">
      <th> 1 1/2” </th>
      <td>41</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td>1.900  </td>
                <td>48.26 </td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td> 0.138 </td>
                <td> 3.51</td>
            </tr>
        </table>
      </td>
      <td> 112.9 </td>
      <td> 80</td>
      <td> </td>
      <td> Black</td>
    </tr>
    <!-- end -->
    <tr class="alert alert-success ">
      <th>  2 </th>
      <td>53 </td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td> 2.375  </td>
                <td> 60.33</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td> 0.146 </td>
                <td>3.71 </td>
            </tr>
        </table>
      </td>
      <td>  150.6</td>
      <td>  60</td>
      <td> </td>
      <td> Blue</td>
    </tr>
    <!-- end -->
    <tr class="alert alert-success ">
      <th>  25 </th>
      <td> 63</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td>  2.875 </td>
                <td> 73.03</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td>  0.193</td>
                <td>4.90 </td>
            </tr>
        </table>
      </td>
      <td> 239.0 </td>
      <td> 37 </td>
      <td> </td>
      <td> Black</td>
    </tr>
    <!-- end -->
    <tr class="alert alert-success ">
      <th> 3  </th>
      <td> 78</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td>  3.500 </td>
                <td> 88.90</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td> 0.205 </td>
                <td> 5.21</td>
            </tr>
        </table>
      </td>
      <td> 309.6 </td>
      <td> 30 </td>
      <td> </td>
      <td> Blue</td>
    </tr>
    <!-- end -->
    <tr class="alert alert-success ">
      <th> 3 1/2”  </th>
      <td>91 </td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td> 4.000  </td>
                <td>101.60 </td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td> 0.215 </td>
                <td>5.46 </td>
            </tr>
        </table>
      </td>
      <td>  376.9</td>
      <td>  25</td>
      <td> </td>
      <td> Black</td>
    </tr>
    <!-- end -->
    <tr class="alert alert-success ">
      <th>  4 </th>
      <td>103 </td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td> 4.500  </td>
                <td> 114.30</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td> 0.225 </td>
                <td> 5.72</td>
            </tr>
        </table>
      </td>
      <td> 441.0 </td>
      <td>  20</td>
      <td> </td>
      <td> Blue</td>
    </tr>
    <!-- end -->
    <tr class="alert alert-success ">
      <th> 5  </th>
      <td> 129</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td> 5.563  </td>
                <td> 141.30</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td> 0.245 </td>
                <td>6.22</td>
            </tr>
        </table>
      </td>
      <td> 535.8 </td>
      <td> 15</td>
      <td> </td>
      <td> Blue</td>
    </tr>
    <!-- end -->
    <tr class="alert alert-success ">
      <th>  5 </th>
      <td>155 </td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td>  6.625 </td>
                <td>168.28 </td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td> 0.266 </td>
                <td> 6.76</td>
            </tr>
        </table>
      </td>
      <td> 791.7 </td>
      <td> 10 </td>
      <td> </td>
      <td> Blue</td>
    </tr>
    <!-- end -->
    
  </tbody>
</table>


<div class="application mt-4">
    <div class="tollrance w50">
        <h5>Applicable Tolerances -</h5>
    Length: 10 Feet (3.05 Meters) +/- 0.25 inch (6.35 mm)<br>
Outside Diameter: <b>TRADE SIZE ½” TO 1-1/2”: ±0.015” (±0.38MM)<br>
; TRADE SIZE 2” – 6” : ±1%</b><br>
**Wall thickness mentioned are just for information only and not
a requirement of UL standard.<br>
<br>
We also produce rigid conduits in 20 feet (6 Meters) length.
20 Feet conduit provide quick and easy installation and also
fewer fittings.
    </div>
    <div class="tollrance w50">
        <h5>Features of Goodluck Rigid Conduits –</h5>
        <ul>
            <li>1. Higher corrosion resistance with superior & uniform galvanizing quality.</li>
            <li>2. Accurate dimensions and uniform wall thickness.</li>
            <li>3. Clean and smooth internal surface.</li>
            <li>4. Easy coupling and fast installation.</li>
            <li>5. Easy wire pushing and pulling.</li>
            <li>6. Easy & good fabrication properties like smooth cutting, bending & welding.</li>
            <li>7. Uniform and consistent quality.</li>
            <li>8. Meets the underwriters laboratories specifications Ul6.</li>
            <li>9. Tested and certified for safety.</li>
          

        </ul>
        </div>
</div>
            </div>
        </div>
            </div>
        </div>
    </div>
</section>
 


</div>
<?php include './inc/footer.php';?>