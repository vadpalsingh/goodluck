

<!doctype html>
         <html class="no-js" lang="zxx">
            
         <head>
               <meta charset="utf-8">
               <meta http-equiv="x-ua-compatible" content="ie=edge">
               <title>  Goodluck India Limited (Pipes And Tubes) :: Home  </title>
               <meta name="description" content="">
<?php include './inc/header.php'; ?>


<main>
    <!-- hero -->
 
    <section class="good_luck">
        <div id="carouselExample" class="carousel slide banner__slider" data-bs-ride="carousel" data-pause="false">
            <div class="carousel-inner cdwtubesbanner">
                
                <div class="carousel-item active ">
                    <img src="../assets/PipesAndTubes/banner/home_b.jpg" class="d-block w-100" alt="...">
			 
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Stringent and uncompromising Quality Control from Raw Material stage to Finished Goods</p>
                    </div>
                </div>
                <div class="carousel-item ">
                    <img src="../assets/PipesAndTubes/pipe/p_b1.png" class="d-block w-100" alt="...">
			 
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Defined allocation for each Product Category ensures 100% delivery performance </p>
                    </div>
                </div>
                <div class="carousel-item ">
                    <img src="../assets/PipesAndTubes/banner/home_b3.png" class="d-block w-100" alt="...">
					<!-- <h5> Sikandrabad Plant</h5> -->
                   
                </div>
                <div class="carousel-item ">
                    <img src="../assets/PipesAndTubes/banner/plant.jpg" class="d-block w-100" alt="...">
					<!-- <h5> Sikandrabad Plant</h5> -->
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Offers the most exclusive range both in terms of capacity and capability </p>
                    </div>
                </div>
                <div class="carousel-item ">
                    <img src="../assets/PipesAndTubes/banner/fancing.jpg" class="d-block w-100" alt="...">
					<!-- <h5> Sikandrabad Plant</h5> -->
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Defined allocation for each Product Category ensures 100% delivery performance </p>
                    </div>
                </div>
               
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>
    <!-- hero -->
          
    <!-- hero -->



    <!-- about -->
    <section class="p60 abou_t">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="comman-head">
                        <h2 class="pb3">Welcome To Tubes & Pipes </h2>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="g_bout">


                        <div class="home-about wow fadeInUp mb-2" data-wow-delay=".2s"
                            style="visibility: visible; animation-delay: 0.2s;">
                            <blockquote>
                            Goodluck India Limited is a versatile business unit that has a diverse product range produced across our north India manufacturing locations and Bhuj, Gujarat.
                            </blockquote>
                            <p class="wow fadeInUp mb-2" data-wow-delay=".5s"
                                style="visibility: visible; animation-delay: 0.8s;">
                                Goodluck India Ltd. engaged in manufacturing and exporting of a wide range of ERW Hot Dip Galvanized Pipes, Black Pipes, Black & GI hollow sections, CR coils, CRCA, Galvanized & Plain Corrugated Sheets. We also specialize in providing Telecommunication, solar mounting and railway structures. These are acclaimed for high tensile strength, long service life and higher efficiency.

                            </p>


                           
                                <div class="read-more">
                                <a href="overview.php">Know more</a>
                            </div>
                        </div>
                        <img src="../assets/PipesAndTubes/banner/banner2.png" class="d-block w-100 r_5 mt-4" alt="...">


                    </div>
                </div>
                <div class="col-md-4">
                    <h4 style="border-bottom: 1px solid #eee;
                    padding-bottom: 5px;">FACTS</h4>
                    <div class="a_im_g">
                        <div class="row g-4 align-items-center">
                            <div class="col-sm-12">
                                
                                <div class="gray pipes cunt p-3  wow fadeIn " 
                                 >
                                    <!-- <i class="fa fa-briefcase"></i> -->
                                    <p   class="number" style="display:inline-block">1,94,000 MT Per Annum   </p>
                                    <span style="font-size: 14px;     display: block;    margin: 0; color:#000"></span>
                                    <p>Production Capacity </p>
                                </div>
                                <!-- <div class="bg-secondary p-4 wow fadeIn" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeIn;"><i class="fa fa-ship fa-2x text-white mb-3"></i>
                                <h2 class="text-white mb-2" data-toggle="counter-up">1234</h2>
                                <p class="text-white mb-0">Complete Shipments</p>
                                </div> -->
                               
                                <div class="gray pipes cunt p-3  wow fadeIn" >

                                    <!-- <i class="fa fa-user"></i> -->
                                    <p   class="number">500 +  </p>
                                    <span></span>
                                    <p> Customers spread across the Country</p>
                                </div>
                                <div class="gray pipes cunt p-3  wow fadeIn" >

                                    <!-- <i class="fa fa-user"></i> -->
                                    <p   class="number">100 +</p>
                                    <span></span>
                                    <p> Countries Export </p>
                                </div>
                                <div class="gray pipes cunt p-3  wow fadeIn" >
                                    <!-- <i class="fa fa-user"></i> -->
                                    <p   class="number">1500+</p>
                                    <span></span>
                                    <p>Work Force</p>
                                </div>
                                <div class="gray pipes cunt p-3  wow fadeIn" >
                                    <!-- <i class="fa fa-user"></i> -->
                                    <p   class="number">5 Galvanizing Plants </p>
                                    <span></span>
                                    <p>1,20,000 MT/ Annum</p>
                                    </div>
                                
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- counter -->
      <!-- service -->

      <section class="brand__area p60">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle">
                        <h2>OUr Products</h2>
                        <span class="headerLine"></span>
                        <p class="p_color">Goodluck India Limited is Established in the year 1986, Goodluck India Limited is an ISO 9001:2015, TS-1694,EMS 1401, OH&SMS 45001, CE certified & UL approved
                            
                            organization, Goodluck India Limited  engaged in manufacturing and exporting of ERW Hot Dip Galvanized Pipes, Black Pipes, Black & GI hollow sections, CR coils, CRCA, Galvanized & Plain Corrugated Sheets.</p>
                    </div>
                </div>
            </div>
            <div class="row  ">
                <div class="col-md-3">
                <div class="brand__product mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/PipesAndTubes/product/hot_g.jpg" alt="" class="res">
                                        </div>
                                        <h3>ERW Hot dipped Galvanized Pipes </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3">
                <div class="brand__product mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/PipesAndTubes/pipe/black.png" alt="" class="res">
                                        </div>
                                        <h3>ERW Black Round Pipes </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3">
                <div class="brand__product mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/PipesAndTubes/pipe/sq_p.png" alt="" class="res">
                                        </div>
                                        <h3>Square Tubes </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3">
                <div class="brand__product mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/PipesAndTubes/pipe/ractangula_p.png" alt="" class="res">
                                        </div>
                                        <h3>Rectangular Tubes</h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3 ">
                <div class="brand__product mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/PipesAndTubes/pipe/pre_g.png" alt="" class="res">
                                        </div>
                                        <h3>Pre Galvanized Pipes </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3 ">
                <div class="brand__product mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/PipesAndTubes/pipe/elect_p.png" alt="" class="res">
                                        </div>
                                        <h3>Painted Pipes ( Black, Red and Blue ) </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3 ">
                <div class="brand__product mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/PipesAndTubes/pipe/rig_coduct_p.png" alt="" class="res">
                                        </div>
                                        <h3> Rigid Conduits - UL6 </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3 ">
                <div class="brand__product mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/PipesAndTubes/pipe/emt_979.png" alt="" class="res">
                                        </div>
                                        <h3>EMT Conduits UL797</h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3 ">
                <div class="brand__product mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/PipesAndTubes/pipe/elect_p1.png" alt="" class="res">
                                        </div>
                                        <h3> Intermediate Metal Conduit UL1242</h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3 ">
                <div class="brand__product mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/PipesAndTubes/pipe/gl_coial.png" alt="" class="res">
                                        </div>
                                        <h3> CR Coils and Sheets  </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3 ">
                <div class="brand__product mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/PipesAndTubes/product/glv_sheets.jpg" alt="" class="res">
                                        </div>
                                        <h3> Galvanized Coils  </h3>
                                    </div>
                                </a>
                            </div>
                </div>
                <div class="col-md-3 ">
                <div class="brand__product mb-3 pr_b">
                                <a href="#">
                                    <div class="produ_ct">
                                        <div class="p_im_g">
                                            <img src="../assets/PipesAndTubes/pipe/cru_p.png" alt="" class="res">
                                        </div>
                                        <h3> Corrugated Sheets  </h3>
                                    </div>
                                </a>
                            </div>
                </div>
            </div>
            <!-- <div class="col-md-12">
                    <div class="ViewAll text-center mt-4">
                        <button class="v_all"><i class="fa fa-plus"></i> ViewAll</button>
                    </div>
                  </div> -->
        </div>
    </section>

    <!-- service -->
 
    <!-- expites -->
       <section class="p60 ex_pites">
        <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle">
                        <h2>Application</h2>
                        <span class="headerLine"></span>
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <ul class="applaction">
                        <li>
                        <div class="expites pos">
                        <div class="app_img  pos">
                        <img src="../assets/PipesAndTubes/application/agricultureIrrigationIndustry.png" alt="">
                        <h3 class="exp_h">Agriculture & Irrigation Industry</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Agriculture & Irrigation Industry</h3></a>
                        </div>
                         </div>
                        </li>
                        <li>
                        <div class="expites pos">
                        <div class="app_img  pos">
                        <img src="../assets/PipesAndTubes/application/constructionActivities.png" alt="">
                        <h3 class="exp_h">Construction Activities</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Construction Activities</h3></a>
                        </div>
                         </div>
                        </li>
                        <li>
                        <div class="expites pos">
                        <div class="app_img  pos">
                        <img src="../assets/PipesAndTubes/application/scaffolding.png" alt="">
                        <h3 class="exp_h">Scaffolding</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Scaffolding</h3></a>
                        </div>
                         </div>
                        </li>
                        <li>
                        <div class="expites pos">
                        <div class="app_img  pos">
                        <img src="../assets/PipesAndTubes/application/CasinginBoreWells.jpg" alt="">
                        <h3 class="exp_h">Casing in Bore Wells</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Casing in Bore Wells</h3></a>
                        </div>
                         </div>
                        </li>
                        <li>
                        <div class="expites pos">
                        <div class="app_img  pos">
                        <img src="../assets/PipesAndTubes/application/Fencing.jpg" alt="">
                        <h3 class="exp_h">Fencing</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Fencing</h3></a>
                        </div>
                         </div>
                        </li>
                        <!-- end -->
                        <li>
                        <div class="expites pos">
                        <div class="app_img  pos">
                        <img src="../assets/PipesAndTubes/application/StructuralApplication.jpg" alt="">
                        <h3 class="exp_h">Structural Application</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Structural Application</h3></a>
                        </div>
                         </div>
                        </li>
                        <!-- end -->
                        <li>
                        <div class="expites pos">
                        <div class="app_img  pos">
                        <img src="../assets/PipesAndTubes/application/WaterandGasTransmission.jpg" alt="">
                        <h3 class="exp_h">Water and Gas Transmission</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Water and Gas Transmission</h3></a>
                        </div>
                         </div>
                        </li>
                        <!-- end -->
                        <li>
                        <div class="expites pos">
                        <div class="app_img  pos">
                        <img src="../assets/PipesAndTubes/application/SprinklerHydrantsystems.jpg" alt="">
                        <h3 class="exp_h">Sprinkler and Hydrant systems</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Sprinkler and Hydrant systems</h3></a>
                        </div>
                         </div>
                        </li>
                        <!-- end -->
                        <li>
                        <div class="expites pos">
                        <div class="app_img  pos">
                        <img src="../assets/PipesAndTubes/application/Boilersms.jpg" alt="">
                        <h3 class="exp_h">Boilers</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Boilers</h3></a>
                        </div>
                         </div>
                        </li>
                        <!-- end -->
                        <li>
                        <div class="expites pos">
                        <div class="app_img  pos">
                        <img src="../assets/PipesAndTubes/application/HVAC.jpg" alt="">
                        <h3 class="exp_h">HVAC</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">HVAC</h3></a>
                        </div>
                         </div>
                        </li>
                        <!-- end -->
                        <li>
                        <div class="expites pos">
                        <div class="app_img  pos">
                        <img src="../assets/PipesAndTubes/application/AutomobileChassisFraming.jpg" alt="">
                        <h3 class="exp_h">Automobile Chassis and Framing</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Automobile Chassis and Framing</h3></a>
                        </div>
                         </div>
                        </li>
                        
                        <!-- end -->
                        <li>
                        <div class="expites pos">
                        <div class="app_img  pos">
                        <img src="../assets/PipesAndTubes/application/SolarMountingStructures.jpg" alt="">
                        <h3 class="exp_h">Solar Mounting and Structures</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Solar Mounting and Structures</h3></a>
                        </div>
                         </div>
                        </li>
                        <!-- end -->
                        <li>
                        <div class="expites pos">
                        <div class="app_img  pos">
                        <img src="../assets/PipesAndTubes/application/ElectricalTransmissions.jpg" alt="">
                        <h3 class="exp_h">Electrical Transmissions ( conduits)</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Electrical Transmissions ( conduits)</h3></a>
                        </div>
                         </div>
                        </li>
                        <!-- end -->
                        
                        <li>
                        <div class="expites pos">
                        <div class="app_img  pos">
                        <img src="../assets/PipesAndTubes/application/DoorWindowFrames.jpg" alt="">
                        <h3 class="exp_h">Door & Window Frames</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Door & Window Frames</h3></a>
                        </div>
                         </div>
                        </li>
                        <!-- end -->
                        
                        <li>
                        <div class="expites pos">
                        <div class="app_img  pos">
                        <img src="../assets/PipesAndTubes/application/SugarIndustries.jpg" alt="">
                        <h3 class="exp_h">Sugar Industries</h3>
                        </div>
                        <div class="exp_contant">
                           <a href="#"> <h3 class="exp_h">Sugar Industries</h3></a>
                        </div>
                         </div>
                        </li>
                        <!-- end -->

                    </ul>
                </div>
            </div>
        </div>

       </section>
    <!-- expites -->
  
   

    <!-- brand__area start -->
    <section class="brand__area1 p60" style="    background-color: #fdfdfd;">
        <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle">
                        <h2>Customers/Approvals </h2>
                        <span class="headerLine"></span>
                      
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="brand__slider p-0 swiper-container">
                        <div class="swiper-wrapper">
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/1.png" alt="1"></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/2.png" alt="2"></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/3.png" alt="3"></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/4.png" alt="4"></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/5.png" alt="5"></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/6.png" alt="6"></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/7.png" alt="7"></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/8.png" alt="8"></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/9.png" alt="9"></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/10.png" alt="10"></a>
                            </div>
                           
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/11.png" alt="12"></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/12.png" alt="13"></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/13.png" alt="14"></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/14.png" alt="15"></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/15.png" alt="16"></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/16.png" alt="17"></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/17.png" alt="18"></a>
                            </div>
                           
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </section>
    <!-- brand__area end -->
   
</main>



<?php include './inc/footer.php';?>
