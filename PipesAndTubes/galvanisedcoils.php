
<!doctype html>
         <html class="no-js" lang="zxx">
            
         <head>
               <meta charset="utf-8">
               <meta http-equiv="x-ua-compatible" content="ie=edge">
               <title>  Goodluck India Limited (Pipes And Tubes) :: Galvanized Pipe  </title>
               <meta name="description" content="">
<?php include './inc/header.php';?>
<div class="contant">
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                 
                <div class="carousel-item active">
                    <img src="../assets/PipesAndTubes/pipe/glv_coils.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Defined allocation for each Product Category ensures 100% delivery performance </p>
                    </div>
                </div>
                
            </div>
            
        </div>
    </section>
    <!-- hero -->
 
<section class="coials bg-light pt-2 pb-2 ">
<div class="container">
<div class="row align-items-center ">
<div class="col-md-6">
                <div class="gj_coial wow fadeInUp mb-2" data-wow-delay="1s"
                                style="visibility: visible; animation-delay: 1s;">
                                <h2>GALVANISED COILS</h2>
                    <h4>Galvanized Coils - Soft Grade</h4>
                    <p><b>STEEL GRADE</b></p>
                    <p><b>YST120 - YST 550 Mpa</b></p>
                    <h3>Coils and Sheets</h3>
                    <div class="gl_detail">
                    <div class="gl_cd w50">  
                        
                    <ul>
                        <li><span>Thickness</span> <span> : 1.00 mm to 3.50 mm</span></li>
                        <li><span>Width</span> <span> : 351 mm to 650 mm </span></li>
                        <li><span>Outer diameter </span> <span>: 1500 mm maximum</span></li>
                        <li><span>Packet Weight (Sheets) </span> <span>: 3 MT Max</span></li>
                        <li><span>Cut to Length</span> <span> : 4800 mm (Max)</span></li>
                    </ul>
                        </div>
                    <div class="gl_cd w50"> 
                    <ul>
                        <li><span>Coating</span> <span>: 80 gsm – 300 gsm (both sides)</span></li>
                        <li><span>Coil inner diameter </span> <span>: 508 mm/610 mm</span></li>
                        <li><span>Coil Weight</span> <span>: 5 - 6 MT maximum</span></li>
                        <li><span>Coil Weight</span> <span>: Bright Regular Spangles</span></li>
                      
                    </ul>   </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="gj_img wow fadeInUp mb-2" data-wow-delay="1s"
                                style="visibility: visible; animation-delay: 1s;" >
                  <img src="../assets/PipesAndTubes/pipe/gl_coial.png" alt="" class="w-100 r_5">
                </div>
            </div>
    </div>
</div>
 </section>


 <section class="coials  pt-5 pb-5">
<div class="container">
<div class="row  ">
        <div class="col-md-6">
                <div class="gj_img text-center wow fadeInUp mb-2" data-wow-delay="1s"
                                style="visibility: visible; animation-delay: 1s;">
                  <img src="../assets/PipesAndTubes/banner/g2.png" alt="" class=" r_5">
                </div>
            </div>
        <div class="col-md-6">
                <div class="gj_coial wow fadeInUp mb-2" data-wow-delay="1s"
                                style="visibility: visible; animation-delay: 1s;">
                <h3 class="mt-4">For Coils </h3>
                    <p class="text-dark">Each Coil is wrapped with VCI paper,
covered with Galvanized sheets and
edge protected. This packing is secure
through three metal straps on the
circumference and 4 – 5 straps through
the eye of the Coil.</p>
<p>Each Coil is tested after coating.</p>
                 <h5>Applications</h5>
                    <p><b>Pipes and Tubes, Furlins, Ducting etc.</b></p>
                    
                     
            </div>
            
        </div>
    </div>
</div>
 </section>
 <!-- end-sheets -->
 <section class="coials   sheets pb-5" style="background-color: #ecffeb;">
<div class="container">
<div class="row align-items-center ">
        
        <div class="col-md-6">
                <div class="gj_coial wow fadeInUp mb-2" data-wow-delay="1s"
                                style="visibility: visible; animation-delay: 1s;">
                <h3 class="mt-4">For Sheets </h3>
                    <p class="text-dark">
                    Sheets are wrapped in VCI paper/air
bubble polythene and covered with GP/
CR sheets by metal straps, edge
protected and fastened on wooden skids
                    </p>
<p><b>We can also do GI as per ASTM A123 and
can also supply as per Spec ASTM A653</b></p>
                  
                    <p>Packing as per customer's requirement can be provided</p>
            </div>
        </div>
        <div class="col-md-6">
                <div class="gj_img text-center wow fadeInUp mb-2" data-wow-delay="1s"
                                style="visibility: visible; animation-delay: 1s;">
                  <img src="../assets/PipesAndTubes/banner/g3.png" alt="" class=" r_5">
                </div>
            </div>
    </div>
</div>
 </section>
 <!-- end-sheets -->



</div>
<?php include './inc/footer.php';?>