
<!doctype html>
         <html class="no-js" lang="zxx">
            
         <head>
               <meta charset="utf-8">
               <meta http-equiv="x-ua-compatible" content="ie=edge">
               <title>  Goodluck India Limited (Pipes And Tubes) :: Pre Galvanized   </title>
               <meta name="description" content="">
<?php include './inc/header.php';?>
<div class="contant">
  

    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                <div class="carousel-item  active">
                    <img src="../assets/PipesAndTubes/pipe/gv_b.png" class="d-block w-100" alt="...">
			 
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Stringent and uncompromising Quality Control from Raw Material stage to Finished Goods</p>
                    </div>
                </div>
                
            </div>
          
        </div>
    </section>

    <!-- hero -->

<section class="ptb  ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2> PRE GALVANIZED PIPES</h2>
                <p>The Pre Galvanized Pipes made using qualitative steel are pre coated from zinc that further adds to the strength of metal and resist corrosion. These are available in several lengths, sizes and thickness as per the requirements of our customers. The pipe is made from finest metal, making it corrosion resistant and robust in design. We give due emphasis on providing dimensionally uniform pipes, which are manufactured with precision and accuracy. We go through several quality checks before making our pre galvanized pipes available in the market.</p>
          
                <div class="c_enterd1">
                <h4><b>AVAILABLE SHAPES :</b> Round, Rectangular & Square</h4>
            
                </div>
            </div>
           
           
        </div>

        <div class="row">
            <div class="col-md-6">
            <div class="pro_img text-center">
                <img src="../assets/PipesAndTubes/detail/preglv.png" class=" r_5  " alt="...">
                </div>
            </div>
            <div class="col-md-6">
                <div class="pro_img text-center">
                <img src="../assets/PipesAndTubes/pipe/pre_g.png" class=" r_5  " alt="...">
                </div>
            </div>
           
        </div>
    </div>
</section>
 


</div>
<?php include './inc/footer.php';?>