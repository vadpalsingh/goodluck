<!doctype html>
         <html class="no-js" lang="zxx">
            
         <head>
               <meta charset="utf-8">
               <meta http-equiv="x-ua-compatible" content="ie=edge">
               <title>  Goodluck India Limited (Pipes And Tubes) :: Square  </title>
               <meta name="description" content="">

<?php include './inc/header.php';?>
<div class="contant">
 

    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                <div class="carousel-item  active">
                    <img src="../assets/PipesAndTubes/pipe/square.png" class="d-block w-100" alt="...">
			 
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Defined allocation for each Product Category ensures 100% delivery performance</p>
                    </div>
                </div>
                
            </div>
          
        </div>
    </section>

    <!-- hero -->

<section class="ptb  ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>SQUARE SECTION PIPES </h2>
                <p>The Square Sections enhance the reliability of the structure. These are manufactured using graded steel in compliance with set international standards. The square sections are used as support in automotive industry, transmission tower plants, machinery industry, construction industry and many others. These sections are made using high grade steel metal in compliance with international standards. The square section holds superb tensile strength and is rust resistant with ability to offer long working life. The sections are tested on various parameters, which are tensile, bending, flattening and other strengths. </p>
                <div class="forgung ">
            <div class="c_enterd1">
        
          
          <h4 class="">Applications:-</h4>
          <ul class=" pro_duct">
            <li class="text-dark">Construction industry</li>
            <li class="text-dark">Machinery manufacturing industry</li>
            <li class="text-dark">Storage systems</li>
            <li class="text-dark">Transmission towers</li>
            <li class="text-dark">Automotive industry</li>
          </ul>
 
            </div>
        </div>
           
            </div>
           
            
        </div>
        <div class="row">
        <div class="col-md-6">
                <div class="pro_img text-center">
                <img src="../assets/PipesAndTubes/detail/square.png" class="  r_5 wow fadeInRight mb-2" data-wow-delay="1s"
                                style="visibility: visible; animation-delay: 2s;" alt="...">
                </div>
            </div>
            <div class="col-md-6">
                <div class="pro_img text-center">
                <img src="../assets/PipesAndTubes/pipe/sq_p.png" class="  r_5 wow fadeInRight mb-2" data-wow-delay="1s"
                                style="visibility: visible; animation-delay: 2s;" alt="...">
                </div>
            </div>
        </div>
    </div>
</section>
 


</div>
<?php include './inc/footer.php';?>