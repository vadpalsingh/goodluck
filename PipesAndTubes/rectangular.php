
<!doctype html>
         <html class="no-js" lang="zxx">
            
         <head>
               <meta charset="utf-8">
               <meta http-equiv="x-ua-compatible" content="ie=edge">
               <title>  Goodluck India Limited (Pipes And Tubes) :: Rectangular  </title>
               <meta name="description" content="">
<?php include './inc/header.php';?>
<div class="contant">
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                 
                <div class="carousel-item active">
                    <img src="../assets/PipesAndTubes/pipe/ractangular.png" class="d-block w-100" alt="...">
                    <!-- <div class="carousel-caption d-none d-md-block c2 bg_tr" data-wow-delay="1s"
                                style="visibility: visible; animation-delay: 1s;">
                           
                        
                        <p>Stringent and uncompromising Quality Control from Raw Material stage to Finished Goods</p>
               
                    </div> -->
                </div>
                 
            </div>
            
        </div>
    </section>
    <!-- hero -->

<section class="ptb  ">
    <div class="container">
        <div class="row">
            <div class="col-md-6   wow fadeInUp mb-2" data-wow-delay="0.8s"
                                style="visibility: visible; animation-delay: 1s;">
                <h2> RECTANGULAR  PIPES </h2>
                <p>The Rectangular Sections are constructed using finest steel. These are preferred for their robustness. The rectangular section has precise dimensions and accuracy, which is one of the trademarks of our high grade manufacturing abilities. It is tested on several international standards, where tensile strength, flattening, bending and drift expansion. It is preferred in several industries such as furniture industry, construction industry, automotive industry and many more.</p>
                <div class="forgung ">
            <div class="c_enterd1">
            <span class="s_csr text-dark">RECTANGULAR SECTION PIPES</span>
            
          <ul class=" pro_duct">
            <li class="text-dark"><b>Section :-</b> 30 X 12 to 200 X 150 mm</li>
            <li class="text-dark"><b>WT :-</b> 1.20 - 5.00 mm</li>
            <li class="text-dark"><b>Length :-</b> 8.50 MTR</li>
          
          </ul>
          <h4 class="mt-2">Applications:-</h4>
          <ul class="pro_duct">
            <li class="text-dark">Construction industry</li>
            <li class="text-dark">Machinery manufacturing industry</li>
            <li class="text-dark">Storage systems</li>
            <li class="text-dark">Transmission towers</li>
            <li class="text-dark">Automotive industry</li>
          </ul>
 
            </div>
        </div>
            </div>
            <div class="col-md-6">
                <div class="pro_img text-center  wow fadeInRight mb-2" data-wow-delay="1s"
                                style="visibility: visible; animation-delay: 1s;">
                <img src="../assets/PipesAndTubes/pipe/ractangula_p.png" class=" r_5  " alt="...">
                </div>
            </div>
           
        </div>
    </div>
</section>
 


</div>
<?php include './inc/footer.php';?>