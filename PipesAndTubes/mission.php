
<!doctype html>
         <html class="no-js" lang="zxx">
            
         <head>
               <meta charset="utf-8">
               <meta http-equiv="x-ua-compatible" content="ie=edge">
               <title>  Goodluck India Limited (Pipes And Tubes) :: Overview  </title>
               <meta name="description" content="">
<?php include './inc/header.php';?>
<div class="contant">
    <!-- <section class="brand__area1  title1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle1 mt-0 mb-3">
                        <h2>Over-View</h2>
                        <span class="headerLine"></span>

                    </div>
                </div>
            </div>
        </div>

    </section> -->

    <style>
   
    </style>
        <!-- hero -->

<section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
            <div class="carousel-item  active ">
                    <img src="../assets/PipesAndTubes/banner/mision.png" class="d-block w-100" alt="...">
					
                    <!-- <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Offers the most exclusive range both in terms of capacity and capability</p>
                    </div> -->
                </div>
            </div>
          
        </div>
    </section>
    <!-- hero -->


    <section class="ptb bg_l   " style="padding-top:0px!important">
 
 
 


<div class="container">
    <div class="orw">
        <div class="col-md-12">
   
<div class="tab-content" id="myTabContent">
 
    <div class="tab-pane fade show active" id="Vision" role="tabpanel" aria-labelledby="contact-tab">
    <div id=" " class="mt8_0 vms mt-5 pt-5 pb-5" style="">
         <div class="container">
            <div class="row mb-5" style="padding-bottom: 3%;">
               <div class="col-md-6  ">
                  <h3 class="about_heading">OUR MISSION</h3>
                  <ul class="mission">
                        <li>We are an innovative, multi-national industrial group in the global market of seamless stainless steel tubes.</li>

                       <li> We seek customer satisfaction through a portfolio of products and services that are constantly being developed.</li>

                       <li> We are a reliable company that fulfills its obligations with internal and external clients.</li>

                       <li> We grow in a profitable and sustainable way.</li>

                       <li> We take it upon ourselves to effectively manage the return on all of our investments and to reward our shareholders.</li>

                       <li> We seek excellence, through rigorous process management and the systematic application of ongoing improvement.</li>

                       <li> We undertake to constantly foster a safe and pleasant workplace, whilst respecting the environment.</li>

                       <li> We seek to contribute to the development of society and our suppliers, developing as professionals and people, working as a team and constantly measuring our results.</li>
                  </ul>
                   
               </div>
               <div class="col-md-6">
                  <div class="about_img">
                      <img src="https://taion.in/template/photo/assets/img/mission.jpg" alt="vision" style=" width: 100%;    border-radius: 5px;">
                  </div>
               </div>
            </div>
              <div class="row mt-5 align-items-center">
               <div class="col-md-6  ">
                 <img src="https://taion.in/template/photo/assets/img/vision.jpg" alt="mission" style=" width: 100%;    border-radius: 5px;">
               </div>
               <div class="col-md-6">
                  <div class="about_img">
                     <h3 class="about_heading">OUR  VISION </h3>
                  
                  <p class="  "> We aspire to be a global supplier and benchmark in innovative tubular solutions in advanced materials, offering service and management excellence. While fulfilling and exceeding customer expectations, our goal is to remain profitably sustainable and focused on the personal development of our people. </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div> 
    <!-- end -->
</div>
        </div>
    </div>
</div>

</section></div>
<?php include './inc/footer.php';?>