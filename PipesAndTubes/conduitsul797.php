
<!doctype html>
         <html class="no-js" lang="zxx">
            
         <head>
               <meta charset="utf-8">
               <meta http-equiv="x-ua-compatible" content="ie=edge">
               <title>  Goodluck India Limited (Pipes And Tubes) :: conduitus797  </title>
               <meta name="description" content="">
<?php include './inc/header.php';?>
<div class="contant">
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                <div class="carousel-item  active">
                    <img src="../assets/PipesAndTubes/pipe/emt.png" class="d-block w-100" alt="...">
			 
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Stringent and uncompromising Quality Control from Raw Material stage to Finished Goods</p>
                    </div>
                </div>
                
            </div>
          
        </div>
    </section>
    <!-- hero -->

<section class="ptb  ">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>Electrical conduits</h2>
                <h4>UL 6 - Galvanized Rigid Metal Conduits</h4>
                <p class=" emt">
                Goodluck EMT is a steel raceway of circular hollow sections
which is non-threaded i.e. plain ends and is produced by high
quality mild steel hot rolled coils. The exterior and interior of
the EMT is protected by superior quality zinc coating done
by in-line hot dipped galvanizing process. Additionally the
exterior surface of EMT is coated by clear anti-corrosion
coating or a zinc chromate coating for higher protection
against corrosion giving the EMT long life and good smooth
finish. The internal surface of the EMT is further coated with
an organic zinc lubricants or higher grade organic epoxy
resin by a state of art patented on – line automatic
machine. This coating gives EMT a smooth internal surface
which makes wire pushing and pulling easy and also
protects against the corrosion.
 
                </p>
            </div>
            <div class="col-md-6">
                <div class="pro_img text-center">
                <img src="../assets/PipesAndTubes/pipe/emt_979.png" class=" r_5  " alt="...">
                </div>
            </div>
            <div class="col-md-12">
            <div class="forgung ">

            <div class="c_enterd1">
              
          
          
           <table class="table table-bordered">
  <thead class="table-dark">
    <tr>
    
      <th scope="col">Trade Size</th>
      <th scope="col">Metric  </th>
      <th scope="col">Outside Diameter*</th>
      <th scope="col">Nominal Wall Thickness**</th>
      <th scope="col">Approx Weight/100 ft (30.5m)</th>
      <th scope="col">Inner Budle Qty</th>
      <th scope="col">Master Budle Qty</th>
      
    </tr>
  </thead>
  <tbody>
    <tr>
      <th></th>
      <td></td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">In </td>
                <td style="width: 50%;">Mm</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;"> In </td>
                <td  style="width: 50%;">Mm</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">lb </td>
                <td style="width: 50%;">Kg</td>
            </tr>
        </table></td>
      <td>Pcs</td>
      <td>Pcs</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>1/2"</th>
      <td>16</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">0.706 </td>
                <td style="width: 50%;">17.93</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">0.042 </td>
                <td style="width: 50%;">1.07</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">30 </td>
                <td style="width: 50%;">13.6</td>
            </tr>
        </table></td>
      <td>10.0</td>
      <td>700</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>3/4”</th>
      <td>21</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">0.922 </td>
                <td style="width: 50%;">23.42</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">0.049 </td>
                <td style="width: 50%;">1.24</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">46 </td>
                <td style="width: 50%;">20.9</td>
            </tr>
        </table></td>
      <td>10.0</td>
      <td>500</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>1</th>
      <td>27</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">1.163 </td>
                <td style="width: 50%;">29.54</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">0.057 </td>
                <td style="width: 50%;">1.45</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">67 </td>
                <td style="width: 50%;">30.4</td>
            </tr>
        </table></td>
      <td>10.0</td>
      <td>300</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>1 1/4”</th>
      <td>35</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">1.51 </td>
                <td style="width: 50%;">38.35</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">0.065 </td>
                <td style="width: 50%;">1.65</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">101 </td>
                <td style="width: 50%;">45.8</td>
            </tr>
        </table></td>
      <td>5.0</td>
      <td>200</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>1 1/2”</th>
      <td>41</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">1.74 </td>
                <td style="width: 50%;">44.2</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">0.065 </td>
                <td style="width: 50%;">1.65</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">116 </td>
                <td style="width: 50%;">52.6</td>
            </tr>
        </table></td>
      <td>5.0</td>
      <td>150</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>2"</th>
      <td>53</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">2.197 </td>
                <td style="width: 50%;">55.8</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">0.065 </td>
                <td style="width: 50%;">1.65</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">148 </td>
                <td style="width: 50%;">67.1</td>
            </tr>
        </table></td>
      <td></td>
      <td>120</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>2 1/2”</th>
      <td>63</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">2.875 </td>
                <td style="width: 50%;">73.03</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">  0.072</td>
                <td style="width: 50%;"> 1.83</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;"> 216 </td>
                <td style="width: 50%;"> 1.83</td>
            </tr>
        </table></td>
      <td> </td>
      <td>61</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>3</th>
      <td>78</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">3.5 </td>
                <td style="width: 50%;">88.9</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">0.072 </td>
                <td style="width: 50%;">1.83</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">263 </td>
                <td style="width: 50%;">119.3</td>
            </tr>
        </table></td>
      <td></td>
      <td>37</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>4</th>
      <td>103</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">4.5 </td>
                <td style="width: 50%;">114.3</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">0.083 </td>
                <td style="width: 50%;">2.11</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">393 </td>
                <td style="width: 50%;">178.3</td>
            </tr>
        </table></td>
      <td></td>
      <td>30</td>
      
    </tr>
    <!-- end -->
   
  </tbody>
</table>


<div class="application mt-4">
    <div class="tollrance w50">
        <h5>Applicable Tolerances -</h5>
    <p class="text-dark">Length: 10 Feet (3.05 Meters) +/- 0.25 inch (6.35 mm)
Outside Diameter:</p>
<p class="toll_divde text-dark"><b> TRADE SIZE ½” TO 2” (16 to 53)</b> <b>: ± 0.005” (±0.13 mm)</b></p>
<p class="toll_divde text-dark"><b> TRADE SIZE 2 ½” (63) </b> <b>: ±  0.010” (0.25 mm)</b></p>
<p class="toll_divde text-dark"><b> TRADE SIZE 3” (78)</b> <b>: ±  0.050” (0.38 mm)</b></p>
<p class="toll_divde text-dark"><b> TRADE SIZE ½” TO 2” (16 to 53)</b> <b>: ± 0.005” (±0.13 mm)</b></p>
<p class="toll_divde text-dark"><b>TRADE SIZE 3½” TO 4” (91 to 103 ): ± 0.020” (±0.51 mm)</b></p>
    </div>
    <div class="tollrance w50">
        <h5>Features of Goodluck Rigid Conduits –</h5>
        <ul>
            <li>1. Higher Corrosion resistance with superior and uniform galvanizing quality.</li>
            <li>2. Accurate dimensions and uniform wall thickness.</li>
            <li>3. High quality internal coating with high grade organic epoxy resins/zinc</li>
            <li>4. Clean, smooth and defect-free interior surface.</li>
            <li>5. Easy and fast installation.</li>
           <li> 6. Easy wire pushing and pulling.</li>
            <li>7. Easy & good fabrication properties like smooth cutting, bending & welding.</li>
           <li> 8. Uniform and consistent quality.</li>
           <li> 9. Meets the Underwriters Laboratories specifications UL797.</li>
           <li> 10. Tested and certified for Safety.</li>
          

        </ul>
        </div>
</div>
            </div>
        </div>
            </div>
        </div>
    </div>
</section>
 


</div>
<?php include './inc/footer.php';?>