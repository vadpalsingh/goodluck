
<!doctype html>
         <html class="no-js" lang="zxx">
            
         <head>
               <meta charset="utf-8">
               <meta http-equiv="x-ua-compatible" content="ie=edge">
               <title>  Goodluck India Limited (Pipes And Tubes) :: Capability  </title>
               <meta name="description" content="">
<?php include './inc/header.php';?>
<div class="contant">
    <!-- <section class="brand__area1  title1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle1 mt-0 mb-3">
                        <h2>Over-View</h2>
                        <span class="headerLine"></span>

                    </div>
                </div>
            </div>
        </div>

    </section> -->

    <!-- hero -->

<section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
            <div class="carousel-item  active ">
                    <img src="../assets/PipesAndTubes/banner/capiblity.png" class="d-block w-100" alt="...">
					
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Offers the most exclusive range both in terms of capacity and capability</p>
                    </div>
                </div>
            </div>
          
        </div>
    </section>
    <!-- hero -->


    <section class="ptb     " style="padding-top:0px!important">
 
    <div class="innerpagenav pb-4">
<ul class="nav nav-tabs units mb-3 pagenav" id="myTab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#Black" type="button" role="tab" aria-controls="home" aria-selected="true">Black Round Pipe</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#roundpipes" type="button" role="tab" aria-controls="profile" aria-selected="false">Galvanized round Pipe</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#Square" type="button" role="tab" aria-controls="contact" aria-selected="false">Square and Rectangular Black/Galvanized Pipe</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#Coils" type="button" role="tab" aria-controls="contact" aria-selected="false">CR Coils</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#GalvanizedCoils" type="button" role="tab" aria-controls="contact" aria-selected="false">Galvanized Coils</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#Corrugated" type="button" role="tab" aria-controls="contact" aria-selected="false">Corrugated Sheets </a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#Specification" type="button" role="tab" aria-controls="contact" aria-selected="false">Manufacturing Specification </a>
  </li>

</ul>
</div>

<div class="container">
    <div class="orw">
        <div class="col-md-12">
   
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="Black" role="tabpanel" aria-labelledby="home-tab">

  <div class="container">
            <div class="row align-items-center">

                <div class="col-md-12">


                    <div class="forgung ">
                        
                            <span class="s_csr text-dark mb-2">Black Round Pipes </span>
                            <p class="text-dark justify"> The Black Round Pipes is Electric Resistance Welded round tube. Our pipes are developed under the stringent supervision of experienced engineer’s team, who tests these on several quality parameters. The black round pipes are available in several finish with wide size range and length options.  <br>These tubes find their application in water, gas & sewage pipes, structural purposes, idlers / conveyors and water wells (casing pipes). Apart from these, our tubes are also extensively used in electric poles, automotive purposes, scaffolds and air, steam, oil & gas transmission.

</p>
                            <div class="indes mt-3">
                                <h4>Features:-</h4>
                                <ul class="cp_app ">
                                    <li>Heat resistant</li>
                                    <li>Corrosion resistant</li>
                                    <li>Mostly preferred in supply of gas and oil</li>
                                    <li>Easy to install</li>
                                    <li>Meets all the Indian standards</li>
                                
                                </ul>
                            </div>
                    </div>
                </div>
               
            </div>

<div class="row align-items-center ">
<div class="col-md-6">
                    <div class="himage  borde_r">
                        <img src="../assets/PipesAndTubes/detail/black.png" alt="" class="w100 r_5">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="himage  borde_r">
                        <img src="../assets/PipesAndTubes/pipe/black.png" alt="" class="w100 r_5" style="    height: 462px;">
                    </div>
                </div>
</div>

        </div>
  </div>
  <!-- end -->
  <div class="tab-pane fade" id="roundpipes" role="tabpanel" aria-labelledby="profile-tab">

  <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="forgung ">
                            <span class="s_csr text-dark mb-2">Galvanized Round Pipe </span>
                            <p class="text-dark justify"> Galvanized steel pipe has been coated with a layer of zinc .Galvanized pipe maintains a wide range of beneficial qualities that allow to be used in highly corrosive environments. The zinc provides a barrier against corrosion so that the pipe may be exposed to the outdoor environmental elements. <br>The protective barrier proves equally effective against damage from indoor humidity. It is most commonly used for outdoor construction like fences and handrails, or for some interior plumbing.</p>
                            <div class="indes mt-3">
                                <h4>Features</h4>
                                <ul class="cp_app ">
                                    <li>Good weld-ability</li>
                                    <li>Supreme resistance to torsion</li>
                                    <li>Cost beneficial</li>
                                    <li>High impact strength</li>
                                    <li>Corrosion resistant</li>
                                    <li>Supreme tensile strength</li>
                                    <li>Dimensional accuracy</li>
                                    <li>Easy to transport</li>
                                </ul>
                            </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-between">
            <div class="col-md-6">
                    <div class="himage  borde_r">
                        <img src="../assets/PipesAndTubes/detail/erw.png" alt="" class="w100 r_5">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="himage  borde_r">
                        <img src="../assets/PipesAndTubes/capiblity/Galvanizedroundpipes.png" alt="" class="w100 r_5" style="    height: 462px;">
                    </div>
                </div>
            </div>
        </div>
  </div>
  <!-- end -->
  <div class="tab-pane fade" id="Square" role="tabpanel" aria-labelledby="contact-tab">
    
  <div class="container">
            <div class="row align-items-center">

                <div class="col-md-12">

                    <div class="forgung ">
                        
                            <span class="s_csr text-dark mb-2 justify">Square and Rectangular Black/Galvanized Section </span>
                            <p class="text-dark justify">The Square & Rectangular Sections are constructed using finest steel. These are preferred for their robustness. The Square & rectangular section has precise dimensions and accuracy, which is one of the trademarks of our high grade manufacturing abilities. It is tested on several international standards, where tensile strength, flattening, bending and drift expansion. It is preferred in several industries such as furniture industry, construction industry, automotive industry and many more.</p>
                            
                            <div class="indes mt-3">
                                <h4>Features</h4>
                                <ul class="cp_app ">
                                    <li>Good weld-ability</li>
                                    <li>Supreme resistance to torsion</li>
                                    <li>Cost beneficial</li>
                                    <li>High impact strength</li>
                                    <li>Corrosion resistant</li>
                                    <li>Supreme tensile strength</li>
                                    <li>Dimensional accuracy</li>
                                    <li>Easy to transport</li>
                                </ul>
                            </div>
                    </div>
                </div>
                
            </div>
            <div class="row align-items-center justify-content-between">
        <div class="col-md-5">
                <div class="pro_img text-center borde_r">
                <img src="../assets/PipesAndTubes/detail/square.png" class="r_5 wow fadeInRight w100  mb-2" data-wow-delay="1s" style="visibility: visible; animation-delay: 1s;" alt="...">
                </div>
            </div>
            <div class="col-md-5">
                <div class="pro_img text-center borde_r">
                <img src="../assets/PipesAndTubes/pipe/sq_p.png" class="r_5 wow fadeInRight w100  mb-2" data-wow-delay="1s" style="visibility: visible; animation-delay: 1s;" alt="...">
                </div>
            </div>
        </div>

        </div>
  </div> 
  <!-- end -->

  <div class="tab-pane fade" id="Coils" role="tabpanel" aria-labelledby="contact-tab">
    
    <div class="container">
              <div class="row align-items-center">
  
                  <div class="col-md-6">
  
  
                      <div class="forgung ">
                          
                              <span class="s_csr text-dark mb-2">CR Coils</span>
                              <div class="indes mt-3">
                                  <p class="text-dark justify">We are offering excellent CR (Cold Rolled) Coils that are made from finest steel metal and produced from hot rolled, before being pickled. The coils are obtained through cold rolling where the metal is melted below its re-crystallization temperature, which enhances their strength. It is then shrinked by a single stand cold roll steel mill directly or by reversing mill that consist of several single stands in a series. The coil is passed through pairs of rolls in order to reduce thickness and achieve uniformity.</p>
                                  <p class="text-dark justify">
                                  Our coils are demanded all over the country, chiefly in automobile components, consumer durables items, industrial goods and precision tubes. Moreover, we find its applications in chassis for vehicles, earth moving equipment, galvanized sheets, construction of railways coaches, drums & barrels, material handling equipment, tin plates, agricultural equipment, and many others. The cold rolled coils offered by us are made as per the IS 513, JIS 3132, EN 10051 and DIN 1614 quality standards.
                                  </p>
                                 
                              </div>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="himage  ">
                      <img src="../assets/PipesAndTubes/product/banner/g1.jpg" alt="" class="w100 r_5">
                      </div>
                  </div>
              </div>
          </div>
    </div> 
    <!-- end -->
    <div class="tab-pane fade" id="GalvanizedCoils" role="tabpanel" aria-labelledby="contact-tab">
    
    <div class="container">
              <div class="row align-items-center">
  
                  <div class="col-md-6">
  
  
                      <div class="forgung ">
                          
                              <span class="s_csr text-dark mb-2">Galvanized Coils   </span>
                <p class="text-dark justify justify">Manufactured using finest quality of steel, Square and Rectangular Pipes are extensively used in welded steel frames which experience loads from multiple directions. The shapes of pipes suit multiple axis loading with having uniform geometry along with two or more cross section axes. This enhances the uniform strength of these pipes, making them better choice for columns. These are manufactured through the process where flat steel plate is slowly changed in shape to achieve round where the edges are presented to weld. Then, the edges are welded together to form the master tube. This master tube, which is also referred as mother tube goes through a sequence of shaping stands, which form the final square or rectangular shape.</p>
                              <div class="indes mt-3">
                                 <!-- <h4>Features</h4> -->
                                  <!-- <ul class="indes">
                                      <li>Cost effective</li>
                                      <li>Longer life</li>
                                      <li>Cleared through all certifications & quality standards</li>
                                      <li>Made from high grade steel</li>
                                      <li>Highly robust</li>
                                      <li>Easy to install</li>
                                      <li>Corrosion resistant</li>
                                      <li>Uniformity in sheets thickness</li>
                                      <li>High impact strength</li>
                                  </ul> -->
                              </div>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="himage  ">
                          <img src="../assets/PipesAndTubes/pipe/gl_coial.png" alt="" class="w100 r_5">
                      </div>
                  </div>
            

              </div>
          </div>
    </div> 
    <!-- end -->

    <div class="tab-pane fade" id="Corrugated" role="tabpanel" aria-labelledby="contact-tab">
    
    <div class="container">
              <div class="row align-items-center">
  
                  <div class="col-md-12">
  
  
                      <div class="forgung ">
                          
                          <span class="s_csr text-dark mb-2">Corrugated Sheets </span>
                         <p class="text-dark justify">Manufactured using finest quality of steel, Square and Rectangular Pipes are extensively used in welded steel frames which experience loads from multiple directions. The shapes of pipes suit multiple axis loading with having uniform geometry along with two or more cross section axes. This enhances the uniform strength of these pipes, making them better choice for columns. These are manufactured through the process where flat steel plate is slowly changed in shape to achieve round where the edges are presented to weld. Then, the edges are welded together to form the master tube. This master tube, which is also referred as mother tube goes through a sequence of shaping stands, which form the final square or rectangular shape.</p>
                              
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="himage  borde_r">
                          <img src="../assets/PipesAndTubes/detail/corruated_c.png" alt="" class="w100 r_5">
                      </div>
                  </div>
                   <div class="col-md-6">
                   <div class="himage  borde_r">
                   <img src="../assets/PipesAndTubes/pipe/cru_p.png" alt="" class="w100 r_5" style="    height: 462px;">
                    </div>
                   </div> 

              </div>
          </div>
    </div> 
    <!-- end -->
    <div class="tab-pane fade" id="Specification" role="tabpanel" aria-labelledby="contact-tab">
    
    <div class="container">
              <div class="row align-items-center">
  
                  <div class="col-md-6">
  
  
                      <div class="forgung ">
                      <div class="himage  borde_r">
                          <img src="../assets/PipesAndTubes/pipe/spec.png" alt="" class="w100 r_5">
                      </div>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="himage borde_r ">
                          <img src="../assets/PipesAndTubes/pipe/spec1.png" alt="" class="w100 r_5">
                      </div>
                  </div>
                   <div class="col-md-12">

                   </div> 

              </div>
          </div>
    </div> 
    <!-- end -->


    
</div>
        </div>
    </div>
</div>

</section></div>
<?php include './inc/footer.php';?>