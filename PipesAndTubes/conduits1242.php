

<!doctype html>
         <html class="no-js" lang="zxx">
            
         <head>
               <meta charset="utf-8">
               <meta http-equiv="x-ua-compatible" content="ie=edge">
               <title>  Goodluck India Limited (Pipes And Tubes) :: Conduits1242  </title>
               <meta name="description" content="">

<?php include './inc/header.php';?>
<div class="contant">
<section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                
                <div class="carousel-item active">
                    <img src="../assets/PipesAndTubes/pipe/emt.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Defined allocation for each Product Category ensures 100% delivery performance </p>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
    <!-- hero -->

<section class="ptb  ">
    <div class="container">
        <div class="row">
            <div class="col-md-6 wow fadeInUp mb-2" data-wow-delay="1s"
                                style="visibility: visible; animation-delay: 1s;">
                <h2>Intermediate Metal Conduit UL1242 </h2>
                <h4>UL 1242 - Electrical Intermediate Metal Conduit</h4>
                <p class="text-dark">
                Goodluck IMC CONDUITS are produced of high quality steel
using an electric resistance welding process and hot dipped
galvanized both inside and outside.
Goodluck Intermediate metal cond uit (IMC) are
approximately one third lighter than rigid metal conduit
(RMC) so it keeps our customer's costs down and increase
productivity. IMC can be interchangeably used for rigid
conduits and have same threads and use same couplings
and fittings.
Goodluck IMC conduits have prestigious UL 1242
certifications for IMC.
                </p>
            </div>
            <div class="col-md-6">
                <div class="pro_img text-center wow fadeInUp mb-2 wow fadeInUp mb-2" data-wow-delay="1s"
                                style="visibility: visible; animation-delay: 1s;">
                <img src="../assets/PipesAndTubes/pipe/elect_p1.png" class=" r_5  " alt="...">
                </div>
            </div>
            <div class="col-md-12 wow fadeInUp mb-2 wow fadeInUp mb-2" data-wow-delay="1s"
                                style="visibility: visible; animation-delay: 0.7s;" >
            <div class="forgung ">

            <div class="c_enterd1">
              
          
          
           <table class="table table-bordered">
  <thead class="table-dark">
    <tr>
    
      <th scope="col">Trade Size</th>
      <th scope="col">Metric  Designator</th>
      <th scope="col">Outside Diameter*</th>
      <th scope="col">Nominal Wall Thickness**</th>
      <th scope="col">Approx Weight/100 ft (30.5m)</th>
      <th scope="col">Inner Budle Qty</th>
      <th scope="col">Master Budle Qty</th>
      
    </tr>
  </thead>
  <tbody>
    <tr>
      <th></th>
      <td></td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">In </td>
                <td style="width: 50%;">Mm</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;"> In </td>
                <td  style="width: 50%;">Mm</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">lb </td>
                <td style="width: 50%;">Kg</td>
            </tr>
        </table></td>
      <td>Pcs</td>
      <td>Pcs</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>1/2"</th>
      <td>16</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">0.815  </td>
                <td style="width: 50%;">20.7</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">1.97 </td>
                <td style="width: 50%;">62</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">28.1 </td>
                <td style="width: 50%;">10</td>
            </tr>
        </table></td>
      <td>350</td>
      <td>Yellow</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>3/4"</th>
      <td>21</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">1.029 </td>
                <td style="width: 50%;">26.13</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">2.10</td>
                <td style="width: 50%;">84</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">38.1 </td>
                <td style="width: 50%;">5</td>
            </tr>
        </table></td>
      <td>250</td>
      <td>Green</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>1</th>
      <td>27</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">1.29 </td>
                <td style="width: 50%;">32.76</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">2.35 </td>
                <td style="width: 50%;">119</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">54.00 </td>
                <td style="width: 50%;">5</td>
            </tr>
        </table></td>
      <td>170</td>
      <td>Orange</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>1 1/4”</th>
      <td>35</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">1.638</td>
                <td style="width: 50%;">41.6</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">2.41 </td>
                <td style="width: 50%;">158</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">71.7 </td>
                <td style="width: 50%;"> </td>
            </tr>
        </table></td>
      <td>135</td>
      <td>Green</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>1 1/2”</th>
      <td>41</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">1.883 </td>
                <td style="width: 50%;">47.82</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">2.54 </td>
                <td style="width: 50%;">194</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">88 </td>
                <td style="width: 50%;"> </td>
            </tr>
        </table></td>
      <td>110</td>
      <td>Yellow</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>2"</th>
      <td>53</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">2.36 </td>
                <td style="width: 50%;">59.94</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">2.67 </td>
                <td style="width: 50%;">256</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">116.1 </td>
                <td style="width: 50%;"> </td>
            </tr>
        </table></td>
      <td>80</td>
      <td>Orange</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>2 1/2”</th>
      <td>63</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">2.857 </td>
                <td style="width: 50%;">72.56</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;"> 3.81</td>
                <td style="width: 50%;">441</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;"> 200 </td>
                <td style="width: 50%;"></td>
            </tr>
        </table></td>
      <td>37 </td>
      <td>Yellow</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>3</th>
      <td>78</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">3.476 </td>
                <td style="width: 50%;">88.29</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">3.81 </td>
                <td style="width: 50%;">543</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">246.3 </td>
                <td style="width: 50%;"> </td>
            </tr>
        </table></td>
      <td>30</td>
      <td>Orange</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>3 1/2" </th>
      <td>91</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">3.971 </td>
                <td style="width: 50%;">100.86</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">3.81 </td>
                <td style="width: 50%;">629</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">285.3 </td>
                <td style="width: 50%;"> </td>
            </tr>
        </table></td>
      <td>24</td>
      <td>Yellow</td>
      
    </tr>
    <!-- end -->
    <tr>
      <th>4" </th>
      <td>103</td>
      <td  class="p-0">
        <table class="table  m-0 table-bordered text-center">
            <tr>
                <td style="width: 50%;">4.466 </td>
                <td style="width: 50%;">113.43</td>
            </tr>
        </table>
      </td>
      <td class="p-0">
      <table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">3.81 </td>
                <td style="width: 50%;">700</td>
            </tr>
        </table>
      </td>
      <td class="p-0"><table class="table m-0 table-bordered  text-center">
            <tr >
            <td style="width: 50%;">317.5 </td>
                <td style="width: 50%;"> </td>
            </tr>
        </table></td>
      <td>24</td>
      <td>Orange</td>
      
    </tr>
    <!-- end -->
   
  </tbody>
</table>


<div class="application mt-4">
    <div class="tollrance w50">
        <h5>Applicable Tolerances -</h5>
       <p class="text-dark"><b> *OUTSIDE DIAMETER TOLERANCES:</b></p>
       <p class="text-dark"> TRADE SIZES 1/2" TO 1" : +/- .005" (0.13MM)</p>
       <p class="text-dark"> TRADE SIZES 1-1/4" TO 2" : +/- .0075" (0.19MM)</p>
       <p class="text-dark"> TRADE SIZES 2-1/2" TO 4" : +/- 0.10" (0.25MM)</p>
       <p class="text-dark"> <b>**WALL THICKNESS TOLERANCES:</b></p>
       <p class="text-dark"> TRADE SIZES 1/2"TO 1" : +/- 0.19 MM</p>
       <p class="text-dark"> TRADE SIZES 1 1/4"TO 4" : +/- 0.25 MM</p>
       <p class="text-dark"><b> LENGTH TOLERANCE = 10 FT. (3.05M)</b></p>
       <p class="text-dark"> <b>WITH A TOLERANCE OF +/- .25 IN. (6.35MM)</b></p>
       <p class="text-dark">We can also produce IMC conduits in 20 feet (6 Meters)
length. 20 Feet conduit provided quick and easy
installation and also fewer fittings.</p>
    </div>
    <div class="tollrance w50">
        <h5>Features of Goodluck IMC Conduits  –</h5>
        <ul>
            <li>1. Cost effective as weighs less than Rigid</li>
            <li>2. Quick and easy installations.</li>
            <li>3. Meets the Underwriters Laboratories specifications Ul1242</li>
            <li>4. Tested and certified for Safety.</li>
        </ul>
        </div>
</div>
            </div>
        </div>
            </div>
        </div>
    </div>
</section>
 


</div>
<?php include './inc/footer.php';?>