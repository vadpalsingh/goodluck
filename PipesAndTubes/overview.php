
<!doctype html>
         <html class="no-js" lang="zxx">
            
         <head>
               <meta charset="utf-8">
               <meta http-equiv="x-ua-compatible" content="ie=edge">
               <title>  Goodluck India Limited (Pipes And Tubes) :: Overview  </title>
               <meta name="description" content="">
<?php include './inc/header.php';?>
<div class="contant">
    <!-- <section class="brand__area1  title1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle1 mt-0 mb-3">
                        <h2>Over-View</h2>
                        <span class="headerLine"></span>

                    </div>
                </div>
            </div>
        </div>

    </section> -->

    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
            <div class="carousel-item  active ">
                    <img src="../assets/PipesAndTubes/banner/banner2.png" class="d-block w-100" alt="...">
					
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Offers the most exclusive range both in terms of capacity and capability</p>
                    </div>
                </div>
            </div>
          
        </div>
    </section>
    <!-- hero -->



    <section class="ptb bg_l   " style="padding-top:0px!important">
    
 
<div class="innerpagenav pb-4">
<ul class="nav nav-tabs units   pagenav" id="myTab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#overview" type="button" role="tab" aria-controls="home" aria-selected="true">Overview</a>
  </li>
  
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#Approvals" type="button" role="tab" aria-controls="profile" aria-selected="false">Approvals and Clients</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#Approvals" type="button" role="tab" aria-controls="profile" aria-selected="false">Mission & Vision</a>
  </li>
  


</ul>
</div>


<div class="container">
    <div class="orw">
        <div class="col-md-12">
   
<div class="tab-content" id="myTabContent pt-5">
<div class="tab-pane fade  mt-4 show active" id="overview" role="tabpanel" aria-labelledby="home-tab">

<div class="container">
          <div class="row  ">

              <div class="col-md-6">
                  <div class="overview ">
                      
                          <span class="s_csr text-dark mb-2">Goodluck India is a versatile business unit that has a diverse product range produced across our north India manufacturing locations and Bhuj, Gujarat. </span>
                        
                          <p class="text-dark1 mt-4" >Goodluck India Ltd. engaged in manufacturing and exporting of a wide range of ERW Hot Dip Galvanized Pipes, Black Pipes, Black & GI hollow sections, CR coils, CRCA, Galvanized & Plain Corrugated Sheets. Goodluck India Ltd. has been one of the pioneers in the exports of Pipes and Tubes over 30 years.</p>
                         <p class="text-dark1">Our Black painted and Galvanized Round Pipe & Tubes , Square and rectangular Hollow sections are available in several finish with wide size range and length options. They meet and exceed several global standards of quality and safety like ISO 9001, TS 16949, OH & ISO 45001,PED, CE ,UL and many more.</p>
                        
                       
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="himage  ">
                      <img src="../assets/PipesAndTubes/product/factory.jpg" alt="" class="w100 r_5">
                  </div>
              </div>
              <div class="col-md-12">
                <div class="over_view mt-4">
                <p>The cold rolled coils, Galvanized Coils, Plain and Corrugated Sheets are made in a well-organized plant that offers efficiency and manufacturing control.</p>
                <p class="text-dark">Our plant is situated at Sikandarabad industrial area just 45 km. from Delhi. It has the state-of-the-art tube mills, galvanizing units, cold rolling mills galvanized coil unit and corrugation machines.</p>
                         <p class="text-dark">We are the First Pipe company in India to work towards sustainable Environment and our Unit Goodluck Metallics has received an EPD from International reputed agency.</p>
<span class="s_csr text-dark mb-4 ">Goodluck Metallics </span>
<p class="text-dark mt-3">In order to meet the growing demand of Steel Tubes & Pipes in  both domestic as well as international market and to be more  competitive internationally, the group has set up a new Export  focused facility under its subsidiary name Goodluck Metallics at  Kachchh, (west coast of India) in State of Gujarat close to India’s  premium international sea ports - Mundra and Kandla. The new  ISO 9001 certified facility have added 1,20,000 MT per annum to  our existing capacities and its strategic location; close to the port;  will help the company to overcome global competition. </p>
                         <p class="text-dark">Strategically based close to one of the biggest ports in India - Mundra in Bhuj…we export to over 100 destinations with reduced transit time, faster deliveries, and achieve cost-efficiency.</p>
                         <p class="text-dark">
                         Our products are UL approved & Active Fire Approved, IS approved and are accredited to various certifications like CE, UL and PED certification.
<br>It is also the only Indian Pipe company to have an Environment Product Declaration (EPD).

                         </p>
                </div>
              </div>
          </div>
      </div>
</div>
<!-- end -->
    
  
  <div class="tab-pane fade  mt-4 " id="Approvals" role="tabpanel" aria-labelledby="contact-tab">
    
 
      
    
        <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="sectiontitle">
                        <h2>Customers/Approvals </h2>
                        <span class="headerLine"></span>
                      
                    </div>

                    <div class="swiper-wrapper1 mt-4">
                            <div class="brand__slider-item1  ">
                                <a href="#"><img src="../assets/img/brand/1.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item1  ">
                                <a href="#"><img src="../assets/img/brand/2.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item1  ">
                                <a href="#"><img src="../assets/img/brand/3.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item1 ">
                                <a href="#"><img src="../assets/img/brand/4.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item1  ">
                                <a href="#"><img src="../assets/img/brand/5.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item1">
                                <a href="#"><img src="../assets/img/brand/6.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item1">
                                <a href="#"><img src="../assets/img/brand/7.png" alt=""></a>
                            </div>
                        </div>

                        <div>
                <div class="brand__slider p-0 swiper-container">
                        <div class="swiper-wrapper">
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/11.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/12.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/13.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/14.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/15.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/16.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/17.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/18.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/19.png" alt=""></a>
                            </div>
                            <div class="brand__slider-item swiper-slide">
                                <a href="#"><img src="../assets/img/brand/20.png" alt=""></a>
                            </div>
                            
                        </div>
                    </div>
                </div>
                </div>
            </div>
            
        </div>
   
    
  </div> 
  <!-- end -->
 
</div>
        </div>
    </div>
</div>

</section>


</div>
<?php include './inc/footer.php';?>