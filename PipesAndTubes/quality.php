<!doctype html>
         <html class="no-js" lang="zxx">
            
         <head>
               <meta charset="utf-8">
               <meta http-equiv="x-ua-compatible" content="ie=edge">
               <title>  Goodluck India Limited (Pipes And Tubes) :: Quality  </title>
               <meta name="description" content="">


<?php include './inc/header.php'; ?>
<div class="contant">
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                <div class="carousel-item  active">
                    <img src="../assets/PipesAndTubes/pipe/quality_b.png" class="d-block w-100" alt="...">
			 
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Stringent and uncompromising Quality Control from Raw Material stage to Finished Goods</p>
                    </div>
                </div>
                
            </div>
          
        </div>
    </section>
    <!-- hero -->
 
    <section class="coials bg-light pt-5 pb-5 ">
        <div class="container">
            <div class="row">
            <div class="col-md-6">
                    <div class="sectiontitl">
                        <h2>Quality</h2>
                        <span class="headerLine"></span>
                        <p class="p_color">
                        Quality Assurance begins at the raw material stage itself.
Material is inspected for chemical composition and tested for
other parameters like mechanical properties, gauge variation
etc. for total evaluation of raw material to ascertain its suitability
for the intended end-use-applications.
At the surface-pickling operations, the material is checked for
surface finish before it is fed into the tube mills.<br><br>
At the tube mills, each product for the customer is processed
according to the norms sequenced by the process control
engineers based on stringent international standards and
monitored through uncompromising quality control tests at
every stage.

                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="qlabe">
                    <img src="../assets/PipesAndTubes/qualitylab/PGH52345.jpg" alt="" class="w-100 r_5">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                <div class="sectiontitle mt-5 mb-4">
                        <!-- <h2>Quality</h2>
                        <span class="headerLine"></span> -->
                        <p>The engineers are guided by the latest equipment at the
R&D centre. Parent materials & soundness is checked by
conducting various tests such as yield, tensile, drift expansion,
flattening, bend, impact test, etc, that include Leco Carbon
apparatus, scanning electron microscope, atomic absorption
emission spectrophotometer, universal microscope and micro
hardness tester. This facilitates all the required metallurgical
tests on the materials.</p>
                    </div>
                </div>
               
            </div>
            <div class="row align-items-center ">

                <div class="col-md-3">
                    <div class="Q_lab r_5">
                    <img src="../assets/PipesAndTubes/qualitylab/flating.png" alt="" class="w-100 r_5">
                    <h4>Flattening Test-1</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="Q_lab r_5">
                    <img src="../assets/PipesAndTubes/qualitylab/Hardness.png" alt="" class="w-100 r_5">
                    <h4>Hardness Testing Machine cc</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="Q_lab r_5">
                    <img src="../assets/PipesAndTubes/qualitylab/impect.png" alt="" class="w-100 r_5">
                    <h4>Impact Testing cc</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="Q_lab r_5">
                    <img src="../assets/PipesAndTubes/qualitylab/profile.png" alt="" class="w-100 r_5">
                    <h4>Profile Projector-2 c</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="Q_lab r_5">
                    <img src="../assets/PipesAndTubes/qualitylab/tensl.png" alt="" class="w-100 r_5">
                    <h4>Tensile Testing-2_cc </h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="Q_lab r_5">
                    <img src="../assets/PipesAndTubes/qualitylab/utm.png" alt="" class="w-100 r_5">
                    <h4>UTM Testing Machine-1 cc</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="Q_lab r_5">
                    <img src="../assets/PipesAndTubes/qualitylab/weld.png" alt="" class="w-100 r_5">
                    <h4> Weld Flow Test-1 cc</h4>
                    </div>
                </div>
               
                
            </div>
        </div>
    </section>


    <!-- end-sheets -->
   

</div>
<?php include './inc/footer.php'; ?>