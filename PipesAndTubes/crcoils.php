
<!doctype html>
         <html class="no-js" lang="zxx">
            
         <head>
               <meta charset="utf-8">
               <meta http-equiv="x-ua-compatible" content="ie=edge">
               <title>  Goodluck India Limited (Pipes And Tubes) :: Coils  </title>
               <meta name="description" content="">
<?php include './inc/header.php'; ?>
<div class="contant">
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
                
                <div class="carousel-item active">
                    <img src="../assets/PipesAndTubes/pipe/glv_coils.png" class="d-block w-100"
                        alt="...">
                        <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        
                        <p>Defined allocation for each Product Category ensures 100% delivery performance </p>
                    </div>
                </div>
                
            </div>
          
        </div>
    </section>
    <!-- hero -->
 
    <section class="coials bg-light pt-5 pb-5 ">
        <div class="container">
            <div class="row align-items-center ">
                <div class="col-md-6 wow fadeInLeft mb-2 wow fadeInUp mb-2" data-wow-delay="1s"
                                style="visibility: visible; animation-delay: 1s;">
                    <div class="gj_coial">


                        <h3>CRCA (Cold Rolled Close Annealed) Coils/Sheets</h3>
                        <div class="gl_detail mb-3">
                            <div class="cr_coils w100">

                                <ul>
                                    <li><span>Specifications</span> <span> : JIS G 3141-SPCC-1B & other equivalent
                                            specifications</span></li>
                                    <li><span>Thickness Range</span> <span> : 0.140 mm to 2.00 mm </span></li>
                                    <li><span>Hardness </span> <span>: 90 to 98 HRB or 85 HRB Min.</span></li>
                                    <li><span>Edge Condition </span> <span>: Smooth Mill Edges or Trimmed Edges as
                                            required.</span></li>
                                    <li><span>Surface Finish</span> <span> : Bright</span></li>
                                </ul>
                                <ul>
                                    <li><span>Specifications</span> <span> : IS 513 (D, DD, EDD),
                                            JIS G 3141 (SPCC, SPCD, SPCE),
                                            DIN 1623 (ST12, ST13, ST14), BS 1449 ASTM</span></li>
                                    <li><span>Thickness Range</span> <span> : 0.25 mm to 2.00 mm (Further closed
                                            tolerances on request)</span></li>
                                    <li><span>Width Range (mm) </span> <span>: 50 mm to 1000 mm</span></li>
                                    <li><span>Cut-to-length(mm) </span> <span>: Up to 3500 mm with tolerance of +2/-0
                                            mm</span></li>
                                    <li><span>Coil Weight</span> <span> : 3MT Max</span></li>
                                    <li><span>Coil ID</span> <span> : 508 mm</span></li>
                                    <li><span>Hardness</span> <span> : 45 to 65 HRB (Max) for D/DD, 34/52 (Max) for
                                            EDD</span></li>
                                    <li><span>Edge Condition</span> <span> : Trimmed Edges.</span></li>
                                    <li><span>Surface Finish</span> <span> : Super Bright, Bright, Dull & Matt</span>
                                    </li>

                                </ul>
                            </div>

                        </div>
                        <h3>CRCA (Cold Rolled Close Annealed) Coils/Sheets</h3>
                        <div class="gl_detail">
                            <div class="cr_coils w100">

                                <ul>
                                    <li><span>Specifications</span> <span> : JIS G 3141-SPCC-1B &
other equivalent specifications</span></li>
                                    <li><span>Thickness Range</span> <span> : 0.140 mm to 2.00 mm</span></li>
                                    <li><span>Hardness </span> <span> : 90 to 98 HRB or 85 HRB Min.</span></li>
                                    <li><span>Edge Condition </span> <span> :Smooth Mill Edges or Trimmed Edges as required.</span></li>
                                    <li><span>Surface Finish</span> <span> : Bright</span></li>
                                </ul>
                                
                            </div>

                        </div>


                    </div>
                </div>
                <div class="col-md-6 wow fadeInRight mb-2 wow fadeInUp mb-2" data-wow-delay="1s"
                                style="visibility: visible; animation-delay: 1s;">
                    <div class="gj_img">
                        <img src="../assets/PipesAndTubes/product/banner/g1.jpg" alt="" class="w-100 r_5">
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- end-sheets -->
    <section class="coials   sheets pb-5" style="background-color: #ecffeb;">
        <div class="container">
            <div class="row align-items-center ">

                <div class="col-md-12">
                    <div class="gj_coial">
                        <h3 class="mt-4">Tolerances </h3>
                        <table class="table table-bordered">
                            <thead class="table-dark">
                                <tr>

                                    <th scope="col">Thickness</th>
                                    <th scope="col">Specified Thickness Over</th>
                                    <th scope="col">(MM) Upto</th>
                                    <th scope="col">Tolerances Upto 250 Width (mm) (±)</th>
                                    <th scope="col">Tolerance Over 250 mm Width (mm) (±)</th>


                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td>0.14</td>
                                    <td>0.19</td>
                                    <td> 0.015</td>
                                    <td> 0.015</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>0.20 </td>
                                    <td>0.40 </td>
                                    <td>0.020 </td>
                                    <td>0.025</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td> 0.40</td>
                                    <td> 0.80</td>
                                    <td>0.025</td>
                                    <td>0.030</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td> 0.80</td>
                                    <td> 1.60</td>
                                    <td>0.030</td>
                                    <td>0.035</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>1.60t</td>
                                    <td> 2.00t</td>
                                    <td> 0.035t</td>
                                    <td> 0.055t</td>
                                </tr>
                            <tbody>
                        </table>
                    </div>
                </div>
                
                <!-- <div class="col-md-9">
                     <div class="specification mt-3">
                        <h4>Galvanized Specifications</h4>
                        <h5>JAPANESE IS</h5>
                        <h5>JIS G 3302 / SGCH IS-277</h5>
                        <h3>Coils and Sheets (Full hard)</h3>
                     <div class="gl_detail">
                    <div class="gl_cd w50">  
                        
                    <ul>
                        <li><span>Thickness</span> <span> : 0.10 to 0.70 mm</span></li>
                        <li><span>Width</span> <span> : 750 mm to 1000 mm </span></li>
                        <li><span>Outer diameter </span> <span>: 1500 mm maximum</span></li>
                        <li><span>Packet Weight </span> <span>: 3 MT Max</span></li>
                        <li><span>Cut to Length</span> <span> : 4800 mm (Max)</span></li>
                    </ul>
                        </div>
                    <div class="gl_cd w50"> 
                    <ul>
                        <li><span>Coating</span> <span>: 90 gm – 300 gm / sq.m. (both sides)</span></li>
                        <li><span>Coil inner diameter </span> <span>: 508 mm</span></li>
                        <li><span>Coil Weight</span> <span>: 12 MT maximum</span></li>
                        <li><span>Coil Weight</span> <span>: Bright Regular</span></li>
                      
                    </ul>   </div>
                    </div>
                     </div>
                </div> -->

            </div>
        </div>
    </section>
    <!-- end-sheets -->



</div>
<?php include './inc/footer.php'; ?>