    <?php include './inc/header.php'; ?>
    <main>
    <section calss="good_luck">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner cdwtubesbanner">
            <div class="carousel-item  active ">
                    <img src="./assets/PipesAndTubes/banner/banner2.png" class="d-block w-100" alt="...">
					<h5> Sikandrabad Plant</h5>
                    <div class="carousel-caption d-none d-md-block c2 bg_tr">
                        <p>The company’s capability of producing more than 320,000 metric tons of high-frequency induction welded tubes annually, speaks volumes of its command within the industry. </p>
                     </div>
                </div>
            </div>
          
        </div>
    </section>
    <!-- hero -->
         <!-- services__area start -->
         <section class="services__area pt-60 pb-60" data-background="assets/img/services/services-bg.jpg">
            <div class="container">
               <div class="row mb-4">
                  <div class="col-xl-12">
                     <div class="comman-head">
                       <h2>Our Verticals</h2>
                     </div>
                    
                  </div>
               </div>
               <div class="row mt-30">
                  <div class="col-xl-4 col-lg-4 col-md-6 roundbox mb-40">
                     <span> </span>
                     <span> </span>
                     <span> </span>
                     <span> </span>
                     <div class="services__item services__item-tp text-center mb-40">                     
                           <div class="services__item-content">
                              <div class="ser__icon mb-30">
                                 <img src="./assets/img/blog/4.jpg" alt="">
                              </div>
                              <h5 class="ser__title mb-10">Pipes And Tubes</h5>
                              <div class="ser__more-option mt-10">
                        <a href="PipesAndTubes/index.php">View Details <i class="fal fa-long-arrow-right"></i></a>
                        </div>
                        </div>                        
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 mb-40">
                     <div class="services__item services__item-tp text-center mb-40">                        
                           <div class="services__item-content">
                              <div class="ser__icon mb-30">
                                 <img src="./assets/img/blog/1.jpg" alt="">
                              </div>
                              <h5 class="ser__title mb-10">Forgings</h5>
                              <div class="ser__more-option mt-10">
                        <a href="forging/index.php">View Details <i class="fal fa-long-arrow-right"></i></a>
                        </div>
                        </div>                        
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 mb-40">                    
                     <div class="services__item services__item-tp  text-center mb-40">                     
                           <div class="services__item-content">
                              <div class="ser__icon mb-30">
                                 <img src="./assets/img/about/cwdpipe.jpg" alt="">
                              </div>
                              <h5 class="ser__title mb-10">CDW Tubes</h5>
                              <div class="ser__more-option mt-10">
                        <a href="cdwtubes/index.php">View Details <i class="fal fa-long-arrow-right"></i></a>
                        </div>
                        </div>                        
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6">
                     <div class="services__item services__item-tp text-center mb-40">                       
                           <div class="services__item-content">
                              <div class="ser__icon mb-30">
                                 <img src="./assets/img/blog/3.jpg" alt="">
                              </div>
                              <h5 class="ser__title mb-10"> Coils & Sheets</h5>
                              <div class="ser__more-option mt-10">
                        <a href="CoilsSheets/index.php">View Details <i class="fal fa-long-arrow-right"></i></a>
                        </div>
                        </div>                       
                     </div>
                  </div>
              
                  <div class="col-xl-4 col-lg-4 col-md-6">                   
                     <div class="services__item services__item-tp text-center mb-40">                       
                           <div class="services__item-content">
                              <div class="ser__icon mb-30">
                                 <img src="./assets/img/about/feb.png" alt="">
                              </div>
                              <h5 class="ser__title mb-10">Infra & Energy</h5>
                              <div class="ser__more-option mt-10">
                        <a href="fabrication/index.php">View Details <i class="fal fa-long-arrow-right"></i></a>
                        </div>
                        </div>                      
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 roundbox">
                  <span> </span>
                     <span> </span>
                     <span> </span>
                     <span> </span>
                     <div class="services__item services__item-tp text-center mb-40">                       
                           <div class="services__item-content">
                              <div class="ser__icon mb-30">
                                 <img src="./assets/img/about/road.png" alt="">
                              </div>
                              <h5 class="ser__title mb-10">Road Safety Division</h5>
                              <div class="ser__more-option mt-10">
                        <a href="roadsafetydivision/index.php">View Details <i class="fal fa-long-arrow-right"></i></a>
                        </div>
                        </div>                       
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- services__area end -->

    </main>
    <?php include './inc/footer.php';?>