


 
    <section id="gallery" class="bg-white p-0 ">
        <div class="container">
             

            <!--  -->

            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/img/gallery/g9.jpg"><img src="./assets/img/gallery/g9.png"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/img/gallery/g10.png"><img src="./assets/img/gallery/g10.png"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/img/gallery/g11.png"><img src="./assets/img/gallery/g11.png"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                    <div class="img-wrapper">
                        <a href="./assets/img/gallery/g12.png"><img src="./assets/img/gallery/g12.png"
                                class="img-responsive"></a>
                        <div class="img-overlay">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>

            </div><!-- End row -->
        </div><!-- End image gallery -->
        </div><!-- End container -->
    </section>


 